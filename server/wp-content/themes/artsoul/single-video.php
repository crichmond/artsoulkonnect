<?php get_header(); ?>

  <?php get_template_part('partials/asset', 'featured-media'); ?>

  <section class="main single">
    <div class="global-container">

    <?php if(have_posts()): while (have_posts()) : the_post(); 

    $content = get_the_content();
    if($content != '') { ?>
    <article class="post">
      
      <?php the_terms($post->ID,'video-tags','<div class="post-tags">', ', ', '</div>'); ?>
      
      <?php the_title('<h2 class="post-title"><a href="'. get_permalink() .'">', '</a></h2>'); ?>
      
      <div class="post-meta">
        <?php 
        $user = get_the_author_meta('ID');
        $user_img = get_field('user_image', "user_" . $user);
        
        if($user_img): ?>
        <div class="post-author-image">
          <img src="<?php echo $user_img['sizes']['tiny']; ?>">
        </div>
        <?php endif; ?>

        <div class="post-author">By <?php /*the_author();*/ the_author_link(); ?></div>
        <div class="post-date"><?php echo get_the_date('F j, Y'); ?></div>
      </div>
      
      <div class="post-excerpt">
        <?php the_content(); ?>
      </div>

    </article>
    <?php } 

    endwhile; endif; ?>

    </div><!-- /.global-container -->
  </section><!-- /.single -->

  <?php 
    get_template_part('partials/asset', 'recommended-videos');  
    get_template_part('partials/asset', 'ads-footer'); 
  ?>

<?php get_footer(); ?> 