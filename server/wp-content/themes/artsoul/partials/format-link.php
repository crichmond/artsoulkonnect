<article class="link">
  <div class="format-icon link"></div>

  <div class="global-container">
    
    <div class="link-url">
      <a href="<?php addhttp(get_field('link_url')); ?>"><?php the_field('link_url'); ?></a>
    </div>
    <div class="link-excerpt">
      <?php the_field('description'); ?>
    </div>
    
  </div><!-- /.global-container -->

</article>