<?php if(get_field('news_ads','option')): ?>
<section class="content-row sponsors">
  <div class="global-container">

    <div class="title-row">
      <h2>Sponsors</h2>
    </div><!-- /.title-row -->
    
    <div class="row-12">
    <?php while(has_sub_field('news_ads','option')):
    if(get_sub_field('image')): ?>
    <div class="ad">  
      <?php $ad_link = get_sub_field('link') ? addhttp(get_sub_field('link')) : null; 
      echo $ad_link ? "<a href='$ad_link'>" : ""; ?>
        <?php $img = get_sub_field('image');
        $size = "sponsor-medium"; ?>
        <img src="<?php echo $img['sizes'][$size]; ?>" 
        width="<?php echo $image['sizes'][$size.'-width']; ?>" 
        height="<?php echo $image['sizes'][$size.'-height']; ?>" 
        alt="Sponsored ad">
      <?php echo $ad_link ? "</a>" : ""; ?>
    </div><!-- /.ad -->
    <?php endif; ?>
    <?php endwhile; ?>

    </div><!-- /.row-12 -->
  </div><!-- /.global-container -->
</section><!-- /.content-row -->
<?php endif; ?>