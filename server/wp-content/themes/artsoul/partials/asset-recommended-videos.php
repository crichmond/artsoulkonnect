<section class="content-row recommended-videos">
  <div class="global-container">
  <h2>You might also like:</h2>
  <div class="row-12">
  <?php 
  $args = array(
    'post_type' => 'video',
    'orderby' => 'rand',
    'posts_per_page' => 4,
  );
  
  $recommended_posts_query = new WP_Query($args);
  if ($recommended_posts_query->have_posts()): while($recommended_posts_query->have_posts()): $recommended_posts_query->the_post(); ?>

    <div class="post">    
      <?php if(has_post_thumbnail()): 
          $img = wp_get_attachment_image_src(get_post_thumbnail_id(), 'post-large');
          $bg = $img[0];
        else:
          $img = get_field('default_post_image','option');
          $bg = $img['sizes']['post-large'];
        endif; ?>
      <a class="post-image" href="<?php the_permalink(); ?>" style="
        background-image: url('<?php echo $bg; ?>');
        background-position: 50% 50%;
        background-size: cover;">
      </a>

      <div class="post-title">
        <a href="<?php the_permalink(); ?>"><?php the_title(); ?></a>
      </div>

      <?php the_terms($post->ID,'video-tags','<div class="post-tags">', ', ', '</div>'); ?>
    </div>
    

    <?php endwhile;
    wp_reset_postdata(); 
  endif; ?>
  </div><!-- /.row-12 -->
</div><!-- /.global-container -->
</section><!-- /.content-row -->