<?php 
if(get_field('sidebar_ads','option')): ?>
  <div class="widget widget-ads">
  <?php while(has_sub_field('sidebar_ads','option')):
    if(get_sub_field('image')): ?>
      
      <?php $ad_link = get_sub_field('link') ? addhttp(get_sub_field('link')) : null; 
      echo $ad_link ? "<a href='$ad_link'>" : ""; ?>
        <?php $img = get_sub_field('image');
        $size = "sponsor-medium"; ?>
        <img src="<?php echo $img['sizes'][$size]; ?>" 
        width="<?php echo $image['sizes'][$size.'-width']; ?>" 
        height="<?php echo $image['sizes'][$size.'-height']; ?>" 
        alt="Sponsored ad">
      <?php echo $ad_link ? "</a>" : ""; ?>
    
    <?php endif;
  endwhile; ?>
  </div><!-- /.widget-ads -->
<?php endif; ?>