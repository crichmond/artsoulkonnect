<article class="quote <?php if(get_field('style') == 'twitter') { echo 'twitter'; } ?>">
  <div class="format-icon quote <?php if(get_field('style') == 'twitter') { echo 'twitter'; } ?>"></div>

  <div class="global-container">
  
    <div class="quote-excerpt"><?php the_field('quote'); ?></div> 
    <?php if(get_field('style') == 'twitter') { ?>
      <a class="quote-author" href="http://twitter.com/<?php the_field('author'); ?>"><?php the_field('author'); ?></a>
    <?php } else { ?>
      <div class="quote-author">- <?php the_field('author'); ?></div>
    <?php } ?>
  
  </div><!-- /.global-container -->

</article>