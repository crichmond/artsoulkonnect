<section class="content-row recommended">
  <div class="global-container">
  <h2>You might also like:</h2>

  <div class="row-12">
  <?php 
  $args = array(
    'post_type' => 'post',
    'orderby' => 'rand',
    'posts_per_page' => 4,
    'tax_query' => array(
      array(
        'taxonomy' => 'post_format',
        'field' => 'slug',
        'terms' => array( 'post-format-video' ),
        'operator' => 'NOT IN',
      )
    )
  );
  
  $recommended_posts_query = new WP_Query($args);
  if ($recommended_posts_query->have_posts()): while($recommended_posts_query->have_posts()): $recommended_posts_query->the_post(); 
    
    if(has_post_thumbnail()): 
      $img = wp_get_attachment_image_src(get_post_thumbnail_id(), 'post-large');
      $bg = $img[0];
    else:
      $img = get_field('default_post_image','option');
      $bg = $img['sizes']['post-large'];
    endif; ?>

    <a class="post" href="<?php the_permalink(); ?>" style="
      background-image: url('<?php echo $bg; ?>');
      background-position: 50% 50%;
      background-size: cover;">
      <div class="background" >
        <div class="content">
      
          <div class="post-title">
            <?php the_title(); ?>
          </div>

          <div class="post-author">
            <?php the_author(); ?>
          </div>
          
          <?php
            $posttags = get_the_tags();
            $tags_list = array();

            if ($posttags) {
              foreach($posttags as $tag) {
                $tags_list[] = "<span>" . $tag->name . "</span>"; 
              }
              echo '<div class="post-tags">', join(", ", $tags_list), '</div>';
            }
          ?>
        </div>
      </div><!-- /.background -->
    </a><!-- /.post -->

    <?php endwhile;
    wp_reset_postdata(); 
  endif; ?>
    </div><!-- /.row-12 -->
  </div><!-- /.global-container -->
</section><!-- /.content-row -->