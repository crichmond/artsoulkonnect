
<article>
  <?php if (has_post_thumbnail()): 
    $image = wp_get_attachment_image_src( get_post_thumbnail_id(), 'blog-list' ); ?>
    

  <div class="post-thumb" style="
    background-image: url('<?php echo $image[0]; ?>');
    background-position: center center;
    background-size: cover;
  ">
    <div class="fade audio">
    </div>
  </div>
  <?php endif; ?>

  <div class="post-audio<?php if(!is_single()) { echo ' excerpt'; } ?> sermon">
    <div class="audio-options">
      <audio controls>
        <source src="<?php the_field('audio'); ?>" type="audio/mpeg">
      </audio>
    </div><!-- /.audio-options -->

    <script src="<?php bloginfo('template_directory') ?>/js/modules/mediaelement/mediaelement-and-player.min.js"></script>
    <link rel="stylesheet" href="<?php bloginfo('template_directory') ?>/js/modules/mediaelement/mediaelementplayer.css" />

    <script>
    jQuery(document).ready(function($) {
      $('audio').mediaelementplayer();
    });
    </script>
  </div>

  
  <?php if(is_single()): ?>
    <div class="post-copy">
      <h2 class="post-title"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h2>
      <span class="post-date"><?php echo get_the_date('F j, Y'); ?></span>
      
      <?php the_content(); ?>
      
    </div>   
    <script type="text/javascript">
      jQuery(document).ready(function($) {
        $('.powerpress_link_d').each(function () {
          var href = $(this).attr('href');
          $(this).attr('href', "<?php bloginfo('template_directory'); ?>/download.php?location=" + href);
        });
      });
    </script>

  <?php else: ?>
    <div class="post-copy">
       <h2 class="post-title"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h2>
      <span class="post-date"><?php echo get_the_date('F j, Y'); ?></span>
      
      <?php the_excerpt(); ?>
      <a href="<?php the_permalink(); ?>" class="read-more">Read Post ></a>
    </div> 
  <?php endif; ?>
</article>