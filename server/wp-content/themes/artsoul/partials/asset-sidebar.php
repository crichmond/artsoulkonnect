<div class="widget widget-search">
  <form method="get" id="searchform" action="<?php bloginfo('url'); ?>/">
    <input type="submit" id="searchsubmit" value="" class="submit" />
    <input type="text" value="<?php the_search_query(); ?>" name="s" id="s" placeholder="Search" class="search" />
  </form>
</div><!-- /.widget -->

<div class="widget widget-categories-tags">
  <h3>Categories</h3>
  <?php 
    wp_nav_menu( array(
      'theme_location' => 'categories', 
      'container' => '', 
      'items_wrap' => '<ul>%3$s</ul>'
      ) );
   ?>

  <h3>Tags</h3>
  <?php 
    wp_nav_menu( array(
      'theme_location' => 'tags', 
      'container' => '', 
      'items_wrap' => '<ul>%3$s</ul>'
      ) );
   ?>
</div><!-- /.widget -->

<?php if(get_field('social', 'option')): ?>
<div class="widget widget-social">
  <div class="social-row">
    <?php $counter = 0; 
    while(has_sub_field('social', 'option')): 
      $counter ++; ?>
    <div class="icon">
      <a href="<?php the_sub_field('link'); ?>" class="<?php echo $counter; ?>">
        <?php $icon = get_sub_field('icon'); ?>
        <img src="<?php echo $icon['sizes']['thumbnail']; ?>">
      </a>
    </div>
    <?php 
    if($counter == 6) { 
      $counter = 0; 
    }
    endwhile; 

    $placeholders_needed = 6-$counter;
    $i = 1;
    while($i <= $placeholders_needed) { ?>
      <div  class="icon placeholder">
        <a>
          <img src="<?php bloginfo('template_directory'); ?>/img/spacer.png">
        </a>
      </div>
      <?php $i ++;
    }
    ?>
  </div>
</div>
<?php endif; ?>

<?php get_template_part('partials/asset', 'ads-sidebar'); ?>