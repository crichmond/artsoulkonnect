<?php 
if(has_post_thumbnail()): 
  $img = wp_get_attachment_image_src(get_post_thumbnail_id(), 'post-large');
  $bg = $img[0];
else:
  $img = get_field('default_post_image','option');
  $bg = $img['sizes']['post-large'];
endif; ?>


<section class="featured-media 
  <?php 
    if((is_page() || is_single()) && get_post_format() != 'video') { echo ' single'; }
    if(get_post_format() == 'video') { echo ' video'; } 
  ?>">
  <?php if(get_post_format() != 'video') { ?>
	<div class="background" style="
		background-image: url('<?php echo $bg; ?>');
		background-position: 50% 50%;
    background-size: cover;">
  </div>
  <?php } ?>

  <div class="global-container">
    <?php if(get_field('video_url')): ?>
      <div class="video">
        <?php echo wp_oembed_get(get_field('video_url')); ?>
      </div>
    <?php endif; ?>
    
    <div class="social">
      <ul>
        <li>
          <a class="email_link" target="_blank" href="mailto:?subject=<?php the_title(); ?>&body=<?php the_permalink(); ?>"><i class="fa fa-envelope-o"></i></a>
        </li>
        <li>
          <script>
          function fbs_click() {
            u="<?php the_permalink(); ?>";
            t="<?php the_title; ?>";
            window.open('http://www.facebook.com/sharer.php?u='+encodeURIComponent(u)+'&t='+encodeURIComponent(t),'sharer','toolbar=0,status=0,width=626,height=436');
            return false;
          }
          </script>
          <a class="fb_link" onclick="return fbs_click()" href="#"><i class="fa fa-facebook"></i></a>
        </li>
        <li>
          <a class="twitter_link" href="http://twitter.com/share?text=<?php the_title(); ?>%21&url=<?php the_permalink(); ?>" target="_blank"><i class="fa fa-twitter"></i></a>
        </li>
      </ul>
    </div>
  </div>
</section>