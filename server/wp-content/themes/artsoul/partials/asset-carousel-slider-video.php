<?php 
$slider_cat = get_sub_field('category');
$slider_cat_slug = $slider_cat->slug;
$slider_cat_name = $slider_cat->name;
$query_name = $slider_cat_slug . '_query';

$args = array(
  'post_type' => 'video',
  'posts_per_page' => '8',
  'tax_query' => array(
    array(
      'taxonomy' => 'video-categories',
      'field' => 'slug',
      'terms' => array( $slider_cat_slug )
    )
  )
);

${$query_name} = new WP_Query($args);
if (${$query_name}->have_posts()): ?>
<!-- BEGIN SMALL CAROUSEL -->
  <section class = "small-carousel_wrapper content-row">

    <div class="title-row">
      <div class="global-container">
        <h2><?php echo $slider_cat_name; ?>
          <a href="<?php echo get_bloginfo('url') . "/video-categories/" . $slider_cat_slug . "/"; ?>" class="see-all">See All</a>
        </h2>
      </div><!-- /.global-container -->
    </div><!-- /.title-row -->

    <ul class = "small-carousel is-loading">
    <?php while(${$query_name}->have_posts()): ${$query_name}->the_post(); ?>
  
      <li class = "slide">
        <!-- <h2 class = "small-carousel_title"></h2> -->
        <a href = "<?php the_permalink(); ?>" class = "small-carousel_img carousel-transfer-image ar_16-9">

          <?php if(has_post_thumbnail()): 
            the_post_thumbnail('post-medium');
          else: 
            $img = get_field('default_post_image','option');
            $size = "post-medium"; ?>
            <img src="<?php echo $img['sizes'][$size]; ?>" 
            width="<?php echo $image['sizes'][$size.'-width']; ?>" 
            height="<?php echo $image['sizes'][$size.'-height']; ?>" 
            alt="<?php echo get_the_title(); ?>">
          <?php endif; ?>
        </a>
        <h3 class = "small-carousel_subtitle post-title"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h3>
        <!-- <a class = "small-carousel_read-more" href = "###">Music</a> -->
        <?php the_terms($post->ID,'video-tags','<div class="post-tags">', ', ', '</div>'); ?>
      </li>
      
      <?php endwhile;
      wp_reset_postdata(); ?>
    </ul>
  </section>
  <!-- END SMALL CAROUSEL -->
<?php endif; ?>