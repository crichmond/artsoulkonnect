<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->

    <head>

        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <title>
          <?php 
          $site_title = (get_field('site_title','option') ? get_field('site_title','option') : get_bloginfo('name'));
          $page_title = (get_field('seo_page_title') ? get_field('seo_page_title') : get_the_title());
          
          if(is_front_page() || is_404() || is_category() || is_tax()) { 
            the_field('site_title','option');
          } else { 
            echo "$page_title | $site_title";
          } ?>
        </title>
        
        <meta name="description" content="<?php if(is_front_page()) { the_field('site_description', 'option'); } else {  the_field('meta_description'); } ?>">
        <meta name="keywords" content="<?php the_field('site_keywords', 'option'); ?>">
        
        <!-- Facebook Meta Tags -->
        <?php 
        $post = $wp_query->post;
        if(is_front_page()): ?>
          <meta property="og:url" content="<?php bloginfo('url'); ?>" />
          <meta property="og:description" content='<?php if(is_front_page()) { the_field('site_description', 'option'); } else {  the_field('meta_description'); } ?>' />
          <meta property="og:image" content="<?php bloginfo('template_directory'); ?>/img/facebook.jpg" />
        <?php else: ?>
          <meta property="og:url" content="<?php the_permalink(); ?>" />
          <meta property="og:description" content="<?php $my_excerpt = get_excerpt_by_id($post->ID); echo wp_strip_all_tags($my_excerpt); ?>" />
          <?php if(has_post_thumbnail()): 
            $img = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'large'); ?>
            <meta property="og:image" content="<?php echo $img[0]; ?>" />
          <?php else: ?>
            <meta property="og:image" content="<?php bloginfo('template_directory'); ?>/img/facebook.jpg" />
          <?php endif; ?>
          
        <?php endif; ?>

        <meta name="viewport" content="width=device-width,initial-scale=1">

        <link rel="stylesheet" href="<?php bloginfo('template_directory'); ?>/css/style.css">

        <!-- Favicons
        ================================================== -->
        <link rel="shortcut icon" href="<?php bloginfo('template_directory'); ?>/img/favicon.ico">
        <link rel="apple-touch-icon" href="<?php bloginfo('template_directory'); ?>/img/apple-touch-icon.png">
        <link rel="apple-touch-icon" sizes="72x72" href="<?php bloginfo('template_directory'); ?>/img/apple-touch-icon-72x72.png">
        <link rel="apple-touch-icon" sizes="114x114" href="<?php bloginfo('template_directory'); ?>/img/apple-touch-icon-114x114.png">
        <link rel="apple-touch-icon" sizes="144x144" href="<?php bloginfo('template_directory'); ?>/img/apple-touch-icon-144x144.png">
        
        <?php if (is_singular() && get_option('thread_comments' )) wp_enqueue_script('comment-reply'); wp_head(); ?>
        
        <!-- MediaElement -->
        <link rel="stylesheet" href="<?php bloginfo('template_directory'); ?>/js/modules/mediaelement/mediaelementplayer.css" />
        
        <!-- FontAwesome -->
        <link href="//netdna.bootstrapcdn.com/font-awesome/4.1.0/css/font-awesome.min.css" rel="stylesheet">

        <!-- MagnificPopup -->
        <link rel="stylesheet" href="<?php bloginfo('template_directory'); ?>/js/modules/magnific-popup/magnific-popup.css" />

        <!-- ArtSoul Sliders -->
        <link rel="stylesheet" href="<?php bloginfo('template_directory'); ?>/js/modules/artsoulkonnect/css/sliders.css"><!-- required -->
        <link rel="stylesheet" href="<?php bloginfo('template_directory'); ?>/css/sliders-custom.css">

        <!-- Full-Width Audio Player -->
        <link rel="stylesheet" type="text/css" href="<?php bloginfo('template_directory'); ?>/js/modules/fullwidth-audio-player/source/css/main.css">
        <link rel="stylesheet" type="text/css" href="<?php bloginfo('template_directory'); ?>/js/modules/fullwidth-audio-player/source/css/jquery.fullwidthAudioPlayer.css">
        <link rel="stylesheet" type="text/css" href="<?php bloginfo('template_directory'); ?>/js/modules/fullwidth-audio-player/source/css/jquery.fullwidthAudioPlayer-responsive.css">
        
    </head>

    <body>
      <!--[if lt IE 9]>
        <p class="browsehappy">
          <span class="global-container">
          You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.
          </span>
        </p>
      <![endif]-->

      <div id="search-form" class="search-popup mfp-hide">
        <form method="get" id="searchform" action="<?php bloginfo('url'); ?>/">
          <input type="submit" id="searchsubmit" value="" class="submit" />
          <input type="text" value="<?php the_search_query(); ?>" name="s" id="s" placeholder="SEARCH" class="search" />
        </form>
      </div>

      <header>
        <div class="global-container">
          <h1 class="logo"><a href="<?php bloginfo('url'); ?>"></a></h1>
          <nav class="clearfix desktop-nav hide-mobile">
            <ul class="menu">
            <?php 
              wp_nav_menu( array(
                'theme_location' => 'primary', 
                'container' => false, 
                'items_wrap' => '%3$s'
                ) );
             ?>
              <li class="nav-search"><a href="#search-form">Search</a></li>
            </ul>
          </nav>

          <nav class="clearfix mobile-nav hide-desktop">
            <div class="show-mobile-menu"><span><i></i><i></i><i></i></span></div>
            <ul class="menu hide">
              <?php 
              wp_nav_menu( array(
                'theme_location' => 'mobile', 
                'container' => false, 
                'items_wrap' => '%3$s' 
                ) ); 
              ?>
              <li class="nav-search"><a href="#search-form">Search</a></li>
            </ul>
          </nav>
        </div>
      </header>