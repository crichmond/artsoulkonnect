<?php get_header(); ?>
  
  <?php 
  if(has_post_thumbnail()): 
    $img = wp_get_attachment_image_src(get_post_thumbnail_id(), 'post-large');
    $bg = $img[0];
  ?>

  <section class="featured-media single">
    <div class="background" style="
      background-image: url('<?php echo $bg; ?>');
      background-position: 50% 50%;
      background-size: cover;">
    </div>

    <div class="global-container">
      <div class="social">
        <ul>
          <li>
            <a class="email_link" target="_blank" href="mailto:?subject=<?php the_title(); ?>&body=<?php the_permalink(); ?>"><i class="fa fa-envelope-o"></i></a>
          </li>
          <li>
            <script>
            function fbs_click() {
              u="<?php the_permalink(); ?>";
              t="<?php the_title; ?>";
              window.open('http://www.facebook.com/sharer.php?u='+encodeURIComponent(u)+'&t='+encodeURIComponent(t),'sharer','toolbar=0,status=0,width=626,height=436');
              return false;
            }
            </script>
            <a class="fb_link" onclick="return fbs_click()" href="#"><i class="fa fa-facebook"></i></a>
          </li>
          <li>
            <a class="twitter_link" href="http://twitter.com/share?text=<?php the_title(); ?>%21&url=<?php the_permalink(); ?>" target="_blank"><i class="fa fa-twitter"></i></a>
          </li>
        </ul>
      </div>
    </div>
  </section>

<?php endif; ?>

  <section class="main single">
    <div class="global-container">

    <?php if(have_posts()): while (have_posts()) : the_post(); ?>
    <article class="post">
      <h2 class="post-title"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h2>
      
      <div class="post-excerpt">
        <?php the_content(); ?>
      </div>
    </article>
    <?php endwhile; endif; ?>

    </div><!-- /.global-container -->
  </section>
<?php get_footer(); ?> 