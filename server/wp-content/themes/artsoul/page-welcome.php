<?php 
/* Template Name: Welcome */
get_header(); ?>
  
  <?php 
  if(has_post_thumbnail()): 
    $img = wp_get_attachment_image_src(get_post_thumbnail_id(), 'post-large');
    $bg = $img[0];
  ?>

  <section class="featured-media single welcome">
    <div class="background" style="
      background-image: url('<?php echo $bg; ?>');
      background-position: 50% 50%;
      background-size: cover;">
    </div>

    <div class="global-container">
      
    </div>
  </section>

<?php endif; ?>

  <section class="main single welcome">
    <div class="global-container">

    <?php if(have_posts()): while (have_posts()) : the_post(); ?>
    <article class="post">
      <h2 class="post-title"><?php the_title(); ?></h2>
      
      <div class="post-excerpt">
        
        <?php the_content(); ?>
        
        <h3>YOUR SUBMISSIONS:</h3>
        <?php 
        if ( is_user_logged_in() ):
          global $current_user;
          get_currentuserinfo();

          $author_query = array(
            'post_type' => 'artist',
            'posts_per_page' => '-1',
            'author' => $current_user->ID
            );
          
          $author_posts = new WP_Query($author_query);
          if($author_posts->have_posts()):
            while($author_posts->have_posts()) : $author_posts->the_post();
          ?>
            <div class="profiles-list-item">
              <a href="<?php the_permalink(); ?>" title="<?php the_title_attribute(); ?>" class="title"><?php the_title(); ?></a>
              <a href="<?php the_permalink(); ?>" class="post-view-link"><i class="fa fa-search"></i>View</a>  
              <?php edit_post_link('<i class="fa fa-pencil"></i> Edit'); ?>
            </div>
          <?php           
          endwhile;
        else: ?>
          <div class="profiles-list-item blank">
            Looks like you haven't submitted a profile yet. You may want to <a href="#">watch this video first</a>, then head to the user dashboard and <a href="<?php bloginfo('url'); ?>/wp-admin/post-new.php?post_type=artist">create a new profile!</a>.
          </div>
        <?php endif;
      endif; ?>

      </div>
    </article>
    <?php endwhile; endif; ?>

    </div><!-- /.global-container -->
  </section>
<?php get_footer(); ?> 