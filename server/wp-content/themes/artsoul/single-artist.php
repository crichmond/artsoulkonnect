<?php get_header(); ?>

  <?php 
  if(get_field('banner_image')): 
    $img = get_field('banner_image');
    $bg = $img['sizes']['post-large'];
  else:
    $img = get_field('default_artist_banner_image','option');
    $bg = $img['sizes']['post-large'];
  endif; ?>


  <section class="featured-media 
    <?php if(is_page() || is_single()) { echo ' single'; } ?>
    <?php if(get_post_type() == 'artist') { echo ' artist'; } ?>
    ">
    <div class="background" style="
      background-image: url('<?php echo $bg; ?>');
      background-position: 50% 50%;
      background-size: cover;">
    </div>

    <div class="global-container">
      <div class="profile-image">
        <?php 
        if(get_field('profile_image')): 
          $profile_img = get_field('profile_image');
        else:
          $profile_img = get_field('default_artist_profile_image','option');
        endif; ?>
        <img src="<?php echo $profile_img['sizes']['profile-image']; ?>" alt="Profile image for <?php the_title(); ?>">
      </div>
      
      <div class="social">
        <ul>
          <li>
            <a class="email_link" target="_blank" href="mailto:?subject=<?php the_title(); ?>&body=<?php the_permalink(); ?>" title="Email this page"><i class="fa fa-envelope-o"></i></a>
          </li>
          <li>
            <script>
            function fbs_click() {
              u="<?php the_permalink(); ?>";
              t="<?php the_title; ?>";
              window.open('http://www.facebook.com/sharer.php?u='+encodeURIComponent(u)+'&t='+encodeURIComponent(t),'sharer','toolbar=0,status=0,width=626,height=436');
              return false;
            }
            </script>
            <a class="fb_link" onclick="return fbs_click()" href="#" title="Share on Facebook"><i class="fa fa-facebook"></i></a>
          </li>
          <li>
            <a class="twitter_link" href="http://twitter.com/share?text=<?php the_title(); ?>%21&url=<?php the_permalink(); ?>" target="_blank" title="Tweet this page"><i class="fa fa-twitter"></i></a>
          </li>
        </ul>
      </div>
    </div>
  </section>

  <section class="main single artist">
    <div class="global-container">

    <?php if(have_posts()): while (have_posts()) : the_post(); ?>

      <article class="post">
  
        <?php the_title('<h2 class="post-title"><a href="'. get_permalink() .'">', '</a></h2>'); ?>
        
        <?php the_tags('<div class="post-tags">', ', ', '</div>'); ?>
        
        <div class="post-excerpt">
          <h3>Info</h3>
          <?php the_field('artist_information'); ?>
        </div>
        
        <?php if(get_field('discography')): ?>
        <div class="discography">
          <h3>Discography</h3>
          
          <div class="row-12">
          <?php while(has_sub_field('discography')): ?>
            <div class="album">
            <div class="cover">
              <?php 
              if(get_sub_field('cover')): 
                $cover_img = get_sub_field('cover');
              else:
                $cover_img = get_field('default_album_cover_image','option');
              endif; ?>
              <img src="<?php echo $cover_img['sizes']['album-cover']; ?>" alt="Album cover for <?php the_sub_field('title'); ?>">
            </div>
            
            <div class="tracks">
              <?php if(get_sub_field('tracks')): 
              while(has_sub_field('tracks')): ?>
                <div class="track">
                  <div class="track-title"><?php the_sub_field('title'); ?></div>
                  <div class="track-player">
                    <audio controls>
                      <source src="<?php the_sub_field('audio'); ?>" type="audio/mpeg">
                    Your browser does not support native audio.
                    </audio>
                  </div>
                </div>
              <?php endwhile; endif; ?>
            </div>
          </div><!-- /.album -->
            
          <?php endwhile; ?>
          </div><!-- /.row-12 -->
        </div><!-- /.discography -->
        <?php endif; ?>

        <div class="connect">
          <h3>Connect with <?php the_title(); ?></h3>
          <ul>
            <?php if(get_field('facebook')): ?>
            <li><a href="http://facebook.com/<?php the_field('facebook'); ?>">facebook.com/<?php the_field('facebook'); ?></a></li>
            <?php endif; ?>

            <?php if(get_field('soundcloud')): ?>
            <li><a href="http://soundcloud.com/<?php the_field('soundcloud'); ?>">soundcloud.com/<?php the_field('soundcloud'); ?></a></li>
            <?php endif; ?>

            <?php if(get_field('itunes')): ?>
            <li><a href="<?php the_field('itunes'); ?>">On iTunes</a></li>
            <?php endif; ?>

            <?php if(get_field('twitter')): ?>
            <li><a href="http://twitter.com/<?php the_field('twitter'); ?>">@<?php the_field('twitter'); ?></a></li>
            <?php endif; ?>
          </ul>
        </div>

      </article>
      
    <?php endwhile; endif; ?>
    
    <hr>

    </div><!-- /.global-container -->
  </section><!-- /.single -->

  <?php get_template_part('partials/asset', 'ads-footer'); ?>

<?php get_footer(); ?> 