<?php 
get_header(); ?>

  <section class="main video-archive">
    <div class="global-container">
      
    <?php if (have_posts()): ?>
      <h2 class="archive-title">
        <span>Videos:</span>
        <?php echo single_term_title("", false); ?>
      </h2>

      <div class="row-12">
        <?php $i = 1; while(have_posts()): the_post(); ?>
          <div class="post">
            
            <?php if(has_post_thumbnail()): 
                $img = wp_get_attachment_image_src(get_post_thumbnail_id(), 'post-large');
                $bg = $img[0];
              else:
                $img = get_field('default_post_image','option');
                $bg = $img['sizes']['post-large'];
              endif; ?>
            <a class="post-image" href="<?php the_permalink(); ?>" style="
              background-image: url('<?php echo $bg; ?>');
              background-position: 50% 50%;
              background-size: cover;">
            </a>

            <div class="post-title">
              <a href="<?php the_permalink(); ?>"><?php the_title(); ?></a>
            </div>

            <?php the_terms($post->ID,'video-tags','<div class="post-tags">', ', ', '</div>'); ?>
        </div>

      <?php 
        if ($i % 4 == 0) { echo "<hr class='hr-4'>"; }
        if ($i % 3 == 0) { echo "<hr class='hr-3'>"; }
        if ($i % 2 == 0) { echo "<hr class='hr-2'>"; }
        $i ++; 
      endwhile; ?>
      
      <div class="pagination">
        <div class="posts-prev"><?php previous_posts_link('&nbsp;'); ?></div>
        <?php
        $big = 999999999; // need an unlikely integer
        echo paginate_links( 
          array(
            'base' => str_replace( $big, '%#%', esc_url( get_pagenum_link( $big ) ) ),
            'format' => '/page/%#%',
            'current' => max( 1, get_query_var('paged') ),
            'total' => $wp_query->max_num_pages,
            'prev_next' => false
          )
        );
        ?>
        
        <div class="posts-next"><?php next_posts_link('&nbsp;'); ?></div>
      </div><!-- /.pagination -->

              
          

      </div><!-- /.row-12 -->
    <? endif; ?>

    </div><!-- /.global-container -->
  </section><!-- /.single -->

<?php get_footer(); ?> 