jQuery(document).ready(function($) {
            
  /* MediaElement */
    $('audio').mediaelementplayer();

  /* FitVids */
    $(".global-container").fitVids();


  /* Mobile Menu */
    $('.show-mobile-menu').click(function() {
        $(this).attr('style', '').toggleClass('active');
        $('nav.mobile-nav ul.menu, nav.mobile-nav ul.social').slideToggle('slow', function() {
            $(this).attr('style', '').toggleClass('hide');
        });
    });

    $('nav.mobile-nav ul li ul.sub-menu').parent().prepend('<span class="dropdown">+</span>');
    $('span.dropdown').click(function() {
      $(this).parent().children('.sub-menu').slideToggle();
      $(this).text($(this).text() === '+' ? '–' : '+');
    });

  //Magnific Popup
    $('.nav-search a').magnificPopup({type:'inline'});

  //Full Width Audio Player
    soundManager.url = 'swf/';
    soundManager.flashVersion = 9;
    soundManager.useHTML5Audio = true;
    soundManager.debugMode = false;

    $('#fap').fullwidthAudioPlayer({opened: false, wrapperPosition: 'bottom', });

  // SelectBox
    $('select#genres').selectBox({
        mobile: false
    });

});