// ------------------------------------------------------------------
// OVERFLOW SLIDER OBJ
// ------------------------------------------------------------------
var OverflowSlider = ( function( $ ) {
	var $slider,
		$slides,
		$wrapper,
		$viewport,
		$recentlyOffset,
		mobileWidth = 600,
		mobileHeight = 600,

		// ---------------------------------------------------------------------------------
		// INIT METHODS
		// ---------------------------------------------------------------------------------
		init = function() {
			popVars();
			doneLoading();
		},
		popVars = function() {
			$slider = $( "#overflow-slider" );
			$slides = $slider.find( ".slide" ).not(".bx-clone");
			$wrapper = $slider.closest( ".overflow-slider_wrapper" );
		},
		doneLoading = function() {
			$slider.imagesLoaded( function() {
				var newHeight = 0;

				// images done loading; transfer them to background images...
				VB.Util.transferImages( ".slider-transfer-image" );
				// ... and init the slider
				initSlider();

				if( $slider.offset().top + $slider.height() < window.innerHeight
					&& window.innerWidth <= mobileWidth ) {

					newHeight = window.innerHeight;

					if( newHeight > mobileHeight )
						newHeight = mobileHeight;

					setHeight( newHeight - $slider.offset().top );
				}
			} );
		},

		// ---------------------------------------------------------------------------------
		// SLIDER STATE HANDLERS 
		// Mainly for faking unbroken scrolling. BXSlider only creates one clone at the beginning
		// and end of the slides, and we need two since we're showing the overflow. This code
		// transforms the appropriate slides before/after transition to fake that effect.
		// ---------------------------------------------------------------------------------
		beforeTransHandler = function( $slide, prevIndex, newIndex ) {
			var $tgt,
				offset = 0,
				w = $slide.width(),
				total = $slides.length;

			if( prevIndex === 0 && newIndex === total - 1 ) {
				// we're looping backwards from first to last
				// we need to grab the 2nd-to-last slide and matrix it over to the front of the line momentarily
				$tgt = $slides.eq( total - 2 );

				offset = -w * total;
			} else if( prevIndex === total - 1 && newIndex === 0 ) {
				// we're looping forwards from last to first
				// we need to grab the 2nd slide and matrix it over to the end of the line momentarily
				$tgt = $slides.eq( 1 );

				offset = w * total;
			}

			if( $tgt ) {
				$tgt.css( {
					"transform" : "matrix( 1, 0, 0, 1, " + offset + ", 0 )"
				} );

				$recentlyOffset = $tgt;
			} else {
				$recentlyOffset = null;
			}

			toggleContent( $slides.eq( prevIndex ), false );
		},
		afterTransHandler = function( $slide, prevIndex, newIndex ) {
			if( $recentlyOffset && $recentlyOffset.length ) {
				$recentlyOffset.css( {
					"transform" : ""
				} );
			}

			toggleContent( $slide, true );
		},
		loadHandler = function() {
			 // stupid hack to add a class to our container, since it needs unique styling
			 // also define a couple more vars
			$viewport = $wrapper.find( ".bx-viewport" );
			// show first slide's content
			toggleContent( $slides.eq( 0 ), true );
		},
		toggleContent = function( $tgt, show) {
			$tgt.find( ".overflow-slider_content" )[ show ? "addClass" : "removeClass" ]( "show" );
		},

		// ---------------------------------------------------------------------------------
		// DIMENSIONAL CONTROL
		// ---------------------------------------------------------------------------------
		setHeight = function( height ) {
			if( typeof height !== "number" )
				return;

			$viewport.css( {
				"min-height" : height + "px"
			} );

			$slides.css( {
				"height" : height + "px"
			} );
		},

		// ---------------------------------------------------------------------------------
		// SLIDER INIT
		// ---------------------------------------------------------------------------------
		initSlider = function() {
			$slider
				.removeClass( "overflow-slider_loading" )
				.bxSlider( {
					adaptiveHeight : true,
					slideSelector : ".slide",
					onSliderLoad : loadHandler,
					onSlideBefore : beforeTransHandler,
					onSlideAfter : afterTransHandler
				} );

			$( window ).trigger( "resize" );
		};

	return {
		"init" : init,
		"setHeight" : setHeight
	};
} )( jQuery );

// ------------------------------------------------------------------
// SMALL CAROUSEL OBJ
// ------------------------------------------------------------------
var SmallCarousel = ( function( $ ) {
	var $slider,
		$slides,
		$wrapper,
		$viewport,
		$recentlyOffset,

		// ---------------------------------------------------------------------------------
		// INIT METHODS
		// ---------------------------------------------------------------------------------
		init = function() {
			popVars();
			doneLoading();
		},
		popVars = function() {
			$slider = $( "#small-carousel" );
			$slides = $slider.find( ".slide" ).not(".bx-clone");
			$wrapper = $slider.closest( ".small-carousel_wrapper" );
		},
		doneLoading = function() {
			$slider.imagesLoaded( function() {
				var newHeight = 0;

				// images done loading; transfer them to background images...
				VB.Util.transferImages( ".carousel-transfer-image" );
				// ... and init the slider
				initSlider();
			} );
		},

		// ---------------------------------------------------------------------------------
		// SLIDER STATE HANDLERS 
		// Mainly for faking unbroken scrolling. BXSlider only creates one clone at the beginning
		// and end of the slides, and we need two since we're showing the overflow. This code
		// transforms the appropriate slides before/after transition to fake that effect.
		// ---------------------------------------------------------------------------------
		// beforeTransHandler = function( $slide, prevIndex, newIndex ) {
		// 	var $tgt,
		// 		offset = 0,
		// 		w = $slide.width(),
		// 		total = $slides.length;

		// 	if( prevIndex === 0 && newIndex === total - 1 ) {
		// 		// we're looping backwards from first to last
		// 		// we need to grab the 2nd-to-last slide and matrix it over to the front of the line momentarily
		// 		$tgt = $slides.eq( total - 2 );

		// 		offset = -w * total;
		// 	} else if( prevIndex === total - 1 && newIndex === 0 ) {
		// 		// we're looping forwards from last to first
		// 		// we need to grab the 2nd slide and matrix it over to the end of the line momentarily
		// 		$tgt = $slides.eq( 1 );

		// 		offset = w * total;
		// 	}

		// 	if( $tgt ) {
		// 		$tgt.css( {
		// 			"transform" : "matrix( 1, 0, 0, 1, " + offset + ", 0 )"
		// 		} );

		// 		$recentlyOffset = $tgt;
		// 	} else {
		// 		$recentlyOffset = null;
		// 	}

		// },
		// afterTransHandler = function( $slide, prevIndex, newIndex ) {
		// 	if( $recentlyOffset && $recentlyOffset.length ) {
		// 		$recentlyOffset.css( {
		// 			"transform" : ""
		// 		} );
		// 	}
		// },
		loadHandler = function() {
			 // stupid hack to add a class to our container, since it needs unique styling
			 // also define a couple more vars
			$viewport = $wrapper.find( ".bx-viewport" );
		},
		toggleContent = function( $tgt, show) {
			$tgt.find( ".overflow-slider_content" )[ show ? "addClass" : "removeClass" ]( "show" );
		},

		// ---------------------------------------------------------------------------------
		// SLIDER INIT
		// ---------------------------------------------------------------------------------
		initSlider = function() {
			$slider
				.removeClass( "small-carousel_loading" )
				.bxSlider( {
					adaptiveHeight : true,
					slideSelector : ".slide",
					minSlides: 2,
					maxSlides: 4,
					slideWidth: 256,
					slideMargin: 20,
					onSliderLoad : loadHandler
					// onSlideBefore : beforeTransHandler,
					// onSlideAfter : afterTransHandler
				} );

			$( window ).trigger( "resize" );
		};

	return {
		"init" : init
	};
} )( jQuery );




jQuery( document ).ready( function( $ ) {
	// to initialize, just call TypeOfSlider.init()
	
	OverflowSlider.init();
	SmallCarousel.init();
} );