/* Sliders.js and utilities.js files are compiled into this by CodeKit. */

/* **********************************************
     Begin utilities.js
********************************************** */

jQuery( document ).ready( function( $ ) {
	window.VB = window.VB || {
		Util : ( function() {
				
			/**
			* Iterates over all of the direct children of the 'selector' element, 
			* determines the tallest and then sets all the heights equal.
			* If the element has a 'data-min-width' attribute, the method will
			* suppress the resize below that width.
			*/
			var _normalizeChildHeights = function( selector ){
				var wWidth = VB.Cache.$WIN.innerWidth(),
					$selector = $(selector),
					$manipulatedChildCache = $();

				// reverse so we work from the bottom up
				$( $selector.get().reverse() ).each(function(){
					var minWidth = $(this).data('min-width') || 0,
						maxHeight = 0,
						height = 0,
						multiplier = 1,
						childrenHgtObj = {},
						thresholdLow = 0,
						childrenEqHgtClass = "also-make-equal-heights",
						childHeightManipulate = function( $el, get ) {
							// check to see if we've already manipulated this child element's height...
							if( $el.not( $manipulatedChildCache ).length === 0 ) {
								// ... and if so, gtfo
								return false;
							}

							// this gets or sets the heights of child elements that need height normalization
							var id = $el.attr( "data-equal-heights-id" ),
								h = 0;

							if( get ) {
								// get

								// if this particular group's height hasn't been defined in childrenHgtObj...
								if( typeof childrenHgtObj[ id ] === "undefined" ) {
									// ... then define a dummy object with a 0 height object (we'll set that properly next)
									childrenHgtObj[ id ] = {
										height : 0
									}; 
								}
								// reset height so we can get an accurate reading
								$el.css( { "height" : "" } );
								// set our height
								h = $el.outerHeight();
								// either set a new height or keep the old one, whichever is greater
								childrenHgtObj[ id ].height = ( h > childrenHgtObj[ id ].height ? h : childrenHgtObj[ id ].height );

							} else {
								// set
								// if we have a height defined for this element...
								if( typeof childrenHgtObj[ id ] !== "undefined" ) {
									// ... set it
									$el.css( { 
										"height" : childrenHgtObj[ id ].height + "px" 
									} );
									$manipulatedChildCache = $manipulatedChildCache.add( $el );
								}
							}
						},
						normalizeSubChildHeights = function( $el ) {
							// gather all .also-make-equal-heights elements from this row
							var $childrenEqHgt = $el.find( "." + childrenEqHgtClass );

							// make two passes over the child elements...
							$childrenEqHgt
								.each( function() {
									// ... one to get all their heights so we know the largest...
									childHeightManipulate( $( this ), true );
								} )
								.each( function() {
									// ... and one to normalize all their heights based on the above findings
									childHeightManipulate( $( this ), false );
								} );
							
							childrenHgtObj = {}; // reset childrenHgtObj
						},
						$this = $( this );
					

					if( thresholdLow = $this.attr( "data-normalize-above" ) ) {
						thresholdLow = parseInt( thresholdLow );
						if( Modernizr.mq( "only screen and ( max-width: " + ( thresholdLow - 1 ) + "px )" ) ) {
							$this.children().css( "height", "" ); // reset child heights
							return;
						}
					}


					$this.imagesLoaded( function( images, proper, broken ){
						var $curRow = this,
							$precogRow = $(),
							height = 0,
							$curBlock;
							
						if ( $curRow.hasClass('variable-column') ) {
							//Two column
							var columnCount = 1,
								query = "",
								attr = "";
							
							for( var breakpoint in VB.Cache.BREAKPOINTS ) {
								attr = $curRow.attr( "data-columns-" + breakpoint.toLowerCase() );
								if( typeof attr !== "undefined" ) {
									query = VB.Util.getQuery( breakpoint );

									if( query && Modernizr.mq( query ) ) {
										columnCount = attr;
									}
									
								}
							}

							$curRow.children().each( function( i ) {
								$curBlock = $( this );
								
								if (i % columnCount == 0){
									var j = i,
										k = i + columnCount;

									$precogRow = $();

									// BEGIN NORMALIZING CHILD ELEMENT HEIGHTS
									// if there are elements within the equal-heights primary elements
									// which need height normalization, this will do so
									// it loops over an entire row's worth of child elements before adjusting
									// the parents' heights, since these children impact the parents' heights

									while( j < k ) {
										// build our "row" in advance... we'll use that right below
										var $curChild = $curRow.children().eq( j );

										if( $curChild.length ) {
											$precogRow = $precogRow.add( $curChild );
										}
										j++;
									}
									normalizeSubChildHeights( $precogRow );
									// END NORMALIZING CHILD ELEMENT HEIGHTS

									//Update previous heights and reset maxHeight
									if (i > 0){
										for ( var x = i - columnCount; x < i; x++){
											var $tgtCol = $curRow.children().eq( x );

											$tgtCol.css({
													"height":(wWidth >= minWidth ? maxHeight * multiplier : 'auto')
												});

											
										}
									}
									maxHeight = 0;
								}
								
								$curBlock.css('height', '');  // reset the height
								
								height = $curBlock.outerHeight();
								maxHeight = ( $curBlock.css('display') !== 'none' && height > maxHeight) ? height : maxHeight;

								if (i == $curRow.children().length - 1){
									for (var x = i - ( i % columnCount ); x <= i; x++ ) {
										var $tgt = $curRow.children().eq(x);
										
										$tgt.css({
											"height":(wWidth >= minWidth ? maxHeight * multiplier : 'auto')
										});
									}
								}
							});
						} else {
							// debugger;
							normalizeSubChildHeights( $curRow );
							$curRow.children().each( function( i ) {
								$curBlock = $( this );
								
								$curBlock.css('height', '');  // reset the height
								
								height = $curBlock.outerHeight();
								maxHeight = ( $curBlock.css('display') !== 'none' && height > maxHeight ) ? height : maxHeight;

							}).css({
								"height" : ( wWidth >= minWidth ? maxHeight * multiplier : 'auto' )
							});
						}
					});
				});
				return $selector;
			};

			/**
			* Takes an image within the element (immediate child), hides it, and assigns it as a
			* background image for that element.
			*/
			var $_transferredImages,
				_transferImages = function( selector ) {
					var $selector = $(selector);

					// init $_transferredImages
					$_transferredImages = $_transferredImages || $();

					// remove previously transferred images so we aren't wasting processing power
					$selector = $selector.not( $_transferredImages );

					$selector.each( function() {
						var $this = $( this ),
							$img = $this.children( "img" ),
							src = "",
							$img1;

						if( $img.length ) {
							
							$img1 = $img.eq( 0 );
							src = $img1.attr( "src" );
							$img1.remove();

							$this.css( { 
								"background-image" : "url('" + src + "')"
							} );
						}
					} );

					// add the processed images to our cache
					$_transferredImages = $_transferredImages.add( $selector );

					return $selector;
				};
			
			return {
				'normalizeChildHeights' : _normalizeChildHeights,
				'transferImages' : _transferImages
			};
			
		} )()
	}
} );

/* **********************************************
     Begin main.js
********************************************** */

( function( $ ) {
	"use strict";

	var CommonSliderProps = {
		$slider : null,
		$slides : null,
		$wrapper : null,
		$viewport : null,
		$recentlyOffset : null,

		mobileWidth : 600,
		mobileHeight : 600,
		
		loadingClass : "is-loading",
		slideClass : "slide",
		cloneClass : "bx-clone",
		viewportClass : "bx-viewport"
	};

	// ------------------------------------------------------------------
	// OVERFLOW SLIDER OBJ
	// ------------------------------------------------------------------
	var OverflowSlider = function( el, opts ){
		var self = this,
			props = $.extend( {
				transferImageClass : "slider-transfer-image"
			}, CommonSliderProps ),

			// ---------------------------------------------------------------------------------
			// INIT METHODS
			// ---------------------------------------------------------------------------------
			init = function() {
				popVars();
				doneLoading();
			},
			popVars = function() {
				props.$slider = $( el );
				props.$slides = props.$slider.find( "." + props.slideClass ).not( "." + props.cloneClass );
				props.$wrapper = props.$slider.closest( ".overflow-slider_wrapper" );
			},
			doneLoading = function() {
				props.$slider.imagesLoaded( function() {
					var newHeight = 0;

					// images done loading; transfer them to background images...
					VB.Util.transferImages( "." + props.transferImageClass );
					// ... and init the slider
					initSlider();

					if( props.$slider.offset().top + props.$slider.height() < window.innerHeight
						&& window.innerWidth <= props.mobileWidth ) {

						newHeight = window.innerHeight;

						if( newHeight > props.mobileHeight )
							newHeight = props.mobileHeight;

						setHeight( newHeight - props.$slider.offset().top );
					}
				} );
			},

			// ---------------------------------------------------------------------------------
			// SLIDER STATE HANDLERS 
			// Mainly for faking unbroken scrolling. BXSlider only creates one clone at the beginning
			// and end of the slides, and we need two since we're showing the overflow. This code
			// transforms the appropriate slides before/after transition to fake that effect.
			// ---------------------------------------------------------------------------------
			beforeTransHandler = function( $slide, prevIndex, newIndex ) {
				var $tgt,
					offset = 0,
					w = $slide.width(),
					total = props.$slides.length;

				if( prevIndex === 0 && newIndex === total - 1 ) {
					// we're looping backwards from first to last
					// we need to grab the 2nd-to-last slide and matrix it over to the front of the line momentarily
					$tgt = props.$slides.eq( total - 2 );

					offset = -w * total;
				} else if( prevIndex === total - 1 && newIndex === 0 ) {
					// we're looping forwards from last to first
					// we need to grab the 2nd slide and matrix it over to the end of the line momentarily
					$tgt = props.$slides.eq( 1 );

					offset = w * total;
				}

				if( $tgt ) {
					$tgt.css( {
						"transform" : "matrix( 1, 0, 0, 1, " + offset + ", 0 )"
					} );

					props.$recentlyOffset = $tgt;
				} else {
					props.$recentlyOffset = null;
				}

				toggleContent( props.$slides.eq( prevIndex ), false );
			},
			afterTransHandler = function( $slide, prevIndex, newIndex ) {
				if( props.$recentlyOffset && props.$recentlyOffset.length ) {
					props.$recentlyOffset.css( {
						"transform" : ""
					} );
				}

				toggleContent( $slide, true );
			},
			loadHandler = function() {
				 // stupid hack to add a class to our container, since it needs unique styling
				 // also define a couple more vars
				props.$viewport = props.$wrapper.find( "." + props.viewportClass );
				// show first slide's content
				toggleContent( props.$slides.eq( 0 ), true );
			},
			toggleContent = function( $tgt, show ) {
				$tgt.find( ".overflow-slider_content" )[ show ? "addClass" : "removeClass" ]( "show" );
			},

			// ---------------------------------------------------------------------------------
			// DIMENSIONAL CONTROL
			// ---------------------------------------------------------------------------------
			setHeight = function( height ) {
				if( typeof height !== "number" )
					return;

				props.$viewport.css( {
					"min-height" : height + "px"
				} );

				props.$slides.css( {
					"height" : height + "px"
				} );
			},

			// ---------------------------------------------------------------------------------
			// SLIDER INIT
			// ---------------------------------------------------------------------------------
			initSlider = function() {
				props.$slider
					.removeClass( props.loadingClass )
					.bxSlider( {
						adaptiveHeight : true,
						slideSelector : "." + props.slideClass,
						onSliderLoad : loadHandler,
						onSlideBefore : beforeTransHandler,
						onSlideAfter : afterTransHandler
					} );

				$( window ).trigger( "resize" );
			};

		init();

	};

	$.fn.overflowSlider = function( opts ) {
		if( this.length ) {
			return this.each( function() {
				var $el = $( this ),
					overflowSlider;

				// Return early if this element already has a slider instance
				if( $el.data( "overflowSlider" ) ) {
					return;
				}

				// pass options to slider constructor
				overflowSlider = new OverflowSlider( this, opts );

				// Store slider object in this element's data
				$el.data( "overflowSlider", overflowSlider );
			} );
		}

		return this;
	};


	// ------------------------------------------------------------------
	// SMALL CAROUSEL OBJ
	// ------------------------------------------------------------------
	var SmallCarousel = function( el, opts ) {
		var props = $.extend( {
				transferImageClass : "carousel-transfer-image"
			}, CommonSliderProps ),

			// ---------------------------------------------------------------------------------
			// INIT METHODS
			// ---------------------------------------------------------------------------------
			init = function() {
				popVars();
				doneLoading();
			},
			popVars = function() {
				props.$slider = $( el );
				props.$slides = props.$slider.find( "." + props.slideClass ).not( "." + props.cloneClass );
				props.$wrapper = props.$slider.closest( ".small-carousel_wrapper" );
			},
			doneLoading = function() {
				props.$slider.imagesLoaded( function() {
					var newHeight = 0;

					// images done loading; transfer them to background images...
					VB.Util.transferImages( "." + props.transferImageClass );
					// ... and init the slider
					initSlider();
				} );
			},

			// ---------------------------------------------------------------------------------
			// SLIDER STATE HANDLERS 
			// Mainly for faking unbroken scrolling. BXSlider only creates one clone at the beginning
			// and end of the slides, and we need two since we're showing the overflow. This code
			// transforms the appropriate slides before/after transition to fake that effect.
			// ---------------------------------------------------------------------------------
			// beforeTransHandler = function( $slide, prevIndex, newIndex ) {
			// 	var $tgt,
			// 		offset = 0,
			// 		w = $slide.width(),
			// 		total = $slides.length;

			// 	if( prevIndex === 0 && newIndex === total - 1 ) {
			// 		// we're looping backwards from first to last
			// 		// we need to grab the 2nd-to-last slide and matrix it over to the front of the line momentarily
			// 		$tgt = $slides.eq( total - 2 );

			// 		offset = -w * total;
			// 	} else if( prevIndex === total - 1 && newIndex === 0 ) {
			// 		// we're looping forwards from last to first
			// 		// we need to grab the 2nd slide and matrix it over to the end of the line momentarily
			// 		$tgt = $slides.eq( 1 );

			// 		offset = w * total;
			// 	}

			// 	if( $tgt ) {
			// 		$tgt.css( {
			// 			"transform" : "matrix( 1, 0, 0, 1, " + offset + ", 0 )"
			// 		} );

			// 		$recentlyOffset = $tgt;
			// 	} else {
			// 		$recentlyOffset = null;
			// 	}

			// },
			// afterTransHandler = function( $slide, prevIndex, newIndex ) {
			// 	if( $recentlyOffset && $recentlyOffset.length ) {
			// 		$recentlyOffset.css( {
			// 			"transform" : ""
			// 		} );
			// 	}
			// },
			loadHandler = function() {
				 // stupid hack to add a class to our container, since it needs unique styling
				 // also define a couple more vars
				props.$viewport = props.$wrapper.find( "." + props.viewportClass );
			},
			toggleContent = function( $tgt, show) {
				$tgt.find( ".overflow-slider_content" )[ show ? "addClass" : "removeClass" ]( "show" );
			},

			// ---------------------------------------------------------------------------------
			// SLIDER INIT
			// ---------------------------------------------------------------------------------
			initSlider = function() {
				props.$slider
					.removeClass( "is-loading" )
					.bxSlider( {
						adaptiveHeight : true,
						slideSelector : "." + props.slideClass,
						minSlides: 2,
						maxSlides: 4,
						slideWidth: 240,
						slideMargin: 20,
						onSliderLoad : loadHandler
						// onSlideBefore : beforeTransHandler,
						// onSlideAfter : afterTransHandler
					} );

				$( window ).trigger( "resize" );
			};

		init();
	}

	$.fn.smallCarousel = function( opts ) {
		if( this.length ) {
			return this.each( function() {
				var $el = $( this ),
					smallCarousel;

				// Return early if this element already has a slider instance
				if( $el.data( "smallCarousel" ) ) {
					return;
				}

				// pass options to slider constructor
				smallCarousel = new SmallCarousel( this, opts );

				// Store slider object in this element's data
				$el.data( "smallCarousel", smallCarousel );
			} );
		}

		return this;
	};

	
} )( jQuery );



jQuery( document ).ready( function( $ ) {
	// to initialize, just call TypeOfSlider.init()
	
	$( ".overflow-slider" ).overflowSlider();
	$( ".small-carousel" ).smallCarousel();
} );