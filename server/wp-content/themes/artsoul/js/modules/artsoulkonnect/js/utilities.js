var VB = {
	Util : ( function() {
			
		/**
		* Iterates over all of the direct children of the 'selector' element, 
		* determines the tallest and then sets all the heights equal.
		* If the element has a 'data-min-width' attribute, the method will
		* suppress the resize below that width.
		*/
		var _normalizeChildHeights = function( selector ){
			var wWidth = VB.Cache.$WIN.innerWidth(),
				$selector = $(selector),
				$manipulatedChildCache = $();

			// reverse so we work from the bottom up
			$( $selector.get().reverse() ).each(function(){
				var minWidth = $(this).data('min-width') || 0,
					maxHeight = 0,
					height = 0,
					multiplier = 1,
					childrenHgtObj = {},
					thresholdLow = 0,
					childrenEqHgtClass = "also-make-equal-heights",
					childHeightManipulate = function( $el, get ) {
						// check to see if we've already manipulated this child element's height...
						if( $el.not( $manipulatedChildCache ).length === 0 ) {
							// ... and if so, gtfo
							return false;
						}

						// this gets or sets the heights of child elements that need height normalization
						var id = $el.attr( "data-equal-heights-id" ),
							h = 0;

						if( get ) {
							// get

							// if this particular group's height hasn't been defined in childrenHgtObj...
							if( typeof childrenHgtObj[ id ] === "undefined" ) {
								// ... then define a dummy object with a 0 height object (we'll set that properly next)
								childrenHgtObj[ id ] = {
									height : 0
								}; 
							}
							// reset height so we can get an accurate reading
							$el.css( { "height" : "" } );
							// set our height
							h = $el.outerHeight();
							// either set a new height or keep the old one, whichever is greater
							childrenHgtObj[ id ].height = ( h > childrenHgtObj[ id ].height ? h : childrenHgtObj[ id ].height );

						} else {
							// set
							// if we have a height defined for this element...
							if( typeof childrenHgtObj[ id ] !== "undefined" ) {
								// ... set it
								$el.css( { 
									"height" : childrenHgtObj[ id ].height + "px" 
								} );
								$manipulatedChildCache = $manipulatedChildCache.add( $el );
							}
						}
					},
					normalizeSubChildHeights = function( $el ) {
						// gather all .also-make-equal-heights elements from this row
						var $childrenEqHgt = $el.find( "." + childrenEqHgtClass );

						// make two passes over the child elements...
						$childrenEqHgt
							.each( function() {
								// ... one to get all their heights so we know the largest...
								childHeightManipulate( $( this ), true );
							} )
							.each( function() {
								// ... and one to normalize all their heights based on the above findings
								childHeightManipulate( $( this ), false );
							} );
						
						childrenHgtObj = {}; // reset childrenHgtObj
					},
					$this = $( this );
				

				if( thresholdLow = $this.attr( "data-normalize-above" ) ) {
					thresholdLow = parseInt( thresholdLow );
					if( Modernizr.mq( "only screen and ( max-width: " + ( thresholdLow - 1 ) + "px )" ) ) {
						$this.children().css( "height", "" ); // reset child heights
						return;
					}
				}


				$this.imagesLoaded( function( images, proper, broken ){
					var $curRow = this,
						$precogRow = $(),
						height = 0,
						$curBlock;
						
					if ( $curRow.hasClass('variable-column') ) {
						//Two column
						var columnCount = 1,
							query = "",
							attr = "";
						
						for( var breakpoint in VB.Cache.BREAKPOINTS ) {
							attr = $curRow.attr( "data-columns-" + breakpoint.toLowerCase() );
							if( typeof attr !== "undefined" ) {
								query = VB.Util.getQuery( breakpoint );

								if( query && Modernizr.mq( query ) ) {
									columnCount = attr;
								}
								
							}
						}

						$curRow.children().each( function( i ) {
							$curBlock = $( this );
							
							if (i % columnCount == 0){
								var j = i,
									k = i + columnCount;

								$precogRow = $();

								// BEGIN NORMALIZING CHILD ELEMENT HEIGHTS
								// if there are elements within the equal-heights primary elements
								// which need height normalization, this will do so
								// it loops over an entire row's worth of child elements before adjusting
								// the parents' heights, since these children impact the parents' heights

								while( j < k ) {
									// build our "row" in advance... we'll use that right below
									var $curChild = $curRow.children().eq( j );

									if( $curChild.length ) {
										$precogRow = $precogRow.add( $curChild );
									}
									j++;
								}
								normalizeSubChildHeights( $precogRow );
								// END NORMALIZING CHILD ELEMENT HEIGHTS

								//Update previous heights and reset maxHeight
								if (i > 0){
									for ( var x = i - columnCount; x < i; x++){
										var $tgtCol = $curRow.children().eq( x );

										$tgtCol.css({
												"height":(wWidth >= minWidth ? maxHeight * multiplier : 'auto')
											});

										
									}
								}
								maxHeight = 0;
							}
							
							$curBlock.css('height', '');  // reset the height
							
							height = $curBlock.outerHeight();
							maxHeight = ( $curBlock.css('display') !== 'none' && height > maxHeight) ? height : maxHeight;

							if (i == $curRow.children().length - 1){
								for (var x = i - ( i % columnCount ); x <= i; x++ ) {
									var $tgt = $curRow.children().eq(x);
									
									$tgt.css({
										"height":(wWidth >= minWidth ? maxHeight * multiplier : 'auto')
									});
								}
							}
						});
					} else {
						// debugger;
						normalizeSubChildHeights( $curRow );
						$curRow.children().each( function( i ) {
							$curBlock = $( this );
							
							$curBlock.css('height', '');  // reset the height
							
							height = $curBlock.outerHeight();
							maxHeight = ( $curBlock.css('display') !== 'none' && height > maxHeight ) ? height : maxHeight;

						}).css({
							"height" : ( wWidth >= minWidth ? maxHeight * multiplier : 'auto' )
						});
					}
				});
			});
			return $selector;
		};

		/**
		* Takes an image within the element (immediate child), hides it, and assigns it as a
		* background image for that element.
		*/
		var $_transferredImages,
			_transferImages = function( selector ) {
				var $selector = $(selector);

				// init $_transferredImages
				$_transferredImages = $_transferredImages || $();

				// remove previously transferred images so we aren't wasting processing power
				$selector = $selector.not( $_transferredImages );

				$selector.each( function() {
					var $this = $( this ),
						$img = $this.children( "img" ),
						src = "",
						$img1;

					if( $img.length ) {
						
						$img1 = $img.eq( 0 );
						src = $img1.attr( "src" );
						$img1.remove();

						$this.css( { 
							"background-image" : "url('" + src + "')"
						} );
					}
				} );

				// add the processed images to our cache
				$_transferredImages = $_transferredImages.add( $selector );

				return $selector;
			};
		
		return {
			'normalizeChildHeights' : _normalizeChildHeights,
			'transferImages' : _transferImages
		};
		
	} )()
}