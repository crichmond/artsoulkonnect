<!DOCTYPE html>
<!--[if lt IE 7]> <html class="no-js lt-ie9 lt-ie8 lt-ie7 ie6" lang="en" xmlns="http://www.w3.org/1999/xhtml" xml:lang="eng" xmlns:fb="http://ogp.me/ns/fb#" xmlns:og="http://ogp.me/ns#"> <![endif]-->
<!--[if IE 7]>    <html class="no-js lt-ie9 lt-ie8 ie7" lang="en" xmlns="http://www.w3.org/1999/xhtml" xml:lang="eng" xmlns:fb="http://ogp.me/ns/fb#" xmlns:og="http://ogp.me/ns#"> <![endif]-->
<!--[if IE 8]>    <html class="no-js lt-ie9 ie8" lang="en" xmlns:fb="http://ogp.me/ns/fb#"> <![endif]-->
<!--[if IE 9]>    <html class="no-js ie9" lang="en" xmlns="http://www.w3.org/1999/xhtml" xml:lang="eng" xmlns:fb="http://ogp.me/ns/fb#" xmlns:og="http://ogp.me/ns#"> <![endif]-->
<!--[if (gt IE 9)|!(IE)]><!--> <html lang="en" class="no-js" xmlns="http://www.w3.org/1999/xhtml" xml:lang="eng" xmlns:fb="http://ogp.me/ns/fb#" xmlns:og="http://ogp.me/ns#"> <!--<![endif]-->
<head>

	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">

	<!-- CSS -->
	<!-- this is just some generic page styling, not necessary -->
	<style type = "text/css">
		* {
			-webkit-box-sizing: border-box;
			-moz-box-sizing: border-box;
			box-sizing: border-box;

		}
		body {
			font-family: Arial, sans-serif;
			font-size: 62.5%;
			margin: 0;
		}
		.page-width {
			width: 100%;
			max-width: 1084px;
			margin: 2em auto;
		}
	</style>

	<link rel="stylesheet" href="css/sliders.css"> <!-- required -->

</head>
<body>


	<!-- BEGIN MAIN/LARGE SLIDER -->
	<section class = "overflow-slider_wrapper">
		<ul id = "overflow-slider" class = "overflow-slider_loading">
			<li class = "slide slider-transfer-image">
				<img class = "overflow-slider_img" src = "http://lorempixel.com/1024/768/abstract" />
				<div class = "overflow-slider_content">
					<hgroup class = "overflow-slider_titles">
						<h2 class = "overflow-slider_title">Andy Mineo</h2>
						<h3 class = "overflow-slider_subtitle">Inside the Studio</h3>
					</hgroup>
					<div class = "overflow-slider_body">
						<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque tincidunt metus tortor, ac ornare nisl bibendum vel. Nulla hendrerit tellus in nisl volutpat, quis molestie nisi convallis. Vestibulum ut condimentum ipsum, non dapibus ipsum. Mauris lobortis mi id mattis tristique.</p>
					</div>
					<a class = "overflow-slider_read-more" href = "###">More</a>
				</div>
			</li>
			<li class = "slide slider-transfer-image">
				<img class = "overflow-slider_img" src = "http://lorempixel.com/1024/769/abstract" />
				<div class = "overflow-slider_content">
					<hgroup class = "overflow-slider_titles">
						<h2 class = "overflow-slider_title">Andy Mineo</h2>
						<h3 class = "overflow-slider_subtitle">Inside the Studio</h3>
					</hgroup>
					<div class = "overflow-slider_body">
						<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque tincidunt metus tortor, ac ornare nisl bibendum vel. Nulla hendrerit tellus in nisl volutpat, quis molestie nisi convallis. Vestibulum ut condimentum ipsum, non dapibus ipsum. Mauris lobortis mi id mattis tristique.</p>
						<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque tincidunt metus tortor, ac ornare nisl bibendum vel. Nulla hendrerit tellus in nisl volutpat, quis molestie nisi convallis. Vestibulum ut condimentum ipsum, non dapibus ipsum. Mauris lobortis mi id mattis tristique.</p>
						<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque tincidunt metus tortor, ac ornare nisl bibendum vel. Nulla hendrerit tellus in nisl volutpat, quis molestie nisi convallis. Vestibulum ut condimentum ipsum, non dapibus ipsum. Mauris lobortis mi id mattis tristique.</p>
					</div>
					<a class = "overflow-slider_read-more" href = "###">More</a>
				</div>
			</li>
			<li class = "slide slider-transfer-image">
				<img class = "overflow-slider_img" src = "http://lorempixel.com/1024/770/abstract" />
				<div class = "overflow-slider_content">
					<hgroup class = "overflow-slider_titles">
						<h2 class = "overflow-slider_title">Andy Mineo</h2>
						<h3 class = "overflow-slider_subtitle">Inside the Studio</h3>
					</hgroup>
					<div class = "overflow-slider_body">
						<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque tincidunt metus tortor, ac ornare nisl bibendum vel. Nulla hendrerit tellus in nisl volutpat, quis molestie nisi convallis. Vestibulum ut condimentum ipsum, non dapibus ipsum. Mauris lobortis mi id mattis tristique.</p>
					</div>
					<a class = "overflow-slider_read-more" href = "###">More</a>
				</div>
			</li>
			<li class = "slide slider-transfer-image">
				<img class = "overflow-slider_img" src = "http://lorempixel.com/1024/771/abstract" />
				<div class = "overflow-slider_content">
					<hgroup class = "overflow-slider_titles">
						<h2 class = "overflow-slider_title">Andy Mineo</h2>
						<h3 class = "overflow-slider_subtitle">Inside the Studio</h3>
					</hgroup>
					<div class = "overflow-slider_body">
						<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque tincidunt metus tortor, ac ornare nisl bibendum vel. Nulla hendrerit tellus in nisl volutpat, quis molestie nisi convallis. Vestibulum ut condimentum ipsum, non dapibus ipsum. Mauris lobortis mi id mattis tristique.</p>
						<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque tincidunt metus tortor, ac ornare nisl bibendum vel. Nulla hendrerit tellus in nisl volutpat, quis molestie nisi convallis. Vestibulum ut condimentum ipsum, non dapibus ipsum. Mauris lobortis mi id mattis tristique.</p>
					</div>
					<a class = "overflow-slider_read-more" href = "###">More</a>
				</div>
			</li>
			<li class = "slide slider-transfer-image">
				<img class = "overflow-slider_img" src = "http://lorempixel.com/1024/772/abstract" />
				<div class = "overflow-slider_content">
					<hgroup class = "overflow-slider_titles">
						<h2 class = "overflow-slider_title">Andy Mineo</h2>
						<h3 class = "overflow-slider_subtitle">Inside the Studio</h3>
					</hgroup>
					<div class = "overflow-slider_body">
						<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque tincidunt metus tortor, ac ornare nisl bibendum vel. Nulla hendrerit tellus in nisl volutpat, quis molestie nisi convallis. Vestibulum ut condimentum ipsum, non dapibus ipsum. Mauris lobortis mi id mattis tristique.</p>
					</div>
					<a class = "overflow-slider_read-more" href = "###">More</a>
				</div>
			</li>
		</ul>
	</section>
	
	<!-- END MAIN/LARGE SLIDER -->








	<!-- BEGIN FILLER CONTENT -->
	<div class = "page-width">
		<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus vulputate orci nec tellus pulvinar rhoncus. Aenean lacus sapien, hendrerit vitae nisi sit amet, egestas tincidunt nulla. Donec eleifend aliquam massa, sed vehicula elit scelerisque sit amet. Etiam semper nunc fringilla nulla pulvinar, sed iaculis neque auctor. Nam porttitor tortor euismod nisl consectetur, in bibendum arcu posuere. Curabitur at facilisis est. Donec odio sapien, condimentum a erat ac, interdum fermentum ipsum. Nam congue leo enim, ut pretium nibh placerat sed.</p>
		<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus vulputate orci nec tellus pulvinar rhoncus. Aenean lacus sapien, hendrerit vitae nisi sit amet, egestas tincidunt nulla. Donec eleifend aliquam massa, sed vehicula elit scelerisque sit amet. Etiam semper nunc fringilla nulla pulvinar, sed iaculis neque auctor. Nam porttitor tortor euismod nisl consectetur, in bibendum arcu posuere. Curabitur at facilisis est. Donec odio sapien, condimentum a erat ac, interdum fermentum ipsum. Nam congue leo enim, ut pretium nibh placerat sed.</p>
		<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus vulputate orci nec tellus pulvinar rhoncus. Aenean lacus sapien, hendrerit vitae nisi sit amet, egestas tincidunt nulla. Donec eleifend aliquam massa, sed vehicula elit scelerisque sit amet. Etiam semper nunc fringilla nulla pulvinar, sed iaculis neque auctor. Nam porttitor tortor euismod nisl consectetur, in bibendum arcu posuere. Curabitur at facilisis est. Donec odio sapien, condimentum a erat ac, interdum fermentum ipsum. Nam congue leo enim, ut pretium nibh placerat sed.</p>
	</div>
	<!-- END FILLER CONTENT -->











	<!-- BEGIN SMALL CAROUSEL -->
	<section class = "small-carousel_wrapper">
		<ul id = "small-carousel" class = "small-carousel_loading">
			<li class = "slide">
				<h2 class = "small-carousel_title">What's New</h2>
				<div class = "small-carousel_img carousel-transfer-image ar_16-9">
					<img src = "http://lorempixel.com/100/100/abstract" />
				</div>
				<h3 class = "small-carousel_subtitle">Lecrae Wins His First Grammy</h3>
				<a class = "small-carousel_read-more" href = "###">Music</a>
			</li>
			<li class = "slide">
				<h2 class = "small-carousel_title">Reviews</h2>
				<div class = "small-carousel_img carousel-transfer-image ar_16-9">
					<img src = "http://lorempixel.com/100/200/abstract" />
				</div>
				<h3 class = "small-carousel_subtitle">In Review: Love is All</h3>
				<a class = "small-carousel_read-more" href = "###">Music</a>
			</li>
			<li class = "slide">
				<h2 class = "small-carousel_title">Coming Soon</h2>
				<div class = "small-carousel_img carousel-transfer-image ar_16-9">
					<img src = "http://lorempixel.com/100/100/abstract" />
				</div>
				<h3 class = "small-carousel_subtitle">Mars Hill's Own | Citizens</h3>
				<a class = "small-carousel_read-more" href = "###">Music</a>
			</li>
			<li class = "slide">
				<h2 class = "small-carousel_title">Interview</h2>
				<div class = "small-carousel_img carousel-transfer-image ar_16-9">
					<img src = "http://lorempixel.com/200/100/abstract" />
				</div>
				<h3 class = "small-carousel_subtitle">Sitting Down with Mike Hicks</h3>
				<a class = "small-carousel_read-more" href = "###">Music</a>
			</li>
			<li class = "slide">
				<h2 class = "small-carousel_title">Reviews</h2>
				<div class = "small-carousel_img carousel-transfer-image ar_16-9">
					<img src = "http://lorempixel.com/200/200/abstract" />
				</div>
				<h3 class = "small-carousel_subtitle">In Review: Love is All</h3>
				<a class = "small-carousel_read-more" href = "###">Music</a>
			</li>
			<li class = "slide">
				<h2 class = "small-carousel_title">Reviews</h2>
				<div class = "small-carousel_img carousel-transfer-image ar_16-9">
					<img src = "http://lorempixel.com/200/100/abstract" />
				</div>
				<h3 class = "small-carousel_subtitle">In Review: Love is All</h3>
				<a class = "small-carousel_read-more" href = "###">Music</a>
			</li>
			<li class = "slide">
				<h2 class = "small-carousel_title">Reviews</h2>
				<div class = "small-carousel_img carousel-transfer-image ar_16-9">
					<img src = "http://lorempixel.com/102/100/abstract" />
				</div>
				<h3 class = "small-carousel_subtitle">In Review: Love is All</h3>
				<a class = "small-carousel_read-more" href = "###">Music</a>
			</li>
			<li class = "slide">
				<h2 class = "small-carousel_title">Reviews</h2>
				<div class = "small-carousel_img carousel-transfer-image ar_16-9">
					<img src = "http://lorempixel.com/100/120/abstract" />
				</div>
				<h3 class = "small-carousel_subtitle">In Review: Love is All</h3>
				<a class = "small-carousel_read-more" href = "###">Music</a>
			</li>
		</ul>
	</section>
	<!-- END SMALL CAROUSEL -->










	<!-- BEGIN FILLER CONTENT -->
	<div class = "page-width">
		<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus vulputate orci nec tellus pulvinar rhoncus. Aenean lacus sapien, hendrerit vitae nisi sit amet, egestas tincidunt nulla. Donec eleifend aliquam massa, sed vehicula elit scelerisque sit amet. Etiam semper nunc fringilla nulla pulvinar, sed iaculis neque auctor. Nam porttitor tortor euismod nisl consectetur, in bibendum arcu posuere. Curabitur at facilisis est. Donec odio sapien, condimentum a erat ac, interdum fermentum ipsum. Nam congue leo enim, ut pretium nibh placerat sed.</p>
		<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus vulputate orci nec tellus pulvinar rhoncus. Aenean lacus sapien, hendrerit vitae nisi sit amet, egestas tincidunt nulla. Donec eleifend aliquam massa, sed vehicula elit scelerisque sit amet. Etiam semper nunc fringilla nulla pulvinar, sed iaculis neque auctor. Nam porttitor tortor euismod nisl consectetur, in bibendum arcu posuere. Curabitur at facilisis est. Donec odio sapien, condimentum a erat ac, interdum fermentum ipsum. Nam congue leo enim, ut pretium nibh placerat sed.</p>
		<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus vulputate orci nec tellus pulvinar rhoncus. Aenean lacus sapien, hendrerit vitae nisi sit amet, egestas tincidunt nulla. Donec eleifend aliquam massa, sed vehicula elit scelerisque sit amet. Etiam semper nunc fringilla nulla pulvinar, sed iaculis neque auctor. Nam porttitor tortor euismod nisl consectetur, in bibendum arcu posuere. Curabitur at facilisis est. Donec odio sapien, condimentum a erat ac, interdum fermentum ipsum. Nam congue leo enim, ut pretium nibh placerat sed.</p>
	</div>
	<!-- END FILLER CONTENT -->


	<!--
	JAVASCRIPT IMPORTS
	-->
	<script src="js/vendor/jquery.js"></script> <!-- v1.11.0, but should work with any -->

	<script src="js/vendor-ck.js"></script> <!-- required -->
	<script src="js/sliders-ck.js"></script> <!-- required -->
</body>
</html>