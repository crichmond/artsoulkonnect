<?php 
/* Template Name: Discover */
get_header(); ?>
  
  <?php if(have_posts()): while (have_posts()) : the_post(); ?>

  <?php 
    $default_genre = get_term_by('slug', 'featured', 'genres');  
    $selected_genre = get_query_var('genre');
    
    if (!empty($selected_genre)) {
      $genre = $selected_genre;
    } else {
      $genre = $default_genre->term_id;
    }
    $genre = (string)$genre;

    ?>

  <section class="discover">

    <div class="genre-selector">
      <div class="global-container">
        <a class="submit-artist-btn" href="<?php bloginfo('url'); ?>/wp-admin/post-new.php?post_type=artist">Submit Artist</a>
        <form action="<?php the_permalink(); ?>">
          <label for="genre">Genre//</label>
          <select name="genre" id="genres" onchange="this.form.submit()">
            <?php 
            $terms = get_terms("genres");
              if (!empty($terms) && !is_wp_error($terms)){
                foreach ($terms as $term) {
                  echo "<option value='". $term->term_id ."' ";
                  if($genre == $term->term_id) { echo "selected"; }
                  echo ">$term->name</option>";
                }
              } ?>
          </select>
        </form>
        
      </div><!-- /.global-container -->
    </div><!-- /.genre-selector -->

    <div class="artists" id="load-more">
      <?php 
      $args = array(
        'post_type' => 'artist',
        'posts_per_page' => 30,
        'tax_query' => array(
          array(
            'taxonomy' => 'genres',
            'field' => 'id',
            'terms' => $genre,
          )
        )
      );
      
      $artists_query = new WP_Query($args);
      if ($artists_query->have_posts()): while($artists_query->have_posts()): $artists_query->the_post(); ?>

          <?php $cover = get_field('thumbnail_photo');
          $cover = $cover['sizes']['album-cover']; ?>

          <div class="artist">
            <a href="<?php the_field('featured_audio'); ?>" class="cover fap-single-track" rel="<?php echo $cover; ?>" title="<?php the_title(); ?>" style="
              background: url('<?php echo $cover; ?>') no-repeat center center;
              background-size: cover;
              ">
              <img src="<?php bloginfo('template_directory'); ?>/img/img-discover-spacer.png" alt="Play track from <?php the_title(); ?>">
              <span class="play-btn"></span>
            </a>
            <a href="<?php the_permalink(); ?>" class="profile">
              <?php the_title(); ?>
            </a>
          </div><!-- /.artist -->


        
        <?php endwhile;
        wp_reset_postdata(); 
      endif; ?>
      
    </div><!-- /.artists -->

  
    
  </section><!-- /.discover -->

  <div id="fap"></div>

  

  <?php endwhile; endif; ?>

<?php get_footer(); ?> 