<?php 
/* Template Name: Store */
get_header(); ?>

  <section class="main store">
    <div class="global-container">

    <?php if(have_posts()): while (have_posts()) : the_post(); ?>
      
    <?php if(get_field('products')): ?>
      
      <div class="products">
      <?php $counter = 1;
      while(has_sub_field('products')): ?>
        
        <div class="product">
          <?php $img = get_sub_field('image'); ?>
          <div class="product-image" style="
            background: url('<?php echo $img['sizes']['product-image']; ?>') no-repeat center center;
            background-size: cover;

            ">
            
            <img src="<?php bloginfo('template_directory'); ?>/img/spacer.png" alt="<?php the_sub_field('title'); ?> Image">
          </div>  
          <h3 class="product-title"><?php the_sub_field('title'); ?></h3>
          <div class="product-description"><?php the_sub_field('description'); ?></div>
          <a class="product-link" href="<?php the_sub_field('link'); ?>">Buy</a>
        </div>
        
      <?php 
      if($counter == 2) { 
        echo "<hr class='hr-2'>"; 
        $counter = 1; 
      } else {
        $counter ++;
      }
      endwhile; ?>
      </div><!-- /.products -->

    <?php endif; ?>
      
      
    <?php endwhile; endif; ?>

    </div><!-- /.global-container -->
  </section>
<?php get_footer(); ?> 