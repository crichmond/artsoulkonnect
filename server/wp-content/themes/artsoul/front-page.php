<?php get_header(); ?>
  <?php if(have_posts()): while (have_posts()) : the_post(); ?>

  <!-- BEGIN MAIN/LARGE SLIDER -->
  <?php if(get_field('main_slider')): ?>
  <section class = "overflow-slider_wrapper">
    <ul class = "overflow-slider is-loading">
      <?php while(has_sub_field('main_slider')): ?>
      <li class = "slide slider-transfer-image">
        <?php $img = get_sub_field('image'); ?>
        <img class = "overflow-slider_img" src = "<?php echo $img['sizes']['slider-image']; ?>" />
        <div class = "overflow-slider_content">
          <hgroup class = "overflow-slider_titles">
            <h2 class = "overflow-slider_title"><?php the_sub_field('title'); ?></h2>

            <?php if(get_sub_field('subtitle')): ?>  
            <h3 class = "overflow-slider_subtitle"><?php the_sub_field('subtitle'); ?></h3>
            <?php endif; ?>
          </hgroup>
          
          <?php if(get_sub_field('description')): ?>
          <div class = "overflow-slider_body">
            <p><?php the_sub_field('description'); ?></p>
          </div>
          <?php endif; ?>

          <a class = "overflow-slider_read-more" href = "<?php echo addhttp(get_sub_field('link')); ?>"><?php the_sub_field('link_name'); ?></a>
        </div>
      </li>
      <?php endwhile; ?>
    </ul>
  </section>
  <?php endif; ?>
  <!-- END MAIN/LARGE SLIDER -->
  

  <section class="content-row featured">
    <div class="global-container">

      <div class="title-row">
        <h2>Featured
          <a href="<?php bloginfo('url'); ?>/news/" class="see-all">See All</a>
        </h2>
      </div><!-- /.title-row -->
      
      <div class="row-12">
        <?php 
        $args = array(
          'post_type' => 'post',
          'category_name' => 'featured',
          'posts_per_page' => 2,
        );
        
        $featured_query = new WP_Query($args);
        if ($featured_query->have_posts()): while($featured_query->have_posts()): $featured_query->the_post(); ?>
          
          <div class="post">
            <div class="post-image">
              <a href="<?php the_permalink(); ?>">
              <?php if(has_post_thumbnail()): 
                the_post_thumbnail('post-medium');
              else: 
                $img = get_field('default_post_image','option');
                $size = "post-medium"; ?>
                <img src="<?php echo $img['sizes'][$size]; ?>" 
                width="<?php echo $image['sizes'][$size.'-width']; ?>" 
                height="<?php echo $image['sizes'][$size.'-height']; ?>" 
                alt="<?php echo get_the_title(); ?>">
              <?php endif; ?>
              </a>
            </div>
            <div class="post-title">
              <?php the_title('<a href="'. get_permalink() .'">', '</a>'); ?>
            </div>
            
            <?php the_tags('<div class="post-tags">', ', ', '</div>'); ?>
            
          </div><!-- /.post -->

          <?php endwhile;
          wp_reset_postdata(); 
        endif; ?>
      </div><!-- .row-12 -->
    </div><!-- /.global-container -->
  </section><!-- /.content-row -->
  
  <?php 
  if(get_field('category_sliders')):
    while(has_sub_field('category_sliders')):
      get_template_part('partials/asset', 'carousel-slider');
    endwhile;
  endif;
  ?>


  <?php if(get_field('home_ads','option')): ?>
   <section class="content-row sponsors">
    <div class="global-container">

      <div class="title-row">
        <h2>Sponsors</h2>
      </div><!-- /.title-row -->
      
      <div class="row-12">
      <?php 
      while(has_sub_field('home_ads','option')):
        if(get_sub_field('image')): ?>
        <div class="ad">  
          <?php $ad_link = get_sub_field('link') ? addhttp(get_sub_field('link')) : null; 
          echo $ad_link ? "<a href='$ad_link'>" : ""; ?>
            <?php $img = get_sub_field('image');
            $size = "sponsor-medium"; ?>
            <img src="<?php echo $img['sizes'][$size]; ?>" 
            width="<?php echo $image['sizes'][$size.'-width']; ?>" 
            height="<?php echo $image['sizes'][$size.'-height']; ?>" 
            alt="Sponsored ad">
          <?php echo $ad_link ? "</a>" : ""; ?>
        </div><!-- /.ad -->
        <?php endif; 
      endwhile; ?>
      </div><!-- /.row-12 -->

      </div><!-- /.global-container -->
    </section><!-- /.content-row -->
  <?php endif; ?> 

  

  <?php endwhile; endif; ?>
  
<?php get_footer(); ?> 