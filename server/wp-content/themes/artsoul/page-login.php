<?php 
/* Template Name: Login */
get_header(); ?>
  
  <section class="main single">
    <div class="global-container">

    <?php if(have_posts()): while (have_posts()) : the_post(); ?>
    <article class="post">
      <div class="post-excerpt">
        <?php the_content(); ?>
      </div>

      <div class="login-options">
        <div class="left">
        <?php theme_my_login(array('default_action'=> 'login')); ?>
        </div>

        <div class="center">
          <span>Or</span>
        </div>

        <div class="right">
          Register
          <div class="register">
            <p>Don't have an account yet? Create one now, FREE!</p>

            <a href="<?php bloginfo('url'); ?>/register/" class="register-btn">Register Now</a>
          </div>
        </div>
      </div>
    </article>
    <?php endwhile; endif; ?>

    </div><!-- /.global-container -->
  </section>
<?php get_footer(); ?> 