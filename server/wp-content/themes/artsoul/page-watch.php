<?php 
/* Template Name: Watch */
get_header(); ?>
  <?php if(have_posts()): while (have_posts()) : the_post(); ?>

  <section class="main watch content-row">
    <div class="global-container">
        
      <?php 
      $args = array(
        'post_type' => 'video',
        'posts_per_page' => 1,
        'video-categories' => 'featured'
      );
      
      $featured_video_query = new WP_Query($args);
      if ($featured_video_query->have_posts()): while($featured_video_query->have_posts()): $featured_video_query->the_post(); ?>
        
        <?php if(get_field('video_url')): ?>
        <div class="featured-video">
          <?php echo wp_oembed_get(get_field('video_url')); ?>
        </div>
        <?php endif; ?>

        <?php endwhile;
        wp_reset_postdata(); 
      endif; ?>
    </div><!-- /.global-container -->
  </section><!-- /.main -->

  <?php 
  if(get_field('category_sliders')):
    while(has_sub_field('category_sliders')):
      get_template_part('partials/asset', 'carousel-slider-video');
    endwhile;
  endif;
  ?>

  <?php get_template_part('partials/asset', 'ads-footer'); ?>
  
  <?php endwhile; endif; ?>
  
<?php get_footer(); ?> 