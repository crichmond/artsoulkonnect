<?php 
/* Template Name: Listen */
get_header(); ?>
  
  <?php if(have_posts()): while (have_posts()) : the_post();  
    $bg = get_field('background_image');
    $bg = $bg['sizes']['post-large'];
  ?>

  <section class="main listen" style="
    background-image: url('<?php echo $bg; ?>');
    background-position: 50% 50%;
    background-size: cover;
  ">
    <div class="global-container">
      <div class="header">
        <?php 
          $img = get_field('header_image');
          $img = $img['sizes']['post-large'];
        ?>
        <img src="<?php echo $img; ?>">
      </div>  

      <div class="radio">
        <?php the_field('radio_embed_code'); ?>
      </div>
    </div><!-- /.global-container -->

    <?php if(get_field('listen_ads','option')): ?>
    <section class="content-row sponsors">
      <div class="global-container">

        <div class="title-row">
          <h2>Sponsors</h2>
        </div><!-- /.title-row -->
        
        <div class="row-12">
        <?php while(has_sub_field('listen_ads','option')):
        if(get_sub_field('image')): ?>
        <div class="ad">  
          <?php $ad_link = get_sub_field('link') ? addhttp(get_sub_field('link')) : null; 
          echo $ad_link ? "<a href='$ad_link'>" : ""; ?>
            <?php $img = get_sub_field('image');
            $size = "sponsor-medium"; ?>
            <img src="<?php echo $img['sizes'][$size]; ?>" 
            width="<?php echo $image['sizes'][$size.'-width']; ?>" 
            height="<?php echo $image['sizes'][$size.'-height']; ?>" 
            alt="Sponsored ad">
          <?php echo $ad_link ? "</a>" : ""; ?>
        </div><!-- /.ad -->
        <?php endif; ?>
        <?php endwhile; ?>
        
        </div><!-- /.row-12 -->
      </div><!-- /.global-container -->
    </section><!-- /.content-row -->
    <?php endif; ?>

    
  </section><!-- /.single -->
  <?php endwhile; endif; ?>

<?php get_footer(); ?> 