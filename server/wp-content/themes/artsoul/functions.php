<?php
require_once("functions/class-tgm-plugin-activation.php");
require_once("functions/rocket-required-plugins.php");
require_once("functions/rocket-acf-setup.php");
require_once("functions/rocket-disable-comments.php");
require_once("functions/rocket-show-template.php");
require_once("functions/rocket-dashboard-widgets.php");
require_once("functions/rocket-cpt.php");


// Enqueue jQuery
// --------------------------------------------------
  wp_enqueue_script('jquery');


add_filter('author_rewrite_rules', 'no_author_base_rewrite_rules');
function no_author_base_rewrite_rules($author_rewrite) { 
    global $wpdb;
    $author_rewrite = array();
    $authors = $wpdb->get_results("SELECT user_nicename AS nicename from $wpdb->users");    
    foreach($authors as $author) {
        $author_rewrite["({$author->nicename})/page/?([0-9]+)/?$"] = 'index.php?author_name=$matches[1]&paged=$matches[2]';
        $author_rewrite["({$author->nicename})/?$"] = 'index.php?author_name=$matches[1]';
    }   
    return $author_rewrite;
}


add_filter('author_link', 'no_author_base', 1000, 2);
function no_author_base($link, $author_id) {
    $link_base = trailingslashit(get_option('home'));
    $link = preg_replace("|^{$link_base}author/|", '', $link);
    return $link_base . $link;
}


// Exclude pages from search
// --------------------------------------------------
  
  function SearchFilter($query) {
    if ($query->is_search) {
      $query->set('post_type', 'post');
    }
    return $query;
  }

  add_filter('pre_get_posts','SearchFilter');


// Add query vars for filtering by specific category ids
// -----------------------------------------------------------------
  add_filter('query_vars', 'add_vars');
  function add_vars($new_query_vars) {
    $new_query_vars[] = 'genre';
    return $new_query_vars;
  }

// Add Infinite Scroll
// ------------------------------------------------------------------

  add_theme_support( 'infinite-scroll', array(
    'container' => 'load-more',
  ));


// Remove width & height from images in posts, and wrapping length.
// -------------------------------------------------------------------

  add_filter( 'post_thumbnail_html', 'remove_width_attribute', 10 );
  add_filter( 'image_send_to_editor', 'remove_width_attribute', 10 );

  function remove_width_attribute( $html ) {
     $html = preg_replace( '/(width|height)="\d*"\s/', "", $html );
     return $html;
  }

  update_option('image_default_link_type', 'none');




// Make Post Messages use post type name.
// --------------------------------------------------
  function set_messages($messages) {
    global $post, $post_ID;
    $post_type = get_post_type( $post_ID );

    $obj = get_post_type_object($post_type);
    $singular = $obj->labels->singular_name;

    $messages[$post_type] = array(
    0 => '', // Unused. Messages start at index 1.
    1 => sprintf( __($singular.' updated. <a href="%s">View '.strtolower($singular).'</a>'), esc_url( get_permalink($post_ID) ) ),
    2 => __('Custom field updated.'),
    3 => __('Custom field deleted.'),
    4 => __($singular.' updated.'),
    5 => isset($_GET['revision']) ? sprintf( __($singular.' restored to revision from %s'), wp_post_revision_title( (int) $_GET['revision'], false ) ) : false,
    6 => sprintf( __($singular.' published. <a href="%s">View '.strtolower($singular).'</a>'), esc_url( get_permalink($post_ID) ) ),
    7 => __('Page saved.'),
    8 => sprintf( __($singular.' submitted. <a target="_blank" href="%s">Preview '.strtolower($singular).'</a>'), esc_url( add_query_arg( 'preview', 'true', get_permalink($post_ID) ) ) ),
    9 => sprintf( __($singular.' scheduled for: <strong>%1$s</strong>. <a target="_blank" href="%2$s">Preview '.strtolower($singular).'</a>'), date_i18n( __( 'M j, Y @ G:i' ), strtotime( $post->post_date ) ), esc_url( get_permalink($post_ID) ) ),
    10 => sprintf( __($singular.' draft updated. <a target="_blank" href="%s">Preview '.strtolower($singular).'</a>'), esc_url( add_query_arg( 'preview', 'true', get_permalink($post_ID) ) ) ),
    );
    return $messages;
  }

    add_filter('post_updated_messages', 'set_messages' );


/**
 * Redirect non-admins to the homepage after logging into the site.
 *
 * @since   1.0
 */
function soi_login_redirect( $redirect_to, $request, $user  ) {
  return ( is_array( $user->roles ) && in_array( 'administrator', $user->roles ) ) ? admin_url() : home_url('artist-profiles');
} // end soi_login_redirect
add_filter( 'login_redirect', 'soi_login_redirect', 10, 3 );



// Register Menus 
// --------------------------------------------------
	
	add_theme_support('nav_menus');
	function register_theme_menus() {
	  register_nav_menus(
			array( 
				'primary' => 'Primary',
        'mobile' => 'Mobile',
        'categories' => 'Categories',
        'tags' => 'Tags'
		  )
	  );
	}
	add_action( 'init', 'register_theme_menus' );


// Add Post Format Support 
// --------------------------------------------------

  add_theme_support( 'post-formats', array('video') );


// Add Thumbnail Support 
// --------------------------------------------------
	
	add_theme_support( 'post-thumbnails' );

	if ( function_exists( 'add_image_size' ) ) { 
		add_image_size( 'tiny', 52, 52, true );
    add_image_size( 'post-large', 2000, 1100, true );
    add_image_size( 'post-medium', 620, 292, true );
    add_image_size( 'post-small', 352, 184, true );
    add_image_size( 'sponsor-medium', 620, 310, true );
    add_image_size( 'sponsor-sidebar', 300, 252, true );
    add_image_size( 'album-cover', 250, 250, true );
    add_image_size( 'profile-image', 285, 285, true );
    add_image_size( 'product-image', 450, 450, true );
    add_image_size( 'slider-image', 1100, 600, true );
	}


// Modify excerpt length & ellipsis 
// --------------------------------------------------
  
  function new_excerpt_more( $more ) {
    return '...';
  }
  add_filter('excerpt_more', 'new_excerpt_more');


  function get_excerpt_by_id($post_id){
    $the_post = get_post($post_id); //Gets post ID
    $the_excerpt = $the_post->post_content; //Gets post_content to be used as a basis for the excerpt
    $excerpt_length = 35; //Sets excerpt length by word count
    $the_excerpt = strip_tags(strip_shortcodes($the_excerpt)); //Strips tags and images
    $words = explode(' ', $the_excerpt, $excerpt_length + 1);
    if(count($words) > $excerpt_length) :
      array_pop($words);
      array_push($words, '…');
      $the_excerpt = implode(' ', $words);
    endif;
      $the_excerpt = '<p>' . $the_excerpt . '</p>';
    return $the_excerpt;
  }


// Add Template Shortcodes for Pages/Posts 
// --------------------------------------------------
	
	function fetch_template_url() {
		return get_bloginfo('template_directory');
	}

	function fetch_site_url() {
		return get_bloginfo('url');
	}

	add_shortcode('template-url', 'fetch_template_url');
	add_shortcode('site-url', 'fetch_site_url');


// Add "http://" to links that are missing them
// --------------------------------------------------

  function addhttp($url) {
    if($url != "#") {
      if(filter_var($url, FILTER_VALIDATE_EMAIL)) {
        // If valid address, add "mailto:"
        $url = 'mailto:'.$url;
      } else {
        // If url without "http://", add it
        if (!preg_match("~^(?:f|ht)tps?://~i", $url)) {
            $url = "http://" . $url;
        }
      }
    }
    return $url;
  }

?>