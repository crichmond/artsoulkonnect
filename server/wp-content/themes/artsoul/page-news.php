<?php 
/* Template Name: News */
get_header(); ?>

  <section class="main news">
    <div class="global-container">

      <div class="row-12">
        <div class="posts-column">
          <div class="row-9">
        
            <?php 
            $paged = (get_query_var('paged')) ? get_query_var('paged') : 1;

            $args = array(
              'post_type' => 'post',
              'posts_per_page' => 12,
              'paged' => $paged,
              'tax_query' => array(
                array(
                  'taxonomy' => 'post_format',
                  'field' => 'slug',
                  'terms' => array( 'post-format-video' ),
                  'operator' => 'NOT IN',
                )
              )
            );
            
            $news_query = new WP_Query($args);
            if ($news_query->have_posts()): while($news_query->have_posts()): $news_query->the_post(); 
              
              if(has_post_thumbnail()): 
                $img = wp_get_attachment_image_src(get_post_thumbnail_id(), 'post-large');
                $bg = $img[0];
              else:
                $img = get_field('default_post_image','option');
                $bg = $img['sizes']['post-large'];
              endif; ?>

              <a class="post" href="<?php the_permalink(); ?>" style="
                background-image: url('<?php echo $bg; ?>');
                background-position: 50% 50%;
                background-size: cover;">
                <div class="background" >
                  <div class="content">
                
                    <div class="post-title">
                      <?php the_title(); ?>
                    </div>

                    <div class="post-author">
                      <?php the_author(); ?>
                    </div>
                    
                    <?php
                      $posttags = get_the_tags();
                      $tags_list = array();

                      if ($posttags) {
                        foreach($posttags as $tag) {
                          $tags_list[] = "<span>" . $tag->name . "</span>"; 
                        }
                        echo '<div class="post-tags">', join(", ", $tags_list), '</div>';
                      }
                    ?>
                  </div>
                </div><!-- /.background -->
              </a><!-- /.post -->

              <?php endwhile; ?>
              
              <div class="pagination">
                <div class="posts-prev"><?php previous_posts_link('&nbsp;'); ?></div>
                <?php
                $big = 999999999; // need an unlikely integer
                echo paginate_links( 
                  array(
                    'base' => str_replace( $big, '%#%', esc_url( get_pagenum_link( $big ) ) ),
                    'format' => '/page/%#%',
                    'current' => max( 1, get_query_var('paged') ),
                    'total' => $news_query->max_num_pages,
                    'prev_next' => false
                  )
                );
                ?>
                
                <div class="posts-next"><?php next_posts_link('&nbsp;', $news_query->max_num_pages); ?></div>
              </div><!-- /.pagination -->

              <? wp_reset_postdata(); 
            endif; ?>
          </div><!-- /.row-9 -->
        </div><!-- /.posts-column -->

        <div class="posts-sidebar">
          <?php get_template_part('partials/asset', 'sidebar'); ?>
        </div>
      </div><!-- /.row-12 -->

    </div><!-- /.global-container -->
  </section><!-- /.single -->

<?php get_footer(); ?> 