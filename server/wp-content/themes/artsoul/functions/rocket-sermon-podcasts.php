<?php 
// Add query vars for filtering by specific category ids
// -----------------------------------------------------------------
add_filter('query_vars', 'add_vars');
function add_vars($new_query_vars) {
  $new_query_vars[] = 'speaker_id';
  $new_query_vars[] = 'book_id';
  $new_query_vars[] = 'series_id';
  return $new_query_vars;
}

// Add podcast to taxonomy pages
// -------------------------------------------------------------  
  add_filter( 'pre_get_posts', 'namespace_add_custom_types' );    
  function namespace_add_custom_types( $query ) {
      if(is_tax( 'type' ) ){
        $query->set( 'post_type', array(
         'staff', 'nav_menu_item'
        ));
      }

      if (is_tax('series') || is_tax('book') || is_tax('speaker') || (is_feed() && is_tax('channel'))) {
        $query->set( 'post_type', array('podcast', 'nav_menu_item'));
        $query->set( 'posts_per_page', 9 );
      }
  }



if ( ! function_exists( 'podcast_archive_filter' ) ) {

  function podcast_archive_filter( $query ) {

    if ( is_post_type_archive( 'podcast' ) ) {
      $query->set( 'posts_per_page', 9 );
      return;
    }
  } 
}

add_action( 'pre_get_posts', 'podcast_archive_filter' ); ?>