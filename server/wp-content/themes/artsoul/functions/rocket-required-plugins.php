<?php
// Register & Install Plugins
// ---------------------------------------------------------------------------------

add_action( 'tgmpa_register', 'register_required_plugins' );

// This function is called from the above hook
function register_required_plugins()
{
  // The plugins array allows us to define multiple plugins we want to include.
  // The commented out example shows how we can include and activation a bundled
  // plugin zip file in our theme.
  $plugins = array(
  /*    array(
      'name'               => 'Advanced Custom Fields',
      'slug'               => 'advanced-custom-fields',
      'source'             => get_stylesheet_directory() . '/theme/plugins/advanced-custom-fields.zip',
      'required'           => true,
      'version'            => '',
      'force_activation'   => true, // Force activation because we need advanced custom fields,
      'force_deactivation' => false,
      'external_url'       => ''
    ),
  */
    // This below definition will include the ACF plugin from the Wordpress.org plugins repository
    // and if permissions permit, will allow for it to be automatically downloaded and installed.
    // If permissions don't allow, the user will be prompted into downloading the plugin themselves
    // and installing it manually.
    array(
      'name'    => 'Advanced Custom Fields',
      'slug'    => 'advanced-custom-fields',
      'source'  => get_stylesheet_directory() . '/plugins/advanced-custom-fields.zip',
      'required'  => true,
      'force_activation'   => true,
    ),
    array(
      'name'    => 'Advanced Custom Fields: Gallery Field',
      'slug'    => 'acf-gallery',
      'source'  => get_stylesheet_directory() . '/plugins/acf-gallery.zip',
      'required'  => true,
      'force_activation'   => true,
    ),
    array(
      'name'    => 'Advanced Custom Fields: Options Page',
      'slug'    => 'acf-options-page',
      'source'  => get_stylesheet_directory() . '/plugins/acf-options-page.zip',
      'required'  => true,
      'force_activation'   => true,
    ),
    array(
      'name'    => 'Advanced Custom Fields: Repeater Field',
      'slug'    => 'acf-repeater',
      'source'  => get_stylesheet_directory() . '/plugins/acf-repeater.zip',
      'required'  => true,
      'force_activation'   => true,
    ),
    array(
      'name'    => 'Advanced Custom Fields: Flexible Content Field',
      'slug'    => 'acf-flexible-content',
      'source'  => get_stylesheet_directory() . '/plugins/acf-flexible-content.zip',
      'required'  => true,
      'force_activation'   => true,
    ),
    array(
      'name'    => 'Advanced Custom Fields Repeater Collapser',
      'slug'    => 'advanced-custom-field-repeater-collapser',
      'source'  => get_stylesheet_directory() . '/plugins/advanced-custom-field-repeater-collapser.zip',
      'required'  => true,
      'force_activation'   => true,
    ),
    array(
      'name'    => 'Regenerate Thumbnails',
      'slug'    => 'regenerate-thumbnails',
      'source'  => get_stylesheet_directory() . '/plugins/regenerate-thumbnails.zip',
      'required'  => true,
      'force_activation'   => true,
    ),
    array(
      'name'    => 'Gravity Forms',
      'slug'    => 'gravityforms',
      'source'  => get_stylesheet_directory() . '/plugins/gravityforms.zip',
      'required'  => true,
      'force_activation'   => true,
    ),
    array(
      'name'    => 'Wordpress Importer',
      'slug'    => 'wordpress-importer',
      'source'  => get_stylesheet_directory() . '/plugins/wordpress-importer.zip',
      'required'  => true,
      'force_activation'   => true,
    ),
    array(
      'name'    => 'Google Analyticator',
      'slug'    => 'google-analyticator',
      'source'  => get_stylesheet_directory() . '/plugins/google-analyticator.6.4.7.3.zip',
      'required'  => true,
      'force_activation'   => true,
    ),
    // array(
    //   'name'    => 'Blubrry PowerPress',
    //   'slug'    => 'powerpress',
    //   'source'  => get_stylesheet_directory() . '/plugins/powerpress.zip',
    //   'required'  => true,
    //   'force_activation'   => true,
    // ),
    array(
      'name'    => 'Advanced Post Types Order',
      'slug'    => 'advanced-post-types-order',
      'source'  => get_stylesheet_directory() . '/plugins/advanced-post-types-order.zip',
      'required'  => true,
      'force_activation'   => true,
    ),
  );

  $theme_text_domain = 'wp';

  $config = array(
    'domain'          => $theme_text_domain,
    'default_path'    => '',
    'parent_menu_slug'  => 'themes.php',
    'parent_url_slug'   => 'themes.php',
    'menu'            => 'install-required-plugins',
    'has_notices'       => true,
    'is_automatic'      => false,
    'message'       => '',
    'strings'         => array(
      'page_title'                            => __( 'Install Required Plugins', $theme_text_domain ),
      'menu_title'                            => __( 'Install Plugins', $theme_text_domain ),
      'installing'                            => __( 'Installing Plugin: %s', $theme_text_domain ), // %1$s = plugin name
      'oops'                                  => __( 'Something went wrong with the plugin API.', $theme_text_domain ),
      'notice_can_install_required'           => _n_noop( 'This theme requires the following plugin: %1$s.', 'This theme requires the following plugins: %1$s.' ), // %1$s = plugin name(s)
      'notice_can_install_recommended'      => _n_noop( 'This theme recommends the following plugin: %1$s.', 'This theme recommends the following plugins: %1$s.' ), // %1$s = plugin name(s)
      'notice_cannot_install'           => _n_noop( 'Sorry, but you do not have the correct permissions to install the %s plugin. Contact the administrator of this site for help on getting the plugin installed.', 'Sorry, but you do not have the correct permissions to install the %s plugins. Contact the administrator of this site for help on getting the plugins installed.' ), // %1$s = plugin name(s)
      'notice_can_activate_required'          => _n_noop( 'The following required plugin is currently inactive: %1$s.', 'The following required plugins are currently inactive: %1$s.' ), // %1$s = plugin name(s)
      'notice_can_activate_recommended'     => _n_noop( 'The following recommended plugin is currently inactive: %1$s.', 'The following recommended plugins are currently inactive: %1$s.' ), // %1$s = plugin name(s)
      'notice_cannot_activate'          => _n_noop( 'Sorry, but you do not have the correct permissions to activate the %s plugin. Contact the administrator of this site for help on getting the plugin activated.', 'Sorry, but you do not have the correct permissions to activate the %s plugins. Contact the administrator of this site for help on getting the plugins activated.' ), // %1$s = plugin name(s)
      'notice_ask_to_update'            => _n_noop( 'The following plugin needs to be updated to its latest version to ensure maximum compatibility with this theme: %1$s.', 'The following plugins need to be updated to their latest version to ensure maximum compatibility with this theme: %1$s.' ), // %1$s = plugin name(s)
      'notice_cannot_update'            => _n_noop( 'Sorry, but you do not have the correct permissions to update the %s plugin. Contact the administrator of this site for help on getting the plugin updated.', 'Sorry, but you do not have the correct permissions to update the %s plugins. Contact the administrator of this site for help on getting the plugins updated.' ), // %1$s = plugin name(s)
      'install_link'                  => _n_noop( 'Begin installing plugin', 'Begin installing plugins' ),
      'activate_link'                 => _n_noop( 'Activate installed plugin', 'Activate installed plugins' ),
      'return'                                => __( 'Return to Required Plugins Installer', $theme_text_domain ),
      'plugin_activated'                      => __( 'Plugin activated successfully.', $theme_text_domain ),
      'complete'                  => __( 'All plugins installed and activated successfully. %s', $theme_text_domain ), // %1$s = dashboard link
      'nag_type'                  => 'updated' // Determines admin notice type - can only be 'updated' or 'error'
    )
  );

  tgmpa( $plugins, $config );
}
?>