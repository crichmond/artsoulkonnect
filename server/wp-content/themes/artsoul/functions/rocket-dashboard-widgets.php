<?php 
// Custom WordPress Dashboard Footer, and Support Widget
// ---------------------------------------------------------------------------------

  // Custom WordPress Footer
  function remove_footer_admin () {
    //echo 'Theme by <a href="http://rocketrepublic.com">Rocket Republic</a>.';
  }
  add_filter('admin_footer_text', 'remove_footer_admin');


  // Add a widget in WordPress Dashboard
  function rocket_dashboard_widget_function() {
    // Entering the text between the quotes
    echo "<iframe src='http://rocketrepublic.info/rocket-info/main.html' width='100%'></iframe>";
  }
  function rocket_add_dashboard_widgets() {
    //wp_add_dashboard_widget('wp_dashboard_widget', 'Support Info', 'rocket_dashboard_widget_function');
    add_meta_box('rocket_dashboard_widget', 'Rocket Republic', 'rocket_dashboard_widget_function', 'dashboard', 'side', 'high');
  }
  add_action('wp_dashboard_setup', 'rocket_add_dashboard_widgets' );
?>