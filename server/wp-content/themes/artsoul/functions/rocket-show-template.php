<?php 
// Output the template name at the top of the page
// --------------------------------------------------

  add_action('wp_head', 'show_template'); 
 
  function show_template() {  
    global $template;
    $templateName = substr( $template, strrpos( $template, '/' )+1 );

    echo "<div style='
      position: fixed;
      width: 100%;
      background: rgba(0,0,0,0.5); 
      padding: 10px 0; 
      font-family: Arial, Helvetica, sans-serif; 
      font-size:14px; 
      font-weight:bold; 
      font-style: italic;
      text-align: center;
      color: #FFF;
      z-index: 100;
      bottom: 0;
      '>Template: ";
    print_r($templateName);
    echo "</div>";
  }

?>