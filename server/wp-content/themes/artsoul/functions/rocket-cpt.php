<?php 

// Add Custom Post Types
// --------------------------------------------------

	add_action( 'init', 'custom_post_types' );

	function custom_post_types() {
		
		
		// Artist --------------------------------------------------

			$labels = array(
				'name' => 'Artist',
				'singular_name' => 'Artist',
				'add_new' => 'Add New Artist',
				'add_new_item' => 'Add New Artist',
				'edit_item' => 'Edit Artist',
				'new_item' => 'New Artist',
				'all_items' => 'All Artists',
				'view_item' => 'View this Artist',
				'search_items' => 'Search Artists',
				'not_found' =>  'No Artists',
				'not_found_in_trash' => 'No Artists in Trash',
				'parent_item_colon' => '',
				'menu_name' => 'Artists',
			);

			$args = array(
				'labels' => $labels,
				'public' => true,
				'exclude_from_search' => true,
				'publicly_queryable' => true,
				'has_archive' => true,
				'show_ui' => true,
				'query_var' => true,
				'capability_type' => array('artist', 'artists'),
				'capabilities' => array(
					'edit_post'		 => 'edit_artist',
					'read_post'		 => 'read_artist',
					'delete_post'		 => 'delete_artist',
					'edit_posts'		 => 'edit_artists',
					'edit_others_posts'	 => 'edit_others_artists',
					'publish_posts'		 => 'publish_artists',
					'read_private_posts'	 => 'read_private_artists',
					'delete_posts'           => 'delete_artists',
					'delete_private_posts'   => 'delete_private_artists',
					'delete_published_posts' => 'delete_published_artists',
					'delete_others_posts'    => 'delete_others_artists',
					'edit_private_posts'     => 'edit_private_artists',
					'edit_published_posts'   => 'edit_published_artists',
				),
				'map_meta_cap' => true,
				'rewrite' => array('slug' => 'artists'),
				'hierarchical' => false,
				'menu_position' => null,
				'menu_icon' => 'dashicons-groups',
				//'taxonomies' => array('post_tag'),
				'supports' => array('title', 'author'),
			);

			register_post_type('artist',$args);
			flush_rewrite_rules();

		// Videos --------------------------------------------------

			$labels = array(
				'name' => 'Videos',
				'singular_name' => 'Video',
				'add_new' => 'Add New Video',
				'add_new_item' => 'Add New Video',
				'edit_item' => 'Edit Video',
				'new_item' => 'New Video',
				'all_items' => 'All Videos',
				'view_item' => 'View this Video',
				'search_items' => 'Search Videos',
				'not_found' =>  'No Videos',
				'not_found_in_trash' => 'No Videos in Trash',
				'parent_item_colon' => '',
				'menu_name' => 'Videos',
			);

			$args = array(
				'labels' => $labels,
				'public' => true,
				'exclude_from_search' => false,
				'publicly_queryable' => true,
				'has_archive' => true,
				'show_ui' => true,
				'query_var' => true,
				'capability_type' => 'post',
				'rewrite' => array('slug' => 'video'),
				'hierarchical' => false,
				'menu_position' => null,
				'menu_icon' => 'dashicons-video-alt3',
				'supports' => array('editor', 'title', 'thumbnail'),
			);

			register_post_type('video',$args);
			flush_rewrite_rules();
	} 





	function manage_artist_capabilities() {
    // gets the role to add capabilities to
	    $admin = get_role( 'administrator' );
	    $editor = get_role( 'editor' );
	    $contributor = get_role( 'contributor' );
		// replicate all the remapped capabilites from the custom post type artist
	    $caps = array(
	    	'edit_artist',
				'read_artist',
				'delete_artist',
				'edit_artists',
				'edit_others_artists',
				'publish_artists',
				'read_private_artists',
	      'delete_artists',
	      'delete_private_artists',
	      'delete_published_artists',
	      'delete_others_artists',
	      'edit_private_artists',
	      'edit_published_artists',
	    );
	    // give all the capabilities to the administrator
	    foreach ($caps as $cap) {
		    $admin->add_cap( $cap );
	    }
	    // limited the capabilities to the editor or a custom role 
	    $editor->add_cap( 'edit_artist' );
	    $editor->add_cap( 'edit_artists' );
	    $editor->add_cap( 'read_artists' );

	    // limited the capabilities of the Contributor role 
	    // Artists
	    $contributor->add_cap( 'edit_artist' );
	    $contributor->add_cap( 'edit_artists' );
	    $contributor->add_cap( 'read_artist' );
	    $contributor->add_cap( 'edit_published_artists' );
	    // Posts
	    $contributor->remove_cap( 'read_post' );
	    $contributor->remove_cap( 'edit_post' );
	    $contributor->remove_cap( 'edit_posts' );
	    $contributor->remove_cap( 'edit_others_posts' );
	    $contributor->remove_cap( 'delete_posts' );
	}
	add_action( 'admin_init', 'manage_artist_capabilities');





	if ( current_user_can('contributor') && !current_user_can('upload_files') )
		add_action('admin_init', 'allow_contributor_uploads');

	function allow_contributor_uploads() {
		$contributor = get_role('contributor');
		$contributor->add_cap('upload_files');
	}


	add_action('pre_get_posts','users_own_attachments');
	function users_own_attachments( $wp_query_obj ) {

	    global $current_user, $pagenow;

	    if( !is_a( $current_user, 'WP_User') )
	        return;

	    if( 'upload.php' != $pagenow )
	        return;

	    if( !current_user_can('delete_pages') )
	        $wp_query_obj->set('author', $current_user->id );

	    return;
	}


/**
 * Define default terms for custom taxonomies in WordPress 3.0.1
 *
 * @author    Michael Fields     http://wordpress.mfields.org/
 * @props     John P. Bloch      http://www.johnpbloch.com/
 * @props     Evan Mulins        http://circlecube.com/
 *
 * @since     2010-09-13
 * @alter     2013-01-31
 *
 * @license   GPLv2
 */
function mfields_set_default_object_terms( $post_id, $post ) {
    if ( 'publish' === $post->post_status && $post->post_type === 'artist' ) {
        $defaults = array(
            'genres' => array( 'all-genres' )
            //'your_taxonomy_id' => array( 'your_term_slug', 'your_term_slug' )
            );
        $taxonomies = get_object_taxonomies( $post->post_type );
        foreach ( (array) $taxonomies as $taxonomy ) {
            $terms = wp_get_post_terms( $post_id, $taxonomy );
            if ( empty( $terms ) && array_key_exists( $taxonomy, $defaults ) ) {
                wp_set_object_terms( $post_id, $defaults[$taxonomy], $taxonomy );
            }
        }
    }
}
add_action( 'save_post', 'mfields_set_default_object_terms', 100, 2 );





// Register Taxonomies for artists
// -------------------------------------------------------------------------
	// hook into the init action and call create_artist_taxonomies when it fires
	add_action( 'init', 'create_custom_taxonomies', 0 );


	function create_custom_taxonomies() {
		
		// Add a "Genre" taxonomy
		$labels = array(
			'name'              => _x( 'Genres', 'taxonomy general name' ),
			'singular_name'     => _x( 'Genre', 'taxonomy singular name' ),
			'search_items'      => __( 'Search Genres' ),
			'all_items'         => __( 'All Genres' ),
			'parent_item'       => __( 'Parent Genre' ),
			'parent_item_colon' => __( 'Parent Genre:' ),
			'edit_item'         => __( 'Edit Genre' ),
			'update_item'       => __( 'Update Genre' ),
			'add_new_item'      => __( 'Add New Genre' ),
			'new_item_name'     => __( 'New Genre Name' ),
			'menu_name'         => __( 'Genres' ),
		);

		$args = array(
			'hierarchical'      => true,
			'labels'            => $labels,
			'show_ui'           => true,
			'show_admin_column' => true,
			'update_count_callback' => '_update_post_term_count',
			'query_var'         => true,
			'rewrite'           => true,
		);

		register_taxonomy( 'genres', array( 'artist' ), $args );


		// Add a "Video Categories" taxonomy
		$labels = array(
			'name'              => _x( 'Video Categories', 'taxonomy general name' ),
			'singular_name'     => _x( 'Video Category', 'taxonomy singular name' ),
			'search_items'      => __( 'Search Video Categories' ),
			'all_items'         => __( 'All Video Categories' ),
			'parent_item'       => __( 'Parent Video Category' ),
			'parent_item_colon' => __( 'Parent Video Category:' ),
			'edit_item'         => __( 'Edit Video Category' ),
			'update_item'       => __( 'Update Video Category' ),
			'add_new_item'      => __( 'Add New Video Category' ),
			'new_item_name'     => __( 'New Video Category Name' ),
			'menu_name'         => __( 'Video Categories' ),
		);

		$args = array(
			'hierarchical'      => true,
			'labels'            => $labels,
			'show_ui'           => true,
			'show_admin_column' => true,
			'update_count_callback' => '_update_post_term_count',
			'query_var'         => true,
			'rewrite'           => true,
		);

		register_taxonomy( 'video-categories', array( 'video' ), $args );


		// Add a "Video Tags" taxonomy
		$labels = array(
			'name'              => _x( 'Video Tags', 'taxonomy general name' ),
			'singular_name'     => _x( 'Video Tag', 'taxonomy singular name' ),
			'search_items'      => __( 'Search Video Tags' ),
			'all_items'         => __( 'All Video Tags' ),
			'parent_item'       => __( 'Parent Video Tag' ),
			'parent_item_colon' => __( 'Parent Video Tag:' ),
			'edit_item'         => __( 'Edit Video Tag' ),
			'update_item'       => __( 'Update Video Tag' ),
			'add_new_item'      => __( 'Add New Video Tag' ),
			'new_item_name'     => __( 'New Video Tag Name' ),
			'menu_name'         => __( 'Video Tags' ),
		);

		$args = array(
			'hierarchical'      => false,
			'labels'            => $labels,
			'show_ui'           => true,
			'show_admin_column' => true,
			'update_count_callback' => '_update_post_term_count',
			'query_var'         => true,
			'rewrite'           => true,
		);

		register_taxonomy( 'video-tags', array( 'video' ), $args );


	}
?>