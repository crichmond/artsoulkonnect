<?php get_header(); ?>

  <?php get_template_part('partials/asset', 'featured-media'); ?>

  <section class="main single">
    <div class="global-container">

    <?php if(have_posts()): while (have_posts()) : the_post(); 

      $format = get_post_format();

      if(get_post_format() == false ) {
        $format = 'standard';
      } 
      
      get_template_part( 'partials/format', $format );
      
    endwhile; endif; 

    ?>

    </div><!-- /.global-container -->
  </section><!-- /.single -->

  <?php 
    get_template_part('partials/asset', 'recommended-posts'); 
    get_template_part('partials/asset', 'ads-footer'); 
  ?>

<?php get_footer(); ?> 