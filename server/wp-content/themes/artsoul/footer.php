	<footer class="primary">
		<div class="global-container">
			<div class="columns">
				<div class="row-border"></div>
				<div class="col-1">
					<h3><?php the_field('about_title','option'); ?></h3>
					<p><?php the_field('about_description','option'); ?></p>
				</div><!-- /.col-1 -->

				<div class="row-border-tablet"></div>
				<div class="col-2">
					<h3>news</h3>
					<?php 
          $args = array(
            'post_type' => 'post',
            'posts_per_page' => 3,
            'tax_query' => array(
              array(
                'taxonomy' => 'post_format',
                'field' => 'slug',
                'terms' => array( 'post-format-video' ),
                'operator' => 'NOT IN',
              )
            )
          );
          
          $news_query = new WP_Query($args); ?>
					<ul>
            <?php if ($news_query->have_posts()): while($news_query->have_posts()): $news_query->the_post(); ?>
						<li><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></li>
            <?php endwhile; endif; wp_reset_postdata(); ?>
					</ul>
				</div><!-- /.col-2 -->

				<div class="col-3">
					<ul>
					<?php if(get_field('twitter','option')): ?>
						<li><a href="<?php echo addhttp(get_field('twitter','option')); ?>"><i class="fa fa-twitter fa-2x"></i></a></li>
					<?php endif; ?>

					<?php if(get_field('youtube','option')): ?>
						<li><a href="<?php echo addhttp(get_field('youtube','option')); ?>"><i class="fa fa-youtube fa-2x"></i></a></li>
					<?php endif; ?>
					
					<?php if(get_field('facebook','option')): ?>
						<li><a href="<?php echo addhttp(get_field('facebook','option')); ?>"><i class="fa fa-facebook fa-2x"></i></a></li>
					<?php endif; ?>

					<?php if(get_field('soundcloud','option')): ?>
						<li><a href="<?php echo addhttp(get_field('soundcloud','option')); ?>"><i class="fa fa-soundcloud fa-2x"></i></a></li>
					<?php endif; ?>
					</ul>
				</div><!-- /.col-3 -->
			</div>
		</div><!-- /.global-container -->
	</footer><!-- /.primary -->

	<footer class="secondary">
		<div class="global-container">
			<?php the_field('copyright_statement','option'); ?>
			<hr>
			<a class="rocket" href="http://rocketrepublic.com" title="Rocket Republic | Custom Website Design" target="_blank"></a>
		</div><!-- /.global-container -->
	</footer><!-- /.secondary -->
	

	<!-- FitVids --> 
  <script src="<?php bloginfo('template_directory'); ?>/js/modules/fitvids/jquery.fitvids.js"></script>
  <!-- TypeKit -->
  <script type="text/javascript" src="//use.typekit.net/wqp7hju.js"></script>
  <script type="text/javascript">try{Typekit.load();}catch(e){}</script>
  <!-- MediaElement -->
  <script src="<?php bloginfo('template_directory'); ?>/js/modules/mediaelement/mediaelement-and-player.min.js"></script>
  <!-- MagnificPopup -->
  <script src="<?php bloginfo('template_directory'); ?>/js/modules/magnific-popup/jquery.magnific-popup.min.js"></script>
	<!-- Full-Width Audio Player -->
  <script src="<?php bloginfo('template_directory'); ?>/js/modules/fullwidth-audio-player/source/js/jquery-ui.js" type="text/javascript"></script>
  <script src="<?php bloginfo('template_directory'); ?>/js/modules/fullwidth-audio-player/source/js/soundmanager2-nodebug-jsmin.js" type="text/javascript"></script>
  <script src="<?php bloginfo('template_directory'); ?>/js/modules/fullwidth-audio-player/source/js/jquery.fullwidthAudioPlayer.min.js" type="text/javascript"></script>
  <!-- jQuery Select Box replacement -->
  <script src="<?php bloginfo('template_directory'); ?>/js/modules/jquery-selectBox/jquery.selectBox.js" type="text/javascript"></script>

  <script src="<?php bloginfo('template_directory'); ?>/js/scripts.js" type="text/javascript"></script>


  <!-- ArtSoul Sliders - Left down here because they were breaking other things -->
  <script src="<?php bloginfo('template_directory'); ?>/js/modules/artsoulkonnect/js/vendor-ck.js"></script>
  <script src="<?php bloginfo('template_directory'); ?>/js/modules/artsoulkonnect/js/sliders-ck.js"></script>
  

  

  <?php wp_footer(); ?>
  </body>  
</html>