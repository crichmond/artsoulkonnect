-- MySQL dump 10.13  Distrib 5.5.36-34.2, for debian-linux-gnu (i686)
--
-- Host: 127.0.0.1    Database: wp_artsoul
-- ------------------------------------------------------
-- Server version	5.5.36-34.2-648.lucid-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `wp_apto`
--

DROP TABLE IF EXISTS `wp_apto`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `wp_apto` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `blog_id` int(11) NOT NULL DEFAULT '1',
  `post_id` int(11) NOT NULL,
  `term_id` int(11) NOT NULL,
  `post_type` varchar(128) NOT NULL,
  `taxonomy` varchar(128) NOT NULL,
  `lang` varchar(3) NOT NULL DEFAULT 'en',
  PRIMARY KEY (`id`),
  KEY `term_id` (`term_id`),
  KEY `post_type` (`post_type`),
  KEY `taxonomy` (`taxonomy`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `wp_apto`
--

LOCK TABLES `wp_apto` WRITE;
/*!40000 ALTER TABLE `wp_apto` DISABLE KEYS */;
/*!40000 ALTER TABLE `wp_apto` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `wp_commentmeta`
--

DROP TABLE IF EXISTS `wp_commentmeta`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `wp_commentmeta` (
  `meta_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `comment_id` bigint(20) unsigned NOT NULL DEFAULT '0',
  `meta_key` varchar(255) DEFAULT NULL,
  `meta_value` longtext,
  PRIMARY KEY (`meta_id`),
  KEY `comment_id` (`comment_id`),
  KEY `meta_key` (`meta_key`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `wp_commentmeta`
--

LOCK TABLES `wp_commentmeta` WRITE;
/*!40000 ALTER TABLE `wp_commentmeta` DISABLE KEYS */;
/*!40000 ALTER TABLE `wp_commentmeta` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `wp_comments`
--

DROP TABLE IF EXISTS `wp_comments`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `wp_comments` (
  `comment_ID` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `comment_post_ID` bigint(20) unsigned NOT NULL DEFAULT '0',
  `comment_author` tinytext NOT NULL,
  `comment_author_email` varchar(100) NOT NULL DEFAULT '',
  `comment_author_url` varchar(200) NOT NULL DEFAULT '',
  `comment_author_IP` varchar(100) NOT NULL DEFAULT '',
  `comment_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `comment_date_gmt` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `comment_content` text NOT NULL,
  `comment_karma` int(11) NOT NULL DEFAULT '0',
  `comment_approved` varchar(20) NOT NULL DEFAULT '1',
  `comment_agent` varchar(255) NOT NULL DEFAULT '',
  `comment_type` varchar(20) NOT NULL DEFAULT '',
  `comment_parent` bigint(20) unsigned NOT NULL DEFAULT '0',
  `user_id` bigint(20) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`comment_ID`),
  KEY `comment_post_ID` (`comment_post_ID`),
  KEY `comment_approved_date_gmt` (`comment_approved`,`comment_date_gmt`),
  KEY `comment_date_gmt` (`comment_date_gmt`),
  KEY `comment_parent` (`comment_parent`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `wp_comments`
--

LOCK TABLES `wp_comments` WRITE;
/*!40000 ALTER TABLE `wp_comments` DISABLE KEYS */;
INSERT INTO `wp_comments` VALUES (1,1,'Mr WordPress','','https://wordpress.org/','','2014-05-27 16:26:25','2014-05-27 16:26:25','Hi, this is a comment.\nTo delete a comment, just log in and view the post&#039;s comments. There you will have the option to edit or delete them.',0,'1','','',0,0);
INSERT INTO `wp_comments` VALUES (2,41,'Art Soul','mail@themediadistrict.com','','127.0.0.1','2014-06-02 08:44:00','2014-06-02 12:44:00','comment',0,'1','Mozilla/5.0 (Macintosh; Intel Mac OS X 10_8_5) AppleWebKit/537.75.14 (KHTML, like Gecko) Version/6.1.3 Safari/537.75.14','',0,1);
INSERT INTO `wp_comments` VALUES (3,66,'Art Soul','mail@themediadistrict.com','','127.0.0.1','2014-06-02 08:46:33','2014-06-02 12:46:33','test',0,'1','Mozilla/5.0 (Macintosh; Intel Mac OS X 10_8_5) AppleWebKit/537.75.14 (KHTML, like Gecko) Version/6.1.3 Safari/537.75.14','',0,1);
INSERT INTO `wp_comments` VALUES (4,66,'Art Soul','mail@themediadistrict.com','','127.0.0.1','2014-06-02 08:46:47','2014-06-02 12:46:47','Test 2',0,'1','Mozilla/5.0 (Macintosh; Intel Mac OS X 10_8_5) AppleWebKit/537.75.14 (KHTML, like Gecko) Version/6.1.3 Safari/537.75.14','',0,1);
INSERT INTO `wp_comments` VALUES (5,66,'Art Soul','mail@themediadistrict.com','','127.0.0.1','2014-06-02 08:47:22','2014-06-02 12:47:22','THis is a reply.',0,'1','Mozilla/5.0 (Macintosh; Intel Mac OS X 10_8_5) AppleWebKit/537.75.14 (KHTML, like Gecko) Version/6.1.3 Safari/537.75.14','',3,1);
INSERT INTO `wp_comments` VALUES (6,66,'Art Soul','mail@themediadistrict.com','','127.0.0.1','2014-06-02 08:59:07','2014-06-02 12:59:07','I don\'t agree.',0,'1','Mozilla/5.0 (Macintosh; Intel Mac OS X 10_8_5) AppleWebKit/537.75.14 (KHTML, like Gecko) Version/6.1.3 Safari/537.75.14','',3,1);
/*!40000 ALTER TABLE `wp_comments` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `wp_links`
--

DROP TABLE IF EXISTS `wp_links`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `wp_links` (
  `link_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `link_url` varchar(255) NOT NULL DEFAULT '',
  `link_name` varchar(255) NOT NULL DEFAULT '',
  `link_image` varchar(255) NOT NULL DEFAULT '',
  `link_target` varchar(25) NOT NULL DEFAULT '',
  `link_description` varchar(255) NOT NULL DEFAULT '',
  `link_visible` varchar(20) NOT NULL DEFAULT 'Y',
  `link_owner` bigint(20) unsigned NOT NULL DEFAULT '1',
  `link_rating` int(11) NOT NULL DEFAULT '0',
  `link_updated` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `link_rel` varchar(255) NOT NULL DEFAULT '',
  `link_notes` mediumtext NOT NULL,
  `link_rss` varchar(255) NOT NULL DEFAULT '',
  PRIMARY KEY (`link_id`),
  KEY `link_visible` (`link_visible`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `wp_links`
--

LOCK TABLES `wp_links` WRITE;
/*!40000 ALTER TABLE `wp_links` DISABLE KEYS */;
/*!40000 ALTER TABLE `wp_links` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `wp_options`
--

DROP TABLE IF EXISTS `wp_options`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `wp_options` (
  `option_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `option_name` varchar(64) NOT NULL DEFAULT '',
  `option_value` longtext NOT NULL,
  `autoload` varchar(20) NOT NULL DEFAULT 'yes',
  PRIMARY KEY (`option_id`),
  UNIQUE KEY `option_name` (`option_name`)
) ENGINE=InnoDB AUTO_INCREMENT=16361 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `wp_options`
--

LOCK TABLES `wp_options` WRITE;
/*!40000 ALTER TABLE `wp_options` DISABLE KEYS */;
INSERT INTO `wp_options` VALUES (1,'siteurl','http://artsoul.wpengine.com/','yes');
INSERT INTO `wp_options` VALUES (2,'blogname','ArtSoul','yes');
INSERT INTO `wp_options` VALUES (3,'blogdescription','Just another WordPress site','yes');
INSERT INTO `wp_options` VALUES (4,'users_can_register','1','yes');
INSERT INTO `wp_options` VALUES (5,'admin_email','mail@themediadistrict.com','yes');
INSERT INTO `wp_options` VALUES (6,'start_of_week','0','yes');
INSERT INTO `wp_options` VALUES (7,'use_balanceTags','0','yes');
INSERT INTO `wp_options` VALUES (8,'use_smilies','1','yes');
INSERT INTO `wp_options` VALUES (9,'require_name_email','1','yes');
INSERT INTO `wp_options` VALUES (10,'comments_notify','1','yes');
INSERT INTO `wp_options` VALUES (11,'posts_per_rss','10','yes');
INSERT INTO `wp_options` VALUES (12,'rss_use_excerpt','0','yes');
INSERT INTO `wp_options` VALUES (13,'mailserver_url','mail.example.com','yes');
INSERT INTO `wp_options` VALUES (14,'mailserver_login','login@example.com','yes');
INSERT INTO `wp_options` VALUES (15,'mailserver_pass','password','yes');
INSERT INTO `wp_options` VALUES (16,'mailserver_port','110','yes');
INSERT INTO `wp_options` VALUES (17,'default_category','1','yes');
INSERT INTO `wp_options` VALUES (18,'default_comment_status','open','yes');
INSERT INTO `wp_options` VALUES (19,'default_ping_status','open','yes');
INSERT INTO `wp_options` VALUES (20,'default_pingback_flag','1','yes');
INSERT INTO `wp_options` VALUES (21,'posts_per_page','10','yes');
INSERT INTO `wp_options` VALUES (22,'date_format','F j, Y','yes');
INSERT INTO `wp_options` VALUES (23,'time_format','g:i a','yes');
INSERT INTO `wp_options` VALUES (24,'links_updated_date_format','F j, Y g:i a','yes');
INSERT INTO `wp_options` VALUES (25,'comment_moderation','0','yes');
INSERT INTO `wp_options` VALUES (26,'moderation_notify','1','yes');
INSERT INTO `wp_options` VALUES (27,'permalink_structure','/%category%/%postname%/','yes');
INSERT INTO `wp_options` VALUES (28,'gzipcompression','0','yes');
INSERT INTO `wp_options` VALUES (29,'hack_file','0','yes');
INSERT INTO `wp_options` VALUES (30,'blog_charset','UTF-8','yes');
INSERT INTO `wp_options` VALUES (31,'moderation_keys','','no');
INSERT INTO `wp_options` VALUES (32,'active_plugins','a:12:{i:0;s:29:\"gravityforms/gravityforms.php\";i:1;s:45:\"acf-flexible-content/acf-flexible-content.php\";i:2;s:27:\"acf-gallery/acf-gallery.php\";i:3;s:37:\"acf-options-page/acf-options-page.php\";i:4;s:29:\"acf-repeater/acf-repeater.php\";i:5;s:67:\"advanced-custom-field-repeater-collapser/acf_repeater_collapser.php\";i:6;s:30:\"advanced-custom-fields/acf.php\";i:7;s:55:\"advanced-post-types-order/advanced-post-types-order.php\";i:8;s:43:\"google-analyticator/google-analyticator.php\";i:10;s:47:\"regenerate-thumbnails/regenerate-thumbnails.php\";i:11;s:33:\"user-switching/user-switching.php\";i:12;s:41:\"wordpress-importer/wordpress-importer.php\";}','yes');
INSERT INTO `wp_options` VALUES (33,'home','http://artsoul.wpengine.com/','yes');
INSERT INTO `wp_options` VALUES (34,'category_base','','yes');
INSERT INTO `wp_options` VALUES (35,'ping_sites','http://rpc.pingomatic.com/','yes');
INSERT INTO `wp_options` VALUES (36,'advanced_edit','0','yes');
INSERT INTO `wp_options` VALUES (37,'comment_max_links','2','yes');
INSERT INTO `wp_options` VALUES (38,'gmt_offset','','yes');
INSERT INTO `wp_options` VALUES (39,'default_email_category','1','yes');
INSERT INTO `wp_options` VALUES (40,'recently_edited','','no');
INSERT INTO `wp_options` VALUES (41,'template','artsoul','yes');
INSERT INTO `wp_options` VALUES (42,'stylesheet','artsoul','yes');
INSERT INTO `wp_options` VALUES (43,'comment_whitelist','1','yes');
INSERT INTO `wp_options` VALUES (44,'blacklist_keys','','no');
INSERT INTO `wp_options` VALUES (45,'comment_registration','0','yes');
INSERT INTO `wp_options` VALUES (46,'html_type','text/html','yes');
INSERT INTO `wp_options` VALUES (47,'use_trackback','0','yes');
INSERT INTO `wp_options` VALUES (48,'default_role','contributor','yes');
INSERT INTO `wp_options` VALUES (49,'db_version','27916','yes');
INSERT INTO `wp_options` VALUES (50,'uploads_use_yearmonth_folders','1','yes');
INSERT INTO `wp_options` VALUES (51,'upload_path','','yes');
INSERT INTO `wp_options` VALUES (52,'blog_public','1','yes');
INSERT INTO `wp_options` VALUES (53,'default_link_category','2','yes');
INSERT INTO `wp_options` VALUES (54,'show_on_front','page','yes');
INSERT INTO `wp_options` VALUES (55,'tag_base','','yes');
INSERT INTO `wp_options` VALUES (56,'show_avatars','1','yes');
INSERT INTO `wp_options` VALUES (57,'avatar_rating','G','yes');
INSERT INTO `wp_options` VALUES (58,'upload_url_path','','yes');
INSERT INTO `wp_options` VALUES (59,'thumbnail_size_w','150','yes');
INSERT INTO `wp_options` VALUES (60,'thumbnail_size_h','150','yes');
INSERT INTO `wp_options` VALUES (61,'thumbnail_crop','1','yes');
INSERT INTO `wp_options` VALUES (62,'medium_size_w','300','yes');
INSERT INTO `wp_options` VALUES (63,'medium_size_h','300','yes');
INSERT INTO `wp_options` VALUES (64,'avatar_default','mystery','yes');
INSERT INTO `wp_options` VALUES (65,'large_size_w','1024','yes');
INSERT INTO `wp_options` VALUES (66,'large_size_h','1024','yes');
INSERT INTO `wp_options` VALUES (67,'image_default_link_type','none','yes');
INSERT INTO `wp_options` VALUES (68,'image_default_size','','yes');
INSERT INTO `wp_options` VALUES (69,'image_default_align','','yes');
INSERT INTO `wp_options` VALUES (70,'close_comments_for_old_posts','0','yes');
INSERT INTO `wp_options` VALUES (71,'close_comments_days_old','14','yes');
INSERT INTO `wp_options` VALUES (72,'thread_comments','1','yes');
INSERT INTO `wp_options` VALUES (73,'thread_comments_depth','5','yes');
INSERT INTO `wp_options` VALUES (74,'page_comments','0','yes');
INSERT INTO `wp_options` VALUES (75,'comments_per_page','50','yes');
INSERT INTO `wp_options` VALUES (76,'default_comments_page','newest','yes');
INSERT INTO `wp_options` VALUES (77,'comment_order','asc','yes');
INSERT INTO `wp_options` VALUES (78,'sticky_posts','a:0:{}','yes');
INSERT INTO `wp_options` VALUES (79,'widget_categories','a:2:{i:2;a:4:{s:5:\"title\";s:0:\"\";s:5:\"count\";i:0;s:12:\"hierarchical\";i:0;s:8:\"dropdown\";i:0;}s:12:\"_multiwidget\";i:1;}','yes');
INSERT INTO `wp_options` VALUES (80,'widget_text','a:0:{}','yes');
INSERT INTO `wp_options` VALUES (81,'widget_rss','a:0:{}','yes');
INSERT INTO `wp_options` VALUES (82,'uninstall_plugins','a:0:{}','no');
INSERT INTO `wp_options` VALUES (83,'timezone_string','America/New_York','yes');
INSERT INTO `wp_options` VALUES (84,'page_for_posts','0','yes');
INSERT INTO `wp_options` VALUES (85,'page_on_front','12','yes');
INSERT INTO `wp_options` VALUES (86,'default_post_format','0','yes');
INSERT INTO `wp_options` VALUES (87,'link_manager_enabled','0','yes');
INSERT INTO `wp_options` VALUES (88,'initial_db_version','27916','yes');
INSERT INTO `wp_options` VALUES (89,'wp_user_roles','a:6:{s:13:\"administrator\";a:2:{s:4:\"name\";s:13:\"Administrator\";s:12:\"capabilities\";a:75:{s:13:\"switch_themes\";b:1;s:11:\"edit_themes\";b:1;s:16:\"activate_plugins\";b:1;s:12:\"edit_plugins\";b:1;s:10:\"edit_users\";b:1;s:10:\"edit_files\";b:1;s:14:\"manage_options\";b:1;s:17:\"moderate_comments\";b:1;s:17:\"manage_categories\";b:1;s:12:\"manage_links\";b:1;s:12:\"upload_files\";b:1;s:6:\"import\";b:1;s:15:\"unfiltered_html\";b:1;s:10:\"edit_posts\";b:1;s:17:\"edit_others_posts\";b:1;s:20:\"edit_published_posts\";b:1;s:13:\"publish_posts\";b:1;s:10:\"edit_pages\";b:1;s:4:\"read\";b:1;s:8:\"level_10\";b:1;s:7:\"level_9\";b:1;s:7:\"level_8\";b:1;s:7:\"level_7\";b:1;s:7:\"level_6\";b:1;s:7:\"level_5\";b:1;s:7:\"level_4\";b:1;s:7:\"level_3\";b:1;s:7:\"level_2\";b:1;s:7:\"level_1\";b:1;s:7:\"level_0\";b:1;s:17:\"edit_others_pages\";b:1;s:20:\"edit_published_pages\";b:1;s:13:\"publish_pages\";b:1;s:12:\"delete_pages\";b:1;s:19:\"delete_others_pages\";b:1;s:22:\"delete_published_pages\";b:1;s:12:\"delete_posts\";b:1;s:19:\"delete_others_posts\";b:1;s:22:\"delete_published_posts\";b:1;s:20:\"delete_private_posts\";b:1;s:18:\"edit_private_posts\";b:1;s:18:\"read_private_posts\";b:1;s:20:\"delete_private_pages\";b:1;s:18:\"edit_private_pages\";b:1;s:18:\"read_private_pages\";b:1;s:12:\"delete_users\";b:1;s:12:\"create_users\";b:1;s:17:\"unfiltered_upload\";b:1;s:14:\"edit_dashboard\";b:1;s:14:\"update_plugins\";b:1;s:14:\"delete_plugins\";b:1;s:15:\"install_plugins\";b:1;s:13:\"update_themes\";b:1;s:14:\"install_themes\";b:1;s:11:\"update_core\";b:1;s:10:\"list_users\";b:1;s:12:\"remove_users\";b:1;s:9:\"add_users\";b:1;s:13:\"promote_users\";b:1;s:18:\"edit_theme_options\";b:1;s:13:\"delete_themes\";b:1;s:6:\"export\";b:1;s:11:\"edit_artist\";b:1;s:11:\"read_artist\";b:1;s:13:\"delete_artist\";b:1;s:12:\"edit_artists\";b:1;s:19:\"edit_others_artists\";b:1;s:15:\"publish_artists\";b:1;s:20:\"read_private_artists\";b:1;s:14:\"delete_artists\";b:1;s:22:\"delete_private_artists\";b:1;s:24:\"delete_published_artists\";b:1;s:21:\"delete_others_artists\";b:1;s:20:\"edit_private_artists\";b:1;s:22:\"edit_published_artists\";b:1;}}s:6:\"editor\";a:2:{s:4:\"name\";s:6:\"Editor\";s:12:\"capabilities\";a:37:{s:17:\"moderate_comments\";b:1;s:17:\"manage_categories\";b:1;s:12:\"manage_links\";b:1;s:12:\"upload_files\";b:1;s:15:\"unfiltered_html\";b:1;s:10:\"edit_posts\";b:1;s:17:\"edit_others_posts\";b:1;s:20:\"edit_published_posts\";b:1;s:13:\"publish_posts\";b:1;s:10:\"edit_pages\";b:1;s:4:\"read\";b:1;s:7:\"level_7\";b:1;s:7:\"level_6\";b:1;s:7:\"level_5\";b:1;s:7:\"level_4\";b:1;s:7:\"level_3\";b:1;s:7:\"level_2\";b:1;s:7:\"level_1\";b:1;s:7:\"level_0\";b:1;s:17:\"edit_others_pages\";b:1;s:20:\"edit_published_pages\";b:1;s:13:\"publish_pages\";b:1;s:12:\"delete_pages\";b:1;s:19:\"delete_others_pages\";b:1;s:22:\"delete_published_pages\";b:1;s:12:\"delete_posts\";b:1;s:19:\"delete_others_posts\";b:1;s:22:\"delete_published_posts\";b:1;s:20:\"delete_private_posts\";b:1;s:18:\"edit_private_posts\";b:1;s:18:\"read_private_posts\";b:1;s:20:\"delete_private_pages\";b:1;s:18:\"edit_private_pages\";b:1;s:18:\"read_private_pages\";b:1;s:11:\"edit_artist\";b:1;s:12:\"edit_artists\";b:1;s:12:\"read_artists\";b:1;}}s:6:\"author\";a:2:{s:4:\"name\";s:6:\"Author\";s:12:\"capabilities\";a:10:{s:12:\"upload_files\";b:1;s:10:\"edit_posts\";b:1;s:20:\"edit_published_posts\";b:1;s:13:\"publish_posts\";b:1;s:4:\"read\";b:1;s:7:\"level_2\";b:1;s:7:\"level_1\";b:1;s:7:\"level_0\";b:1;s:12:\"delete_posts\";b:1;s:22:\"delete_published_posts\";b:1;}}s:11:\"contributor\";a:2:{s:4:\"name\";s:11:\"Contributor\";s:12:\"capabilities\";a:8:{s:4:\"read\";b:1;s:7:\"level_1\";b:1;s:7:\"level_0\";b:1;s:11:\"read_artist\";b:1;s:11:\"edit_artist\";b:1;s:12:\"edit_artists\";b:1;s:22:\"edit_published_artists\";b:1;s:12:\"upload_files\";b:1;}}s:10:\"subscriber\";a:2:{s:4:\"name\";s:10:\"Subscriber\";s:12:\"capabilities\";a:2:{s:4:\"read\";b:1;s:7:\"level_0\";b:1;}}s:7:\"revisor\";a:2:{s:4:\"name\";s:7:\"Revisor\";s:12:\"capabilities\";a:17:{s:4:\"read\";b:1;s:18:\"read_private_posts\";b:1;s:18:\"read_private_pages\";b:1;s:10:\"edit_posts\";b:1;s:12:\"delete_posts\";b:1;s:17:\"edit_others_posts\";b:1;s:10:\"edit_pages\";b:1;s:12:\"delete_pages\";b:1;s:17:\"edit_others_pages\";b:1;s:7:\"level_3\";b:1;s:7:\"level_2\";b:1;s:7:\"level_1\";b:1;s:7:\"level_0\";b:1;s:20:\"read_private_artists\";b:1;s:12:\"edit_artists\";b:1;s:19:\"edit_others_artists\";b:1;s:14:\"delete_artists\";b:1;}}}','yes');
INSERT INTO `wp_options` VALUES (90,'widget_search','a:2:{i:2;a:1:{s:5:\"title\";s:0:\"\";}s:12:\"_multiwidget\";i:1;}','yes');
INSERT INTO `wp_options` VALUES (91,'widget_recent-posts','a:2:{i:2;a:2:{s:5:\"title\";s:0:\"\";s:6:\"number\";i:5;}s:12:\"_multiwidget\";i:1;}','yes');
INSERT INTO `wp_options` VALUES (92,'widget_recent-comments','a:2:{i:2;a:2:{s:5:\"title\";s:0:\"\";s:6:\"number\";i:5;}s:12:\"_multiwidget\";i:1;}','yes');
INSERT INTO `wp_options` VALUES (93,'widget_archives','a:2:{i:2;a:3:{s:5:\"title\";s:0:\"\";s:5:\"count\";i:0;s:8:\"dropdown\";i:0;}s:12:\"_multiwidget\";i:1;}','yes');
INSERT INTO `wp_options` VALUES (94,'widget_meta','a:2:{i:2;a:1:{s:5:\"title\";s:0:\"\";}s:12:\"_multiwidget\";i:1;}','yes');
INSERT INTO `wp_options` VALUES (95,'sidebars_widgets','a:3:{s:19:\"wp_inactive_widgets\";a:0:{}s:18:\"orphaned_widgets_1\";a:6:{i:0;s:8:\"search-2\";i:1;s:14:\"recent-posts-2\";i:2;s:17:\"recent-comments-2\";i:3;s:10:\"archives-2\";i:4;s:12:\"categories-2\";i:5;s:6:\"meta-2\";}s:13:\"array_version\";i:3;}','yes');
INSERT INTO `wp_options` VALUES (96,'cron','a:5:{i:1402201587;a:3:{s:16:\"wp_version_check\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:10:\"twicedaily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:43200;}}s:17:\"wp_update_plugins\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:10:\"twicedaily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:43200;}}s:16:\"wp_update_themes\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:10:\"twicedaily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:43200;}}}i:1402211400;a:1:{s:20:\"wp_maybe_auto_update\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:10:\"twicedaily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:43200;}}}i:1402244880;a:1:{s:19:\"wp_scheduled_delete\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:5:\"daily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:86400;}}}i:1402253082;a:1:{s:30:\"wp_scheduled_auto_draft_delete\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:5:\"daily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:86400;}}}s:7:\"version\";i:2;}','yes');
INSERT INTO `wp_options` VALUES (107,'can_compress_scripts','1','yes');
INSERT INTO `wp_options` VALUES (126,'_site_transient_update_core','O:8:\"stdClass\":4:{s:7:\"updates\";a:1:{i:0;O:8:\"stdClass\":10:{s:8:\"response\";s:6:\"latest\";s:8:\"download\";s:59:\"https://downloads.wordpress.org/release/wordpress-3.9.1.zip\";s:6:\"locale\";s:5:\"en_US\";s:8:\"packages\";O:8:\"stdClass\":5:{s:4:\"full\";s:59:\"https://downloads.wordpress.org/release/wordpress-3.9.1.zip\";s:10:\"no_content\";s:70:\"https://downloads.wordpress.org/release/wordpress-3.9.1-no-content.zip\";s:11:\"new_bundled\";s:71:\"https://downloads.wordpress.org/release/wordpress-3.9.1-new-bundled.zip\";s:7:\"partial\";b:0;s:8:\"rollback\";b:0;}s:7:\"current\";s:5:\"3.9.1\";s:7:\"version\";s:5:\"3.9.1\";s:11:\"php_version\";s:5:\"5.2.4\";s:13:\"mysql_version\";s:3:\"5.0\";s:11:\"new_bundled\";s:3:\"3.8\";s:15:\"partial_version\";s:0:\"\";}}s:12:\"last_checked\";i:1402115193;s:15:\"version_checked\";s:5:\"3.9.1\";s:12:\"translations\";a:0:{}}','yes');
INSERT INTO `wp_options` VALUES (127,'_site_transient_update_themes','O:8:\"stdClass\":4:{s:12:\"last_checked\";i:1402115196;s:7:\"checked\";a:3:{s:7:\"artsoul\";s:1:\"1\";s:14:\"twentyfourteen\";s:3:\"1.1\";s:12:\"twentytwelve\";s:3:\"1.4\";}s:8:\"response\";a:0:{}s:12:\"translations\";a:0:{}}','yes');
INSERT INTO `wp_options` VALUES (129,'current_theme','ArtSoul','yes');
INSERT INTO `wp_options` VALUES (130,'theme_mods_artsoul','a:2:{i:0;b:0;s:18:\"nav_menu_locations\";a:4:{s:7:\"primary\";i:2;s:6:\"mobile\";i:2;s:4:\"tags\";i:6;s:10:\"categories\";i:7;}}','yes');
INSERT INTO `wp_options` VALUES (131,'theme_switched','','yes');
INSERT INTO `wp_options` VALUES (132,'cpto_options','a:5:{s:19:\"apto_tables_created\";b:1;s:8:\"autosort\";s:1:\"1\";s:9:\"adminsort\";s:1:\"1\";s:10:\"capability\";s:15:\"install_plugins\";s:12:\"code_version\";s:7:\"2.5.4.8\";}','yes');
INSERT INTO `wp_options` VALUES (133,'ga_version','6.4.7.3','yes');
INSERT INTO `wp_options` VALUES (134,'ga_status','disabled','yes');
INSERT INTO `wp_options` VALUES (135,'ga_disable_gasites','disabled','yes');
INSERT INTO `wp_options` VALUES (136,'ga_analytic_snippet','enabled','yes');
INSERT INTO `wp_options` VALUES (137,'ga_uid','UA-XXXXXXXX-X','yes');
INSERT INTO `wp_options` VALUES (138,'ga_admin_status','enabled','yes');
INSERT INTO `wp_options` VALUES (139,'ga_admin_disable_DimentionIndex','','yes');
INSERT INTO `wp_options` VALUES (140,'ga_admin_disable','remove','yes');
INSERT INTO `wp_options` VALUES (141,'ga_admin_role','a:1:{i:0;s:13:\"administrator\";}','yes');
INSERT INTO `wp_options` VALUES (142,'ga_dashboard_role','a:1:{i:0;s:13:\"administrator\";}','yes');
INSERT INTO `wp_options` VALUES (143,'key_ga_show_ad','1','yes');
INSERT INTO `wp_options` VALUES (144,'ga_adsense','','yes');
INSERT INTO `wp_options` VALUES (145,'ga_extra','','yes');
INSERT INTO `wp_options` VALUES (146,'ga_extra_after','','yes');
INSERT INTO `wp_options` VALUES (147,'ga_event','enabled','yes');
INSERT INTO `wp_options` VALUES (148,'ga_outbound','enabled','yes');
INSERT INTO `wp_options` VALUES (149,'ga_outbound_prefix','outgoing','yes');
INSERT INTO `wp_options` VALUES (150,'ga_enhanced_link_attr','disabled','yes');
INSERT INTO `wp_options` VALUES (151,'ga_downloads','','yes');
INSERT INTO `wp_options` VALUES (152,'ga_downloads_prefix','download','yes');
INSERT INTO `wp_options` VALUES (153,'ga_widgets','enabled','yes');
INSERT INTO `wp_options` VALUES (154,'ga_annon','','yes');
INSERT INTO `wp_options` VALUES (155,'ga_defaults','yes','yes');
INSERT INTO `wp_options` VALUES (156,'ga_google_token','','yes');
INSERT INTO `wp_options` VALUES (157,'gravityformsaddon_gravityformswebapi_version','1.0','yes');
INSERT INTO `wp_options` VALUES (158,'rg_form_version','1.8.3','yes');
INSERT INTO `wp_options` VALUES (161,'acf_version','4.3.8','yes');
INSERT INTO `wp_options` VALUES (164,'nav_menu_options','a:2:{i:0;b:0;s:8:\"auto_add\";a:0:{}}','yes');
INSERT INTO `wp_options` VALUES (232,'options_default_post_image','22','no');
INSERT INTO `wp_options` VALUES (233,'_options_default_post_image','field_538740ce0671a','no');
INSERT INTO `wp_options` VALUES (235,'options_home_ads_0_image','28','no');
INSERT INTO `wp_options` VALUES (236,'_options_home_ads_0_image','field_5387471a83007','no');
INSERT INTO `wp_options` VALUES (237,'options_home_ads_0_link','http://google.com','no');
INSERT INTO `wp_options` VALUES (238,'_options_home_ads_0_link','field_5387474183008','no');
INSERT INTO `wp_options` VALUES (239,'options_home_ads_1_image','27','no');
INSERT INTO `wp_options` VALUES (240,'_options_home_ads_1_image','field_5387471a83007','no');
INSERT INTO `wp_options` VALUES (241,'options_home_ads_1_link','#','no');
INSERT INTO `wp_options` VALUES (242,'_options_home_ads_1_link','field_5387474183008','no');
INSERT INTO `wp_options` VALUES (243,'options_home_ads','2','no');
INSERT INTO `wp_options` VALUES (244,'_options_home_ads','field_5387470583006','no');
INSERT INTO `wp_options` VALUES (252,'_transient_random_seed','30d827139c1058c86c012d89fe56f9ef','yes');
INSERT INTO `wp_options` VALUES (262,'options_news_ads_0_image','27','no');
INSERT INTO `wp_options` VALUES (263,'_options_news_ads_0_image','field_538780e684665','no');
INSERT INTO `wp_options` VALUES (264,'options_news_ads_0_link','#','no');
INSERT INTO `wp_options` VALUES (265,'_options_news_ads_0_link','field_538780e684666','no');
INSERT INTO `wp_options` VALUES (266,'options_news_ads_1_image','28','no');
INSERT INTO `wp_options` VALUES (267,'_options_news_ads_1_image','field_538780e684665','no');
INSERT INTO `wp_options` VALUES (268,'options_news_ads_1_link','#','no');
INSERT INTO `wp_options` VALUES (269,'_options_news_ads_1_link','field_538780e684666','no');
INSERT INTO `wp_options` VALUES (270,'options_news_ads','2','no');
INSERT INTO `wp_options` VALUES (271,'_options_news_ads','field_538780e684664','no');
INSERT INTO `wp_options` VALUES (304,'options_social_0_icon','56','no');
INSERT INTO `wp_options` VALUES (305,'_options_social_0_icon','field_53881969b5ee3','no');
INSERT INTO `wp_options` VALUES (306,'options_social_0_link','http://facebook.com','no');
INSERT INTO `wp_options` VALUES (307,'_options_social_0_link','field_5388197cb5ee4','no');
INSERT INTO `wp_options` VALUES (308,'options_social','7','no');
INSERT INTO `wp_options` VALUES (309,'_options_social','field_5388195db5ee2','no');
INSERT INTO `wp_options` VALUES (310,'options_social_1_icon','62','no');
INSERT INTO `wp_options` VALUES (311,'_options_social_1_icon','field_53881969b5ee3','no');
INSERT INTO `wp_options` VALUES (312,'options_social_1_link','twitter.com','no');
INSERT INTO `wp_options` VALUES (313,'_options_social_1_link','field_5388197cb5ee4','no');
INSERT INTO `wp_options` VALUES (314,'options_social_2_icon','61','no');
INSERT INTO `wp_options` VALUES (315,'_options_social_2_icon','field_53881969b5ee3','no');
INSERT INTO `wp_options` VALUES (316,'options_social_2_link','tumblr.com','no');
INSERT INTO `wp_options` VALUES (317,'_options_social_2_link','field_5388197cb5ee4','no');
INSERT INTO `wp_options` VALUES (318,'options_social_3_icon','60','no');
INSERT INTO `wp_options` VALUES (319,'_options_social_3_icon','field_53881969b5ee3','no');
INSERT INTO `wp_options` VALUES (320,'options_social_3_link','flickr.com','no');
INSERT INTO `wp_options` VALUES (321,'_options_social_3_link','field_5388197cb5ee4','no');
INSERT INTO `wp_options` VALUES (322,'options_social_4_icon','59','no');
INSERT INTO `wp_options` VALUES (323,'_options_social_4_icon','field_53881969b5ee3','no');
INSERT INTO `wp_options` VALUES (324,'options_social_4_link','instagram.com','no');
INSERT INTO `wp_options` VALUES (325,'_options_social_4_link','field_5388197cb5ee4','no');
INSERT INTO `wp_options` VALUES (326,'options_social_5_icon','58','no');
INSERT INTO `wp_options` VALUES (327,'_options_social_5_icon','field_53881969b5ee3','no');
INSERT INTO `wp_options` VALUES (328,'options_social_5_link','vimeo.com','no');
INSERT INTO `wp_options` VALUES (329,'_options_social_5_link','field_5388197cb5ee4','no');
INSERT INTO `wp_options` VALUES (330,'options_social_6_icon','57','no');
INSERT INTO `wp_options` VALUES (331,'_options_social_6_icon','field_53881969b5ee3','no');
INSERT INTO `wp_options` VALUES (332,'options_social_6_link','google.com','no');
INSERT INTO `wp_options` VALUES (333,'_options_social_6_link','field_5388197cb5ee4','no');
INSERT INTO `wp_options` VALUES (374,'options_sidebar_ads_0_image','65','no');
INSERT INTO `wp_options` VALUES (375,'_options_sidebar_ads_0_image','field_5388776d739d3','no');
INSERT INTO `wp_options` VALUES (376,'options_sidebar_ads_0_link','#','no');
INSERT INTO `wp_options` VALUES (377,'_options_sidebar_ads_0_link','field_5388776d739d4','no');
INSERT INTO `wp_options` VALUES (378,'options_sidebar_ads_1_image','65','no');
INSERT INTO `wp_options` VALUES (379,'_options_sidebar_ads_1_image','field_5388776d739d3','no');
INSERT INTO `wp_options` VALUES (380,'options_sidebar_ads_1_link','#','no');
INSERT INTO `wp_options` VALUES (381,'_options_sidebar_ads_1_link','field_5388776d739d4','no');
INSERT INTO `wp_options` VALUES (382,'options_sidebar_ads','2','no');
INSERT INTO `wp_options` VALUES (383,'_options_sidebar_ads','field_5388776d739d2','no');
INSERT INTO `wp_options` VALUES (391,'options_listen_ads_0_image','79','no');
INSERT INTO `wp_options` VALUES (392,'_options_listen_ads_0_image','field_53888cda8bd04','no');
INSERT INTO `wp_options` VALUES (393,'options_listen_ads_0_link','#','no');
INSERT INTO `wp_options` VALUES (394,'_options_listen_ads_0_link','field_53888cda8bd05','no');
INSERT INTO `wp_options` VALUES (395,'options_listen_ads_1_image','80','no');
INSERT INTO `wp_options` VALUES (396,'_options_listen_ads_1_image','field_53888cda8bd04','no');
INSERT INTO `wp_options` VALUES (397,'options_listen_ads_1_link','#','no');
INSERT INTO `wp_options` VALUES (398,'_options_listen_ads_1_link','field_53888cda8bd05','no');
INSERT INTO `wp_options` VALUES (399,'options_listen_ads','2','no');
INSERT INTO `wp_options` VALUES (400,'_options_listen_ads','field_53888cda8bd03','no');
INSERT INTO `wp_options` VALUES (430,'recently_activated','a:3:{s:19:\"jetpack/jetpack.php\";i:1401904716;s:27:\"revisionary/revisionary.php\";i:1401734607;s:37:\"user-role-editor/user-role-editor.php\";i:1401480223;}','yes');
INSERT INTO `wp_options` VALUES (431,'secret_key','URzz13Vgu&PAp=&gLe=l]ORJ^N# X+NmAdXhDSIDyqR+}G5Oz$@IlCA~k #//Gz/','yes');
INSERT INTO `wp_options` VALUES (726,'user_role_editor','a:3:{s:17:\"ure_caps_readable\";i:0;s:24:\"ure_show_deprecated_caps\";i:0;s:19:\"ure_hide_pro_banner\";i:0;}','yes');
INSERT INTO `wp_options` VALUES (727,'wp_backup_user_roles','a:5:{s:13:\"administrator\";a:2:{s:4:\"name\";s:13:\"Administrator\";s:12:\"capabilities\";a:62:{s:13:\"switch_themes\";b:1;s:11:\"edit_themes\";b:1;s:16:\"activate_plugins\";b:1;s:12:\"edit_plugins\";b:1;s:10:\"edit_users\";b:1;s:10:\"edit_files\";b:1;s:14:\"manage_options\";b:1;s:17:\"moderate_comments\";b:1;s:17:\"manage_categories\";b:1;s:12:\"manage_links\";b:1;s:12:\"upload_files\";b:1;s:6:\"import\";b:1;s:15:\"unfiltered_html\";b:1;s:10:\"edit_posts\";b:1;s:17:\"edit_others_posts\";b:1;s:20:\"edit_published_posts\";b:1;s:13:\"publish_posts\";b:1;s:10:\"edit_pages\";b:1;s:4:\"read\";b:1;s:8:\"level_10\";b:1;s:7:\"level_9\";b:1;s:7:\"level_8\";b:1;s:7:\"level_7\";b:1;s:7:\"level_6\";b:1;s:7:\"level_5\";b:1;s:7:\"level_4\";b:1;s:7:\"level_3\";b:1;s:7:\"level_2\";b:1;s:7:\"level_1\";b:1;s:7:\"level_0\";b:1;s:17:\"edit_others_pages\";b:1;s:20:\"edit_published_pages\";b:1;s:13:\"publish_pages\";b:1;s:12:\"delete_pages\";b:1;s:19:\"delete_others_pages\";b:1;s:22:\"delete_published_pages\";b:1;s:12:\"delete_posts\";b:1;s:19:\"delete_others_posts\";b:1;s:22:\"delete_published_posts\";b:1;s:20:\"delete_private_posts\";b:1;s:18:\"edit_private_posts\";b:1;s:18:\"read_private_posts\";b:1;s:20:\"delete_private_pages\";b:1;s:18:\"edit_private_pages\";b:1;s:18:\"read_private_pages\";b:1;s:12:\"delete_users\";b:1;s:12:\"create_users\";b:1;s:17:\"unfiltered_upload\";b:1;s:14:\"edit_dashboard\";b:1;s:14:\"update_plugins\";b:1;s:14:\"delete_plugins\";b:1;s:15:\"install_plugins\";b:1;s:13:\"update_themes\";b:1;s:14:\"install_themes\";b:1;s:11:\"update_core\";b:1;s:10:\"list_users\";b:1;s:12:\"remove_users\";b:1;s:9:\"add_users\";b:1;s:13:\"promote_users\";b:1;s:18:\"edit_theme_options\";b:1;s:13:\"delete_themes\";b:1;s:6:\"export\";b:1;}}s:6:\"editor\";a:2:{s:4:\"name\";s:6:\"Editor\";s:12:\"capabilities\";a:34:{s:17:\"moderate_comments\";b:1;s:17:\"manage_categories\";b:1;s:12:\"manage_links\";b:1;s:12:\"upload_files\";b:1;s:15:\"unfiltered_html\";b:1;s:10:\"edit_posts\";b:1;s:17:\"edit_others_posts\";b:1;s:20:\"edit_published_posts\";b:1;s:13:\"publish_posts\";b:1;s:10:\"edit_pages\";b:1;s:4:\"read\";b:1;s:7:\"level_7\";b:1;s:7:\"level_6\";b:1;s:7:\"level_5\";b:1;s:7:\"level_4\";b:1;s:7:\"level_3\";b:1;s:7:\"level_2\";b:1;s:7:\"level_1\";b:1;s:7:\"level_0\";b:1;s:17:\"edit_others_pages\";b:1;s:20:\"edit_published_pages\";b:1;s:13:\"publish_pages\";b:1;s:12:\"delete_pages\";b:1;s:19:\"delete_others_pages\";b:1;s:22:\"delete_published_pages\";b:1;s:12:\"delete_posts\";b:1;s:19:\"delete_others_posts\";b:1;s:22:\"delete_published_posts\";b:1;s:20:\"delete_private_posts\";b:1;s:18:\"edit_private_posts\";b:1;s:18:\"read_private_posts\";b:1;s:20:\"delete_private_pages\";b:1;s:18:\"edit_private_pages\";b:1;s:18:\"read_private_pages\";b:1;}}s:6:\"author\";a:2:{s:4:\"name\";s:6:\"Author\";s:12:\"capabilities\";a:10:{s:12:\"upload_files\";b:1;s:10:\"edit_posts\";b:1;s:20:\"edit_published_posts\";b:1;s:13:\"publish_posts\";b:1;s:4:\"read\";b:1;s:7:\"level_2\";b:1;s:7:\"level_1\";b:1;s:7:\"level_0\";b:1;s:12:\"delete_posts\";b:1;s:22:\"delete_published_posts\";b:1;}}s:11:\"contributor\";a:2:{s:4:\"name\";s:11:\"Contributor\";s:12:\"capabilities\";a:5:{s:10:\"edit_posts\";b:1;s:4:\"read\";b:1;s:7:\"level_1\";b:1;s:7:\"level_0\";b:1;s:12:\"delete_posts\";b:1;}}s:10:\"subscriber\";a:2:{s:4:\"name\";s:10:\"Subscriber\";s:12:\"capabilities\";a:2:{s:4:\"read\";b:1;s:7:\"level_0\";b:1;}}}','no');
INSERT INTO `wp_options` VALUES (943,'requested_remote_actions_rvy','a:0:{}','yes');
INSERT INTO `wp_options` VALUES (944,'rvy_next_rev_publish_gmt','2035-01-01 00:00:00','yes');
INSERT INTO `wp_options` VALUES (1486,'_transient_twentyfourteen_category_count','2','yes');
INSERT INTO `wp_options` VALUES (1487,'theme_mods_twentyfourteen','a:1:{s:16:\"sidebars_widgets\";a:2:{s:4:\"time\";i:1401713350;s:4:\"data\";a:4:{s:19:\"wp_inactive_widgets\";a:0:{}s:9:\"sidebar-1\";a:6:{i:0;s:8:\"search-2\";i:1;s:14:\"recent-posts-2\";i:2;s:17:\"recent-comments-2\";i:3;s:10:\"archives-2\";i:4;s:12:\"categories-2\";i:5;s:6:\"meta-2\";}s:9:\"sidebar-2\";a:0:{}s:9:\"sidebar-3\";a:0:{}}}}','yes');
INSERT INTO `wp_options` VALUES (2366,'options_default_artist_banner_image','104','no');
INSERT INTO `wp_options` VALUES (2367,'_options_default_artist_banner_image','field_538cbb1d0afa0','no');
INSERT INTO `wp_options` VALUES (2846,'options_default_artist_profile_image','105','no');
INSERT INTO `wp_options` VALUES (2847,'_options_default_artist_profile_image','field_538cc8befb88f','no');
INSERT INTO `wp_options` VALUES (3264,'options_default_album_cover_image','108','no');
INSERT INTO `wp_options` VALUES (3265,'_options_default_album_cover_image','field_538cd40732c33','no');
INSERT INTO `wp_options` VALUES (8432,'options_about_title','artsoulkonnect','no');
INSERT INTO `wp_options` VALUES (8433,'_options_about_title','field_538dd64f4e1f0','no');
INSERT INTO `wp_options` VALUES (8434,'options_about_description','How often have you found yourself spending countless hours online searching for quality entertainment that dares to be original and inspiring at the same time.  In a world where it\'s easier just to create the common works that you know will easily sell, it\'s refreshing to come across talented artists that dare to go beyond that type of mindset and push the envelope in a different direction. ','no');
INSERT INTO `wp_options` VALUES (8435,'_options_about_description','field_538dd68c4e1f1','no');
INSERT INTO `wp_options` VALUES (8436,'options_twitter','twitter.com','no');
INSERT INTO `wp_options` VALUES (8437,'_options_twitter','field_538dd69f4e1f2','no');
INSERT INTO `wp_options` VALUES (8438,'options_facebook','#','no');
INSERT INTO `wp_options` VALUES (8439,'_options_facebook','field_538dd6af4e1f3','no');
INSERT INTO `wp_options` VALUES (8440,'options_youtube','#','no');
INSERT INTO `wp_options` VALUES (8441,'_options_youtube','field_538dd6bd4e1f4','no');
INSERT INTO `wp_options` VALUES (8442,'options_soundcloud','#','no');
INSERT INTO `wp_options` VALUES (8443,'_options_soundcloud','field_538dd6c44e1f5','no');
INSERT INTO `wp_options` VALUES (8444,'options_copyright_statement','© 2014 artsoulkonnect. All rights reserved.','no');
INSERT INTO `wp_options` VALUES (8445,'_options_copyright_statement','field_538dd6ce4e1f6','no');
INSERT INTO `wp_options` VALUES (9281,'_site_transient_timeout_browser_5a598bf766ab24ceec5e21741f7490b9','1402497981','yes');
INSERT INTO `wp_options` VALUES (9282,'_site_transient_browser_5a598bf766ab24ceec5e21741f7490b9','a:9:{s:8:\"platform\";s:9:\"Macintosh\";s:4:\"name\";s:6:\"Safari\";s:7:\"version\";s:5:\"6.1.4\";s:10:\"update_url\";s:28:\"http://www.apple.com/safari/\";s:7:\"img_src\";s:49:\"http://s.wordpress.org/images/browsers/safari.png\";s:11:\"img_src_ssl\";s:48:\"https://wordpress.org/images/browsers/safari.png\";s:15:\"current_version\";s:1:\"5\";s:7:\"upgrade\";b:0;s:8:\"insecure\";b:0;}','yes');
INSERT INTO `wp_options` VALUES (9813,'_site_transient_update_plugins','O:8:\"stdClass\":3:{s:12:\"last_checked\";i:1402115195;s:8:\"response\";a:0:{}s:12:\"translations\";a:0:{}}','yes');
INSERT INTO `wp_options` VALUES (9814,'jetpack_activated','1','yes');
INSERT INTO `wp_options` VALUES (9815,'jetpack_options','a:2:{s:7:\"version\";s:16:\"3.0.1:1401903542\";s:11:\"old_version\";s:16:\"3.0.1:1401903542\";}','yes');
INSERT INTO `wp_options` VALUES (9951,'genres_children','a:2:{i:14;a:5:{i:0;i:3;i:1;i:9;i:2;i:10;i:3;i:11;i:4;i:12;}i:12;a:1:{i:0;i:13;}}','yes');
INSERT INTO `wp_options` VALUES (9974,'options_site_title','ArtSoulKonnect','no');
INSERT INTO `wp_options` VALUES (9975,'_options_site_title','field_52012f259dc17','no');
INSERT INTO `wp_options` VALUES (9976,'options_site_description','','no');
INSERT INTO `wp_options` VALUES (9977,'_options_site_description','field_52012fbd9dc18','no');
INSERT INTO `wp_options` VALUES (9978,'options_site_keywords','','no');
INSERT INTO `wp_options` VALUES (9979,'_options_site_keywords','field_5238940ca06c1','no');
INSERT INTO `wp_options` VALUES (11218,'category_children','a:0:{}','yes');
INSERT INTO `wp_options` VALUES (16267,'limit_login_retries','a:1:{s:14:\"24.154.141.224\";i:3;}','no');
INSERT INTO `wp_options` VALUES (16268,'limit_login_retries_valid','a:1:{s:14:\"24.154.141.224\";i:1402159257;}','no');
INSERT INTO `wp_options` VALUES (16327,'wpe_notices','a:2:{s:4:\"read\";s:0:\"\";s:8:\"messages\";a:0:{}}','yes');
INSERT INTO `wp_options` VALUES (16328,'wpe_notices_ttl','1402119936','yes');
INSERT INTO `wp_options` VALUES (16360,'rewrite_rules','a:100:{s:10:\"artists/?$\";s:26:\"index.php?post_type=artist\";s:40:\"artists/feed/(feed|rdf|rss|rss2|atom)/?$\";s:43:\"index.php?post_type=artist&feed=$matches[1]\";s:35:\"artists/(feed|rdf|rss|rss2|atom)/?$\";s:43:\"index.php?post_type=artist&feed=$matches[1]\";s:27:\"artists/page/([0-9]{1,})/?$\";s:44:\"index.php?post_type=artist&paged=$matches[1]\";s:47:\"category/(.+?)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:52:\"index.php?category_name=$matches[1]&feed=$matches[2]\";s:42:\"category/(.+?)/(feed|rdf|rss|rss2|atom)/?$\";s:52:\"index.php?category_name=$matches[1]&feed=$matches[2]\";s:35:\"category/(.+?)/page/?([0-9]{1,})/?$\";s:53:\"index.php?category_name=$matches[1]&paged=$matches[2]\";s:17:\"category/(.+?)/?$\";s:35:\"index.php?category_name=$matches[1]\";s:44:\"tag/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:42:\"index.php?tag=$matches[1]&feed=$matches[2]\";s:39:\"tag/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:42:\"index.php?tag=$matches[1]&feed=$matches[2]\";s:32:\"tag/([^/]+)/page/?([0-9]{1,})/?$\";s:43:\"index.php?tag=$matches[1]&paged=$matches[2]\";s:14:\"tag/([^/]+)/?$\";s:25:\"index.php?tag=$matches[1]\";s:45:\"type/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:50:\"index.php?post_format=$matches[1]&feed=$matches[2]\";s:40:\"type/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:50:\"index.php?post_format=$matches[1]&feed=$matches[2]\";s:33:\"type/([^/]+)/page/?([0-9]{1,})/?$\";s:51:\"index.php?post_format=$matches[1]&paged=$matches[2]\";s:15:\"type/([^/]+)/?$\";s:33:\"index.php?post_format=$matches[1]\";s:47:\"genres/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:45:\"index.php?genres=$matches[1]&feed=$matches[2]\";s:42:\"genres/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:45:\"index.php?genres=$matches[1]&feed=$matches[2]\";s:35:\"genres/([^/]+)/page/?([0-9]{1,})/?$\";s:46:\"index.php?genres=$matches[1]&paged=$matches[2]\";s:17:\"genres/([^/]+)/?$\";s:28:\"index.php?genres=$matches[1]\";s:35:\"artists/[^/]+/attachment/([^/]+)/?$\";s:32:\"index.php?attachment=$matches[1]\";s:45:\"artists/[^/]+/attachment/([^/]+)/trackback/?$\";s:37:\"index.php?attachment=$matches[1]&tb=1\";s:65:\"artists/[^/]+/attachment/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:60:\"artists/[^/]+/attachment/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:60:\"artists/[^/]+/attachment/([^/]+)/comment-page-([0-9]{1,})/?$\";s:50:\"index.php?attachment=$matches[1]&cpage=$matches[2]\";s:28:\"artists/([^/]+)/trackback/?$\";s:33:\"index.php?artist=$matches[1]&tb=1\";s:48:\"artists/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:45:\"index.php?artist=$matches[1]&feed=$matches[2]\";s:43:\"artists/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:45:\"index.php?artist=$matches[1]&feed=$matches[2]\";s:36:\"artists/([^/]+)/page/?([0-9]{1,})/?$\";s:46:\"index.php?artist=$matches[1]&paged=$matches[2]\";s:43:\"artists/([^/]+)/comment-page-([0-9]{1,})/?$\";s:46:\"index.php?artist=$matches[1]&cpage=$matches[2]\";s:28:\"artists/([^/]+)(/[0-9]+)?/?$\";s:45:\"index.php?artist=$matches[1]&page=$matches[2]\";s:24:\"artists/[^/]+/([^/]+)/?$\";s:32:\"index.php?attachment=$matches[1]\";s:34:\"artists/[^/]+/([^/]+)/trackback/?$\";s:37:\"index.php?attachment=$matches[1]&tb=1\";s:54:\"artists/[^/]+/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:49:\"artists/[^/]+/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:49:\"artists/[^/]+/([^/]+)/comment-page-([0-9]{1,})/?$\";s:50:\"index.php?attachment=$matches[1]&cpage=$matches[2]\";s:12:\"robots\\.txt$\";s:18:\"index.php?robots=1\";s:48:\".*wp-(atom|rdf|rss|rss2|feed|commentsrss2)\\.php$\";s:18:\"index.php?feed=old\";s:20:\".*wp-app\\.php(/.*)?$\";s:19:\"index.php?error=403\";s:18:\".*wp-register.php$\";s:23:\"index.php?register=true\";s:32:\"feed/(feed|rdf|rss|rss2|atom)/?$\";s:27:\"index.php?&feed=$matches[1]\";s:27:\"(feed|rdf|rss|rss2|atom)/?$\";s:27:\"index.php?&feed=$matches[1]\";s:20:\"page/?([0-9]{1,})/?$\";s:28:\"index.php?&paged=$matches[1]\";s:27:\"comment-page-([0-9]{1,})/?$\";s:39:\"index.php?&page_id=12&cpage=$matches[1]\";s:41:\"comments/feed/(feed|rdf|rss|rss2|atom)/?$\";s:42:\"index.php?&feed=$matches[1]&withcomments=1\";s:36:\"comments/(feed|rdf|rss|rss2|atom)/?$\";s:42:\"index.php?&feed=$matches[1]&withcomments=1\";s:44:\"search/(.+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:40:\"index.php?s=$matches[1]&feed=$matches[2]\";s:39:\"search/(.+)/(feed|rdf|rss|rss2|atom)/?$\";s:40:\"index.php?s=$matches[1]&feed=$matches[2]\";s:32:\"search/(.+)/page/?([0-9]{1,})/?$\";s:41:\"index.php?s=$matches[1]&paged=$matches[2]\";s:14:\"search/(.+)/?$\";s:23:\"index.php?s=$matches[1]\";s:27:\"(artsoul)/page/?([0-9]+)/?$\";s:51:\"index.php?author_name=$matches[1]&paged=$matches[2]\";s:12:\"(artsoul)/?$\";s:33:\"index.php?author_name=$matches[1]\";s:28:\"(dkeeling)/page/?([0-9]+)/?$\";s:51:\"index.php?author_name=$matches[1]&paged=$matches[2]\";s:13:\"(dkeeling)/?$\";s:33:\"index.php?author_name=$matches[1]\";s:29:\"(joshshank)/page/?([0-9]+)/?$\";s:51:\"index.php?author_name=$matches[1]&paged=$matches[2]\";s:14:\"(joshshank)/?$\";s:33:\"index.php?author_name=$matches[1]\";s:69:\"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/feed/(feed|rdf|rss|rss2|atom)/?$\";s:80:\"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&feed=$matches[4]\";s:64:\"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/(feed|rdf|rss|rss2|atom)/?$\";s:80:\"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&feed=$matches[4]\";s:57:\"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/page/?([0-9]{1,})/?$\";s:81:\"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&paged=$matches[4]\";s:39:\"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/?$\";s:63:\"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]\";s:56:\"([0-9]{4})/([0-9]{1,2})/feed/(feed|rdf|rss|rss2|atom)/?$\";s:64:\"index.php?year=$matches[1]&monthnum=$matches[2]&feed=$matches[3]\";s:51:\"([0-9]{4})/([0-9]{1,2})/(feed|rdf|rss|rss2|atom)/?$\";s:64:\"index.php?year=$matches[1]&monthnum=$matches[2]&feed=$matches[3]\";s:44:\"([0-9]{4})/([0-9]{1,2})/page/?([0-9]{1,})/?$\";s:65:\"index.php?year=$matches[1]&monthnum=$matches[2]&paged=$matches[3]\";s:26:\"([0-9]{4})/([0-9]{1,2})/?$\";s:47:\"index.php?year=$matches[1]&monthnum=$matches[2]\";s:43:\"([0-9]{4})/feed/(feed|rdf|rss|rss2|atom)/?$\";s:43:\"index.php?year=$matches[1]&feed=$matches[2]\";s:38:\"([0-9]{4})/(feed|rdf|rss|rss2|atom)/?$\";s:43:\"index.php?year=$matches[1]&feed=$matches[2]\";s:31:\"([0-9]{4})/page/?([0-9]{1,})/?$\";s:44:\"index.php?year=$matches[1]&paged=$matches[2]\";s:13:\"([0-9]{4})/?$\";s:26:\"index.php?year=$matches[1]\";s:27:\".?.+?/attachment/([^/]+)/?$\";s:32:\"index.php?attachment=$matches[1]\";s:37:\".?.+?/attachment/([^/]+)/trackback/?$\";s:37:\"index.php?attachment=$matches[1]&tb=1\";s:57:\".?.+?/attachment/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:52:\".?.+?/attachment/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:52:\".?.+?/attachment/([^/]+)/comment-page-([0-9]{1,})/?$\";s:50:\"index.php?attachment=$matches[1]&cpage=$matches[2]\";s:20:\"(.?.+?)/trackback/?$\";s:35:\"index.php?pagename=$matches[1]&tb=1\";s:40:\"(.?.+?)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:47:\"index.php?pagename=$matches[1]&feed=$matches[2]\";s:35:\"(.?.+?)/(feed|rdf|rss|rss2|atom)/?$\";s:47:\"index.php?pagename=$matches[1]&feed=$matches[2]\";s:28:\"(.?.+?)/page/?([0-9]{1,})/?$\";s:48:\"index.php?pagename=$matches[1]&paged=$matches[2]\";s:35:\"(.?.+?)/comment-page-([0-9]{1,})/?$\";s:48:\"index.php?pagename=$matches[1]&cpage=$matches[2]\";s:20:\"(.?.+?)(/[0-9]+)?/?$\";s:47:\"index.php?pagename=$matches[1]&page=$matches[2]\";s:31:\".+?/[^/]+/attachment/([^/]+)/?$\";s:32:\"index.php?attachment=$matches[1]\";s:41:\".+?/[^/]+/attachment/([^/]+)/trackback/?$\";s:37:\"index.php?attachment=$matches[1]&tb=1\";s:61:\".+?/[^/]+/attachment/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:56:\".+?/[^/]+/attachment/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:56:\".+?/[^/]+/attachment/([^/]+)/comment-page-([0-9]{1,})/?$\";s:50:\"index.php?attachment=$matches[1]&cpage=$matches[2]\";s:26:\"(.+?)/([^/]+)/trackback/?$\";s:57:\"index.php?category_name=$matches[1]&name=$matches[2]&tb=1\";s:46:\"(.+?)/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:69:\"index.php?category_name=$matches[1]&name=$matches[2]&feed=$matches[3]\";s:41:\"(.+?)/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:69:\"index.php?category_name=$matches[1]&name=$matches[2]&feed=$matches[3]\";s:34:\"(.+?)/([^/]+)/page/?([0-9]{1,})/?$\";s:70:\"index.php?category_name=$matches[1]&name=$matches[2]&paged=$matches[3]\";s:41:\"(.+?)/([^/]+)/comment-page-([0-9]{1,})/?$\";s:70:\"index.php?category_name=$matches[1]&name=$matches[2]&cpage=$matches[3]\";s:26:\"(.+?)/([^/]+)(/[0-9]+)?/?$\";s:69:\"index.php?category_name=$matches[1]&name=$matches[2]&page=$matches[3]\";s:20:\".+?/[^/]+/([^/]+)/?$\";s:32:\"index.php?attachment=$matches[1]\";s:30:\".+?/[^/]+/([^/]+)/trackback/?$\";s:37:\"index.php?attachment=$matches[1]&tb=1\";s:50:\".+?/[^/]+/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:45:\".+?/[^/]+/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:45:\".+?/[^/]+/([^/]+)/comment-page-([0-9]{1,})/?$\";s:50:\"index.php?attachment=$matches[1]&cpage=$matches[2]\";s:38:\"(.+?)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:52:\"index.php?category_name=$matches[1]&feed=$matches[2]\";s:33:\"(.+?)/(feed|rdf|rss|rss2|atom)/?$\";s:52:\"index.php?category_name=$matches[1]&feed=$matches[2]\";s:26:\"(.+?)/page/?([0-9]{1,})/?$\";s:53:\"index.php?category_name=$matches[1]&paged=$matches[2]\";s:33:\"(.+?)/comment-page-([0-9]{1,})/?$\";s:53:\"index.php?category_name=$matches[1]&cpage=$matches[2]\";s:8:\"(.+?)/?$\";s:35:\"index.php?category_name=$matches[1]\";}','yes');
/*!40000 ALTER TABLE `wp_options` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `wp_postmeta`
--

DROP TABLE IF EXISTS `wp_postmeta`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `wp_postmeta` (
  `meta_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `post_id` bigint(20) unsigned NOT NULL DEFAULT '0',
  `meta_key` varchar(255) DEFAULT NULL,
  `meta_value` longtext,
  PRIMARY KEY (`meta_id`),
  KEY `post_id` (`post_id`),
  KEY `meta_key` (`meta_key`)
) ENGINE=InnoDB AUTO_INCREMENT=1158 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `wp_postmeta`
--

LOCK TABLES `wp_postmeta` WRITE;
/*!40000 ALTER TABLE `wp_postmeta` DISABLE KEYS */;
INSERT INTO `wp_postmeta` VALUES (1,2,'_wp_page_template','default');
INSERT INTO `wp_postmeta` VALUES (74,12,'_edit_last','1');
INSERT INTO `wp_postmeta` VALUES (75,12,'_edit_lock','1402081666:1');
INSERT INTO `wp_postmeta` VALUES (76,12,'_wp_page_template','default');
INSERT INTO `wp_postmeta` VALUES (77,14,'_menu_item_type','post_type');
INSERT INTO `wp_postmeta` VALUES (78,14,'_menu_item_menu_item_parent','0');
INSERT INTO `wp_postmeta` VALUES (79,14,'_menu_item_object_id','12');
INSERT INTO `wp_postmeta` VALUES (80,14,'_menu_item_object','page');
INSERT INTO `wp_postmeta` VALUES (81,14,'_menu_item_target','');
INSERT INTO `wp_postmeta` VALUES (82,14,'_menu_item_classes','a:1:{i:0;s:0:\"\";}');
INSERT INTO `wp_postmeta` VALUES (83,14,'_menu_item_xfn','');
INSERT INTO `wp_postmeta` VALUES (84,14,'_menu_item_url','');
INSERT INTO `wp_postmeta` VALUES (86,15,'_edit_last','1');
INSERT INTO `wp_postmeta` VALUES (87,15,'_edit_lock','1401418772:1');
INSERT INTO `wp_postmeta` VALUES (96,19,'_edit_last','1');
INSERT INTO `wp_postmeta` VALUES (97,19,'_edit_lock','1401418635:1');
INSERT INTO `wp_postmeta` VALUES (102,22,'_wp_attached_file','2014/05/featured-image.jpg');
INSERT INTO `wp_postmeta` VALUES (103,22,'_wp_attachment_metadata','a:5:{s:5:\"width\";i:2000;s:6:\"height\";i:2000;s:4:\"file\";s:26:\"2014/05/featured-image.jpg\";s:5:\"sizes\";a:12:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:26:\"featured-image-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:6:\"medium\";a:4:{s:4:\"file\";s:26:\"featured-image-300x300.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:300;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:5:\"large\";a:4:{s:4:\"file\";s:28:\"featured-image-1024x1024.jpg\";s:5:\"width\";i:1024;s:6:\"height\";i:1024;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:4:\"tiny\";a:4:{s:4:\"file\";s:24:\"featured-image-52x52.jpg\";s:5:\"width\";i:52;s:6:\"height\";i:52;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:10:\"post-large\";a:4:{s:4:\"file\";s:28:\"featured-image-2000x1100.jpg\";s:5:\"width\";i:2000;s:6:\"height\";i:1100;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:11:\"post-medium\";a:4:{s:4:\"file\";s:26:\"featured-image-620x292.jpg\";s:5:\"width\";i:620;s:6:\"height\";i:292;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:10:\"post-small\";a:4:{s:4:\"file\";s:26:\"featured-image-352x184.jpg\";s:5:\"width\";i:352;s:6:\"height\";i:184;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:14:\"sponsor-medium\";a:4:{s:4:\"file\";s:26:\"featured-image-620x310.jpg\";s:5:\"width\";i:620;s:6:\"height\";i:310;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:15:\"sponsor-sidebar\";a:4:{s:4:\"file\";s:26:\"featured-image-300x252.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:252;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:11:\"album-cover\";a:4:{s:4:\"file\";s:26:\"featured-image-250x250.jpg\";s:5:\"width\";i:250;s:6:\"height\";i:250;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:13:\"profile-image\";a:4:{s:4:\"file\";s:26:\"featured-image-285x285.jpg\";s:5:\"width\";i:285;s:6:\"height\";i:285;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:13:\"product-image\";a:4:{s:4:\"file\";s:26:\"featured-image-450x450.jpg\";s:5:\"width\";i:450;s:6:\"height\";i:450;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:10:{s:8:\"aperture\";i:0;s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";i:0;s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";i:0;s:3:\"iso\";i:0;s:13:\"shutter_speed\";i:0;s:5:\"title\";s:0:\"\";}}');
INSERT INTO `wp_postmeta` VALUES (110,23,'_edit_last','1');
INSERT INTO `wp_postmeta` VALUES (111,23,'field_538740c406719','a:8:{s:3:\"key\";s:19:\"field_538740c406719\";s:5:\"label\";s:8:\"Defaults\";s:4:\"name\";s:0:\"\";s:4:\"type\";s:3:\"tab\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";s:1:\"0\";s:17:\"conditional_logic\";a:3:{s:6:\"status\";s:1:\"0\";s:5:\"rules\";a:1:{i:0;a:3:{s:5:\"field\";s:4:\"null\";s:8:\"operator\";s:2:\"==\";s:5:\"value\";s:0:\"\";}}s:8:\"allorany\";s:3:\"all\";}s:8:\"order_no\";i:0;}');
INSERT INTO `wp_postmeta` VALUES (112,23,'field_538740ce0671a','a:11:{s:3:\"key\";s:19:\"field_538740ce0671a\";s:5:\"label\";s:18:\"Default Post Image\";s:4:\"name\";s:18:\"default_post_image\";s:4:\"type\";s:5:\"image\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";s:1:\"0\";s:11:\"save_format\";s:6:\"object\";s:12:\"preview_size\";s:9:\"thumbnail\";s:7:\"library\";s:3:\"all\";s:17:\"conditional_logic\";a:3:{s:6:\"status\";s:1:\"0\";s:5:\"rules\";a:1:{i:0;a:3:{s:5:\"field\";s:4:\"null\";s:8:\"operator\";s:2:\"==\";s:5:\"value\";s:0:\"\";}}s:8:\"allorany\";s:3:\"all\";}s:8:\"order_no\";i:1;}');
INSERT INTO `wp_postmeta` VALUES (114,23,'position','normal');
INSERT INTO `wp_postmeta` VALUES (115,23,'layout','no_box');
INSERT INTO `wp_postmeta` VALUES (116,23,'hide_on_screen','');
INSERT INTO `wp_postmeta` VALUES (117,23,'_edit_lock','1401804400:1');
INSERT INTO `wp_postmeta` VALUES (120,24,'_edit_last','1');
INSERT INTO `wp_postmeta` VALUES (121,24,'field_538746bc83005','a:8:{s:3:\"key\";s:19:\"field_538746bc83005\";s:5:\"label\";s:4:\"Home\";s:4:\"name\";s:0:\"\";s:4:\"type\";s:3:\"tab\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";s:1:\"0\";s:17:\"conditional_logic\";a:3:{s:6:\"status\";s:1:\"0\";s:5:\"rules\";a:1:{i:0;a:3:{s:5:\"field\";s:4:\"null\";s:8:\"operator\";s:2:\"==\";s:5:\"value\";s:0:\"\";}}s:8:\"allorany\";s:3:\"all\";}s:8:\"order_no\";i:0;}');
INSERT INTO `wp_postmeta` VALUES (122,24,'field_5387470583006','a:13:{s:3:\"key\";s:19:\"field_5387470583006\";s:5:\"label\";s:3:\"Ads\";s:4:\"name\";s:8:\"home_ads\";s:4:\"type\";s:8:\"repeater\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";s:1:\"0\";s:10:\"sub_fields\";a:2:{i:0;a:12:{s:3:\"key\";s:19:\"field_5387471a83007\";s:5:\"label\";s:5:\"Image\";s:4:\"name\";s:5:\"image\";s:4:\"type\";s:5:\"image\";s:12:\"instructions\";s:75:\"Images must be at least 620px by 310px for proper cropping and proportions.\";s:8:\"required\";s:1:\"0\";s:12:\"column_width\";s:0:\"\";s:11:\"save_format\";s:6:\"object\";s:12:\"preview_size\";s:9:\"thumbnail\";s:7:\"library\";s:3:\"all\";s:17:\"conditional_logic\";a:3:{s:6:\"status\";s:1:\"0\";s:5:\"rules\";a:1:{i:0;a:3:{s:5:\"field\";s:4:\"null\";s:8:\"operator\";s:2:\"==\";s:5:\"value\";s:0:\"\";}}s:8:\"allorany\";s:3:\"all\";}s:8:\"order_no\";i:0;}i:1;a:15:{s:3:\"key\";s:19:\"field_5387474183008\";s:5:\"label\";s:4:\"Link\";s:4:\"name\";s:4:\"link\";s:4:\"type\";s:4:\"text\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";s:1:\"0\";s:12:\"column_width\";s:0:\"\";s:13:\"default_value\";s:0:\"\";s:11:\"placeholder\";s:0:\"\";s:7:\"prepend\";s:0:\"\";s:6:\"append\";s:0:\"\";s:10:\"formatting\";s:4:\"html\";s:9:\"maxlength\";s:0:\"\";s:17:\"conditional_logic\";a:3:{s:6:\"status\";s:1:\"0\";s:5:\"rules\";a:1:{i:0;a:3:{s:5:\"field\";s:4:\"null\";s:8:\"operator\";s:2:\"==\";s:5:\"value\";s:0:\"\";}}s:8:\"allorany\";s:3:\"all\";}s:8:\"order_no\";i:1;}}s:7:\"row_min\";s:1:\"2\";s:9:\"row_limit\";s:1:\"2\";s:6:\"layout\";s:5:\"table\";s:12:\"button_label\";s:11:\"Add Sponsor\";s:17:\"conditional_logic\";a:3:{s:6:\"status\";s:1:\"0\";s:5:\"rules\";a:1:{i:0;a:3:{s:5:\"field\";s:4:\"null\";s:8:\"operator\";s:2:\"==\";s:5:\"value\";s:0:\"\";}}s:8:\"allorany\";s:3:\"all\";}s:8:\"order_no\";i:1;}');
INSERT INTO `wp_postmeta` VALUES (124,24,'position','normal');
INSERT INTO `wp_postmeta` VALUES (125,24,'layout','no_box');
INSERT INTO `wp_postmeta` VALUES (126,24,'hide_on_screen','');
INSERT INTO `wp_postmeta` VALUES (127,24,'_edit_lock','1401457772:1');
INSERT INTO `wp_postmeta` VALUES (136,27,'_wp_attached_file','2014/05/sponsor-relevant.jpg');
INSERT INTO `wp_postmeta` VALUES (137,27,'_wp_attachment_metadata','a:5:{s:5:\"width\";i:620;s:6:\"height\";i:310;s:4:\"file\";s:28:\"2014/05/sponsor-relevant.jpg\";s:5:\"sizes\";a:9:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:28:\"sponsor-relevant-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:6:\"medium\";a:4:{s:4:\"file\";s:28:\"sponsor-relevant-300x150.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:4:\"tiny\";a:4:{s:4:\"file\";s:26:\"sponsor-relevant-52x52.jpg\";s:5:\"width\";i:52;s:6:\"height\";i:52;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:11:\"post-medium\";a:4:{s:4:\"file\";s:28:\"sponsor-relevant-620x292.jpg\";s:5:\"width\";i:620;s:6:\"height\";i:292;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:10:\"post-small\";a:4:{s:4:\"file\";s:28:\"sponsor-relevant-352x184.jpg\";s:5:\"width\";i:352;s:6:\"height\";i:184;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:15:\"sponsor-sidebar\";a:4:{s:4:\"file\";s:28:\"sponsor-relevant-300x252.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:252;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:11:\"album-cover\";a:4:{s:4:\"file\";s:28:\"sponsor-relevant-250x250.jpg\";s:5:\"width\";i:250;s:6:\"height\";i:250;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:13:\"profile-image\";a:4:{s:4:\"file\";s:28:\"sponsor-relevant-285x285.jpg\";s:5:\"width\";i:285;s:6:\"height\";i:285;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:13:\"product-image\";a:4:{s:4:\"file\";s:28:\"sponsor-relevant-450x310.jpg\";s:5:\"width\";i:450;s:6:\"height\";i:310;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:10:{s:8:\"aperture\";i:0;s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";i:0;s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";i:0;s:3:\"iso\";i:0;s:13:\"shutter_speed\";i:0;s:5:\"title\";s:0:\"\";}}');
INSERT INTO `wp_postmeta` VALUES (138,28,'_wp_attached_file','2014/05/sponsor-soworthloving.jpg');
INSERT INTO `wp_postmeta` VALUES (139,28,'_wp_attachment_metadata','a:5:{s:5:\"width\";i:620;s:6:\"height\";i:310;s:4:\"file\";s:33:\"2014/05/sponsor-soworthloving.jpg\";s:5:\"sizes\";a:9:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:33:\"sponsor-soworthloving-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:6:\"medium\";a:4:{s:4:\"file\";s:33:\"sponsor-soworthloving-300x150.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:4:\"tiny\";a:4:{s:4:\"file\";s:31:\"sponsor-soworthloving-52x52.jpg\";s:5:\"width\";i:52;s:6:\"height\";i:52;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:11:\"post-medium\";a:4:{s:4:\"file\";s:33:\"sponsor-soworthloving-620x292.jpg\";s:5:\"width\";i:620;s:6:\"height\";i:292;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:10:\"post-small\";a:4:{s:4:\"file\";s:33:\"sponsor-soworthloving-352x184.jpg\";s:5:\"width\";i:352;s:6:\"height\";i:184;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:15:\"sponsor-sidebar\";a:4:{s:4:\"file\";s:33:\"sponsor-soworthloving-300x252.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:252;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:11:\"album-cover\";a:4:{s:4:\"file\";s:33:\"sponsor-soworthloving-250x250.jpg\";s:5:\"width\";i:250;s:6:\"height\";i:250;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:13:\"profile-image\";a:4:{s:4:\"file\";s:33:\"sponsor-soworthloving-285x285.jpg\";s:5:\"width\";i:285;s:6:\"height\";i:285;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:13:\"product-image\";a:4:{s:4:\"file\";s:33:\"sponsor-soworthloving-450x310.jpg\";s:5:\"width\";i:450;s:6:\"height\";i:310;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:10:{s:8:\"aperture\";i:0;s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";i:0;s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";i:0;s:3:\"iso\";i:0;s:13:\"shutter_speed\";i:0;s:5:\"title\";s:0:\"\";}}');
INSERT INTO `wp_postmeta` VALUES (145,30,'_wp_attached_file','2014/05/featured-image-kye-kye.jpg');
INSERT INTO `wp_postmeta` VALUES (146,30,'_wp_attachment_metadata','a:5:{s:5:\"width\";i:960;s:6:\"height\";i:404;s:4:\"file\";s:34:\"2014/05/featured-image-kye-kye.jpg\";s:5:\"sizes\";a:10:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:34:\"featured-image-kye-kye-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:6:\"medium\";a:4:{s:4:\"file\";s:34:\"featured-image-kye-kye-300x126.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:126;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:4:\"tiny\";a:4:{s:4:\"file\";s:32:\"featured-image-kye-kye-52x52.jpg\";s:5:\"width\";i:52;s:6:\"height\";i:52;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:11:\"post-medium\";a:4:{s:4:\"file\";s:34:\"featured-image-kye-kye-620x292.jpg\";s:5:\"width\";i:620;s:6:\"height\";i:292;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:10:\"post-small\";a:4:{s:4:\"file\";s:34:\"featured-image-kye-kye-352x184.jpg\";s:5:\"width\";i:352;s:6:\"height\";i:184;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:14:\"sponsor-medium\";a:4:{s:4:\"file\";s:34:\"featured-image-kye-kye-620x310.jpg\";s:5:\"width\";i:620;s:6:\"height\";i:310;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:15:\"sponsor-sidebar\";a:4:{s:4:\"file\";s:34:\"featured-image-kye-kye-300x252.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:252;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:11:\"album-cover\";a:4:{s:4:\"file\";s:34:\"featured-image-kye-kye-250x250.jpg\";s:5:\"width\";i:250;s:6:\"height\";i:250;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:13:\"profile-image\";a:4:{s:4:\"file\";s:34:\"featured-image-kye-kye-285x285.jpg\";s:5:\"width\";i:285;s:6:\"height\";i:285;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:13:\"product-image\";a:4:{s:4:\"file\";s:34:\"featured-image-kye-kye-450x404.jpg\";s:5:\"width\";i:450;s:6:\"height\";i:404;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:10:{s:8:\"aperture\";i:0;s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";i:0;s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";i:0;s:3:\"iso\";i:0;s:13:\"shutter_speed\";i:0;s:5:\"title\";s:0:\"\";}}');
INSERT INTO `wp_postmeta` VALUES (147,15,'_thumbnail_id','30');
INSERT INTO `wp_postmeta` VALUES (150,31,'_edit_last','1');
INSERT INTO `wp_postmeta` VALUES (151,31,'field_53877770a24d7','a:11:{s:3:\"key\";s:19:\"field_53877770a24d7\";s:5:\"label\";s:10:\"User Image\";s:4:\"name\";s:10:\"user_image\";s:4:\"type\";s:5:\"image\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";s:1:\"0\";s:11:\"save_format\";s:6:\"object\";s:12:\"preview_size\";s:9:\"thumbnail\";s:7:\"library\";s:3:\"all\";s:17:\"conditional_logic\";a:3:{s:6:\"status\";s:1:\"0\";s:5:\"rules\";a:1:{i:0;a:2:{s:5:\"field\";s:4:\"null\";s:8:\"operator\";s:2:\"==\";}}s:8:\"allorany\";s:3:\"all\";}s:8:\"order_no\";i:0;}');
INSERT INTO `wp_postmeta` VALUES (152,31,'rule','a:5:{s:5:\"param\";s:7:\"ef_user\";s:8:\"operator\";s:2:\"==\";s:5:\"value\";s:3:\"all\";s:8:\"order_no\";i:0;s:8:\"group_no\";i:0;}');
INSERT INTO `wp_postmeta` VALUES (153,31,'position','normal');
INSERT INTO `wp_postmeta` VALUES (154,31,'layout','no_box');
INSERT INTO `wp_postmeta` VALUES (155,31,'hide_on_screen','');
INSERT INTO `wp_postmeta` VALUES (156,31,'_edit_lock','1401388063:1');
INSERT INTO `wp_postmeta` VALUES (157,24,'field_538780d184663','a:8:{s:3:\"key\";s:19:\"field_538780d184663\";s:5:\"label\";s:4:\"News\";s:4:\"name\";s:0:\"\";s:4:\"type\";s:3:\"tab\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";s:1:\"0\";s:17:\"conditional_logic\";a:3:{s:6:\"status\";s:1:\"0\";s:5:\"rules\";a:1:{i:0;a:3:{s:5:\"field\";s:4:\"null\";s:8:\"operator\";s:2:\"==\";s:5:\"value\";s:0:\"\";}}s:8:\"allorany\";s:3:\"all\";}s:8:\"order_no\";i:2;}');
INSERT INTO `wp_postmeta` VALUES (158,24,'field_538780e684664','a:13:{s:3:\"key\";s:19:\"field_538780e684664\";s:5:\"label\";s:3:\"Ads\";s:4:\"name\";s:8:\"news_ads\";s:4:\"type\";s:8:\"repeater\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";s:1:\"0\";s:10:\"sub_fields\";a:2:{i:0;a:12:{s:3:\"key\";s:19:\"field_538780e684665\";s:5:\"label\";s:5:\"Image\";s:4:\"name\";s:5:\"image\";s:4:\"type\";s:5:\"image\";s:12:\"instructions\";s:75:\"Images must be at least 620px by 310px for proper cropping and proportions.\";s:8:\"required\";s:1:\"0\";s:12:\"column_width\";s:0:\"\";s:11:\"save_format\";s:6:\"object\";s:12:\"preview_size\";s:9:\"thumbnail\";s:7:\"library\";s:3:\"all\";s:17:\"conditional_logic\";a:3:{s:6:\"status\";s:1:\"0\";s:5:\"rules\";a:1:{i:0;a:3:{s:5:\"field\";s:4:\"null\";s:8:\"operator\";s:2:\"==\";s:5:\"value\";s:0:\"\";}}s:8:\"allorany\";s:3:\"all\";}s:8:\"order_no\";i:0;}i:1;a:15:{s:3:\"key\";s:19:\"field_538780e684666\";s:5:\"label\";s:4:\"Link\";s:4:\"name\";s:4:\"link\";s:4:\"type\";s:4:\"text\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";s:1:\"0\";s:12:\"column_width\";s:0:\"\";s:13:\"default_value\";s:0:\"\";s:11:\"placeholder\";s:0:\"\";s:7:\"prepend\";s:0:\"\";s:6:\"append\";s:0:\"\";s:10:\"formatting\";s:4:\"html\";s:9:\"maxlength\";s:0:\"\";s:17:\"conditional_logic\";a:3:{s:6:\"status\";s:1:\"0\";s:5:\"rules\";a:1:{i:0;a:3:{s:5:\"field\";s:4:\"null\";s:8:\"operator\";s:2:\"==\";s:5:\"value\";s:0:\"\";}}s:8:\"allorany\";s:3:\"all\";}s:8:\"order_no\";i:1;}}s:7:\"row_min\";s:1:\"2\";s:9:\"row_limit\";s:1:\"2\";s:6:\"layout\";s:5:\"table\";s:12:\"button_label\";s:11:\"Add Sponsor\";s:17:\"conditional_logic\";a:3:{s:6:\"status\";s:1:\"0\";s:5:\"rules\";a:1:{i:0;a:3:{s:5:\"field\";s:4:\"null\";s:8:\"operator\";s:2:\"==\";s:5:\"value\";s:0:\"\";}}s:8:\"allorany\";s:3:\"all\";}s:8:\"order_no\";i:3;}');
INSERT INTO `wp_postmeta` VALUES (170,1,'_edit_lock','1402077314:1');
INSERT INTO `wp_postmeta` VALUES (172,1,'_edit_last','1');
INSERT INTO `wp_postmeta` VALUES (179,41,'_edit_last','2');
INSERT INTO `wp_postmeta` VALUES (180,41,'_edit_lock','1401465083:2');
INSERT INTO `wp_postmeta` VALUES (183,43,'_wp_attached_file','2014/05/featured-image-dummy.jpg');
INSERT INTO `wp_postmeta` VALUES (184,43,'_wp_attachment_metadata','a:5:{s:5:\"width\";i:1920;s:6:\"height\";i:1920;s:4:\"file\";s:32:\"2014/05/featured-image-dummy.jpg\";s:5:\"sizes\";a:12:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:32:\"featured-image-dummy-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:6:\"medium\";a:4:{s:4:\"file\";s:32:\"featured-image-dummy-300x300.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:300;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:5:\"large\";a:4:{s:4:\"file\";s:34:\"featured-image-dummy-1024x1024.jpg\";s:5:\"width\";i:1024;s:6:\"height\";i:1024;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:4:\"tiny\";a:4:{s:4:\"file\";s:30:\"featured-image-dummy-52x52.jpg\";s:5:\"width\";i:52;s:6:\"height\";i:52;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:10:\"post-large\";a:4:{s:4:\"file\";s:34:\"featured-image-dummy-1920x1100.jpg\";s:5:\"width\";i:1920;s:6:\"height\";i:1100;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:11:\"post-medium\";a:4:{s:4:\"file\";s:32:\"featured-image-dummy-620x292.jpg\";s:5:\"width\";i:620;s:6:\"height\";i:292;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:10:\"post-small\";a:4:{s:4:\"file\";s:32:\"featured-image-dummy-352x184.jpg\";s:5:\"width\";i:352;s:6:\"height\";i:184;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:14:\"sponsor-medium\";a:4:{s:4:\"file\";s:32:\"featured-image-dummy-620x310.jpg\";s:5:\"width\";i:620;s:6:\"height\";i:310;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:15:\"sponsor-sidebar\";a:4:{s:4:\"file\";s:32:\"featured-image-dummy-300x252.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:252;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:11:\"album-cover\";a:4:{s:4:\"file\";s:32:\"featured-image-dummy-250x250.jpg\";s:5:\"width\";i:250;s:6:\"height\";i:250;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:13:\"profile-image\";a:4:{s:4:\"file\";s:32:\"featured-image-dummy-285x285.jpg\";s:5:\"width\";i:285;s:6:\"height\";i:285;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:13:\"product-image\";a:4:{s:4:\"file\";s:32:\"featured-image-dummy-450x450.jpg\";s:5:\"width\";i:450;s:6:\"height\";i:450;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:10:{s:8:\"aperture\";i:0;s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";i:0;s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";i:0;s:3:\"iso\";i:0;s:13:\"shutter_speed\";i:0;s:5:\"title\";s:0:\"\";}}');
INSERT INTO `wp_postmeta` VALUES (185,41,'_thumbnail_id','43');
INSERT INTO `wp_postmeta` VALUES (188,2,'_wp_trash_meta_status','publish');
INSERT INTO `wp_postmeta` VALUES (189,2,'_wp_trash_meta_time','1401419664');
INSERT INTO `wp_postmeta` VALUES (190,45,'_edit_last','1');
INSERT INTO `wp_postmeta` VALUES (191,45,'_wp_page_template','page-news.php');
INSERT INTO `wp_postmeta` VALUES (192,45,'_edit_lock','1401454641:1');
INSERT INTO `wp_postmeta` VALUES (193,47,'_edit_last','1');
INSERT INTO `wp_postmeta` VALUES (194,47,'_wp_page_template','page-watch.php');
INSERT INTO `wp_postmeta` VALUES (195,47,'_edit_lock','1402083034:1');
INSERT INTO `wp_postmeta` VALUES (196,49,'_menu_item_type','post_type');
INSERT INTO `wp_postmeta` VALUES (197,49,'_menu_item_menu_item_parent','0');
INSERT INTO `wp_postmeta` VALUES (198,49,'_menu_item_object_id','47');
INSERT INTO `wp_postmeta` VALUES (199,49,'_menu_item_object','page');
INSERT INTO `wp_postmeta` VALUES (200,49,'_menu_item_target','');
INSERT INTO `wp_postmeta` VALUES (201,49,'_menu_item_classes','a:1:{i:0;s:0:\"\";}');
INSERT INTO `wp_postmeta` VALUES (202,49,'_menu_item_xfn','');
INSERT INTO `wp_postmeta` VALUES (203,49,'_menu_item_url','');
INSERT INTO `wp_postmeta` VALUES (205,50,'_menu_item_type','post_type');
INSERT INTO `wp_postmeta` VALUES (206,50,'_menu_item_menu_item_parent','0');
INSERT INTO `wp_postmeta` VALUES (207,50,'_menu_item_object_id','45');
INSERT INTO `wp_postmeta` VALUES (208,50,'_menu_item_object','page');
INSERT INTO `wp_postmeta` VALUES (209,50,'_menu_item_target','');
INSERT INTO `wp_postmeta` VALUES (210,50,'_menu_item_classes','a:1:{i:0;s:0:\"\";}');
INSERT INTO `wp_postmeta` VALUES (211,50,'_menu_item_xfn','');
INSERT INTO `wp_postmeta` VALUES (212,50,'_menu_item_url','');
INSERT INTO `wp_postmeta` VALUES (214,51,'_menu_item_type','taxonomy');
INSERT INTO `wp_postmeta` VALUES (215,51,'_menu_item_menu_item_parent','0');
INSERT INTO `wp_postmeta` VALUES (216,51,'_menu_item_object_id','4');
INSERT INTO `wp_postmeta` VALUES (217,51,'_menu_item_object','post_tag');
INSERT INTO `wp_postmeta` VALUES (218,51,'_menu_item_target','');
INSERT INTO `wp_postmeta` VALUES (219,51,'_menu_item_classes','a:1:{i:0;s:0:\"\";}');
INSERT INTO `wp_postmeta` VALUES (220,51,'_menu_item_xfn','');
INSERT INTO `wp_postmeta` VALUES (221,51,'_menu_item_url','');
INSERT INTO `wp_postmeta` VALUES (223,52,'_menu_item_type','taxonomy');
INSERT INTO `wp_postmeta` VALUES (224,52,'_menu_item_menu_item_parent','0');
INSERT INTO `wp_postmeta` VALUES (225,52,'_menu_item_object_id','5');
INSERT INTO `wp_postmeta` VALUES (226,52,'_menu_item_object','post_tag');
INSERT INTO `wp_postmeta` VALUES (227,52,'_menu_item_target','');
INSERT INTO `wp_postmeta` VALUES (228,52,'_menu_item_classes','a:1:{i:0;s:0:\"\";}');
INSERT INTO `wp_postmeta` VALUES (229,52,'_menu_item_xfn','');
INSERT INTO `wp_postmeta` VALUES (230,52,'_menu_item_url','');
INSERT INTO `wp_postmeta` VALUES (232,53,'_menu_item_type','taxonomy');
INSERT INTO `wp_postmeta` VALUES (233,53,'_menu_item_menu_item_parent','0');
INSERT INTO `wp_postmeta` VALUES (234,53,'_menu_item_object_id','1');
INSERT INTO `wp_postmeta` VALUES (235,53,'_menu_item_object','category');
INSERT INTO `wp_postmeta` VALUES (236,53,'_menu_item_target','');
INSERT INTO `wp_postmeta` VALUES (237,53,'_menu_item_classes','a:1:{i:0;s:0:\"\";}');
INSERT INTO `wp_postmeta` VALUES (238,53,'_menu_item_xfn','');
INSERT INTO `wp_postmeta` VALUES (239,53,'_menu_item_url','');
INSERT INTO `wp_postmeta` VALUES (241,54,'_menu_item_type','taxonomy');
INSERT INTO `wp_postmeta` VALUES (242,54,'_menu_item_menu_item_parent','0');
INSERT INTO `wp_postmeta` VALUES (243,54,'_menu_item_object_id','3');
INSERT INTO `wp_postmeta` VALUES (244,54,'_menu_item_object','category');
INSERT INTO `wp_postmeta` VALUES (245,54,'_menu_item_target','');
INSERT INTO `wp_postmeta` VALUES (246,54,'_menu_item_classes','a:1:{i:0;s:0:\"\";}');
INSERT INTO `wp_postmeta` VALUES (247,54,'_menu_item_xfn','');
INSERT INTO `wp_postmeta` VALUES (248,54,'_menu_item_url','');
INSERT INTO `wp_postmeta` VALUES (250,23,'field_53881956b5ee1','a:8:{s:3:\"key\";s:19:\"field_53881956b5ee1\";s:5:\"label\";s:6:\"Social\";s:4:\"name\";s:0:\"\";s:4:\"type\";s:3:\"tab\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";s:1:\"0\";s:17:\"conditional_logic\";a:3:{s:6:\"status\";s:1:\"0\";s:5:\"rules\";a:1:{i:0;a:3:{s:5:\"field\";s:4:\"null\";s:8:\"operator\";s:2:\"==\";s:5:\"value\";s:0:\"\";}}s:8:\"allorany\";s:3:\"all\";}s:8:\"order_no\";i:5;}');
INSERT INTO `wp_postmeta` VALUES (251,23,'field_5388195db5ee2','a:13:{s:3:\"key\";s:19:\"field_5388195db5ee2\";s:5:\"label\";s:6:\"Social\";s:4:\"name\";s:6:\"social\";s:4:\"type\";s:8:\"repeater\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";s:1:\"0\";s:10:\"sub_fields\";a:2:{i:0;a:12:{s:3:\"key\";s:19:\"field_53881969b5ee3\";s:5:\"label\";s:4:\"Icon\";s:4:\"name\";s:4:\"icon\";s:4:\"type\";s:5:\"image\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";s:1:\"0\";s:12:\"column_width\";s:0:\"\";s:11:\"save_format\";s:6:\"object\";s:12:\"preview_size\";s:9:\"thumbnail\";s:7:\"library\";s:3:\"all\";s:17:\"conditional_logic\";a:3:{s:6:\"status\";s:1:\"0\";s:5:\"rules\";a:1:{i:0;a:3:{s:5:\"field\";s:4:\"null\";s:8:\"operator\";s:2:\"==\";s:5:\"value\";s:0:\"\";}}s:8:\"allorany\";s:3:\"all\";}s:8:\"order_no\";i:0;}i:1;a:15:{s:3:\"key\";s:19:\"field_5388197cb5ee4\";s:5:\"label\";s:4:\"Link\";s:4:\"name\";s:4:\"link\";s:4:\"type\";s:4:\"text\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";s:1:\"0\";s:12:\"column_width\";s:0:\"\";s:13:\"default_value\";s:0:\"\";s:11:\"placeholder\";s:0:\"\";s:7:\"prepend\";s:0:\"\";s:6:\"append\";s:0:\"\";s:10:\"formatting\";s:4:\"html\";s:9:\"maxlength\";s:0:\"\";s:17:\"conditional_logic\";a:3:{s:6:\"status\";s:1:\"0\";s:5:\"rules\";a:1:{i:0;a:3:{s:5:\"field\";s:4:\"null\";s:8:\"operator\";s:2:\"==\";s:5:\"value\";s:0:\"\";}}s:8:\"allorany\";s:3:\"all\";}s:8:\"order_no\";i:1;}}s:7:\"row_min\";s:0:\"\";s:9:\"row_limit\";s:0:\"\";s:6:\"layout\";s:5:\"table\";s:12:\"button_label\";s:15:\"Add Social Icon\";s:17:\"conditional_logic\";a:3:{s:6:\"status\";s:1:\"0\";s:5:\"rules\";a:1:{i:0;a:3:{s:5:\"field\";s:4:\"null\";s:8:\"operator\";s:2:\"==\";s:5:\"value\";s:0:\"\";}}s:8:\"allorany\";s:3:\"all\";}s:8:\"order_no\";i:6;}');
INSERT INTO `wp_postmeta` VALUES (253,56,'_wp_attached_file','2014/05/ico-block-fb.jpg');
INSERT INTO `wp_postmeta` VALUES (254,56,'_wp_attachment_metadata','a:5:{s:5:\"width\";i:70;s:6:\"height\";i:70;s:4:\"file\";s:24:\"2014/05/ico-block-fb.jpg\";s:5:\"sizes\";a:1:{s:4:\"tiny\";a:4:{s:4:\"file\";s:22:\"ico-block-fb-52x52.jpg\";s:5:\"width\";i:52;s:6:\"height\";i:52;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:10:{s:8:\"aperture\";i:0;s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";i:0;s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";i:0;s:3:\"iso\";i:0;s:13:\"shutter_speed\";i:0;s:5:\"title\";s:0:\"\";}}');
INSERT INTO `wp_postmeta` VALUES (255,57,'_wp_attached_file','2014/05/ico-block-g+.jpg');
INSERT INTO `wp_postmeta` VALUES (256,57,'_wp_attachment_metadata','a:5:{s:5:\"width\";i:70;s:6:\"height\";i:70;s:4:\"file\";s:24:\"2014/05/ico-block-g+.jpg\";s:5:\"sizes\";a:1:{s:4:\"tiny\";a:4:{s:4:\"file\";s:22:\"ico-block-g+-52x52.jpg\";s:5:\"width\";i:52;s:6:\"height\";i:52;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:10:{s:8:\"aperture\";i:0;s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";i:0;s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";i:0;s:3:\"iso\";i:0;s:13:\"shutter_speed\";i:0;s:5:\"title\";s:0:\"\";}}');
INSERT INTO `wp_postmeta` VALUES (257,58,'_wp_attached_file','2014/05/ico-block-vm.jpg');
INSERT INTO `wp_postmeta` VALUES (258,58,'_wp_attachment_metadata','a:5:{s:5:\"width\";i:70;s:6:\"height\";i:70;s:4:\"file\";s:24:\"2014/05/ico-block-vm.jpg\";s:5:\"sizes\";a:1:{s:4:\"tiny\";a:4:{s:4:\"file\";s:22:\"ico-block-vm-52x52.jpg\";s:5:\"width\";i:52;s:6:\"height\";i:52;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:10:{s:8:\"aperture\";i:0;s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";i:0;s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";i:0;s:3:\"iso\";i:0;s:13:\"shutter_speed\";i:0;s:5:\"title\";s:0:\"\";}}');
INSERT INTO `wp_postmeta` VALUES (259,59,'_wp_attached_file','2014/05/ico-block-inst.jpg');
INSERT INTO `wp_postmeta` VALUES (260,59,'_wp_attachment_metadata','a:5:{s:5:\"width\";i:70;s:6:\"height\";i:70;s:4:\"file\";s:26:\"2014/05/ico-block-inst.jpg\";s:5:\"sizes\";a:1:{s:4:\"tiny\";a:4:{s:4:\"file\";s:24:\"ico-block-inst-52x52.jpg\";s:5:\"width\";i:52;s:6:\"height\";i:52;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:10:{s:8:\"aperture\";i:0;s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";i:0;s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";i:0;s:3:\"iso\";i:0;s:13:\"shutter_speed\";i:0;s:5:\"title\";s:0:\"\";}}');
INSERT INTO `wp_postmeta` VALUES (261,60,'_wp_attached_file','2014/05/ico-block-flkr.jpg');
INSERT INTO `wp_postmeta` VALUES (262,60,'_wp_attachment_metadata','a:5:{s:5:\"width\";i:70;s:6:\"height\";i:70;s:4:\"file\";s:26:\"2014/05/ico-block-flkr.jpg\";s:5:\"sizes\";a:1:{s:4:\"tiny\";a:4:{s:4:\"file\";s:24:\"ico-block-flkr-52x52.jpg\";s:5:\"width\";i:52;s:6:\"height\";i:52;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:10:{s:8:\"aperture\";i:0;s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";i:0;s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";i:0;s:3:\"iso\";i:0;s:13:\"shutter_speed\";i:0;s:5:\"title\";s:0:\"\";}}');
INSERT INTO `wp_postmeta` VALUES (263,61,'_wp_attached_file','2014/05/ico-block-tblr.jpg');
INSERT INTO `wp_postmeta` VALUES (264,61,'_wp_attachment_metadata','a:5:{s:5:\"width\";i:70;s:6:\"height\";i:70;s:4:\"file\";s:26:\"2014/05/ico-block-tblr.jpg\";s:5:\"sizes\";a:1:{s:4:\"tiny\";a:4:{s:4:\"file\";s:24:\"ico-block-tblr-52x52.jpg\";s:5:\"width\";i:52;s:6:\"height\";i:52;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:10:{s:8:\"aperture\";i:0;s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";i:0;s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";i:0;s:3:\"iso\";i:0;s:13:\"shutter_speed\";i:0;s:5:\"title\";s:0:\"\";}}');
INSERT INTO `wp_postmeta` VALUES (265,62,'_wp_attached_file','2014/05/ico-block-tw.jpg');
INSERT INTO `wp_postmeta` VALUES (266,62,'_wp_attachment_metadata','a:5:{s:5:\"width\";i:70;s:6:\"height\";i:70;s:4:\"file\";s:24:\"2014/05/ico-block-tw.jpg\";s:5:\"sizes\";a:1:{s:4:\"tiny\";a:4:{s:4:\"file\";s:22:\"ico-block-tw-52x52.jpg\";s:5:\"width\";i:52;s:6:\"height\";i:52;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:10:{s:8:\"aperture\";i:0;s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";i:0;s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";i:0;s:3:\"iso\";i:0;s:13:\"shutter_speed\";i:0;s:5:\"title\";s:0:\"\";}}');
INSERT INTO `wp_postmeta` VALUES (267,24,'field_5388776d739d2','a:13:{s:3:\"key\";s:19:\"field_5388776d739d2\";s:5:\"label\";s:3:\"Ads\";s:4:\"name\";s:11:\"sidebar_ads\";s:4:\"type\";s:8:\"repeater\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";s:1:\"0\";s:10:\"sub_fields\";a:2:{i:0;a:12:{s:3:\"key\";s:19:\"field_5388776d739d3\";s:5:\"label\";s:5:\"Image\";s:4:\"name\";s:5:\"image\";s:4:\"type\";s:5:\"image\";s:12:\"instructions\";s:75:\"Images must be at least 250px by 210px for proper cropping and proportions.\";s:8:\"required\";s:1:\"0\";s:12:\"column_width\";s:0:\"\";s:11:\"save_format\";s:6:\"object\";s:12:\"preview_size\";s:9:\"thumbnail\";s:7:\"library\";s:3:\"all\";s:17:\"conditional_logic\";a:3:{s:6:\"status\";s:1:\"0\";s:5:\"rules\";a:1:{i:0;a:3:{s:5:\"field\";s:4:\"null\";s:8:\"operator\";s:2:\"==\";s:5:\"value\";s:0:\"\";}}s:8:\"allorany\";s:3:\"all\";}s:8:\"order_no\";i:0;}i:1;a:15:{s:3:\"key\";s:19:\"field_5388776d739d4\";s:5:\"label\";s:4:\"Link\";s:4:\"name\";s:4:\"link\";s:4:\"type\";s:4:\"text\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";s:1:\"0\";s:12:\"column_width\";s:0:\"\";s:13:\"default_value\";s:0:\"\";s:11:\"placeholder\";s:0:\"\";s:7:\"prepend\";s:0:\"\";s:6:\"append\";s:0:\"\";s:10:\"formatting\";s:4:\"html\";s:9:\"maxlength\";s:0:\"\";s:17:\"conditional_logic\";a:3:{s:6:\"status\";s:1:\"0\";s:5:\"rules\";a:1:{i:0;a:3:{s:5:\"field\";s:4:\"null\";s:8:\"operator\";s:2:\"==\";s:5:\"value\";s:0:\"\";}}s:8:\"allorany\";s:3:\"all\";}s:8:\"order_no\";i:1;}}s:7:\"row_min\";s:0:\"\";s:9:\"row_limit\";s:0:\"\";s:6:\"layout\";s:5:\"table\";s:12:\"button_label\";s:11:\"Add Sponsor\";s:17:\"conditional_logic\";a:3:{s:6:\"status\";s:1:\"0\";s:5:\"rules\";a:1:{i:0;a:3:{s:5:\"field\";s:4:\"null\";s:8:\"operator\";s:2:\"==\";s:5:\"value\";s:0:\"\";}}s:8:\"allorany\";s:3:\"all\";}s:8:\"order_no\";i:5;}');
INSERT INTO `wp_postmeta` VALUES (268,24,'field_5388775c739d1','a:8:{s:3:\"key\";s:19:\"field_5388775c739d1\";s:5:\"label\";s:7:\"Sidebar\";s:4:\"name\";s:0:\"\";s:4:\"type\";s:3:\"tab\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";s:1:\"0\";s:17:\"conditional_logic\";a:3:{s:6:\"status\";s:1:\"0\";s:5:\"rules\";a:1:{i:0;a:3:{s:5:\"field\";s:4:\"null\";s:8:\"operator\";s:2:\"==\";s:5:\"value\";s:0:\"\";}}s:8:\"allorany\";s:3:\"all\";}s:8:\"order_no\";i:4;}');
INSERT INTO `wp_postmeta` VALUES (277,65,'_wp_attached_file','2014/05/sponsor-whatlife.jpg');
INSERT INTO `wp_postmeta` VALUES (278,65,'_wp_attachment_metadata','a:5:{s:5:\"width\";i:300;s:6:\"height\";i:252;s:4:\"file\";s:28:\"2014/05/sponsor-whatlife.jpg\";s:5:\"sizes\";a:5:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:28:\"sponsor-whatlife-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:4:\"tiny\";a:4:{s:4:\"file\";s:26:\"sponsor-whatlife-52x52.jpg\";s:5:\"width\";i:52;s:6:\"height\";i:52;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:10:\"post-small\";a:4:{s:4:\"file\";s:28:\"sponsor-whatlife-300x184.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:184;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:11:\"album-cover\";a:4:{s:4:\"file\";s:28:\"sponsor-whatlife-250x250.jpg\";s:5:\"width\";i:250;s:6:\"height\";i:250;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:13:\"profile-image\";a:4:{s:4:\"file\";s:28:\"sponsor-whatlife-285x252.jpg\";s:5:\"width\";i:285;s:6:\"height\";i:252;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:10:{s:8:\"aperture\";i:0;s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";i:0;s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";i:0;s:3:\"iso\";i:0;s:13:\"shutter_speed\";i:0;s:5:\"title\";s:0:\"\";}}');
INSERT INTO `wp_postmeta` VALUES (279,66,'_edit_last','1');
INSERT INTO `wp_postmeta` VALUES (280,66,'_edit_lock','1401457168:1');
INSERT INTO `wp_postmeta` VALUES (283,68,'_edit_last','1');
INSERT INTO `wp_postmeta` VALUES (284,68,'field_538885f2ebadf','a:14:{s:3:\"key\";s:19:\"field_538885f2ebadf\";s:5:\"label\";s:9:\"Video URL\";s:4:\"name\";s:9:\"video_url\";s:4:\"type\";s:4:\"text\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";s:1:\"0\";s:13:\"default_value\";s:0:\"\";s:11:\"placeholder\";s:0:\"\";s:7:\"prepend\";s:0:\"\";s:6:\"append\";s:0:\"\";s:10:\"formatting\";s:4:\"html\";s:9:\"maxlength\";s:0:\"\";s:17:\"conditional_logic\";a:3:{s:6:\"status\";s:1:\"0\";s:5:\"rules\";a:1:{i:0;a:3:{s:5:\"field\";s:4:\"null\";s:8:\"operator\";s:2:\"==\";s:5:\"value\";s:0:\"\";}}s:8:\"allorany\";s:3:\"all\";}s:8:\"order_no\";i:0;}');
INSERT INTO `wp_postmeta` VALUES (286,68,'position','acf_after_title');
INSERT INTO `wp_postmeta` VALUES (287,68,'layout','no_box');
INSERT INTO `wp_postmeta` VALUES (288,68,'hide_on_screen','');
INSERT INTO `wp_postmeta` VALUES (289,68,'_edit_lock','1401458255:1');
INSERT INTO `wp_postmeta` VALUES (294,69,'video_url','http://vimeo.com/85960055');
INSERT INTO `wp_postmeta` VALUES (295,69,'_video_url','field_538885f2ebadf');
INSERT INTO `wp_postmeta` VALUES (296,66,'video_url','http://vimeo.com/85960055');
INSERT INTO `wp_postmeta` VALUES (297,66,'_video_url','field_538885f2ebadf');
INSERT INTO `wp_postmeta` VALUES (298,68,'rule','a:5:{s:5:\"param\";s:11:\"post_format\";s:8:\"operator\";s:2:\"==\";s:5:\"value\";s:5:\"video\";s:8:\"order_no\";i:0;s:8:\"group_no\";i:0;}');
INSERT INTO `wp_postmeta` VALUES (301,70,'video_url','http://vimeo.com/85960055');
INSERT INTO `wp_postmeta` VALUES (302,70,'_video_url','field_538885f2ebadf');
INSERT INTO `wp_postmeta` VALUES (303,71,'_wp_attached_file','2014/05/featured-image-rodent.jpg');
INSERT INTO `wp_postmeta` VALUES (304,71,'_wp_attachment_metadata','a:5:{s:5:\"width\";i:2800;s:6:\"height\";i:1862;s:4:\"file\";s:33:\"2014/05/featured-image-rodent.jpg\";s:5:\"sizes\";a:12:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:33:\"featured-image-rodent-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:6:\"medium\";a:4:{s:4:\"file\";s:33:\"featured-image-rodent-300x199.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:199;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:5:\"large\";a:4:{s:4:\"file\";s:34:\"featured-image-rodent-1024x680.jpg\";s:5:\"width\";i:1024;s:6:\"height\";i:680;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:4:\"tiny\";a:4:{s:4:\"file\";s:31:\"featured-image-rodent-52x52.jpg\";s:5:\"width\";i:52;s:6:\"height\";i:52;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:10:\"post-large\";a:4:{s:4:\"file\";s:35:\"featured-image-rodent-2000x1100.jpg\";s:5:\"width\";i:2000;s:6:\"height\";i:1100;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:11:\"post-medium\";a:4:{s:4:\"file\";s:33:\"featured-image-rodent-620x292.jpg\";s:5:\"width\";i:620;s:6:\"height\";i:292;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:10:\"post-small\";a:4:{s:4:\"file\";s:33:\"featured-image-rodent-352x184.jpg\";s:5:\"width\";i:352;s:6:\"height\";i:184;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:14:\"sponsor-medium\";a:4:{s:4:\"file\";s:33:\"featured-image-rodent-620x310.jpg\";s:5:\"width\";i:620;s:6:\"height\";i:310;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:15:\"sponsor-sidebar\";a:4:{s:4:\"file\";s:33:\"featured-image-rodent-300x252.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:252;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:11:\"album-cover\";a:4:{s:4:\"file\";s:33:\"featured-image-rodent-250x250.jpg\";s:5:\"width\";i:250;s:6:\"height\";i:250;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:13:\"profile-image\";a:4:{s:4:\"file\";s:33:\"featured-image-rodent-285x285.jpg\";s:5:\"width\";i:285;s:6:\"height\";i:285;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:13:\"product-image\";a:4:{s:4:\"file\";s:33:\"featured-image-rodent-450x450.jpg\";s:5:\"width\";i:450;s:6:\"height\";i:450;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:10:{s:8:\"aperture\";i:0;s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";i:0;s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";i:0;s:3:\"iso\";i:0;s:13:\"shutter_speed\";i:0;s:5:\"title\";s:0:\"\";}}');
INSERT INTO `wp_postmeta` VALUES (305,66,'_thumbnail_id','71');
INSERT INTO `wp_postmeta` VALUES (308,72,'_edit_last','1');
INSERT INTO `wp_postmeta` VALUES (309,72,'_wp_page_template','page-listen.php');
INSERT INTO `wp_postmeta` VALUES (310,72,'_edit_lock','1401458827:1');
INSERT INTO `wp_postmeta` VALUES (311,74,'_menu_item_type','post_type');
INSERT INTO `wp_postmeta` VALUES (312,74,'_menu_item_menu_item_parent','0');
INSERT INTO `wp_postmeta` VALUES (313,74,'_menu_item_object_id','72');
INSERT INTO `wp_postmeta` VALUES (314,74,'_menu_item_object','page');
INSERT INTO `wp_postmeta` VALUES (315,74,'_menu_item_target','');
INSERT INTO `wp_postmeta` VALUES (316,74,'_menu_item_classes','a:1:{i:0;s:0:\"\";}');
INSERT INTO `wp_postmeta` VALUES (317,74,'_menu_item_xfn','');
INSERT INTO `wp_postmeta` VALUES (318,74,'_menu_item_url','');
INSERT INTO `wp_postmeta` VALUES (320,24,'field_53888cc38bd02','a:8:{s:3:\"key\";s:19:\"field_53888cc38bd02\";s:5:\"label\";s:6:\"Listen\";s:4:\"name\";s:0:\"\";s:4:\"type\";s:3:\"tab\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";s:1:\"0\";s:17:\"conditional_logic\";a:3:{s:6:\"status\";s:1:\"0\";s:5:\"rules\";a:1:{i:0;a:2:{s:5:\"field\";s:4:\"null\";s:8:\"operator\";s:2:\"==\";}}s:8:\"allorany\";s:3:\"all\";}s:8:\"order_no\";i:6;}');
INSERT INTO `wp_postmeta` VALUES (321,24,'field_53888cda8bd03','a:13:{s:3:\"key\";s:19:\"field_53888cda8bd03\";s:5:\"label\";s:3:\"Ads\";s:4:\"name\";s:10:\"listen_ads\";s:4:\"type\";s:8:\"repeater\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";s:1:\"0\";s:10:\"sub_fields\";a:2:{i:0;a:12:{s:3:\"key\";s:19:\"field_53888cda8bd04\";s:5:\"label\";s:5:\"Image\";s:4:\"name\";s:5:\"image\";s:4:\"type\";s:5:\"image\";s:12:\"instructions\";s:75:\"Images must be at least 620px by 310px for proper cropping and proportions.\";s:8:\"required\";s:1:\"0\";s:12:\"column_width\";s:0:\"\";s:11:\"save_format\";s:6:\"object\";s:12:\"preview_size\";s:9:\"thumbnail\";s:7:\"library\";s:3:\"all\";s:17:\"conditional_logic\";a:3:{s:6:\"status\";s:1:\"0\";s:5:\"rules\";a:1:{i:0;a:3:{s:5:\"field\";s:4:\"null\";s:8:\"operator\";s:2:\"==\";s:5:\"value\";s:0:\"\";}}s:8:\"allorany\";s:3:\"all\";}s:8:\"order_no\";i:0;}i:1;a:15:{s:3:\"key\";s:19:\"field_53888cda8bd05\";s:5:\"label\";s:4:\"Link\";s:4:\"name\";s:4:\"link\";s:4:\"type\";s:4:\"text\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";s:1:\"0\";s:12:\"column_width\";s:0:\"\";s:13:\"default_value\";s:0:\"\";s:11:\"placeholder\";s:0:\"\";s:7:\"prepend\";s:0:\"\";s:6:\"append\";s:0:\"\";s:10:\"formatting\";s:4:\"html\";s:9:\"maxlength\";s:0:\"\";s:17:\"conditional_logic\";a:3:{s:6:\"status\";s:1:\"0\";s:5:\"rules\";a:1:{i:0;a:3:{s:5:\"field\";s:4:\"null\";s:8:\"operator\";s:2:\"==\";s:5:\"value\";s:0:\"\";}}s:8:\"allorany\";s:3:\"all\";}s:8:\"order_no\";i:1;}}s:7:\"row_min\";s:1:\"2\";s:9:\"row_limit\";s:1:\"2\";s:6:\"layout\";s:5:\"table\";s:12:\"button_label\";s:11:\"Add Sponsor\";s:17:\"conditional_logic\";a:3:{s:6:\"status\";s:1:\"0\";s:5:\"rules\";a:1:{i:0;a:2:{s:5:\"field\";s:4:\"null\";s:8:\"operator\";s:2:\"==\";}}s:8:\"allorany\";s:3:\"all\";}s:8:\"order_no\";i:7;}');
INSERT INTO `wp_postmeta` VALUES (322,24,'rule','a:5:{s:5:\"param\";s:12:\"options_page\";s:8:\"operator\";s:2:\"==\";s:5:\"value\";s:20:\"acf-options-sponsors\";s:8:\"order_no\";i:0;s:8:\"group_no\";i:0;}');
INSERT INTO `wp_postmeta` VALUES (323,75,'_edit_last','1');
INSERT INTO `wp_postmeta` VALUES (324,75,'field_53888efb6f1b3','a:11:{s:3:\"key\";s:19:\"field_53888efb6f1b3\";s:5:\"label\";s:16:\"Background Image\";s:4:\"name\";s:16:\"background_image\";s:4:\"type\";s:5:\"image\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";s:1:\"0\";s:11:\"save_format\";s:6:\"object\";s:12:\"preview_size\";s:9:\"thumbnail\";s:7:\"library\";s:3:\"all\";s:17:\"conditional_logic\";a:3:{s:6:\"status\";s:1:\"0\";s:5:\"rules\";a:1:{i:0;a:2:{s:5:\"field\";s:4:\"null\";s:8:\"operator\";s:2:\"==\";}}s:8:\"allorany\";s:3:\"all\";}s:8:\"order_no\";i:0;}');
INSERT INTO `wp_postmeta` VALUES (325,75,'field_53888f0e6f1b4','a:11:{s:3:\"key\";s:19:\"field_53888f0e6f1b4\";s:5:\"label\";s:12:\"Header Image\";s:4:\"name\";s:12:\"header_image\";s:4:\"type\";s:5:\"image\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";s:1:\"0\";s:11:\"save_format\";s:6:\"object\";s:12:\"preview_size\";s:9:\"thumbnail\";s:7:\"library\";s:3:\"all\";s:17:\"conditional_logic\";a:3:{s:6:\"status\";s:1:\"0\";s:5:\"rules\";a:1:{i:0;a:2:{s:5:\"field\";s:4:\"null\";s:8:\"operator\";s:2:\"==\";}}s:8:\"allorany\";s:3:\"all\";}s:8:\"order_no\";i:1;}');
INSERT INTO `wp_postmeta` VALUES (326,75,'rule','a:5:{s:5:\"param\";s:13:\"page_template\";s:8:\"operator\";s:2:\"==\";s:5:\"value\";s:15:\"page-listen.php\";s:8:\"order_no\";i:0;s:8:\"group_no\";i:0;}');
INSERT INTO `wp_postmeta` VALUES (327,75,'position','normal');
INSERT INTO `wp_postmeta` VALUES (328,75,'layout','no_box');
INSERT INTO `wp_postmeta` VALUES (329,75,'hide_on_screen','');
INSERT INTO `wp_postmeta` VALUES (330,75,'_edit_lock','1401475925:1');
INSERT INTO `wp_postmeta` VALUES (331,72,'background_image','76');
INSERT INTO `wp_postmeta` VALUES (332,72,'_background_image','field_53888efb6f1b3');
INSERT INTO `wp_postmeta` VALUES (333,72,'header_image','77');
INSERT INTO `wp_postmeta` VALUES (334,72,'_header_image','field_53888f0e6f1b4');
INSERT INTO `wp_postmeta` VALUES (335,76,'_wp_attached_file','2014/05/bg-radio.jpg');
INSERT INTO `wp_postmeta` VALUES (336,76,'_wp_attachment_metadata','a:5:{s:5:\"width\";i:1752;s:6:\"height\";i:1168;s:4:\"file\";s:20:\"2014/05/bg-radio.jpg\";s:5:\"sizes\";a:12:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:20:\"bg-radio-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:6:\"medium\";a:4:{s:4:\"file\";s:20:\"bg-radio-300x200.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:200;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:5:\"large\";a:4:{s:4:\"file\";s:21:\"bg-radio-1024x682.jpg\";s:5:\"width\";i:1024;s:6:\"height\";i:682;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:4:\"tiny\";a:4:{s:4:\"file\";s:18:\"bg-radio-52x52.jpg\";s:5:\"width\";i:52;s:6:\"height\";i:52;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:10:\"post-large\";a:4:{s:4:\"file\";s:22:\"bg-radio-1752x1100.jpg\";s:5:\"width\";i:1752;s:6:\"height\";i:1100;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:11:\"post-medium\";a:4:{s:4:\"file\";s:20:\"bg-radio-620x292.jpg\";s:5:\"width\";i:620;s:6:\"height\";i:292;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:10:\"post-small\";a:4:{s:4:\"file\";s:20:\"bg-radio-352x184.jpg\";s:5:\"width\";i:352;s:6:\"height\";i:184;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:14:\"sponsor-medium\";a:4:{s:4:\"file\";s:20:\"bg-radio-620x310.jpg\";s:5:\"width\";i:620;s:6:\"height\";i:310;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:15:\"sponsor-sidebar\";a:4:{s:4:\"file\";s:20:\"bg-radio-300x252.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:252;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:11:\"album-cover\";a:4:{s:4:\"file\";s:20:\"bg-radio-250x250.jpg\";s:5:\"width\";i:250;s:6:\"height\";i:250;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:13:\"profile-image\";a:4:{s:4:\"file\";s:20:\"bg-radio-285x285.jpg\";s:5:\"width\";i:285;s:6:\"height\";i:285;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:13:\"product-image\";a:4:{s:4:\"file\";s:20:\"bg-radio-450x450.jpg\";s:5:\"width\";i:450;s:6:\"height\";i:450;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:10:{s:8:\"aperture\";i:0;s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";i:0;s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";i:0;s:3:\"iso\";i:0;s:13:\"shutter_speed\";i:0;s:5:\"title\";s:0:\"\";}}');
INSERT INTO `wp_postmeta` VALUES (337,77,'_wp_attached_file','2014/05/header-radio.png');
INSERT INTO `wp_postmeta` VALUES (338,77,'_wp_attachment_metadata','a:5:{s:5:\"width\";i:1024;s:6:\"height\";i:175;s:4:\"file\";s:24:\"2014/05/header-radio.png\";s:5:\"sizes\";a:10:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:24:\"header-radio-150x150.png\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:9:\"image/png\";}s:6:\"medium\";a:4:{s:4:\"file\";s:23:\"header-radio-300x51.png\";s:5:\"width\";i:300;s:6:\"height\";i:51;s:9:\"mime-type\";s:9:\"image/png\";}s:4:\"tiny\";a:4:{s:4:\"file\";s:22:\"header-radio-52x52.png\";s:5:\"width\";i:52;s:6:\"height\";i:52;s:9:\"mime-type\";s:9:\"image/png\";}s:11:\"post-medium\";a:4:{s:4:\"file\";s:24:\"header-radio-620x175.png\";s:5:\"width\";i:620;s:6:\"height\";i:175;s:9:\"mime-type\";s:9:\"image/png\";}s:10:\"post-small\";a:4:{s:4:\"file\";s:24:\"header-radio-352x175.png\";s:5:\"width\";i:352;s:6:\"height\";i:175;s:9:\"mime-type\";s:9:\"image/png\";}s:14:\"sponsor-medium\";a:4:{s:4:\"file\";s:24:\"header-radio-620x175.png\";s:5:\"width\";i:620;s:6:\"height\";i:175;s:9:\"mime-type\";s:9:\"image/png\";}s:15:\"sponsor-sidebar\";a:4:{s:4:\"file\";s:24:\"header-radio-300x175.png\";s:5:\"width\";i:300;s:6:\"height\";i:175;s:9:\"mime-type\";s:9:\"image/png\";}s:11:\"album-cover\";a:4:{s:4:\"file\";s:24:\"header-radio-250x175.png\";s:5:\"width\";i:250;s:6:\"height\";i:175;s:9:\"mime-type\";s:9:\"image/png\";}s:13:\"profile-image\";a:4:{s:4:\"file\";s:24:\"header-radio-285x175.png\";s:5:\"width\";i:285;s:6:\"height\";i:175;s:9:\"mime-type\";s:9:\"image/png\";}s:13:\"product-image\";a:4:{s:4:\"file\";s:24:\"header-radio-450x175.png\";s:5:\"width\";i:450;s:6:\"height\";i:175;s:9:\"mime-type\";s:9:\"image/png\";}}s:10:\"image_meta\";a:10:{s:8:\"aperture\";i:0;s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";i:0;s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";i:0;s:3:\"iso\";i:0;s:13:\"shutter_speed\";i:0;s:5:\"title\";s:0:\"\";}}');
INSERT INTO `wp_postmeta` VALUES (339,78,'background_image','76');
INSERT INTO `wp_postmeta` VALUES (340,78,'_background_image','field_53888efb6f1b3');
INSERT INTO `wp_postmeta` VALUES (341,78,'header_image','77');
INSERT INTO `wp_postmeta` VALUES (342,78,'_header_image','field_53888f0e6f1b4');
INSERT INTO `wp_postmeta` VALUES (343,79,'_wp_attached_file','2014/05/sponsor-relevant-2.jpg');
INSERT INTO `wp_postmeta` VALUES (344,79,'_wp_attachment_metadata','a:5:{s:5:\"width\";i:620;s:6:\"height\";i:310;s:4:\"file\";s:30:\"2014/05/sponsor-relevant-2.jpg\";s:5:\"sizes\";a:9:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:30:\"sponsor-relevant-2-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:6:\"medium\";a:4:{s:4:\"file\";s:30:\"sponsor-relevant-2-300x150.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:4:\"tiny\";a:4:{s:4:\"file\";s:28:\"sponsor-relevant-2-52x52.jpg\";s:5:\"width\";i:52;s:6:\"height\";i:52;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:11:\"post-medium\";a:4:{s:4:\"file\";s:30:\"sponsor-relevant-2-620x292.jpg\";s:5:\"width\";i:620;s:6:\"height\";i:292;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:10:\"post-small\";a:4:{s:4:\"file\";s:30:\"sponsor-relevant-2-352x184.jpg\";s:5:\"width\";i:352;s:6:\"height\";i:184;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:15:\"sponsor-sidebar\";a:4:{s:4:\"file\";s:30:\"sponsor-relevant-2-300x252.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:252;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:11:\"album-cover\";a:4:{s:4:\"file\";s:30:\"sponsor-relevant-2-250x250.jpg\";s:5:\"width\";i:250;s:6:\"height\";i:250;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:13:\"profile-image\";a:4:{s:4:\"file\";s:30:\"sponsor-relevant-2-285x285.jpg\";s:5:\"width\";i:285;s:6:\"height\";i:285;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:13:\"product-image\";a:4:{s:4:\"file\";s:30:\"sponsor-relevant-2-450x310.jpg\";s:5:\"width\";i:450;s:6:\"height\";i:310;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:10:{s:8:\"aperture\";i:0;s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";i:0;s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";i:0;s:3:\"iso\";i:0;s:13:\"shutter_speed\";i:0;s:5:\"title\";s:0:\"\";}}');
INSERT INTO `wp_postmeta` VALUES (345,80,'_wp_attached_file','2014/05/sponsor-relevant-3.jpg');
INSERT INTO `wp_postmeta` VALUES (346,80,'_wp_attachment_metadata','a:5:{s:5:\"width\";i:620;s:6:\"height\";i:310;s:4:\"file\";s:30:\"2014/05/sponsor-relevant-3.jpg\";s:5:\"sizes\";a:9:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:30:\"sponsor-relevant-3-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:6:\"medium\";a:4:{s:4:\"file\";s:30:\"sponsor-relevant-3-300x150.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:4:\"tiny\";a:4:{s:4:\"file\";s:28:\"sponsor-relevant-3-52x52.jpg\";s:5:\"width\";i:52;s:6:\"height\";i:52;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:11:\"post-medium\";a:4:{s:4:\"file\";s:30:\"sponsor-relevant-3-620x292.jpg\";s:5:\"width\";i:620;s:6:\"height\";i:292;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:10:\"post-small\";a:4:{s:4:\"file\";s:30:\"sponsor-relevant-3-352x184.jpg\";s:5:\"width\";i:352;s:6:\"height\";i:184;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:15:\"sponsor-sidebar\";a:4:{s:4:\"file\";s:30:\"sponsor-relevant-3-300x252.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:252;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:11:\"album-cover\";a:4:{s:4:\"file\";s:30:\"sponsor-relevant-3-250x250.jpg\";s:5:\"width\";i:250;s:6:\"height\";i:250;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:13:\"profile-image\";a:4:{s:4:\"file\";s:30:\"sponsor-relevant-3-285x285.jpg\";s:5:\"width\";i:285;s:6:\"height\";i:285;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:13:\"product-image\";a:4:{s:4:\"file\";s:30:\"sponsor-relevant-3-450x310.jpg\";s:5:\"width\";i:450;s:6:\"height\";i:310;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:10:{s:8:\"aperture\";i:0;s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";i:0;s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";i:0;s:3:\"iso\";i:0;s:13:\"shutter_speed\";i:0;s:5:\"title\";s:0:\"\";}}');
INSERT INTO `wp_postmeta` VALUES (347,81,'_edit_last','1');
INSERT INTO `wp_postmeta` VALUES (348,81,'_edit_lock','1401464184:1');
INSERT INTO `wp_postmeta` VALUES (349,82,'_edit_last','1');
INSERT INTO `wp_postmeta` VALUES (350,82,'_edit_lock','1401468393:2');
INSERT INTO `wp_postmeta` VALUES (351,82,'_wp_page_template','default');
INSERT INTO `wp_postmeta` VALUES (352,81,'_wp_trash_meta_status','draft');
INSERT INTO `wp_postmeta` VALUES (353,81,'_wp_trash_meta_time','1401465075');
INSERT INTO `wp_postmeta` VALUES (356,82,'_wp_trash_meta_status','publish');
INSERT INTO `wp_postmeta` VALUES (357,82,'_wp_trash_meta_time','1401468537');
INSERT INTO `wp_postmeta` VALUES (358,86,'_edit_lock','1401469355:1');
INSERT INTO `wp_postmeta` VALUES (359,86,'_edit_last','1');
INSERT INTO `wp_postmeta` VALUES (360,86,'_wp_page_template','page-discover.php');
INSERT INTO `wp_postmeta` VALUES (361,88,'_menu_item_type','post_type');
INSERT INTO `wp_postmeta` VALUES (362,88,'_menu_item_menu_item_parent','0');
INSERT INTO `wp_postmeta` VALUES (363,88,'_menu_item_object_id','86');
INSERT INTO `wp_postmeta` VALUES (364,88,'_menu_item_object','page');
INSERT INTO `wp_postmeta` VALUES (365,88,'_menu_item_target','');
INSERT INTO `wp_postmeta` VALUES (366,88,'_menu_item_classes','a:1:{i:0;s:0:\"\";}');
INSERT INTO `wp_postmeta` VALUES (367,88,'_menu_item_xfn','');
INSERT INTO `wp_postmeta` VALUES (368,88,'_menu_item_url','');
INSERT INTO `wp_postmeta` VALUES (370,89,'_edit_lock','1401904129:1');
INSERT INTO `wp_postmeta` VALUES (371,89,'_edit_last','1');
INSERT INTO `wp_postmeta` VALUES (372,90,'_edit_lock','1402111088:1');
INSERT INTO `wp_postmeta` VALUES (373,90,'_edit_last','1');
INSERT INTO `wp_postmeta` VALUES (374,90,'field_5388d0c5ca3f2','a:8:{s:3:\"key\";s:19:\"field_5388d0c5ca3f2\";s:5:\"label\";s:5:\"Audio\";s:4:\"name\";s:0:\"\";s:4:\"type\";s:3:\"tab\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";s:1:\"0\";s:17:\"conditional_logic\";a:3:{s:6:\"status\";s:1:\"0\";s:5:\"rules\";a:1:{i:0;a:3:{s:5:\"field\";s:4:\"null\";s:8:\"operator\";s:2:\"==\";s:5:\"value\";s:0:\"\";}}s:8:\"allorany\";s:3:\"all\";}s:8:\"order_no\";i:6;}');
INSERT INTO `wp_postmeta` VALUES (375,90,'field_5388d0d5ca3f3','a:13:{s:3:\"key\";s:19:\"field_5388d0d5ca3f3\";s:5:\"label\";s:11:\"Discography\";s:4:\"name\";s:11:\"discography\";s:4:\"type\";s:8:\"repeater\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";s:1:\"0\";s:10:\"sub_fields\";a:3:{i:0;a:15:{s:3:\"key\";s:19:\"field_5388d100ca3f4\";s:5:\"label\";s:5:\"Title\";s:4:\"name\";s:5:\"title\";s:4:\"type\";s:4:\"text\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";s:1:\"0\";s:12:\"column_width\";s:0:\"\";s:13:\"default_value\";s:0:\"\";s:11:\"placeholder\";s:0:\"\";s:7:\"prepend\";s:0:\"\";s:6:\"append\";s:0:\"\";s:10:\"formatting\";s:4:\"html\";s:9:\"maxlength\";s:0:\"\";s:17:\"conditional_logic\";a:3:{s:6:\"status\";s:1:\"0\";s:5:\"rules\";a:1:{i:0;a:3:{s:5:\"field\";s:4:\"null\";s:8:\"operator\";s:2:\"==\";s:5:\"value\";s:0:\"\";}}s:8:\"allorany\";s:3:\"all\";}s:8:\"order_no\";i:0;}i:1;a:12:{s:3:\"key\";s:19:\"field_5388d10bca3f5\";s:5:\"label\";s:5:\"Cover\";s:4:\"name\";s:5:\"cover\";s:4:\"type\";s:5:\"image\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";s:1:\"0\";s:12:\"column_width\";s:0:\"\";s:11:\"save_format\";s:6:\"object\";s:12:\"preview_size\";s:9:\"thumbnail\";s:7:\"library\";s:3:\"all\";s:17:\"conditional_logic\";a:3:{s:6:\"status\";s:1:\"0\";s:5:\"rules\";a:1:{i:0;a:3:{s:5:\"field\";s:4:\"null\";s:8:\"operator\";s:2:\"==\";s:5:\"value\";s:0:\"\";}}s:8:\"allorany\";s:3:\"all\";}s:8:\"order_no\";i:1;}i:2;a:14:{s:3:\"key\";s:19:\"field_5388d113ca3f6\";s:5:\"label\";s:6:\"Tracks\";s:4:\"name\";s:6:\"tracks\";s:4:\"type\";s:8:\"repeater\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";s:1:\"0\";s:12:\"column_width\";s:0:\"\";s:10:\"sub_fields\";a:2:{i:0;a:15:{s:3:\"key\";s:19:\"field_538cd0a5bd590\";s:5:\"label\";s:5:\"Title\";s:4:\"name\";s:5:\"title\";s:4:\"type\";s:4:\"text\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";s:1:\"0\";s:12:\"column_width\";s:0:\"\";s:13:\"default_value\";s:0:\"\";s:11:\"placeholder\";s:0:\"\";s:7:\"prepend\";s:0:\"\";s:6:\"append\";s:0:\"\";s:10:\"formatting\";s:4:\"html\";s:9:\"maxlength\";s:0:\"\";s:17:\"conditional_logic\";a:3:{s:6:\"status\";s:1:\"0\";s:5:\"rules\";a:1:{i:0;a:3:{s:5:\"field\";s:4:\"null\";s:8:\"operator\";s:2:\"==\";s:5:\"value\";s:0:\"\";}}s:8:\"allorany\";s:3:\"all\";}s:8:\"order_no\";i:0;}i:1;a:11:{s:3:\"key\";s:19:\"field_538cd0b5bd591\";s:5:\"label\";s:5:\"Audio\";s:4:\"name\";s:5:\"audio\";s:4:\"type\";s:4:\"file\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";s:1:\"0\";s:12:\"column_width\";s:0:\"\";s:11:\"save_format\";s:3:\"url\";s:7:\"library\";s:3:\"all\";s:17:\"conditional_logic\";a:3:{s:6:\"status\";s:1:\"0\";s:5:\"rules\";a:1:{i:0;a:3:{s:5:\"field\";s:4:\"null\";s:8:\"operator\";s:2:\"==\";s:5:\"value\";s:0:\"\";}}s:8:\"allorany\";s:3:\"all\";}s:8:\"order_no\";i:1;}}s:7:\"row_min\";s:0:\"\";s:9:\"row_limit\";s:0:\"\";s:6:\"layout\";s:5:\"table\";s:12:\"button_label\";s:9:\"Add Track\";s:17:\"conditional_logic\";a:3:{s:6:\"status\";s:1:\"0\";s:5:\"rules\";a:1:{i:0;a:3:{s:5:\"field\";s:4:\"null\";s:8:\"operator\";s:2:\"==\";s:5:\"value\";s:0:\"\";}}s:8:\"allorany\";s:3:\"all\";}s:8:\"order_no\";i:2;}}s:7:\"row_min\";s:0:\"\";s:9:\"row_limit\";s:0:\"\";s:6:\"layout\";s:3:\"row\";s:12:\"button_label\";s:9:\"Add Album\";s:17:\"conditional_logic\";a:3:{s:6:\"status\";s:1:\"0\";s:5:\"rules\";a:1:{i:0;a:3:{s:5:\"field\";s:4:\"null\";s:8:\"operator\";s:2:\"==\";s:5:\"value\";s:0:\"\";}}s:8:\"allorany\";s:3:\"all\";}s:8:\"order_no\";i:8;}');
INSERT INTO `wp_postmeta` VALUES (377,90,'position','normal');
INSERT INTO `wp_postmeta` VALUES (378,90,'layout','no_box');
INSERT INTO `wp_postmeta` VALUES (379,90,'hide_on_screen','');
INSERT INTO `wp_postmeta` VALUES (380,90,'field_5388d131c1cbf','a:8:{s:3:\"key\";s:19:\"field_5388d131c1cbf\";s:5:\"label\";s:6:\"Images\";s:4:\"name\";s:0:\"\";s:4:\"type\";s:3:\"tab\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";s:1:\"0\";s:17:\"conditional_logic\";a:3:{s:6:\"status\";s:1:\"0\";s:5:\"rules\";a:1:{i:0;a:3:{s:5:\"field\";s:4:\"null\";s:8:\"operator\";s:2:\"==\";s:5:\"value\";s:0:\"\";}}s:8:\"allorany\";s:3:\"all\";}s:8:\"order_no\";i:2;}');
INSERT INTO `wp_postmeta` VALUES (381,90,'field_5388d19fc1cc1','a:11:{s:3:\"key\";s:19:\"field_5388d19fc1cc1\";s:5:\"label\";s:12:\"Banner Image\";s:4:\"name\";s:12:\"banner_image\";s:4:\"type\";s:5:\"image\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";s:1:\"0\";s:11:\"save_format\";s:6:\"object\";s:12:\"preview_size\";s:9:\"thumbnail\";s:7:\"library\";s:3:\"all\";s:17:\"conditional_logic\";a:3:{s:6:\"status\";s:1:\"0\";s:5:\"rules\";a:1:{i:0;a:3:{s:5:\"field\";s:4:\"null\";s:8:\"operator\";s:2:\"==\";s:5:\"value\";s:0:\"\";}}s:8:\"allorany\";s:3:\"all\";}s:8:\"order_no\";i:4;}');
INSERT INTO `wp_postmeta` VALUES (382,90,'field_5388d14bc1cc0','a:11:{s:3:\"key\";s:19:\"field_5388d14bc1cc0\";s:5:\"label\";s:20:\"Featured Album Cover\";s:4:\"name\";s:15:\"thumbnail_photo\";s:4:\"type\";s:5:\"image\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";s:1:\"1\";s:11:\"save_format\";s:6:\"object\";s:12:\"preview_size\";s:9:\"thumbnail\";s:7:\"library\";s:3:\"all\";s:17:\"conditional_logic\";a:3:{s:6:\"status\";s:1:\"0\";s:5:\"rules\";a:1:{i:0;a:2:{s:5:\"field\";s:4:\"null\";s:8:\"operator\";s:2:\"==\";}}s:8:\"allorany\";s:3:\"all\";}s:8:\"order_no\";i:5;}');
INSERT INTO `wp_postmeta` VALUES (384,89,'large_photo','');
INSERT INTO `wp_postmeta` VALUES (385,89,'_large_photo','field_5388d19fc1cc1');
INSERT INTO `wp_postmeta` VALUES (386,89,'thumbnail_photo','91');
INSERT INTO `wp_postmeta` VALUES (387,89,'_thumbnail_photo','field_5388d14bc1cc0');
INSERT INTO `wp_postmeta` VALUES (388,89,'discography_0_title','');
INSERT INTO `wp_postmeta` VALUES (389,89,'_discography_0_title','field_5388d100ca3f4');
INSERT INTO `wp_postmeta` VALUES (390,89,'discography_0_cover','');
INSERT INTO `wp_postmeta` VALUES (391,89,'_discography_0_cover','field_5388d10bca3f5');
INSERT INTO `wp_postmeta` VALUES (392,89,'discography_0_tracks','0');
INSERT INTO `wp_postmeta` VALUES (393,89,'_discography_0_tracks','field_5388d113ca3f6');
INSERT INTO `wp_postmeta` VALUES (394,89,'discography','1');
INSERT INTO `wp_postmeta` VALUES (395,89,'_discography','field_5388d0d5ca3f3');
INSERT INTO `wp_postmeta` VALUES (396,91,'_wp_attached_file','2014/05/album-cover-allsonsanddaughterslive.jpg');
INSERT INTO `wp_postmeta` VALUES (397,91,'_wp_attachment_metadata','a:5:{s:5:\"width\";i:596;s:6:\"height\";i:596;s:4:\"file\";s:47:\"2014/05/album-cover-allsonsanddaughterslive.jpg\";s:5:\"sizes\";a:10:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:47:\"album-cover-allsonsanddaughterslive-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:6:\"medium\";a:4:{s:4:\"file\";s:47:\"album-cover-allsonsanddaughterslive-300x300.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:300;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:4:\"tiny\";a:4:{s:4:\"file\";s:45:\"album-cover-allsonsanddaughterslive-52x52.jpg\";s:5:\"width\";i:52;s:6:\"height\";i:52;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:11:\"post-medium\";a:4:{s:4:\"file\";s:47:\"album-cover-allsonsanddaughterslive-596x292.jpg\";s:5:\"width\";i:596;s:6:\"height\";i:292;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:10:\"post-small\";a:4:{s:4:\"file\";s:47:\"album-cover-allsonsanddaughterslive-352x184.jpg\";s:5:\"width\";i:352;s:6:\"height\";i:184;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:14:\"sponsor-medium\";a:4:{s:4:\"file\";s:47:\"album-cover-allsonsanddaughterslive-596x310.jpg\";s:5:\"width\";i:596;s:6:\"height\";i:310;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:15:\"sponsor-sidebar\";a:4:{s:4:\"file\";s:47:\"album-cover-allsonsanddaughterslive-300x252.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:252;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:11:\"album-cover\";a:4:{s:4:\"file\";s:47:\"album-cover-allsonsanddaughterslive-250x250.jpg\";s:5:\"width\";i:250;s:6:\"height\";i:250;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:13:\"profile-image\";a:4:{s:4:\"file\";s:47:\"album-cover-allsonsanddaughterslive-285x285.jpg\";s:5:\"width\";i:285;s:6:\"height\";i:285;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:13:\"product-image\";a:4:{s:4:\"file\";s:47:\"album-cover-allsonsanddaughterslive-450x450.jpg\";s:5:\"width\";i:450;s:6:\"height\";i:450;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:10:{s:8:\"aperture\";i:0;s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";i:0;s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";i:0;s:3:\"iso\";i:0;s:13:\"shutter_speed\";i:0;s:5:\"title\";s:0:\"\";}}');
INSERT INTO `wp_postmeta` VALUES (398,90,'field_5388d3ecce90f','a:10:{s:3:\"key\";s:19:\"field_5388d3ecce90f\";s:5:\"label\";s:14:\"Featured Audio\";s:4:\"name\";s:14:\"featured_audio\";s:4:\"type\";s:4:\"file\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";s:1:\"1\";s:11:\"save_format\";s:3:\"url\";s:7:\"library\";s:3:\"all\";s:17:\"conditional_logic\";a:3:{s:6:\"status\";s:1:\"0\";s:5:\"rules\";a:1:{i:0;a:3:{s:5:\"field\";s:4:\"null\";s:8:\"operator\";s:2:\"==\";s:5:\"value\";s:0:\"\";}}s:8:\"allorany\";s:3:\"all\";}s:8:\"order_no\";i:7;}');
INSERT INTO `wp_postmeta` VALUES (400,92,'_edit_lock','1402115140:1');
INSERT INTO `wp_postmeta` VALUES (401,92,'_edit_last','1');
INSERT INTO `wp_postmeta` VALUES (402,92,'large_photo','');
INSERT INTO `wp_postmeta` VALUES (403,92,'_large_photo','field_5388d19fc1cc1');
INSERT INTO `wp_postmeta` VALUES (404,92,'thumbnail_photo','96');
INSERT INTO `wp_postmeta` VALUES (405,92,'_thumbnail_photo','field_5388d14bc1cc0');
INSERT INTO `wp_postmeta` VALUES (406,92,'featured_audio','95');
INSERT INTO `wp_postmeta` VALUES (407,92,'_featured_audio','field_5388d3ecce90f');
INSERT INTO `wp_postmeta` VALUES (408,92,'discography','0');
INSERT INTO `wp_postmeta` VALUES (409,92,'_discography','field_5388d0d5ca3f3');
INSERT INTO `wp_postmeta` VALUES (411,89,'featured_audio','95');
INSERT INTO `wp_postmeta` VALUES (412,89,'_featured_audio','field_5388d3ecce90f');
INSERT INTO `wp_postmeta` VALUES (413,95,'_wp_attached_file','2014/05/Turkey_Tune.mp3');
INSERT INTO `wp_postmeta` VALUES (414,95,'_wp_attachment_metadata','a:17:{s:10:\"dataformat\";s:3:\"mp3\";s:8:\"channels\";i:2;s:11:\"sample_rate\";i:44100;s:7:\"bitrate\";i:256000;s:11:\"channelmode\";s:6:\"stereo\";s:12:\"bitrate_mode\";s:3:\"cbr\";s:8:\"lossless\";b:0;s:15:\"encoder_options\";s:6:\"CBR256\";s:17:\"compression_ratio\";d:0.181405895691609975184377390178269706666469573974609375;s:10:\"fileformat\";s:3:\"mp3\";s:8:\"filesize\";i:980144;s:9:\"mime_type\";s:10:\"audio/mpeg\";s:6:\"length\";i:31;s:16:\"length_formatted\";s:4:\"0:31\";s:5:\"title\";s:11:\"Turkey_Tune\";s:7:\"comment\";s:3:\"0\0\0\";s:10:\"encoded_by\";s:12:\"iTunes 8.0.1\";}');
INSERT INTO `wp_postmeta` VALUES (415,96,'_wp_attached_file','2014/05/album-cover-andymineo.jpg');
INSERT INTO `wp_postmeta` VALUES (416,96,'_wp_attachment_metadata','a:5:{s:5:\"width\";i:640;s:6:\"height\";i:640;s:4:\"file\";s:33:\"2014/05/album-cover-andymineo.jpg\";s:5:\"sizes\";a:10:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:33:\"album-cover-andymineo-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:6:\"medium\";a:4:{s:4:\"file\";s:33:\"album-cover-andymineo-300x300.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:300;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:4:\"tiny\";a:4:{s:4:\"file\";s:31:\"album-cover-andymineo-52x52.jpg\";s:5:\"width\";i:52;s:6:\"height\";i:52;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:11:\"post-medium\";a:4:{s:4:\"file\";s:33:\"album-cover-andymineo-620x292.jpg\";s:5:\"width\";i:620;s:6:\"height\";i:292;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:10:\"post-small\";a:4:{s:4:\"file\";s:33:\"album-cover-andymineo-352x184.jpg\";s:5:\"width\";i:352;s:6:\"height\";i:184;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:14:\"sponsor-medium\";a:4:{s:4:\"file\";s:33:\"album-cover-andymineo-620x310.jpg\";s:5:\"width\";i:620;s:6:\"height\";i:310;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:15:\"sponsor-sidebar\";a:4:{s:4:\"file\";s:33:\"album-cover-andymineo-300x252.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:252;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:11:\"album-cover\";a:4:{s:4:\"file\";s:33:\"album-cover-andymineo-250x250.jpg\";s:5:\"width\";i:250;s:6:\"height\";i:250;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:13:\"profile-image\";a:4:{s:4:\"file\";s:33:\"album-cover-andymineo-285x285.jpg\";s:5:\"width\";i:285;s:6:\"height\";i:285;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:13:\"product-image\";a:4:{s:4:\"file\";s:33:\"album-cover-andymineo-450x450.jpg\";s:5:\"width\";i:450;s:6:\"height\";i:450;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:10:{s:8:\"aperture\";i:0;s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";i:0;s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";i:0;s:3:\"iso\";i:0;s:13:\"shutter_speed\";i:0;s:5:\"title\";s:0:\"\";}}');
INSERT INTO `wp_postmeta` VALUES (417,97,'_edit_lock','1402115157:1');
INSERT INTO `wp_postmeta` VALUES (418,97,'_edit_last','1');
INSERT INTO `wp_postmeta` VALUES (419,98,'_wp_attached_file','2014/06/album-cover-soundsabsurd.jpg');
INSERT INTO `wp_postmeta` VALUES (420,98,'_wp_attachment_metadata','a:5:{s:5:\"width\";i:621;s:6:\"height\";i:617;s:4:\"file\";s:36:\"2014/06/album-cover-soundsabsurd.jpg\";s:5:\"sizes\";a:10:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:36:\"album-cover-soundsabsurd-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:6:\"medium\";a:4:{s:4:\"file\";s:36:\"album-cover-soundsabsurd-300x298.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:298;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:4:\"tiny\";a:4:{s:4:\"file\";s:34:\"album-cover-soundsabsurd-52x52.jpg\";s:5:\"width\";i:52;s:6:\"height\";i:52;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:11:\"post-medium\";a:4:{s:4:\"file\";s:36:\"album-cover-soundsabsurd-620x292.jpg\";s:5:\"width\";i:620;s:6:\"height\";i:292;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:10:\"post-small\";a:4:{s:4:\"file\";s:36:\"album-cover-soundsabsurd-352x184.jpg\";s:5:\"width\";i:352;s:6:\"height\";i:184;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:14:\"sponsor-medium\";a:4:{s:4:\"file\";s:36:\"album-cover-soundsabsurd-620x310.jpg\";s:5:\"width\";i:620;s:6:\"height\";i:310;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:15:\"sponsor-sidebar\";a:4:{s:4:\"file\";s:36:\"album-cover-soundsabsurd-300x252.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:252;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:11:\"album-cover\";a:4:{s:4:\"file\";s:36:\"album-cover-soundsabsurd-250x250.jpg\";s:5:\"width\";i:250;s:6:\"height\";i:250;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:13:\"profile-image\";a:4:{s:4:\"file\";s:36:\"album-cover-soundsabsurd-285x285.jpg\";s:5:\"width\";i:285;s:6:\"height\";i:285;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:13:\"product-image\";a:4:{s:4:\"file\";s:36:\"album-cover-soundsabsurd-450x450.jpg\";s:5:\"width\";i:450;s:6:\"height\";i:450;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:10:{s:8:\"aperture\";i:0;s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";i:0;s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";i:0;s:3:\"iso\";i:0;s:13:\"shutter_speed\";i:0;s:5:\"title\";s:0:\"\";}}');
INSERT INTO `wp_postmeta` VALUES (421,97,'large_photo','');
INSERT INTO `wp_postmeta` VALUES (422,97,'_large_photo','field_5388d19fc1cc1');
INSERT INTO `wp_postmeta` VALUES (423,97,'thumbnail_photo','98');
INSERT INTO `wp_postmeta` VALUES (424,97,'_thumbnail_photo','field_5388d14bc1cc0');
INSERT INTO `wp_postmeta` VALUES (425,97,'featured_audio','95');
INSERT INTO `wp_postmeta` VALUES (426,97,'_featured_audio','field_5388d3ecce90f');
INSERT INTO `wp_postmeta` VALUES (427,97,'discography','0');
INSERT INTO `wp_postmeta` VALUES (428,97,'_discography','field_5388d0d5ca3f3');
INSERT INTO `wp_postmeta` VALUES (429,99,'_edit_lock','1401902195:1');
INSERT INTO `wp_postmeta` VALUES (430,99,'_edit_last','1');
INSERT INTO `wp_postmeta` VALUES (431,100,'_wp_attached_file','2014/06/album-cover-kutless.jpg');
INSERT INTO `wp_postmeta` VALUES (432,100,'_wp_attachment_metadata','a:5:{s:5:\"width\";i:333;s:6:\"height\";i:333;s:4:\"file\";s:31:\"2014/06/album-cover-kutless.jpg\";s:5:\"sizes\";a:9:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:31:\"album-cover-kutless-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:6:\"medium\";a:4:{s:4:\"file\";s:31:\"album-cover-kutless-300x300.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:300;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:4:\"tiny\";a:4:{s:4:\"file\";s:29:\"album-cover-kutless-52x52.jpg\";s:5:\"width\";i:52;s:6:\"height\";i:52;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:11:\"post-medium\";a:4:{s:4:\"file\";s:31:\"album-cover-kutless-333x292.jpg\";s:5:\"width\";i:333;s:6:\"height\";i:292;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:10:\"post-small\";a:4:{s:4:\"file\";s:31:\"album-cover-kutless-333x184.jpg\";s:5:\"width\";i:333;s:6:\"height\";i:184;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:14:\"sponsor-medium\";a:4:{s:4:\"file\";s:31:\"album-cover-kutless-333x310.jpg\";s:5:\"width\";i:333;s:6:\"height\";i:310;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:15:\"sponsor-sidebar\";a:4:{s:4:\"file\";s:31:\"album-cover-kutless-300x252.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:252;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:11:\"album-cover\";a:4:{s:4:\"file\";s:31:\"album-cover-kutless-250x250.jpg\";s:5:\"width\";i:250;s:6:\"height\";i:250;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:13:\"profile-image\";a:4:{s:4:\"file\";s:31:\"album-cover-kutless-285x285.jpg\";s:5:\"width\";i:285;s:6:\"height\";i:285;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:10:{s:8:\"aperture\";i:0;s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";i:0;s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";i:0;s:3:\"iso\";i:0;s:13:\"shutter_speed\";i:0;s:5:\"title\";s:0:\"\";}}');
INSERT INTO `wp_postmeta` VALUES (433,99,'large_photo','');
INSERT INTO `wp_postmeta` VALUES (434,99,'_large_photo','field_5388d19fc1cc1');
INSERT INTO `wp_postmeta` VALUES (435,99,'thumbnail_photo','100');
INSERT INTO `wp_postmeta` VALUES (436,99,'_thumbnail_photo','field_5388d14bc1cc0');
INSERT INTO `wp_postmeta` VALUES (437,99,'featured_audio','');
INSERT INTO `wp_postmeta` VALUES (438,99,'_featured_audio','field_5388d3ecce90f');
INSERT INTO `wp_postmeta` VALUES (439,99,'discography','0');
INSERT INTO `wp_postmeta` VALUES (440,99,'_discography','field_5388d0d5ca3f3');
INSERT INTO `wp_postmeta` VALUES (441,101,'_edit_lock','1401892782:1');
INSERT INTO `wp_postmeta` VALUES (442,101,'_edit_last','1');
INSERT INTO `wp_postmeta` VALUES (443,102,'_wp_attached_file','2014/06/album-cover-deraj.jpg');
INSERT INTO `wp_postmeta` VALUES (444,102,'_wp_attachment_metadata','a:5:{s:5:\"width\";i:1685;s:6:\"height\";i:1685;s:4:\"file\";s:29:\"2014/06/album-cover-deraj.jpg\";s:5:\"sizes\";a:12:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:29:\"album-cover-deraj-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:6:\"medium\";a:4:{s:4:\"file\";s:29:\"album-cover-deraj-300x300.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:300;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:5:\"large\";a:4:{s:4:\"file\";s:31:\"album-cover-deraj-1024x1024.jpg\";s:5:\"width\";i:1024;s:6:\"height\";i:1024;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:4:\"tiny\";a:4:{s:4:\"file\";s:27:\"album-cover-deraj-52x52.jpg\";s:5:\"width\";i:52;s:6:\"height\";i:52;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:10:\"post-large\";a:4:{s:4:\"file\";s:31:\"album-cover-deraj-1685x1100.jpg\";s:5:\"width\";i:1685;s:6:\"height\";i:1100;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:11:\"post-medium\";a:4:{s:4:\"file\";s:29:\"album-cover-deraj-620x292.jpg\";s:5:\"width\";i:620;s:6:\"height\";i:292;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:10:\"post-small\";a:4:{s:4:\"file\";s:29:\"album-cover-deraj-352x184.jpg\";s:5:\"width\";i:352;s:6:\"height\";i:184;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:14:\"sponsor-medium\";a:4:{s:4:\"file\";s:29:\"album-cover-deraj-620x310.jpg\";s:5:\"width\";i:620;s:6:\"height\";i:310;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:15:\"sponsor-sidebar\";a:4:{s:4:\"file\";s:29:\"album-cover-deraj-300x252.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:252;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:11:\"album-cover\";a:4:{s:4:\"file\";s:29:\"album-cover-deraj-250x250.jpg\";s:5:\"width\";i:250;s:6:\"height\";i:250;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:13:\"profile-image\";a:4:{s:4:\"file\";s:29:\"album-cover-deraj-285x285.jpg\";s:5:\"width\";i:285;s:6:\"height\";i:285;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:13:\"product-image\";a:4:{s:4:\"file\";s:29:\"album-cover-deraj-450x450.jpg\";s:5:\"width\";i:450;s:6:\"height\";i:450;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:10:{s:8:\"aperture\";i:0;s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";i:0;s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";i:0;s:3:\"iso\";i:0;s:13:\"shutter_speed\";i:0;s:5:\"title\";s:0:\"\";}}');
INSERT INTO `wp_postmeta` VALUES (445,101,'large_photo','');
INSERT INTO `wp_postmeta` VALUES (446,101,'_large_photo','field_5388d19fc1cc1');
INSERT INTO `wp_postmeta` VALUES (447,101,'thumbnail_photo','102');
INSERT INTO `wp_postmeta` VALUES (448,101,'_thumbnail_photo','field_5388d14bc1cc0');
INSERT INTO `wp_postmeta` VALUES (449,101,'featured_audio','95');
INSERT INTO `wp_postmeta` VALUES (450,101,'_featured_audio','field_5388d3ecce90f');
INSERT INTO `wp_postmeta` VALUES (451,101,'discography','2');
INSERT INTO `wp_postmeta` VALUES (452,101,'_discography','field_5388d0d5ca3f3');
INSERT INTO `wp_postmeta` VALUES (454,90,'field_538cba2564c21','a:11:{s:3:\"key\";s:19:\"field_538cba2564c21\";s:5:\"label\";s:13:\"Profile Image\";s:4:\"name\";s:13:\"profile_image\";s:4:\"type\";s:5:\"image\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";s:1:\"0\";s:11:\"save_format\";s:6:\"object\";s:12:\"preview_size\";s:9:\"thumbnail\";s:7:\"library\";s:3:\"all\";s:17:\"conditional_logic\";a:3:{s:6:\"status\";s:1:\"0\";s:5:\"rules\";a:1:{i:0;a:3:{s:5:\"field\";s:4:\"null\";s:8:\"operator\";s:2:\"==\";s:5:\"value\";s:0:\"\";}}s:8:\"allorany\";s:3:\"all\";}s:8:\"order_no\";i:3;}');
INSERT INTO `wp_postmeta` VALUES (456,103,'_wp_attached_file','2014/06/profile-deraj.jpg');
INSERT INTO `wp_postmeta` VALUES (457,103,'_wp_attachment_metadata','a:5:{s:5:\"width\";i:309;s:6:\"height\";i:309;s:4:\"file\";s:25:\"2014/06/profile-deraj.jpg\";s:5:\"sizes\";a:8:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:25:\"profile-deraj-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:6:\"medium\";a:4:{s:4:\"file\";s:25:\"profile-deraj-300x300.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:300;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:4:\"tiny\";a:4:{s:4:\"file\";s:23:\"profile-deraj-52x52.jpg\";s:5:\"width\";i:52;s:6:\"height\";i:52;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:11:\"post-medium\";a:4:{s:4:\"file\";s:25:\"profile-deraj-309x292.jpg\";s:5:\"width\";i:309;s:6:\"height\";i:292;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:10:\"post-small\";a:4:{s:4:\"file\";s:25:\"profile-deraj-309x184.jpg\";s:5:\"width\";i:309;s:6:\"height\";i:184;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:15:\"sponsor-sidebar\";a:4:{s:4:\"file\";s:25:\"profile-deraj-300x252.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:252;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:11:\"album-cover\";a:4:{s:4:\"file\";s:25:\"profile-deraj-250x250.jpg\";s:5:\"width\";i:250;s:6:\"height\";i:250;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:13:\"profile-image\";a:4:{s:4:\"file\";s:25:\"profile-deraj-285x285.jpg\";s:5:\"width\";i:285;s:6:\"height\";i:285;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:10:{s:8:\"aperture\";i:0;s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";i:0;s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";i:0;s:3:\"iso\";i:0;s:13:\"shutter_speed\";i:0;s:5:\"title\";s:0:\"\";}}');
INSERT INTO `wp_postmeta` VALUES (458,101,'profile_image','103');
INSERT INTO `wp_postmeta` VALUES (459,101,'_profile_image','field_538cba2564c21');
INSERT INTO `wp_postmeta` VALUES (460,101,'banner_image','102');
INSERT INTO `wp_postmeta` VALUES (461,101,'_banner_image','field_5388d19fc1cc1');
INSERT INTO `wp_postmeta` VALUES (462,23,'field_538cbb1d0afa0','a:11:{s:3:\"key\";s:19:\"field_538cbb1d0afa0\";s:5:\"label\";s:27:\"Default Artist Banner Image\";s:4:\"name\";s:27:\"default_artist_banner_image\";s:4:\"type\";s:5:\"image\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";s:1:\"0\";s:11:\"save_format\";s:6:\"object\";s:12:\"preview_size\";s:9:\"thumbnail\";s:7:\"library\";s:3:\"all\";s:17:\"conditional_logic\";a:3:{s:6:\"status\";s:1:\"0\";s:5:\"rules\";a:1:{i:0;a:3:{s:5:\"field\";s:4:\"null\";s:8:\"operator\";s:2:\"==\";s:5:\"value\";s:0:\"\";}}s:8:\"allorany\";s:3:\"all\";}s:8:\"order_no\";i:2;}');
INSERT INTO `wp_postmeta` VALUES (464,104,'_wp_attached_file','2014/06/default-artist.jpg');
INSERT INTO `wp_postmeta` VALUES (465,104,'_wp_attachment_metadata','a:5:{s:5:\"width\";i:2000;s:6:\"height\";i:2000;s:4:\"file\";s:26:\"2014/06/default-artist.jpg\";s:5:\"sizes\";a:12:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:26:\"default-artist-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:6:\"medium\";a:4:{s:4:\"file\";s:26:\"default-artist-300x300.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:300;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:5:\"large\";a:4:{s:4:\"file\";s:28:\"default-artist-1024x1024.jpg\";s:5:\"width\";i:1024;s:6:\"height\";i:1024;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:4:\"tiny\";a:4:{s:4:\"file\";s:24:\"default-artist-52x52.jpg\";s:5:\"width\";i:52;s:6:\"height\";i:52;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:10:\"post-large\";a:4:{s:4:\"file\";s:28:\"default-artist-2000x1100.jpg\";s:5:\"width\";i:2000;s:6:\"height\";i:1100;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:11:\"post-medium\";a:4:{s:4:\"file\";s:26:\"default-artist-620x292.jpg\";s:5:\"width\";i:620;s:6:\"height\";i:292;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:10:\"post-small\";a:4:{s:4:\"file\";s:26:\"default-artist-352x184.jpg\";s:5:\"width\";i:352;s:6:\"height\";i:184;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:14:\"sponsor-medium\";a:4:{s:4:\"file\";s:26:\"default-artist-620x310.jpg\";s:5:\"width\";i:620;s:6:\"height\";i:310;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:15:\"sponsor-sidebar\";a:4:{s:4:\"file\";s:26:\"default-artist-300x252.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:252;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:11:\"album-cover\";a:4:{s:4:\"file\";s:26:\"default-artist-250x250.jpg\";s:5:\"width\";i:250;s:6:\"height\";i:250;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:13:\"profile-image\";a:4:{s:4:\"file\";s:26:\"default-artist-285x285.jpg\";s:5:\"width\";i:285;s:6:\"height\";i:285;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:13:\"product-image\";a:4:{s:4:\"file\";s:26:\"default-artist-450x450.jpg\";s:5:\"width\";i:450;s:6:\"height\";i:450;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:10:{s:8:\"aperture\";i:0;s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";i:0;s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";i:0;s:3:\"iso\";i:0;s:13:\"shutter_speed\";i:0;s:5:\"title\";s:0:\"\";}}');
INSERT INTO `wp_postmeta` VALUES (466,23,'field_538cc8befb88f','a:11:{s:3:\"key\";s:19:\"field_538cc8befb88f\";s:5:\"label\";s:28:\"Default Artist Profile Image\";s:4:\"name\";s:28:\"default_artist_profile_image\";s:4:\"type\";s:5:\"image\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";s:1:\"0\";s:11:\"save_format\";s:6:\"object\";s:12:\"preview_size\";s:9:\"thumbnail\";s:7:\"library\";s:3:\"all\";s:17:\"conditional_logic\";a:3:{s:6:\"status\";s:1:\"0\";s:5:\"rules\";a:1:{i:0;a:3:{s:5:\"field\";s:4:\"null\";s:8:\"operator\";s:2:\"==\";s:5:\"value\";s:0:\"\";}}s:8:\"allorany\";s:3:\"all\";}s:8:\"order_no\";i:3;}');
INSERT INTO `wp_postmeta` VALUES (468,105,'_wp_attached_file','2014/06/default-artist-profile.jpg');
INSERT INTO `wp_postmeta` VALUES (469,105,'_wp_attachment_metadata','a:5:{s:5:\"width\";i:400;s:6:\"height\";i:400;s:4:\"file\";s:34:\"2014/06/default-artist-profile.jpg\";s:5:\"sizes\";a:9:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:34:\"default-artist-profile-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:6:\"medium\";a:4:{s:4:\"file\";s:34:\"default-artist-profile-300x300.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:300;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:4:\"tiny\";a:4:{s:4:\"file\";s:32:\"default-artist-profile-52x52.jpg\";s:5:\"width\";i:52;s:6:\"height\";i:52;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:11:\"post-medium\";a:4:{s:4:\"file\";s:34:\"default-artist-profile-400x292.jpg\";s:5:\"width\";i:400;s:6:\"height\";i:292;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:10:\"post-small\";a:4:{s:4:\"file\";s:34:\"default-artist-profile-352x184.jpg\";s:5:\"width\";i:352;s:6:\"height\";i:184;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:14:\"sponsor-medium\";a:4:{s:4:\"file\";s:34:\"default-artist-profile-400x310.jpg\";s:5:\"width\";i:400;s:6:\"height\";i:310;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:15:\"sponsor-sidebar\";a:4:{s:4:\"file\";s:34:\"default-artist-profile-300x252.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:252;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:11:\"album-cover\";a:4:{s:4:\"file\";s:34:\"default-artist-profile-250x250.jpg\";s:5:\"width\";i:250;s:6:\"height\";i:250;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:13:\"profile-image\";a:4:{s:4:\"file\";s:34:\"default-artist-profile-285x285.jpg\";s:5:\"width\";i:285;s:6:\"height\";i:285;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:10:{s:8:\"aperture\";i:0;s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";i:0;s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";i:0;s:3:\"iso\";i:0;s:13:\"shutter_speed\";i:0;s:5:\"title\";s:0:\"\";}}');
INSERT INTO `wp_postmeta` VALUES (470,90,'field_538ccb85967ba','a:8:{s:3:\"key\";s:19:\"field_538ccb85967ba\";s:5:\"label\";s:4:\"Info\";s:4:\"name\";s:0:\"\";s:4:\"type\";s:3:\"tab\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";s:1:\"0\";s:17:\"conditional_logic\";a:3:{s:6:\"status\";s:1:\"0\";s:5:\"rules\";a:1:{i:0;a:3:{s:5:\"field\";s:4:\"null\";s:8:\"operator\";s:2:\"==\";s:5:\"value\";s:0:\"\";}}s:8:\"allorany\";s:3:\"all\";}s:8:\"order_no\";i:0;}');
INSERT INTO `wp_postmeta` VALUES (471,90,'field_538ccb5c967b9','a:11:{s:3:\"key\";s:19:\"field_538ccb5c967b9\";s:5:\"label\";s:18:\"Artist Information\";s:4:\"name\";s:18:\"artist_information\";s:4:\"type\";s:7:\"wysiwyg\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";s:1:\"0\";s:13:\"default_value\";s:0:\"\";s:7:\"toolbar\";s:4:\"full\";s:12:\"media_upload\";s:2:\"no\";s:17:\"conditional_logic\";a:3:{s:6:\"status\";s:1:\"0\";s:5:\"rules\";a:1:{i:0;a:3:{s:5:\"field\";s:4:\"null\";s:8:\"operator\";s:2:\"==\";s:5:\"value\";s:0:\"\";}}s:8:\"allorany\";s:3:\"all\";}s:8:\"order_no\";i:1;}');
INSERT INTO `wp_postmeta` VALUES (473,106,'_edit_lock','1401736681:2');
INSERT INTO `wp_postmeta` VALUES (474,106,'_edit_last','2');
INSERT INTO `wp_postmeta` VALUES (475,106,'artist_information','');
INSERT INTO `wp_postmeta` VALUES (476,106,'_artist_information','field_538ccb5c967b9');
INSERT INTO `wp_postmeta` VALUES (477,106,'profile_image','');
INSERT INTO `wp_postmeta` VALUES (478,106,'_profile_image','field_538cba2564c21');
INSERT INTO `wp_postmeta` VALUES (479,106,'banner_image','');
INSERT INTO `wp_postmeta` VALUES (480,106,'_banner_image','field_5388d19fc1cc1');
INSERT INTO `wp_postmeta` VALUES (481,106,'thumbnail_photo','');
INSERT INTO `wp_postmeta` VALUES (482,106,'_thumbnail_photo','field_5388d14bc1cc0');
INSERT INTO `wp_postmeta` VALUES (483,106,'featured_audio','');
INSERT INTO `wp_postmeta` VALUES (484,106,'_featured_audio','field_5388d3ecce90f');
INSERT INTO `wp_postmeta` VALUES (485,106,'discography','0');
INSERT INTO `wp_postmeta` VALUES (486,106,'_discography','field_5388d0d5ca3f3');
INSERT INTO `wp_postmeta` VALUES (487,101,'artist_information','Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam in volutpat purus. Integer orci mi, rutrum ac viverra quis, ultrices id augue. Fusce nibh nulla, pretium non elementum hendrerit, lobortis sit amet magna. Vivamus metus massa, sagittis non feugiat at, condimentum imperdiet dolor. Cras vitae libero quis tellus iaculis interdum vel non arcu. Aliquam commodo scelerisque placerat. Pellentesque vestibulum, elit ac vehicula auctor, ligula urna lacinia ligula, sed aliquam purus tellus et nibh. Vestibulum quam risus, accumsan eu tincidunt a, molestie eu felis. Morbi sit amet dui ac metus semper pellentesque. Quisque egestas posuere bibendum. Aliquam tristique placerat gravida. Quisque urna leo, dapibus sit amet placerat id, sollicitudin in lorem.');
INSERT INTO `wp_postmeta` VALUES (488,101,'_artist_information','field_538ccb5c967b9');
INSERT INTO `wp_postmeta` VALUES (490,107,'_wp_attached_file','2014/06/album-cover-deraj-nofear.jpg');
INSERT INTO `wp_postmeta` VALUES (491,107,'_wp_attachment_metadata','a:5:{s:5:\"width\";i:540;s:6:\"height\";i:540;s:4:\"file\";s:36:\"2014/06/album-cover-deraj-nofear.jpg\";s:5:\"sizes\";a:10:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:36:\"album-cover-deraj-nofear-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:6:\"medium\";a:4:{s:4:\"file\";s:36:\"album-cover-deraj-nofear-300x300.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:300;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:4:\"tiny\";a:4:{s:4:\"file\";s:34:\"album-cover-deraj-nofear-52x52.jpg\";s:5:\"width\";i:52;s:6:\"height\";i:52;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:11:\"post-medium\";a:4:{s:4:\"file\";s:36:\"album-cover-deraj-nofear-540x292.jpg\";s:5:\"width\";i:540;s:6:\"height\";i:292;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:10:\"post-small\";a:4:{s:4:\"file\";s:36:\"album-cover-deraj-nofear-352x184.jpg\";s:5:\"width\";i:352;s:6:\"height\";i:184;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:14:\"sponsor-medium\";a:4:{s:4:\"file\";s:36:\"album-cover-deraj-nofear-540x310.jpg\";s:5:\"width\";i:540;s:6:\"height\";i:310;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:15:\"sponsor-sidebar\";a:4:{s:4:\"file\";s:36:\"album-cover-deraj-nofear-300x252.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:252;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:11:\"album-cover\";a:4:{s:4:\"file\";s:36:\"album-cover-deraj-nofear-250x250.jpg\";s:5:\"width\";i:250;s:6:\"height\";i:250;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:13:\"profile-image\";a:4:{s:4:\"file\";s:36:\"album-cover-deraj-nofear-285x285.jpg\";s:5:\"width\";i:285;s:6:\"height\";i:285;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:13:\"product-image\";a:4:{s:4:\"file\";s:36:\"album-cover-deraj-nofear-450x450.jpg\";s:5:\"width\";i:450;s:6:\"height\";i:450;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:10:{s:8:\"aperture\";i:0;s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";i:0;s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";i:0;s:3:\"iso\";i:0;s:13:\"shutter_speed\";i:0;s:5:\"title\";s:0:\"\";}}');
INSERT INTO `wp_postmeta` VALUES (492,101,'discography_0_title','No Fear');
INSERT INTO `wp_postmeta` VALUES (493,101,'_discography_0_title','field_5388d100ca3f4');
INSERT INTO `wp_postmeta` VALUES (494,101,'discography_0_cover','107');
INSERT INTO `wp_postmeta` VALUES (495,101,'_discography_0_cover','field_5388d10bca3f5');
INSERT INTO `wp_postmeta` VALUES (496,101,'discography_0_tracks_0_title','Sample Track 1');
INSERT INTO `wp_postmeta` VALUES (497,101,'_discography_0_tracks_0_title','field_538cd0a5bd590');
INSERT INTO `wp_postmeta` VALUES (498,101,'discography_0_tracks_0_audio','95');
INSERT INTO `wp_postmeta` VALUES (499,101,'_discography_0_tracks_0_audio','field_538cd0b5bd591');
INSERT INTO `wp_postmeta` VALUES (500,101,'discography_0_tracks_1_title','Sample Track 2');
INSERT INTO `wp_postmeta` VALUES (501,101,'_discography_0_tracks_1_title','field_538cd0a5bd590');
INSERT INTO `wp_postmeta` VALUES (502,101,'discography_0_tracks_1_audio','95');
INSERT INTO `wp_postmeta` VALUES (503,101,'_discography_0_tracks_1_audio','field_538cd0b5bd591');
INSERT INTO `wp_postmeta` VALUES (504,101,'discography_0_tracks_2_title','Sample Track 3');
INSERT INTO `wp_postmeta` VALUES (505,101,'_discography_0_tracks_2_title','field_538cd0a5bd590');
INSERT INTO `wp_postmeta` VALUES (506,101,'discography_0_tracks_2_audio','95');
INSERT INTO `wp_postmeta` VALUES (507,101,'_discography_0_tracks_2_audio','field_538cd0b5bd591');
INSERT INTO `wp_postmeta` VALUES (508,101,'discography_0_tracks_3_title','Sample Track 4');
INSERT INTO `wp_postmeta` VALUES (509,101,'_discography_0_tracks_3_title','field_538cd0a5bd590');
INSERT INTO `wp_postmeta` VALUES (510,101,'discography_0_tracks_3_audio','95');
INSERT INTO `wp_postmeta` VALUES (511,101,'_discography_0_tracks_3_audio','field_538cd0b5bd591');
INSERT INTO `wp_postmeta` VALUES (512,101,'discography_0_tracks_4_title','Sample Track 5');
INSERT INTO `wp_postmeta` VALUES (513,101,'_discography_0_tracks_4_title','field_538cd0a5bd590');
INSERT INTO `wp_postmeta` VALUES (514,101,'discography_0_tracks_4_audio','95');
INSERT INTO `wp_postmeta` VALUES (515,101,'_discography_0_tracks_4_audio','field_538cd0b5bd591');
INSERT INTO `wp_postmeta` VALUES (516,101,'discography_0_tracks_5_title','Sample Track 6');
INSERT INTO `wp_postmeta` VALUES (517,101,'_discography_0_tracks_5_title','field_538cd0a5bd590');
INSERT INTO `wp_postmeta` VALUES (518,101,'discography_0_tracks_5_audio','95');
INSERT INTO `wp_postmeta` VALUES (519,101,'_discography_0_tracks_5_audio','field_538cd0b5bd591');
INSERT INTO `wp_postmeta` VALUES (520,101,'discography_0_tracks_6_title','Sample Track 7');
INSERT INTO `wp_postmeta` VALUES (521,101,'_discography_0_tracks_6_title','field_538cd0a5bd590');
INSERT INTO `wp_postmeta` VALUES (522,101,'discography_0_tracks_6_audio','95');
INSERT INTO `wp_postmeta` VALUES (523,101,'_discography_0_tracks_6_audio','field_538cd0b5bd591');
INSERT INTO `wp_postmeta` VALUES (524,101,'discography_0_tracks_7_title','Sample Track 8');
INSERT INTO `wp_postmeta` VALUES (525,101,'_discography_0_tracks_7_title','field_538cd0a5bd590');
INSERT INTO `wp_postmeta` VALUES (526,101,'discography_0_tracks_7_audio','95');
INSERT INTO `wp_postmeta` VALUES (527,101,'_discography_0_tracks_7_audio','field_538cd0b5bd591');
INSERT INTO `wp_postmeta` VALUES (528,101,'discography_0_tracks_8_title','Sample Track 9');
INSERT INTO `wp_postmeta` VALUES (529,101,'_discography_0_tracks_8_title','field_538cd0a5bd590');
INSERT INTO `wp_postmeta` VALUES (530,101,'discography_0_tracks_8_audio','95');
INSERT INTO `wp_postmeta` VALUES (531,101,'_discography_0_tracks_8_audio','field_538cd0b5bd591');
INSERT INTO `wp_postmeta` VALUES (532,101,'discography_0_tracks_9_title','Sample Track 10');
INSERT INTO `wp_postmeta` VALUES (533,101,'_discography_0_tracks_9_title','field_538cd0a5bd590');
INSERT INTO `wp_postmeta` VALUES (534,101,'discography_0_tracks_9_audio','95');
INSERT INTO `wp_postmeta` VALUES (535,101,'_discography_0_tracks_9_audio','field_538cd0b5bd591');
INSERT INTO `wp_postmeta` VALUES (536,101,'discography_0_tracks','10');
INSERT INTO `wp_postmeta` VALUES (537,101,'_discography_0_tracks','field_5388d113ca3f6');
INSERT INTO `wp_postmeta` VALUES (538,23,'field_538cd40732c33','a:11:{s:3:\"key\";s:19:\"field_538cd40732c33\";s:5:\"label\";s:25:\"Default Album Cover Image\";s:4:\"name\";s:25:\"default_album_cover_image\";s:4:\"type\";s:5:\"image\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";s:1:\"0\";s:11:\"save_format\";s:6:\"object\";s:12:\"preview_size\";s:9:\"thumbnail\";s:7:\"library\";s:3:\"all\";s:17:\"conditional_logic\";a:3:{s:6:\"status\";s:1:\"0\";s:5:\"rules\";a:1:{i:0;a:3:{s:5:\"field\";s:4:\"null\";s:8:\"operator\";s:2:\"==\";s:5:\"value\";s:0:\"\";}}s:8:\"allorany\";s:3:\"all\";}s:8:\"order_no\";i:4;}');
INSERT INTO `wp_postmeta` VALUES (540,108,'_wp_attached_file','2014/06/default-album-cover.jpg');
INSERT INTO `wp_postmeta` VALUES (541,108,'_wp_attachment_metadata','a:5:{s:5:\"width\";i:600;s:6:\"height\";i:600;s:4:\"file\";s:31:\"2014/06/default-album-cover.jpg\";s:5:\"sizes\";a:10:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:31:\"default-album-cover-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:6:\"medium\";a:4:{s:4:\"file\";s:31:\"default-album-cover-300x300.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:300;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:4:\"tiny\";a:4:{s:4:\"file\";s:29:\"default-album-cover-52x52.jpg\";s:5:\"width\";i:52;s:6:\"height\";i:52;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:11:\"post-medium\";a:4:{s:4:\"file\";s:31:\"default-album-cover-600x292.jpg\";s:5:\"width\";i:600;s:6:\"height\";i:292;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:10:\"post-small\";a:4:{s:4:\"file\";s:31:\"default-album-cover-352x184.jpg\";s:5:\"width\";i:352;s:6:\"height\";i:184;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:14:\"sponsor-medium\";a:4:{s:4:\"file\";s:31:\"default-album-cover-600x310.jpg\";s:5:\"width\";i:600;s:6:\"height\";i:310;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:15:\"sponsor-sidebar\";a:4:{s:4:\"file\";s:31:\"default-album-cover-300x252.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:252;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:11:\"album-cover\";a:4:{s:4:\"file\";s:31:\"default-album-cover-250x250.jpg\";s:5:\"width\";i:250;s:6:\"height\";i:250;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:13:\"profile-image\";a:4:{s:4:\"file\";s:31:\"default-album-cover-285x285.jpg\";s:5:\"width\";i:285;s:6:\"height\";i:285;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:13:\"product-image\";a:4:{s:4:\"file\";s:31:\"default-album-cover-450x450.jpg\";s:5:\"width\";i:450;s:6:\"height\";i:450;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:10:{s:8:\"aperture\";i:0;s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";i:0;s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";i:0;s:3:\"iso\";i:0;s:13:\"shutter_speed\";i:0;s:5:\"title\";s:0:\"\";}}');
INSERT INTO `wp_postmeta` VALUES (542,90,'field_538cdc533f86f','a:8:{s:3:\"key\";s:19:\"field_538cdc533f86f\";s:5:\"label\";s:6:\"Social\";s:4:\"name\";s:0:\"\";s:4:\"type\";s:3:\"tab\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";s:1:\"0\";s:17:\"conditional_logic\";a:3:{s:6:\"status\";s:1:\"0\";s:5:\"rules\";a:1:{i:0;a:3:{s:5:\"field\";s:4:\"null\";s:8:\"operator\";s:2:\"==\";s:5:\"value\";s:0:\"\";}}s:8:\"allorany\";s:3:\"all\";}s:8:\"order_no\";i:9;}');
INSERT INTO `wp_postmeta` VALUES (543,90,'field_538cdc8a3f870','a:14:{s:3:\"key\";s:19:\"field_538cdc8a3f870\";s:5:\"label\";s:8:\"Facebook\";s:4:\"name\";s:8:\"facebook\";s:4:\"type\";s:4:\"text\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";s:1:\"0\";s:13:\"default_value\";s:0:\"\";s:11:\"placeholder\";s:0:\"\";s:7:\"prepend\";s:13:\"facebook.com/\";s:6:\"append\";s:0:\"\";s:10:\"formatting\";s:4:\"html\";s:9:\"maxlength\";s:0:\"\";s:17:\"conditional_logic\";a:3:{s:6:\"status\";s:1:\"0\";s:5:\"rules\";a:1:{i:0;a:3:{s:5:\"field\";s:4:\"null\";s:8:\"operator\";s:2:\"==\";s:5:\"value\";s:0:\"\";}}s:8:\"allorany\";s:3:\"all\";}s:8:\"order_no\";i:10;}');
INSERT INTO `wp_postmeta` VALUES (544,90,'field_538cdc9a3f871','a:14:{s:3:\"key\";s:19:\"field_538cdc9a3f871\";s:5:\"label\";s:10:\"Soundcloud\";s:4:\"name\";s:10:\"soundcloud\";s:4:\"type\";s:4:\"text\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";s:1:\"0\";s:13:\"default_value\";s:0:\"\";s:11:\"placeholder\";s:0:\"\";s:7:\"prepend\";s:15:\"soundcloud.com/\";s:6:\"append\";s:0:\"\";s:10:\"formatting\";s:4:\"html\";s:9:\"maxlength\";s:0:\"\";s:17:\"conditional_logic\";a:3:{s:6:\"status\";s:1:\"0\";s:5:\"rules\";a:1:{i:0;a:3:{s:5:\"field\";s:4:\"null\";s:8:\"operator\";s:2:\"==\";s:5:\"value\";s:0:\"\";}}s:8:\"allorany\";s:3:\"all\";}s:8:\"order_no\";i:11;}');
INSERT INTO `wp_postmeta` VALUES (545,90,'field_538cdcd83f872','a:14:{s:3:\"key\";s:19:\"field_538cdcd83f872\";s:5:\"label\";s:6:\"iTunes\";s:4:\"name\";s:6:\"itunes\";s:4:\"type\";s:4:\"text\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";s:1:\"0\";s:13:\"default_value\";s:0:\"\";s:11:\"placeholder\";s:0:\"\";s:7:\"prepend\";s:0:\"\";s:6:\"append\";s:0:\"\";s:10:\"formatting\";s:4:\"html\";s:9:\"maxlength\";s:0:\"\";s:17:\"conditional_logic\";a:3:{s:6:\"status\";s:1:\"0\";s:5:\"rules\";a:1:{i:0;a:3:{s:5:\"field\";s:4:\"null\";s:8:\"operator\";s:2:\"==\";s:5:\"value\";s:0:\"\";}}s:8:\"allorany\";s:3:\"all\";}s:8:\"order_no\";i:12;}');
INSERT INTO `wp_postmeta` VALUES (546,90,'field_538cdce83f873','a:14:{s:3:\"key\";s:19:\"field_538cdce83f873\";s:5:\"label\";s:7:\"Twitter\";s:4:\"name\";s:7:\"twitter\";s:4:\"type\";s:4:\"text\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";s:1:\"0\";s:13:\"default_value\";s:0:\"\";s:11:\"placeholder\";s:0:\"\";s:7:\"prepend\";s:12:\"twitter.com/\";s:6:\"append\";s:0:\"\";s:10:\"formatting\";s:4:\"html\";s:9:\"maxlength\";s:0:\"\";s:17:\"conditional_logic\";a:3:{s:6:\"status\";s:1:\"0\";s:5:\"rules\";a:1:{i:0;a:3:{s:5:\"field\";s:4:\"null\";s:8:\"operator\";s:2:\"==\";s:5:\"value\";s:0:\"\";}}s:8:\"allorany\";s:3:\"all\";}s:8:\"order_no\";i:13;}');
INSERT INTO `wp_postmeta` VALUES (548,101,'facebook','deraj');
INSERT INTO `wp_postmeta` VALUES (549,101,'_facebook','field_538cdc8a3f870');
INSERT INTO `wp_postmeta` VALUES (550,101,'soundcloud','derajmusic');
INSERT INTO `wp_postmeta` VALUES (551,101,'_soundcloud','field_538cdc9a3f871');
INSERT INTO `wp_postmeta` VALUES (552,101,'itunes','https://itunes.apple.com/us/artist/deraj/id340877143');
INSERT INTO `wp_postmeta` VALUES (553,101,'_itunes','field_538cdcd83f872');
INSERT INTO `wp_postmeta` VALUES (554,101,'twitter','justderaj');
INSERT INTO `wp_postmeta` VALUES (555,101,'_twitter','field_538cdce83f873');
INSERT INTO `wp_postmeta` VALUES (556,101,'discography_1_title','Test Album');
INSERT INTO `wp_postmeta` VALUES (557,101,'_discography_1_title','field_5388d100ca3f4');
INSERT INTO `wp_postmeta` VALUES (558,101,'discography_1_cover','');
INSERT INTO `wp_postmeta` VALUES (559,101,'_discography_1_cover','field_5388d10bca3f5');
INSERT INTO `wp_postmeta` VALUES (560,101,'discography_1_tracks_0_title','Sample Track 1');
INSERT INTO `wp_postmeta` VALUES (561,101,'_discography_1_tracks_0_title','field_538cd0a5bd590');
INSERT INTO `wp_postmeta` VALUES (562,101,'discography_1_tracks_0_audio','95');
INSERT INTO `wp_postmeta` VALUES (563,101,'_discography_1_tracks_0_audio','field_538cd0b5bd591');
INSERT INTO `wp_postmeta` VALUES (564,101,'discography_1_tracks_1_title','Sample Track 2');
INSERT INTO `wp_postmeta` VALUES (565,101,'_discography_1_tracks_1_title','field_538cd0a5bd590');
INSERT INTO `wp_postmeta` VALUES (566,101,'discography_1_tracks_1_audio','95');
INSERT INTO `wp_postmeta` VALUES (567,101,'_discography_1_tracks_1_audio','field_538cd0b5bd591');
INSERT INTO `wp_postmeta` VALUES (568,101,'discography_1_tracks_2_title','Sample Track 3');
INSERT INTO `wp_postmeta` VALUES (569,101,'_discography_1_tracks_2_title','field_538cd0a5bd590');
INSERT INTO `wp_postmeta` VALUES (570,101,'discography_1_tracks_2_audio','95');
INSERT INTO `wp_postmeta` VALUES (571,101,'_discography_1_tracks_2_audio','field_538cd0b5bd591');
INSERT INTO `wp_postmeta` VALUES (572,101,'discography_1_tracks','3');
INSERT INTO `wp_postmeta` VALUES (573,101,'_discography_1_tracks','field_5388d113ca3f6');
INSERT INTO `wp_postmeta` VALUES (574,109,'_edit_lock','1401804059:1');
INSERT INTO `wp_postmeta` VALUES (575,109,'_edit_last','1');
INSERT INTO `wp_postmeta` VALUES (576,109,'_wp_page_template','page-store.php');
INSERT INTO `wp_postmeta` VALUES (577,111,'_menu_item_type','post_type');
INSERT INTO `wp_postmeta` VALUES (578,111,'_menu_item_menu_item_parent','0');
INSERT INTO `wp_postmeta` VALUES (579,111,'_menu_item_object_id','109');
INSERT INTO `wp_postmeta` VALUES (580,111,'_menu_item_object','page');
INSERT INTO `wp_postmeta` VALUES (581,111,'_menu_item_target','');
INSERT INTO `wp_postmeta` VALUES (582,111,'_menu_item_classes','a:1:{i:0;s:0:\"\";}');
INSERT INTO `wp_postmeta` VALUES (583,111,'_menu_item_xfn','');
INSERT INTO `wp_postmeta` VALUES (584,111,'_menu_item_url','');
INSERT INTO `wp_postmeta` VALUES (586,112,'_edit_lock','1401799285:1');
INSERT INTO `wp_postmeta` VALUES (587,113,'_edit_lock','1401799406:1');
INSERT INTO `wp_postmeta` VALUES (588,115,'_edit_lock','1401801029:1');
INSERT INTO `wp_postmeta` VALUES (589,115,'_edit_last','1');
INSERT INTO `wp_postmeta` VALUES (591,115,'position','normal');
INSERT INTO `wp_postmeta` VALUES (592,115,'layout','no_box');
INSERT INTO `wp_postmeta` VALUES (593,115,'hide_on_screen','a:12:{i:0;s:11:\"the_content\";i:1;s:7:\"excerpt\";i:2;s:13:\"custom_fields\";i:3;s:10:\"discussion\";i:4;s:8:\"comments\";i:5;s:9:\"revisions\";i:6;s:6:\"author\";i:7;s:6:\"format\";i:8;s:14:\"featured_image\";i:9;s:10:\"categories\";i:10;s:4:\"tags\";i:11;s:15:\"send-trackbacks\";}');
INSERT INTO `wp_postmeta` VALUES (594,116,'_edit_lock','1401799499:1');
INSERT INTO `wp_postmeta` VALUES (595,116,'_edit_last','1');
INSERT INTO `wp_postmeta` VALUES (596,116,'_wp_page_template','default');
INSERT INTO `wp_postmeta` VALUES (598,115,'field_538dc92a2e54d','a:13:{s:3:\"key\";s:19:\"field_538dc92a2e54d\";s:5:\"label\";s:8:\"Products\";s:4:\"name\";s:8:\"products\";s:4:\"type\";s:8:\"repeater\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";s:1:\"0\";s:10:\"sub_fields\";a:4:{i:0;a:15:{s:3:\"key\";s:19:\"field_538dc965d58e1\";s:5:\"label\";s:5:\"TItle\";s:4:\"name\";s:5:\"title\";s:4:\"type\";s:4:\"text\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";s:1:\"0\";s:12:\"column_width\";s:0:\"\";s:13:\"default_value\";s:0:\"\";s:11:\"placeholder\";s:0:\"\";s:7:\"prepend\";s:0:\"\";s:6:\"append\";s:0:\"\";s:10:\"formatting\";s:4:\"html\";s:9:\"maxlength\";s:0:\"\";s:17:\"conditional_logic\";a:3:{s:6:\"status\";s:1:\"0\";s:5:\"rules\";a:1:{i:0;a:2:{s:5:\"field\";s:4:\"null\";s:8:\"operator\";s:2:\"==\";}}s:8:\"allorany\";s:3:\"all\";}s:8:\"order_no\";i:0;}i:1;a:12:{s:3:\"key\";s:19:\"field_538dc971d58e2\";s:5:\"label\";s:5:\"Image\";s:4:\"name\";s:5:\"image\";s:4:\"type\";s:5:\"image\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";s:1:\"0\";s:12:\"column_width\";s:0:\"\";s:11:\"save_format\";s:6:\"object\";s:12:\"preview_size\";s:9:\"thumbnail\";s:7:\"library\";s:3:\"all\";s:17:\"conditional_logic\";a:3:{s:6:\"status\";s:1:\"0\";s:5:\"rules\";a:1:{i:0;a:2:{s:5:\"field\";s:4:\"null\";s:8:\"operator\";s:2:\"==\";}}s:8:\"allorany\";s:3:\"all\";}s:8:\"order_no\";i:1;}i:2;a:12:{s:3:\"key\";s:19:\"field_538dc987d58e3\";s:5:\"label\";s:11:\"Description\";s:4:\"name\";s:11:\"description\";s:4:\"type\";s:7:\"wysiwyg\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";s:1:\"0\";s:12:\"column_width\";s:0:\"\";s:13:\"default_value\";s:0:\"\";s:7:\"toolbar\";s:5:\"basic\";s:12:\"media_upload\";s:2:\"no\";s:17:\"conditional_logic\";a:3:{s:6:\"status\";s:1:\"0\";s:5:\"rules\";a:1:{i:0;a:2:{s:5:\"field\";s:4:\"null\";s:8:\"operator\";s:2:\"==\";}}s:8:\"allorany\";s:3:\"all\";}s:8:\"order_no\";i:2;}i:3;a:15:{s:3:\"key\";s:19:\"field_538dc9abd58e4\";s:5:\"label\";s:4:\"Link\";s:4:\"name\";s:4:\"link\";s:4:\"type\";s:4:\"text\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";s:1:\"0\";s:12:\"column_width\";s:0:\"\";s:13:\"default_value\";s:0:\"\";s:11:\"placeholder\";s:0:\"\";s:7:\"prepend\";s:0:\"\";s:6:\"append\";s:0:\"\";s:10:\"formatting\";s:4:\"html\";s:9:\"maxlength\";s:0:\"\";s:17:\"conditional_logic\";a:3:{s:6:\"status\";s:1:\"0\";s:5:\"rules\";a:1:{i:0;a:2:{s:5:\"field\";s:4:\"null\";s:8:\"operator\";s:2:\"==\";}}s:8:\"allorany\";s:3:\"all\";}s:8:\"order_no\";i:3;}}s:7:\"row_min\";s:0:\"\";s:9:\"row_limit\";s:0:\"\";s:6:\"layout\";s:5:\"table\";s:12:\"button_label\";s:11:\"Add Product\";s:17:\"conditional_logic\";a:3:{s:6:\"status\";s:1:\"0\";s:5:\"rules\";a:1:{i:0;a:2:{s:5:\"field\";s:4:\"null\";s:8:\"operator\";s:2:\"==\";}}s:8:\"allorany\";s:3:\"all\";}s:8:\"order_no\";i:0;}');
INSERT INTO `wp_postmeta` VALUES (600,115,'rule','a:5:{s:5:\"param\";s:13:\"page_template\";s:8:\"operator\";s:2:\"==\";s:5:\"value\";s:14:\"page-store.php\";s:8:\"order_no\";i:0;s:8:\"group_no\";i:0;}');
INSERT INTO `wp_postmeta` VALUES (601,109,'products','4');
INSERT INTO `wp_postmeta` VALUES (602,109,'_products','field_538dc92a2e54d');
INSERT INTO `wp_postmeta` VALUES (603,118,'_wp_attached_file','2014/06/product-album.jpg');
INSERT INTO `wp_postmeta` VALUES (604,118,'_wp_attachment_metadata','a:5:{s:5:\"width\";i:450;s:6:\"height\";i:450;s:4:\"file\";s:25:\"2014/06/product-album.jpg\";s:5:\"sizes\";a:9:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:25:\"product-album-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:6:\"medium\";a:4:{s:4:\"file\";s:25:\"product-album-300x300.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:300;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:4:\"tiny\";a:4:{s:4:\"file\";s:23:\"product-album-52x52.jpg\";s:5:\"width\";i:52;s:6:\"height\";i:52;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:11:\"post-medium\";a:4:{s:4:\"file\";s:25:\"product-album-450x292.jpg\";s:5:\"width\";i:450;s:6:\"height\";i:292;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:10:\"post-small\";a:4:{s:4:\"file\";s:25:\"product-album-352x184.jpg\";s:5:\"width\";i:352;s:6:\"height\";i:184;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:14:\"sponsor-medium\";a:4:{s:4:\"file\";s:25:\"product-album-450x310.jpg\";s:5:\"width\";i:450;s:6:\"height\";i:310;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:15:\"sponsor-sidebar\";a:4:{s:4:\"file\";s:25:\"product-album-300x252.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:252;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:11:\"album-cover\";a:4:{s:4:\"file\";s:25:\"product-album-250x250.jpg\";s:5:\"width\";i:250;s:6:\"height\";i:250;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:13:\"profile-image\";a:4:{s:4:\"file\";s:25:\"product-album-285x285.jpg\";s:5:\"width\";i:285;s:6:\"height\";i:285;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:10:{s:8:\"aperture\";i:0;s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";i:0;s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";i:0;s:3:\"iso\";i:0;s:13:\"shutter_speed\";i:0;s:5:\"title\";s:0:\"\";}}');
INSERT INTO `wp_postmeta` VALUES (605,119,'products_0_title','Botnit Album');
INSERT INTO `wp_postmeta` VALUES (606,119,'_products_0_title','field_538dc965d58e1');
INSERT INTO `wp_postmeta` VALUES (607,119,'products_0_image','118');
INSERT INTO `wp_postmeta` VALUES (608,119,'_products_0_image','field_538dc971d58e2');
INSERT INTO `wp_postmeta` VALUES (609,119,'products_0_description','Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam in volutpat purus. Integer orci mi, rutrum ac viverra quis, ultrices id augue. Fusce nibh nulla, pretium non elementum hendrerit, lobortis sit amet magna. Vivamus metus massa, sagittis non feugiat at, condimentum imperdiet.');
INSERT INTO `wp_postmeta` VALUES (610,119,'_products_0_description','field_538dc987d58e3');
INSERT INTO `wp_postmeta` VALUES (611,119,'products_0_link','#');
INSERT INTO `wp_postmeta` VALUES (612,119,'_products_0_link','field_538dc9abd58e4');
INSERT INTO `wp_postmeta` VALUES (613,119,'products_1_title','Deraj Album');
INSERT INTO `wp_postmeta` VALUES (614,119,'_products_1_title','field_538dc965d58e1');
INSERT INTO `wp_postmeta` VALUES (615,119,'products_1_image','102');
INSERT INTO `wp_postmeta` VALUES (616,119,'_products_1_image','field_538dc971d58e2');
INSERT INTO `wp_postmeta` VALUES (617,119,'products_1_description','Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam in volutpat purus. Integer orci mi, rutrum ac viverra quis, ultrices id augue. Fusce nibh nulla, pretium non elementum hendrerit, lobortis sit amet magna. Vivamus metus massa, sagittis non feugiat at, condimentum imperdiet.');
INSERT INTO `wp_postmeta` VALUES (618,119,'_products_1_description','field_538dc987d58e3');
INSERT INTO `wp_postmeta` VALUES (619,119,'products_1_link','#');
INSERT INTO `wp_postmeta` VALUES (620,119,'_products_1_link','field_538dc9abd58e4');
INSERT INTO `wp_postmeta` VALUES (621,119,'products_2_title','Record Album');
INSERT INTO `wp_postmeta` VALUES (622,119,'_products_2_title','field_538dc965d58e1');
INSERT INTO `wp_postmeta` VALUES (623,119,'products_2_image','76');
INSERT INTO `wp_postmeta` VALUES (624,119,'_products_2_image','field_538dc971d58e2');
INSERT INTO `wp_postmeta` VALUES (625,119,'products_2_description','Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam in volutpat purus. Integer orci mi, rutrum ac viverra quis, ultrices id augue. Fusce nibh nulla, pretium non elementum hendrerit, lobortis sit amet magna. Vivamus metus massa, sagittis non feugiat at, condimentum imperdiet.');
INSERT INTO `wp_postmeta` VALUES (626,119,'_products_2_description','field_538dc987d58e3');
INSERT INTO `wp_postmeta` VALUES (627,119,'products_2_link','#');
INSERT INTO `wp_postmeta` VALUES (628,119,'_products_2_link','field_538dc9abd58e4');
INSERT INTO `wp_postmeta` VALUES (629,119,'products_3_title','Botnit Album');
INSERT INTO `wp_postmeta` VALUES (630,119,'_products_3_title','field_538dc965d58e1');
INSERT INTO `wp_postmeta` VALUES (631,119,'products_3_image','118');
INSERT INTO `wp_postmeta` VALUES (632,119,'_products_3_image','field_538dc971d58e2');
INSERT INTO `wp_postmeta` VALUES (633,119,'products_3_description','Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam in volutpat purus. Integer orci mi, rutrum ac viverra quis, ultrices id augue. Fusce nibh nulla, pretium non elementum hendrerit, lobortis sit amet magna. Vivamus metus massa, sagittis non feugiat at, condimentum imperdiet.');
INSERT INTO `wp_postmeta` VALUES (634,119,'_products_3_description','field_538dc987d58e3');
INSERT INTO `wp_postmeta` VALUES (635,119,'products_3_link','#');
INSERT INTO `wp_postmeta` VALUES (636,119,'_products_3_link','field_538dc9abd58e4');
INSERT INTO `wp_postmeta` VALUES (637,119,'products','4');
INSERT INTO `wp_postmeta` VALUES (638,119,'_products','field_538dc92a2e54d');
INSERT INTO `wp_postmeta` VALUES (639,109,'products_0_title','Botnit Album');
INSERT INTO `wp_postmeta` VALUES (640,109,'_products_0_title','field_538dc965d58e1');
INSERT INTO `wp_postmeta` VALUES (641,109,'products_0_image','118');
INSERT INTO `wp_postmeta` VALUES (642,109,'_products_0_image','field_538dc971d58e2');
INSERT INTO `wp_postmeta` VALUES (643,109,'products_0_description','Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam in volutpat purus. Integer orci mi, rutrum ac viverra quis, ultrices id augue. Fusce nibh nulla, pretium non elementum hendrerit, lobortis sit amet magna. Vivamus metus massa, sagittis non feugiat at, condimentum imperdiet.');
INSERT INTO `wp_postmeta` VALUES (644,109,'_products_0_description','field_538dc987d58e3');
INSERT INTO `wp_postmeta` VALUES (645,109,'products_0_link','#');
INSERT INTO `wp_postmeta` VALUES (646,109,'_products_0_link','field_538dc9abd58e4');
INSERT INTO `wp_postmeta` VALUES (647,109,'products_1_title','Deraj Album');
INSERT INTO `wp_postmeta` VALUES (648,109,'_products_1_title','field_538dc965d58e1');
INSERT INTO `wp_postmeta` VALUES (649,109,'products_1_image','102');
INSERT INTO `wp_postmeta` VALUES (650,109,'_products_1_image','field_538dc971d58e2');
INSERT INTO `wp_postmeta` VALUES (651,109,'products_1_description','Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam in volutpat purus. Integer orci mi, rutrum ac viverra quis, ultrices id augue.');
INSERT INTO `wp_postmeta` VALUES (652,109,'_products_1_description','field_538dc987d58e3');
INSERT INTO `wp_postmeta` VALUES (653,109,'products_1_link','#');
INSERT INTO `wp_postmeta` VALUES (654,109,'_products_1_link','field_538dc9abd58e4');
INSERT INTO `wp_postmeta` VALUES (655,109,'products_2_title','Record Album');
INSERT INTO `wp_postmeta` VALUES (656,109,'_products_2_title','field_538dc965d58e1');
INSERT INTO `wp_postmeta` VALUES (657,109,'products_2_image','76');
INSERT INTO `wp_postmeta` VALUES (658,109,'_products_2_image','field_538dc971d58e2');
INSERT INTO `wp_postmeta` VALUES (659,109,'products_2_description','Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam in volutpat purus. Integer orci mi, rutrum ac viverra quis, ultrices id augue. Fusce nibh nulla, pretium non elementum hendrerit, lobortis sit amet magna. Vivamus metus massa, sagittis non feugiat at, condimentum imperdiet.');
INSERT INTO `wp_postmeta` VALUES (660,109,'_products_2_description','field_538dc987d58e3');
INSERT INTO `wp_postmeta` VALUES (661,109,'products_2_link','#');
INSERT INTO `wp_postmeta` VALUES (662,109,'_products_2_link','field_538dc9abd58e4');
INSERT INTO `wp_postmeta` VALUES (663,109,'products_3_title','Botnit Album');
INSERT INTO `wp_postmeta` VALUES (664,109,'_products_3_title','field_538dc965d58e1');
INSERT INTO `wp_postmeta` VALUES (665,109,'products_3_image','118');
INSERT INTO `wp_postmeta` VALUES (666,109,'_products_3_image','field_538dc971d58e2');
INSERT INTO `wp_postmeta` VALUES (667,109,'products_3_description','Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam in volutpat purus. Integer orci mi, rutrum ac viverra quis, ultrices id augue. Fusce nibh nulla, pretium non elementum hendrerit, lobortis sit amet magna. Vivamus metus massa, sagittis non feugiat at, condimentum imperdiet.');
INSERT INTO `wp_postmeta` VALUES (668,109,'_products_3_description','field_538dc987d58e3');
INSERT INTO `wp_postmeta` VALUES (669,109,'products_3_link','#');
INSERT INTO `wp_postmeta` VALUES (670,109,'_products_3_link','field_538dc9abd58e4');
INSERT INTO `wp_postmeta` VALUES (671,120,'products_0_title','Botnit Album');
INSERT INTO `wp_postmeta` VALUES (672,120,'_products_0_title','field_538dc965d58e1');
INSERT INTO `wp_postmeta` VALUES (673,120,'products_0_image','118');
INSERT INTO `wp_postmeta` VALUES (674,120,'_products_0_image','field_538dc971d58e2');
INSERT INTO `wp_postmeta` VALUES (675,120,'products_0_description','Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam in volutpat purus. Integer orci mi, rutrum ac viverra quis, ultrices id augue. Fusce nibh nulla, pretium non elementum hendrerit, lobortis sit amet magna. Vivamus metus massa, sagittis non feugiat at, condimentum imperdiet.');
INSERT INTO `wp_postmeta` VALUES (676,120,'_products_0_description','field_538dc987d58e3');
INSERT INTO `wp_postmeta` VALUES (677,120,'products_0_link','#');
INSERT INTO `wp_postmeta` VALUES (678,120,'_products_0_link','field_538dc9abd58e4');
INSERT INTO `wp_postmeta` VALUES (679,120,'products_1_title','Deraj Album');
INSERT INTO `wp_postmeta` VALUES (680,120,'_products_1_title','field_538dc965d58e1');
INSERT INTO `wp_postmeta` VALUES (681,120,'products_1_image','102');
INSERT INTO `wp_postmeta` VALUES (682,120,'_products_1_image','field_538dc971d58e2');
INSERT INTO `wp_postmeta` VALUES (683,120,'products_1_description','Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam in volutpat purus. Integer orci mi, rutrum ac viverra quis, ultrices id augue.');
INSERT INTO `wp_postmeta` VALUES (684,120,'_products_1_description','field_538dc987d58e3');
INSERT INTO `wp_postmeta` VALUES (685,120,'products_1_link','#');
INSERT INTO `wp_postmeta` VALUES (686,120,'_products_1_link','field_538dc9abd58e4');
INSERT INTO `wp_postmeta` VALUES (687,120,'products_2_title','Record Album');
INSERT INTO `wp_postmeta` VALUES (688,120,'_products_2_title','field_538dc965d58e1');
INSERT INTO `wp_postmeta` VALUES (689,120,'products_2_image','76');
INSERT INTO `wp_postmeta` VALUES (690,120,'_products_2_image','field_538dc971d58e2');
INSERT INTO `wp_postmeta` VALUES (691,120,'products_2_description','Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam in volutpat purus. Integer orci mi, rutrum ac viverra quis, ultrices id augue. Fusce nibh nulla, pretium non elementum hendrerit, lobortis sit amet magna. Vivamus metus massa, sagittis non feugiat at, condimentum imperdiet.');
INSERT INTO `wp_postmeta` VALUES (692,120,'_products_2_description','field_538dc987d58e3');
INSERT INTO `wp_postmeta` VALUES (693,120,'products_2_link','#');
INSERT INTO `wp_postmeta` VALUES (694,120,'_products_2_link','field_538dc9abd58e4');
INSERT INTO `wp_postmeta` VALUES (695,120,'products_3_title','Botnit Album');
INSERT INTO `wp_postmeta` VALUES (696,120,'_products_3_title','field_538dc965d58e1');
INSERT INTO `wp_postmeta` VALUES (697,120,'products_3_image','118');
INSERT INTO `wp_postmeta` VALUES (698,120,'_products_3_image','field_538dc971d58e2');
INSERT INTO `wp_postmeta` VALUES (699,120,'products_3_description','Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam in volutpat purus. Integer orci mi, rutrum ac viverra quis, ultrices id augue. Fusce nibh nulla, pretium non elementum hendrerit, lobortis sit amet magna. Vivamus metus massa, sagittis non feugiat at, condimentum imperdiet.');
INSERT INTO `wp_postmeta` VALUES (700,120,'_products_3_description','field_538dc987d58e3');
INSERT INTO `wp_postmeta` VALUES (701,120,'products_3_link','#');
INSERT INTO `wp_postmeta` VALUES (702,120,'_products_3_link','field_538dc9abd58e4');
INSERT INTO `wp_postmeta` VALUES (703,120,'products','4');
INSERT INTO `wp_postmeta` VALUES (704,120,'_products','field_538dc92a2e54d');
INSERT INTO `wp_postmeta` VALUES (706,23,'field_538dd6404e1ef','a:8:{s:3:\"key\";s:19:\"field_538dd6404e1ef\";s:5:\"label\";s:6:\"Footer\";s:4:\"name\";s:0:\"\";s:4:\"type\";s:3:\"tab\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";s:1:\"0\";s:17:\"conditional_logic\";a:3:{s:6:\"status\";s:1:\"0\";s:5:\"rules\";a:1:{i:0;a:2:{s:5:\"field\";s:4:\"null\";s:8:\"operator\";s:2:\"==\";}}s:8:\"allorany\";s:3:\"all\";}s:8:\"order_no\";i:7;}');
INSERT INTO `wp_postmeta` VALUES (707,23,'field_538dd64f4e1f0','a:14:{s:3:\"key\";s:19:\"field_538dd64f4e1f0\";s:5:\"label\";s:11:\"About Title\";s:4:\"name\";s:11:\"about_title\";s:4:\"type\";s:4:\"text\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";s:1:\"0\";s:13:\"default_value\";s:0:\"\";s:11:\"placeholder\";s:0:\"\";s:7:\"prepend\";s:0:\"\";s:6:\"append\";s:0:\"\";s:10:\"formatting\";s:4:\"html\";s:9:\"maxlength\";s:0:\"\";s:17:\"conditional_logic\";a:3:{s:6:\"status\";s:1:\"0\";s:5:\"rules\";a:1:{i:0;a:2:{s:5:\"field\";s:4:\"null\";s:8:\"operator\";s:2:\"==\";}}s:8:\"allorany\";s:3:\"all\";}s:8:\"order_no\";i:8;}');
INSERT INTO `wp_postmeta` VALUES (708,23,'field_538dd68c4e1f1','a:13:{s:3:\"key\";s:19:\"field_538dd68c4e1f1\";s:5:\"label\";s:17:\"About Description\";s:4:\"name\";s:17:\"about_description\";s:4:\"type\";s:8:\"textarea\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";s:1:\"0\";s:13:\"default_value\";s:0:\"\";s:11:\"placeholder\";s:0:\"\";s:9:\"maxlength\";s:0:\"\";s:4:\"rows\";s:0:\"\";s:10:\"formatting\";s:2:\"br\";s:17:\"conditional_logic\";a:3:{s:6:\"status\";s:1:\"0\";s:5:\"rules\";a:1:{i:0;a:2:{s:5:\"field\";s:4:\"null\";s:8:\"operator\";s:2:\"==\";}}s:8:\"allorany\";s:3:\"all\";}s:8:\"order_no\";i:9;}');
INSERT INTO `wp_postmeta` VALUES (709,23,'field_538dd69f4e1f2','a:14:{s:3:\"key\";s:19:\"field_538dd69f4e1f2\";s:5:\"label\";s:7:\"Twitter\";s:4:\"name\";s:7:\"twitter\";s:4:\"type\";s:4:\"text\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";s:1:\"0\";s:13:\"default_value\";s:0:\"\";s:11:\"placeholder\";s:0:\"\";s:7:\"prepend\";s:0:\"\";s:6:\"append\";s:0:\"\";s:10:\"formatting\";s:4:\"html\";s:9:\"maxlength\";s:0:\"\";s:17:\"conditional_logic\";a:3:{s:6:\"status\";s:1:\"0\";s:5:\"rules\";a:1:{i:0;a:2:{s:5:\"field\";s:4:\"null\";s:8:\"operator\";s:2:\"==\";}}s:8:\"allorany\";s:3:\"all\";}s:8:\"order_no\";i:10;}');
INSERT INTO `wp_postmeta` VALUES (710,23,'field_538dd6af4e1f3','a:14:{s:3:\"key\";s:19:\"field_538dd6af4e1f3\";s:5:\"label\";s:8:\"Facebook\";s:4:\"name\";s:8:\"facebook\";s:4:\"type\";s:4:\"text\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";s:1:\"0\";s:13:\"default_value\";s:0:\"\";s:11:\"placeholder\";s:0:\"\";s:7:\"prepend\";s:0:\"\";s:6:\"append\";s:0:\"\";s:10:\"formatting\";s:4:\"html\";s:9:\"maxlength\";s:0:\"\";s:17:\"conditional_logic\";a:3:{s:6:\"status\";s:1:\"0\";s:5:\"rules\";a:1:{i:0;a:2:{s:5:\"field\";s:4:\"null\";s:8:\"operator\";s:2:\"==\";}}s:8:\"allorany\";s:3:\"all\";}s:8:\"order_no\";i:11;}');
INSERT INTO `wp_postmeta` VALUES (711,23,'field_538dd6bd4e1f4','a:14:{s:3:\"key\";s:19:\"field_538dd6bd4e1f4\";s:5:\"label\";s:7:\"YouTube\";s:4:\"name\";s:7:\"youtube\";s:4:\"type\";s:4:\"text\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";s:1:\"0\";s:13:\"default_value\";s:0:\"\";s:11:\"placeholder\";s:0:\"\";s:7:\"prepend\";s:0:\"\";s:6:\"append\";s:0:\"\";s:10:\"formatting\";s:4:\"html\";s:9:\"maxlength\";s:0:\"\";s:17:\"conditional_logic\";a:3:{s:6:\"status\";s:1:\"0\";s:5:\"rules\";a:1:{i:0;a:2:{s:5:\"field\";s:4:\"null\";s:8:\"operator\";s:2:\"==\";}}s:8:\"allorany\";s:3:\"all\";}s:8:\"order_no\";i:12;}');
INSERT INTO `wp_postmeta` VALUES (712,23,'field_538dd6c44e1f5','a:14:{s:3:\"key\";s:19:\"field_538dd6c44e1f5\";s:5:\"label\";s:10:\"SoundCloud\";s:4:\"name\";s:10:\"soundcloud\";s:4:\"type\";s:4:\"text\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";s:1:\"0\";s:13:\"default_value\";s:0:\"\";s:11:\"placeholder\";s:0:\"\";s:7:\"prepend\";s:0:\"\";s:6:\"append\";s:0:\"\";s:10:\"formatting\";s:4:\"html\";s:9:\"maxlength\";s:0:\"\";s:17:\"conditional_logic\";a:3:{s:6:\"status\";s:1:\"0\";s:5:\"rules\";a:1:{i:0;a:2:{s:5:\"field\";s:4:\"null\";s:8:\"operator\";s:2:\"==\";}}s:8:\"allorany\";s:3:\"all\";}s:8:\"order_no\";i:13;}');
INSERT INTO `wp_postmeta` VALUES (713,23,'field_538dd6ce4e1f6','a:14:{s:3:\"key\";s:19:\"field_538dd6ce4e1f6\";s:5:\"label\";s:19:\"Copyright Statement\";s:4:\"name\";s:19:\"copyright_statement\";s:4:\"type\";s:4:\"text\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";s:1:\"0\";s:13:\"default_value\";s:0:\"\";s:11:\"placeholder\";s:0:\"\";s:7:\"prepend\";s:0:\"\";s:6:\"append\";s:0:\"\";s:10:\"formatting\";s:4:\"html\";s:9:\"maxlength\";s:0:\"\";s:17:\"conditional_logic\";a:3:{s:6:\"status\";s:1:\"0\";s:5:\"rules\";a:1:{i:0;a:2:{s:5:\"field\";s:4:\"null\";s:8:\"operator\";s:2:\"==\";}}s:8:\"allorany\";s:3:\"all\";}s:8:\"order_no\";i:14;}');
INSERT INTO `wp_postmeta` VALUES (714,23,'rule','a:5:{s:5:\"param\";s:12:\"options_page\";s:8:\"operator\";s:2:\"==\";s:5:\"value\";s:24:\"acf-options-site-options\";s:8:\"order_no\";i:0;s:8:\"group_no\";i:0;}');
INSERT INTO `wp_postmeta` VALUES (715,92,'artist_information','');
INSERT INTO `wp_postmeta` VALUES (716,92,'_artist_information','field_538ccb5c967b9');
INSERT INTO `wp_postmeta` VALUES (717,92,'profile_image','');
INSERT INTO `wp_postmeta` VALUES (718,92,'_profile_image','field_538cba2564c21');
INSERT INTO `wp_postmeta` VALUES (719,92,'banner_image','');
INSERT INTO `wp_postmeta` VALUES (720,92,'_banner_image','field_5388d19fc1cc1');
INSERT INTO `wp_postmeta` VALUES (721,92,'facebook','');
INSERT INTO `wp_postmeta` VALUES (722,92,'_facebook','field_538cdc8a3f870');
INSERT INTO `wp_postmeta` VALUES (723,92,'soundcloud','');
INSERT INTO `wp_postmeta` VALUES (724,92,'_soundcloud','field_538cdc9a3f871');
INSERT INTO `wp_postmeta` VALUES (725,92,'itunes','');
INSERT INTO `wp_postmeta` VALUES (726,92,'_itunes','field_538cdcd83f872');
INSERT INTO `wp_postmeta` VALUES (727,92,'twitter','');
INSERT INTO `wp_postmeta` VALUES (728,92,'_twitter','field_538cdce83f873');
INSERT INTO `wp_postmeta` VALUES (729,89,'artist_information','');
INSERT INTO `wp_postmeta` VALUES (730,89,'_artist_information','field_538ccb5c967b9');
INSERT INTO `wp_postmeta` VALUES (731,89,'profile_image','');
INSERT INTO `wp_postmeta` VALUES (732,89,'_profile_image','field_538cba2564c21');
INSERT INTO `wp_postmeta` VALUES (733,89,'banner_image','');
INSERT INTO `wp_postmeta` VALUES (734,89,'_banner_image','field_5388d19fc1cc1');
INSERT INTO `wp_postmeta` VALUES (735,89,'facebook','');
INSERT INTO `wp_postmeta` VALUES (736,89,'_facebook','field_538cdc8a3f870');
INSERT INTO `wp_postmeta` VALUES (737,89,'soundcloud','');
INSERT INTO `wp_postmeta` VALUES (738,89,'_soundcloud','field_538cdc9a3f871');
INSERT INTO `wp_postmeta` VALUES (739,89,'itunes','');
INSERT INTO `wp_postmeta` VALUES (740,89,'_itunes','field_538cdcd83f872');
INSERT INTO `wp_postmeta` VALUES (741,89,'twitter','');
INSERT INTO `wp_postmeta` VALUES (742,89,'_twitter','field_538cdce83f873');
INSERT INTO `wp_postmeta` VALUES (745,225,'_edit_last','1');
INSERT INTO `wp_postmeta` VALUES (746,225,'field_52012f259dc17','a:14:{s:3:\"key\";s:19:\"field_52012f259dc17\";s:5:\"label\";s:10:\"Site Title\";s:4:\"name\";s:10:\"site_title\";s:4:\"type\";s:4:\"text\";s:12:\"instructions\";s:84:\"This displays in the browser title, and also serves as the site for search engines. \";s:8:\"required\";s:1:\"0\";s:13:\"default_value\";s:0:\"\";s:11:\"placeholder\";s:0:\"\";s:7:\"prepend\";s:0:\"\";s:6:\"append\";s:0:\"\";s:10:\"formatting\";s:4:\"none\";s:9:\"maxlength\";s:2:\"70\";s:17:\"conditional_logic\";a:3:{s:6:\"status\";s:1:\"0\";s:5:\"rules\";a:1:{i:0;a:3:{s:5:\"field\";s:4:\"null\";s:8:\"operator\";s:2:\"==\";s:5:\"value\";s:0:\"\";}}s:8:\"allorany\";s:3:\"all\";}s:8:\"order_no\";i:0;}');
INSERT INTO `wp_postmeta` VALUES (747,225,'field_52012fbd9dc18','a:13:{s:3:\"key\";s:19:\"field_52012fbd9dc18\";s:5:\"label\";s:16:\"Site Description\";s:4:\"name\";s:16:\"site_description\";s:4:\"type\";s:8:\"textarea\";s:12:\"instructions\";s:109:\"This is the general site description that will be used in Google search results. (Limited to 156 characters.)\";s:8:\"required\";s:1:\"0\";s:13:\"default_value\";s:0:\"\";s:11:\"placeholder\";s:0:\"\";s:9:\"maxlength\";s:0:\"\";s:4:\"rows\";s:0:\"\";s:10:\"formatting\";s:4:\"none\";s:17:\"conditional_logic\";a:3:{s:6:\"status\";s:1:\"0\";s:5:\"rules\";a:1:{i:0;a:3:{s:5:\"field\";s:4:\"null\";s:8:\"operator\";s:2:\"==\";s:5:\"value\";s:0:\"\";}}s:8:\"allorany\";s:3:\"all\";}s:8:\"order_no\";i:1;}');
INSERT INTO `wp_postmeta` VALUES (748,225,'position','side');
INSERT INTO `wp_postmeta` VALUES (749,225,'layout','default');
INSERT INTO `wp_postmeta` VALUES (750,225,'hide_on_screen','');
INSERT INTO `wp_postmeta` VALUES (751,225,'field_5238940ca06c1','a:13:{s:3:\"key\";s:19:\"field_5238940ca06c1\";s:5:\"label\";s:13:\"Site Keywords\";s:4:\"name\";s:13:\"site_keywords\";s:4:\"type\";s:8:\"textarea\";s:12:\"instructions\";s:164:\"Enter descriptive terms and keywords (separated by comma) that users might search for. Keep repetition to a minimum.  It is best practice stay under 30 words total.\";s:8:\"required\";s:1:\"0\";s:13:\"default_value\";s:0:\"\";s:11:\"placeholder\";s:0:\"\";s:9:\"maxlength\";s:3:\"240\";s:4:\"rows\";s:0:\"\";s:10:\"formatting\";s:4:\"none\";s:17:\"conditional_logic\";a:3:{s:6:\"status\";s:1:\"0\";s:5:\"rules\";a:1:{i:0;a:3:{s:5:\"field\";s:4:\"null\";s:8:\"operator\";s:2:\"==\";s:5:\"value\";s:0:\"\";}}s:8:\"allorany\";s:3:\"all\";}s:8:\"order_no\";i:2;}');
INSERT INTO `wp_postmeta` VALUES (753,750,'_edit_last','1');
INSERT INTO `wp_postmeta` VALUES (754,750,'field_52012d032f7a9','a:14:{s:3:\"key\";s:19:\"field_52012d032f7a9\";s:5:\"label\";s:14:\"SEO Page Title\";s:4:\"name\";s:14:\"seo_page_title\";s:4:\"type\";s:4:\"text\";s:12:\"instructions\";s:97:\"The SEO title is the main link that shows up in Google search results (limited to 70 characters).\";s:8:\"required\";s:1:\"0\";s:13:\"default_value\";s:0:\"\";s:11:\"placeholder\";s:0:\"\";s:7:\"prepend\";s:0:\"\";s:6:\"append\";s:0:\"\";s:10:\"formatting\";s:4:\"none\";s:9:\"maxlength\";s:2:\"70\";s:17:\"conditional_logic\";a:3:{s:6:\"status\";s:1:\"0\";s:5:\"rules\";a:1:{i:0;a:3:{s:5:\"field\";s:4:\"null\";s:8:\"operator\";s:2:\"==\";s:5:\"value\";s:0:\"\";}}s:8:\"allorany\";s:3:\"all\";}s:8:\"order_no\";i:0;}');
INSERT INTO `wp_postmeta` VALUES (755,750,'field_52012d6e2f7aa','a:13:{s:3:\"key\";s:19:\"field_52012d6e2f7aa\";s:5:\"label\";s:16:\"Meta Description\";s:4:\"name\";s:16:\"meta_description\";s:4:\"type\";s:8:\"textarea\";s:12:\"instructions\";s:101:\"This will show up un the Google search results underneath the page title (limited to 150 characters).\";s:8:\"required\";s:1:\"0\";s:13:\"default_value\";s:0:\"\";s:11:\"placeholder\";s:0:\"\";s:9:\"maxlength\";s:3:\"150\";s:4:\"rows\";s:0:\"\";s:10:\"formatting\";s:4:\"none\";s:17:\"conditional_logic\";a:3:{s:6:\"status\";s:1:\"0\";s:5:\"rules\";a:1:{i:0;a:3:{s:5:\"field\";s:4:\"null\";s:8:\"operator\";s:2:\"==\";s:5:\"value\";s:0:\"\";}}s:8:\"allorany\";s:3:\"all\";}s:8:\"order_no\";i:1;}');
INSERT INTO `wp_postmeta` VALUES (756,750,'position','side');
INSERT INTO `wp_postmeta` VALUES (757,750,'layout','default');
INSERT INTO `wp_postmeta` VALUES (758,750,'hide_on_screen','');
INSERT INTO `wp_postmeta` VALUES (759,750,'rule','a:5:{s:5:\"param\";s:9:\"post_type\";s:8:\"operator\";s:2:\"==\";s:5:\"value\";s:4:\"post\";s:8:\"order_no\";i:0;s:8:\"group_no\";i:0;}');
INSERT INTO `wp_postmeta` VALUES (760,750,'rule','a:5:{s:5:\"param\";s:9:\"post_type\";s:8:\"operator\";s:2:\"==\";s:5:\"value\";s:4:\"page\";s:8:\"order_no\";i:0;s:8:\"group_no\";i:1;}');
INSERT INTO `wp_postmeta` VALUES (761,750,'rule','a:5:{s:5:\"param\";s:9:\"post_type\";s:8:\"operator\";s:2:\"==\";s:5:\"value\";s:6:\"person\";s:8:\"order_no\";i:0;s:8:\"group_no\";i:2;}');
INSERT INTO `wp_postmeta` VALUES (762,750,'rule','a:5:{s:5:\"param\";s:9:\"post_type\";s:8:\"operator\";s:2:\"==\";s:5:\"value\";s:7:\"podcast\";s:8:\"order_no\";i:0;s:8:\"group_no\";i:3;}');
INSERT INTO `wp_postmeta` VALUES (763,750,'rule','a:5:{s:5:\"param\";s:9:\"post_type\";s:8:\"operator\";s:2:\"==\";s:5:\"value\";s:7:\"webcast\";s:8:\"order_no\";i:0;s:8:\"group_no\";i:4;}');
INSERT INTO `wp_postmeta` VALUES (764,750,'rule','a:5:{s:5:\"param\";s:9:\"post_type\";s:8:\"operator\";s:2:\"==\";s:5:\"value\";s:7:\"webinar\";s:8:\"order_no\";i:0;s:8:\"group_no\";i:5;}');
INSERT INTO `wp_postmeta` VALUES (765,750,'rule','a:5:{s:5:\"param\";s:9:\"post_type\";s:8:\"operator\";s:2:\"==\";s:5:\"value\";s:4:\"book\";s:8:\"order_no\";i:0;s:8:\"group_no\";i:6;}');
INSERT INTO `wp_postmeta` VALUES (766,750,'rule','a:5:{s:5:\"param\";s:9:\"post_type\";s:8:\"operator\";s:2:\"==\";s:5:\"value\";s:5:\"ebook\";s:8:\"order_no\";i:0;s:8:\"group_no\";i:7;}');
INSERT INTO `wp_postmeta` VALUES (767,750,'rule','a:5:{s:5:\"param\";s:9:\"post_type\";s:8:\"operator\";s:2:\"==\";s:5:\"value\";s:6:\"course\";s:8:\"order_no\";i:0;s:8:\"group_no\";i:8;}');
INSERT INTO `wp_postmeta` VALUES (768,750,'rule','a:5:{s:5:\"param\";s:9:\"post_type\";s:8:\"operator\";s:2:\"==\";s:5:\"value\";s:19:\"digital_access_pass\";s:8:\"order_no\";i:0;s:8:\"group_no\";i:9;}');
INSERT INTO `wp_postmeta` VALUES (769,225,'_edit_lock','1401904874:1');
INSERT INTO `wp_postmeta` VALUES (771,225,'rule','a:5:{s:5:\"param\";s:12:\"options_page\";s:8:\"operator\";s:2:\"==\";s:5:\"value\";s:24:\"acf-options-site-options\";s:8:\"order_no\";i:0;s:8:\"group_no\";i:0;}');
INSERT INTO `wp_postmeta` VALUES (772,95,'_edit_lock','1401995686:1');
INSERT INTO `wp_postmeta` VALUES (773,752,'_edit_lock','1402082776:1');
INSERT INTO `wp_postmeta` VALUES (774,752,'_edit_last','1');
INSERT INTO `wp_postmeta` VALUES (775,752,'field_5391f75a9a761','a:8:{s:3:\"key\";s:19:\"field_5391f75a9a761\";s:5:\"label\";s:11:\"Main Slider\";s:4:\"name\";s:0:\"\";s:4:\"type\";s:3:\"tab\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";s:1:\"0\";s:17:\"conditional_logic\";a:3:{s:6:\"status\";s:1:\"0\";s:5:\"rules\";a:1:{i:0;a:3:{s:5:\"field\";s:4:\"null\";s:8:\"operator\";s:2:\"==\";s:5:\"value\";s:0:\"\";}}s:8:\"allorany\";s:3:\"all\";}s:8:\"order_no\";i:0;}');
INSERT INTO `wp_postmeta` VALUES (776,752,'field_5391f76b9a762','a:13:{s:3:\"key\";s:19:\"field_5391f76b9a762\";s:5:\"label\";s:11:\"Main Slider\";s:4:\"name\";s:11:\"main_slider\";s:4:\"type\";s:8:\"repeater\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";s:1:\"0\";s:10:\"sub_fields\";a:6:{i:0;a:12:{s:3:\"key\";s:19:\"field_5391f7819a763\";s:5:\"label\";s:5:\"Image\";s:4:\"name\";s:5:\"image\";s:4:\"type\";s:5:\"image\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";s:1:\"0\";s:12:\"column_width\";s:0:\"\";s:11:\"save_format\";s:6:\"object\";s:12:\"preview_size\";s:9:\"thumbnail\";s:7:\"library\";s:3:\"all\";s:17:\"conditional_logic\";a:3:{s:6:\"status\";s:1:\"0\";s:5:\"rules\";a:1:{i:0;a:3:{s:5:\"field\";s:4:\"null\";s:8:\"operator\";s:2:\"==\";s:5:\"value\";s:0:\"\";}}s:8:\"allorany\";s:3:\"all\";}s:8:\"order_no\";i:0;}i:1;a:15:{s:3:\"key\";s:19:\"field_5391f7b39a764\";s:5:\"label\";s:5:\"Title\";s:4:\"name\";s:5:\"title\";s:4:\"type\";s:4:\"text\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";s:1:\"0\";s:12:\"column_width\";s:0:\"\";s:13:\"default_value\";s:0:\"\";s:11:\"placeholder\";s:0:\"\";s:7:\"prepend\";s:0:\"\";s:6:\"append\";s:0:\"\";s:10:\"formatting\";s:4:\"html\";s:9:\"maxlength\";s:0:\"\";s:17:\"conditional_logic\";a:3:{s:6:\"status\";s:1:\"0\";s:5:\"rules\";a:1:{i:0;a:3:{s:5:\"field\";s:4:\"null\";s:8:\"operator\";s:2:\"==\";s:5:\"value\";s:0:\"\";}}s:8:\"allorany\";s:3:\"all\";}s:8:\"order_no\";i:1;}i:2;a:15:{s:3:\"key\";s:19:\"field_5391f7b99a765\";s:5:\"label\";s:8:\"Subtitle\";s:4:\"name\";s:8:\"subtitle\";s:4:\"type\";s:4:\"text\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";s:1:\"0\";s:12:\"column_width\";s:0:\"\";s:13:\"default_value\";s:0:\"\";s:11:\"placeholder\";s:0:\"\";s:7:\"prepend\";s:0:\"\";s:6:\"append\";s:0:\"\";s:10:\"formatting\";s:4:\"html\";s:9:\"maxlength\";s:0:\"\";s:17:\"conditional_logic\";a:3:{s:6:\"status\";s:1:\"0\";s:5:\"rules\";a:1:{i:0;a:3:{s:5:\"field\";s:4:\"null\";s:8:\"operator\";s:2:\"==\";s:5:\"value\";s:0:\"\";}}s:8:\"allorany\";s:3:\"all\";}s:8:\"order_no\";i:2;}i:3;a:14:{s:3:\"key\";s:19:\"field_5391f7c19a766\";s:5:\"label\";s:11:\"Description\";s:4:\"name\";s:11:\"description\";s:4:\"type\";s:8:\"textarea\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";s:1:\"0\";s:12:\"column_width\";s:0:\"\";s:13:\"default_value\";s:0:\"\";s:11:\"placeholder\";s:0:\"\";s:9:\"maxlength\";s:0:\"\";s:4:\"rows\";s:0:\"\";s:10:\"formatting\";s:2:\"br\";s:17:\"conditional_logic\";a:3:{s:6:\"status\";s:1:\"0\";s:5:\"rules\";a:1:{i:0;a:3:{s:5:\"field\";s:4:\"null\";s:8:\"operator\";s:2:\"==\";s:5:\"value\";s:0:\"\";}}s:8:\"allorany\";s:3:\"all\";}s:8:\"order_no\";i:3;}i:4;a:15:{s:3:\"key\";s:19:\"field_5391f7ea9a767\";s:5:\"label\";s:4:\"Link\";s:4:\"name\";s:4:\"link\";s:4:\"type\";s:4:\"text\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";s:1:\"0\";s:12:\"column_width\";s:0:\"\";s:13:\"default_value\";s:0:\"\";s:11:\"placeholder\";s:0:\"\";s:7:\"prepend\";s:0:\"\";s:6:\"append\";s:0:\"\";s:10:\"formatting\";s:4:\"html\";s:9:\"maxlength\";s:0:\"\";s:17:\"conditional_logic\";a:3:{s:6:\"status\";s:1:\"0\";s:5:\"rules\";a:1:{i:0;a:3:{s:5:\"field\";s:4:\"null\";s:8:\"operator\";s:2:\"==\";s:5:\"value\";s:0:\"\";}}s:8:\"allorany\";s:3:\"all\";}s:8:\"order_no\";i:4;}i:5;a:15:{s:3:\"key\";s:19:\"field_5391f7ff9a768\";s:5:\"label\";s:9:\"Link Name\";s:4:\"name\";s:9:\"link_name\";s:4:\"type\";s:4:\"text\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";s:1:\"0\";s:12:\"column_width\";s:0:\"\";s:13:\"default_value\";s:4:\"More\";s:11:\"placeholder\";s:0:\"\";s:7:\"prepend\";s:0:\"\";s:6:\"append\";s:0:\"\";s:10:\"formatting\";s:4:\"html\";s:9:\"maxlength\";s:0:\"\";s:17:\"conditional_logic\";a:3:{s:6:\"status\";s:1:\"0\";s:5:\"rules\";a:1:{i:0;a:3:{s:5:\"field\";s:4:\"null\";s:8:\"operator\";s:2:\"==\";s:5:\"value\";s:0:\"\";}}s:8:\"allorany\";s:3:\"all\";}s:8:\"order_no\";i:5;}}s:7:\"row_min\";s:0:\"\";s:9:\"row_limit\";s:0:\"\";s:6:\"layout\";s:5:\"table\";s:12:\"button_label\";s:9:\"Add Slide\";s:17:\"conditional_logic\";a:3:{s:6:\"status\";s:1:\"0\";s:5:\"rules\";a:1:{i:0;a:3:{s:5:\"field\";s:4:\"null\";s:8:\"operator\";s:2:\"==\";s:5:\"value\";s:0:\"\";}}s:8:\"allorany\";s:3:\"all\";}s:8:\"order_no\";i:1;}');
INSERT INTO `wp_postmeta` VALUES (778,752,'position','normal');
INSERT INTO `wp_postmeta` VALUES (779,752,'layout','no_box');
INSERT INTO `wp_postmeta` VALUES (780,752,'hide_on_screen','a:11:{i:0;s:11:\"the_content\";i:1;s:7:\"excerpt\";i:2;s:13:\"custom_fields\";i:3;s:10:\"discussion\";i:4;s:8:\"comments\";i:5;s:6:\"author\";i:6;s:6:\"format\";i:7;s:14:\"featured_image\";i:8;s:10:\"categories\";i:9;s:4:\"tags\";i:10;s:15:\"send-trackbacks\";}');
INSERT INTO `wp_postmeta` VALUES (783,754,'seo_page_title','');
INSERT INTO `wp_postmeta` VALUES (784,754,'_seo_page_title','field_52012d032f7a9');
INSERT INTO `wp_postmeta` VALUES (785,754,'meta_description','');
INSERT INTO `wp_postmeta` VALUES (786,754,'_meta_description','field_52012d6e2f7aa');
INSERT INTO `wp_postmeta` VALUES (787,754,'main_slider_0_image','753');
INSERT INTO `wp_postmeta` VALUES (788,754,'_main_slider_0_image','field_5391f7819a763');
INSERT INTO `wp_postmeta` VALUES (789,754,'main_slider_0_title','Andy Mineo');
INSERT INTO `wp_postmeta` VALUES (790,754,'_main_slider_0_title','field_5391f7b39a764');
INSERT INTO `wp_postmeta` VALUES (791,754,'main_slider_0_subtitle','Inside the studio');
INSERT INTO `wp_postmeta` VALUES (792,754,'_main_slider_0_subtitle','field_5391f7b99a765');
INSERT INTO `wp_postmeta` VALUES (793,754,'main_slider_0_description','“It is absolutely undeniable that hip hop is becoming the universal language,” Mineo says expressing an unbridled enthusiasm for his artistic vehicle. “It’s so influential because you are able to say so much in a short period of time. The essence of hip hop is the boldness of it so you’re able to be exactly who you are. ');
INSERT INTO `wp_postmeta` VALUES (794,754,'_main_slider_0_description','field_5391f7c19a766');
INSERT INTO `wp_postmeta` VALUES (795,754,'main_slider_0_link','#');
INSERT INTO `wp_postmeta` VALUES (796,754,'_main_slider_0_link','field_5391f7ea9a767');
INSERT INTO `wp_postmeta` VALUES (797,754,'main_slider_0_link_name','More');
INSERT INTO `wp_postmeta` VALUES (798,754,'_main_slider_0_link_name','field_5391f7ff9a768');
INSERT INTO `wp_postmeta` VALUES (799,754,'main_slider','1');
INSERT INTO `wp_postmeta` VALUES (800,754,'_main_slider','field_5391f76b9a762');
INSERT INTO `wp_postmeta` VALUES (801,12,'seo_page_title','');
INSERT INTO `wp_postmeta` VALUES (802,12,'_seo_page_title','field_52012d032f7a9');
INSERT INTO `wp_postmeta` VALUES (803,12,'meta_description','');
INSERT INTO `wp_postmeta` VALUES (804,12,'_meta_description','field_52012d6e2f7aa');
INSERT INTO `wp_postmeta` VALUES (805,12,'main_slider_0_image','755');
INSERT INTO `wp_postmeta` VALUES (806,12,'_main_slider_0_image','field_5391f7819a763');
INSERT INTO `wp_postmeta` VALUES (807,12,'main_slider_0_title','Andy Mineo');
INSERT INTO `wp_postmeta` VALUES (808,12,'_main_slider_0_title','field_5391f7b39a764');
INSERT INTO `wp_postmeta` VALUES (809,12,'main_slider_0_subtitle','Inside the studio');
INSERT INTO `wp_postmeta` VALUES (810,12,'_main_slider_0_subtitle','field_5391f7b99a765');
INSERT INTO `wp_postmeta` VALUES (811,12,'main_slider_0_description','“It is absolutely undeniable that hip hop is becoming the universal language,” Mineo says expressing an unbridled enthusiasm for his artistic vehicle. “It’s so influential because you are able to say so much in a short period of time. The essence of hip hop is the boldness of it so you’re able to be exactly who you are. ');
INSERT INTO `wp_postmeta` VALUES (812,12,'_main_slider_0_description','field_5391f7c19a766');
INSERT INTO `wp_postmeta` VALUES (813,12,'main_slider_0_link','#');
INSERT INTO `wp_postmeta` VALUES (814,12,'_main_slider_0_link','field_5391f7ea9a767');
INSERT INTO `wp_postmeta` VALUES (815,12,'main_slider_0_link_name','More');
INSERT INTO `wp_postmeta` VALUES (816,12,'_main_slider_0_link_name','field_5391f7ff9a768');
INSERT INTO `wp_postmeta` VALUES (817,12,'main_slider','3');
INSERT INTO `wp_postmeta` VALUES (818,12,'_main_slider','field_5391f76b9a762');
INSERT INTO `wp_postmeta` VALUES (819,755,'_wp_attached_file','2014/05/slide-andymineo.jpg');
INSERT INTO `wp_postmeta` VALUES (820,755,'_wp_attachment_metadata','a:5:{s:5:\"width\";i:1100;s:6:\"height\";i:680;s:4:\"file\";s:27:\"2014/05/slide-andymineo.jpg\";s:5:\"sizes\";a:12:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:27:\"slide-andymineo-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:6:\"medium\";a:4:{s:4:\"file\";s:27:\"slide-andymineo-300x185.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:185;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:5:\"large\";a:4:{s:4:\"file\";s:28:\"slide-andymineo-1024x633.jpg\";s:5:\"width\";i:1024;s:6:\"height\";i:633;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:4:\"tiny\";a:4:{s:4:\"file\";s:25:\"slide-andymineo-52x52.jpg\";s:5:\"width\";i:52;s:6:\"height\";i:52;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:11:\"post-medium\";a:4:{s:4:\"file\";s:27:\"slide-andymineo-620x292.jpg\";s:5:\"width\";i:620;s:6:\"height\";i:292;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:10:\"post-small\";a:4:{s:4:\"file\";s:27:\"slide-andymineo-352x184.jpg\";s:5:\"width\";i:352;s:6:\"height\";i:184;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:14:\"sponsor-medium\";a:4:{s:4:\"file\";s:27:\"slide-andymineo-620x310.jpg\";s:5:\"width\";i:620;s:6:\"height\";i:310;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:15:\"sponsor-sidebar\";a:4:{s:4:\"file\";s:27:\"slide-andymineo-300x252.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:252;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:11:\"album-cover\";a:4:{s:4:\"file\";s:27:\"slide-andymineo-250x250.jpg\";s:5:\"width\";i:250;s:6:\"height\";i:250;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:13:\"profile-image\";a:4:{s:4:\"file\";s:27:\"slide-andymineo-285x285.jpg\";s:5:\"width\";i:285;s:6:\"height\";i:285;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:13:\"product-image\";a:4:{s:4:\"file\";s:27:\"slide-andymineo-450x450.jpg\";s:5:\"width\";i:450;s:6:\"height\";i:450;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:12:\"slider-image\";a:4:{s:4:\"file\";s:28:\"slide-andymineo-1100x600.jpg\";s:5:\"width\";i:1100;s:6:\"height\";i:600;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:10:{s:8:\"aperture\";i:0;s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";i:0;s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";i:0;s:3:\"iso\";i:0;s:13:\"shutter_speed\";i:0;s:5:\"title\";s:0:\"\";}}');
INSERT INTO `wp_postmeta` VALUES (821,756,'seo_page_title','');
INSERT INTO `wp_postmeta` VALUES (822,756,'_seo_page_title','field_52012d032f7a9');
INSERT INTO `wp_postmeta` VALUES (823,756,'meta_description','');
INSERT INTO `wp_postmeta` VALUES (824,756,'_meta_description','field_52012d6e2f7aa');
INSERT INTO `wp_postmeta` VALUES (825,756,'main_slider_0_image','755');
INSERT INTO `wp_postmeta` VALUES (826,756,'_main_slider_0_image','field_5391f7819a763');
INSERT INTO `wp_postmeta` VALUES (827,756,'main_slider_0_title','Andy Mineo');
INSERT INTO `wp_postmeta` VALUES (828,756,'_main_slider_0_title','field_5391f7b39a764');
INSERT INTO `wp_postmeta` VALUES (829,756,'main_slider_0_subtitle','Inside the studio');
INSERT INTO `wp_postmeta` VALUES (830,756,'_main_slider_0_subtitle','field_5391f7b99a765');
INSERT INTO `wp_postmeta` VALUES (831,756,'main_slider_0_description','“It is absolutely undeniable that hip hop is becoming the universal language,” Mineo says expressing an unbridled enthusiasm for his artistic vehicle. “It’s so influential because you are able to say so much in a short period of time. The essence of hip hop is the boldness of it so you’re able to be exactly who you are. ');
INSERT INTO `wp_postmeta` VALUES (832,756,'_main_slider_0_description','field_5391f7c19a766');
INSERT INTO `wp_postmeta` VALUES (833,756,'main_slider_0_link','#');
INSERT INTO `wp_postmeta` VALUES (834,756,'_main_slider_0_link','field_5391f7ea9a767');
INSERT INTO `wp_postmeta` VALUES (835,756,'main_slider_0_link_name','More');
INSERT INTO `wp_postmeta` VALUES (836,756,'_main_slider_0_link_name','field_5391f7ff9a768');
INSERT INTO `wp_postmeta` VALUES (837,756,'main_slider','1');
INSERT INTO `wp_postmeta` VALUES (838,756,'_main_slider','field_5391f76b9a762');
INSERT INTO `wp_postmeta` VALUES (839,757,'_wp_attached_file','2014/05/slide-test.jpg');
INSERT INTO `wp_postmeta` VALUES (840,757,'_wp_attachment_metadata','a:5:{s:5:\"width\";i:1209;s:6:\"height\";i:1071;s:4:\"file\";s:22:\"2014/05/slide-test.jpg\";s:5:\"sizes\";a:12:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:22:\"slide-test-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:6:\"medium\";a:4:{s:4:\"file\";s:22:\"slide-test-300x265.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:265;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:5:\"large\";a:4:{s:4:\"file\";s:23:\"slide-test-1024x907.jpg\";s:5:\"width\";i:1024;s:6:\"height\";i:907;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:4:\"tiny\";a:4:{s:4:\"file\";s:20:\"slide-test-52x52.jpg\";s:5:\"width\";i:52;s:6:\"height\";i:52;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:11:\"post-medium\";a:4:{s:4:\"file\";s:22:\"slide-test-620x292.jpg\";s:5:\"width\";i:620;s:6:\"height\";i:292;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:10:\"post-small\";a:4:{s:4:\"file\";s:22:\"slide-test-352x184.jpg\";s:5:\"width\";i:352;s:6:\"height\";i:184;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:14:\"sponsor-medium\";a:4:{s:4:\"file\";s:22:\"slide-test-620x310.jpg\";s:5:\"width\";i:620;s:6:\"height\";i:310;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:15:\"sponsor-sidebar\";a:4:{s:4:\"file\";s:22:\"slide-test-300x252.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:252;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:11:\"album-cover\";a:4:{s:4:\"file\";s:22:\"slide-test-250x250.jpg\";s:5:\"width\";i:250;s:6:\"height\";i:250;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:13:\"profile-image\";a:4:{s:4:\"file\";s:22:\"slide-test-285x285.jpg\";s:5:\"width\";i:285;s:6:\"height\";i:285;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:13:\"product-image\";a:4:{s:4:\"file\";s:22:\"slide-test-450x450.jpg\";s:5:\"width\";i:450;s:6:\"height\";i:450;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:12:\"slider-image\";a:4:{s:4:\"file\";s:23:\"slide-test-1100x600.jpg\";s:5:\"width\";i:1100;s:6:\"height\";i:600;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:10:{s:8:\"aperture\";i:0;s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";i:0;s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";i:0;s:3:\"iso\";i:0;s:13:\"shutter_speed\";i:0;s:5:\"title\";s:0:\"\";}}');
INSERT INTO `wp_postmeta` VALUES (841,758,'seo_page_title','');
INSERT INTO `wp_postmeta` VALUES (842,758,'_seo_page_title','field_52012d032f7a9');
INSERT INTO `wp_postmeta` VALUES (843,758,'meta_description','');
INSERT INTO `wp_postmeta` VALUES (844,758,'_meta_description','field_52012d6e2f7aa');
INSERT INTO `wp_postmeta` VALUES (845,758,'main_slider_0_image','755');
INSERT INTO `wp_postmeta` VALUES (846,758,'_main_slider_0_image','field_5391f7819a763');
INSERT INTO `wp_postmeta` VALUES (847,758,'main_slider_0_title','Andy Mineo');
INSERT INTO `wp_postmeta` VALUES (848,758,'_main_slider_0_title','field_5391f7b39a764');
INSERT INTO `wp_postmeta` VALUES (849,758,'main_slider_0_subtitle','Inside the studio');
INSERT INTO `wp_postmeta` VALUES (850,758,'_main_slider_0_subtitle','field_5391f7b99a765');
INSERT INTO `wp_postmeta` VALUES (851,758,'main_slider_0_description','“It is absolutely undeniable that hip hop is becoming the universal language,” Mineo says expressing an unbridled enthusiasm for his artistic vehicle. “It’s so influential because you are able to say so much in a short period of time. The essence of hip hop is the boldness of it so you’re able to be exactly who you are. ');
INSERT INTO `wp_postmeta` VALUES (852,758,'_main_slider_0_description','field_5391f7c19a766');
INSERT INTO `wp_postmeta` VALUES (853,758,'main_slider_0_link','#');
INSERT INTO `wp_postmeta` VALUES (854,758,'_main_slider_0_link','field_5391f7ea9a767');
INSERT INTO `wp_postmeta` VALUES (855,758,'main_slider_0_link_name','More');
INSERT INTO `wp_postmeta` VALUES (856,758,'_main_slider_0_link_name','field_5391f7ff9a768');
INSERT INTO `wp_postmeta` VALUES (857,758,'main_slider_1_image','757');
INSERT INTO `wp_postmeta` VALUES (858,758,'_main_slider_1_image','field_5391f7819a763');
INSERT INTO `wp_postmeta` VALUES (859,758,'main_slider_1_title','Some Title Here');
INSERT INTO `wp_postmeta` VALUES (860,758,'_main_slider_1_title','field_5391f7b39a764');
INSERT INTO `wp_postmeta` VALUES (861,758,'main_slider_1_subtitle','This is the subtitle');
INSERT INTO `wp_postmeta` VALUES (862,758,'_main_slider_1_subtitle','field_5391f7b99a765');
INSERT INTO `wp_postmeta` VALUES (863,758,'main_slider_1_description','Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Morbi commodo, ipsum sed pharetra gravida, orci magna rhoncus neque, id pulvinar odio lorem non turpis. Nullam sit amet enim. Suspendisse id velit vitae ligula volutpat condimentum. Aliquam erat volutpat. Sed quis velit. Nulla facilisi. ');
INSERT INTO `wp_postmeta` VALUES (864,758,'_main_slider_1_description','field_5391f7c19a766');
INSERT INTO `wp_postmeta` VALUES (865,758,'main_slider_1_link','#');
INSERT INTO `wp_postmeta` VALUES (866,758,'_main_slider_1_link','field_5391f7ea9a767');
INSERT INTO `wp_postmeta` VALUES (867,758,'main_slider_1_link_name','More');
INSERT INTO `wp_postmeta` VALUES (868,758,'_main_slider_1_link_name','field_5391f7ff9a768');
INSERT INTO `wp_postmeta` VALUES (869,758,'main_slider','2');
INSERT INTO `wp_postmeta` VALUES (870,758,'_main_slider','field_5391f76b9a762');
INSERT INTO `wp_postmeta` VALUES (871,12,'main_slider_1_image','757');
INSERT INTO `wp_postmeta` VALUES (872,12,'_main_slider_1_image','field_5391f7819a763');
INSERT INTO `wp_postmeta` VALUES (873,12,'main_slider_1_title','Some Title Here');
INSERT INTO `wp_postmeta` VALUES (874,12,'_main_slider_1_title','field_5391f7b39a764');
INSERT INTO `wp_postmeta` VALUES (875,12,'main_slider_1_subtitle','This is the subtitle');
INSERT INTO `wp_postmeta` VALUES (876,12,'_main_slider_1_subtitle','field_5391f7b99a765');
INSERT INTO `wp_postmeta` VALUES (877,12,'main_slider_1_description','Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Morbi commodo, ipsum sed pharetra gravida, orci magna rhoncus neque, id pulvinar odio lorem non turpis. Nullam sit amet enim. Suspendisse id velit vitae ligula volutpat condimentum. Aliquam erat volutpat. Sed quis velit. Nulla facilisi. ');
INSERT INTO `wp_postmeta` VALUES (878,12,'_main_slider_1_description','field_5391f7c19a766');
INSERT INTO `wp_postmeta` VALUES (879,12,'main_slider_1_link','#');
INSERT INTO `wp_postmeta` VALUES (880,12,'_main_slider_1_link','field_5391f7ea9a767');
INSERT INTO `wp_postmeta` VALUES (881,12,'main_slider_1_link_name','More');
INSERT INTO `wp_postmeta` VALUES (882,12,'_main_slider_1_link_name','field_5391f7ff9a768');
INSERT INTO `wp_postmeta` VALUES (883,1,'_thumbnail_id','80');
INSERT INTO `wp_postmeta` VALUES (886,760,'seo_page_title','');
INSERT INTO `wp_postmeta` VALUES (887,760,'_seo_page_title','field_52012d032f7a9');
INSERT INTO `wp_postmeta` VALUES (888,760,'meta_description','');
INSERT INTO `wp_postmeta` VALUES (889,760,'_meta_description','field_52012d6e2f7aa');
INSERT INTO `wp_postmeta` VALUES (890,1,'seo_page_title','');
INSERT INTO `wp_postmeta` VALUES (891,1,'_seo_page_title','field_52012d032f7a9');
INSERT INTO `wp_postmeta` VALUES (892,1,'meta_description','');
INSERT INTO `wp_postmeta` VALUES (893,1,'_meta_description','field_52012d6e2f7aa');
INSERT INTO `wp_postmeta` VALUES (896,752,'field_539208a78115b','a:13:{s:3:\"key\";s:19:\"field_539208a78115b\";s:5:\"label\";s:16:\"Category Sliders\";s:4:\"name\";s:16:\"category_sliders\";s:4:\"type\";s:8:\"repeater\";s:12:\"instructions\";s:71:\"Choose Categories that you would like to create a carousel slider from.\";s:8:\"required\";s:1:\"0\";s:10:\"sub_fields\";a:1:{i:0;a:14:{s:3:\"key\";s:19:\"field_539208dc8115c\";s:5:\"label\";s:8:\"Category\";s:4:\"name\";s:8:\"category\";s:4:\"type\";s:8:\"taxonomy\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";s:1:\"0\";s:12:\"column_width\";s:0:\"\";s:8:\"taxonomy\";s:8:\"category\";s:10:\"field_type\";s:6:\"select\";s:10:\"allow_null\";s:1:\"0\";s:15:\"load_save_terms\";s:1:\"0\";s:13:\"return_format\";s:6:\"object\";s:17:\"conditional_logic\";a:3:{s:6:\"status\";s:1:\"0\";s:5:\"rules\";a:1:{i:0;a:3:{s:5:\"field\";s:4:\"null\";s:8:\"operator\";s:2:\"==\";s:5:\"value\";s:0:\"\";}}s:8:\"allorany\";s:3:\"all\";}s:8:\"order_no\";i:0;}}s:7:\"row_min\";s:0:\"\";s:9:\"row_limit\";s:0:\"\";s:6:\"layout\";s:5:\"table\";s:12:\"button_label\";s:10:\"Add Slider\";s:17:\"conditional_logic\";a:3:{s:6:\"status\";s:1:\"0\";s:5:\"rules\";a:1:{i:0;a:3:{s:5:\"field\";s:4:\"null\";s:8:\"operator\";s:2:\"==\";s:5:\"value\";s:0:\"\";}}s:8:\"allorany\";s:3:\"all\";}s:8:\"order_no\";i:3;}');
INSERT INTO `wp_postmeta` VALUES (898,752,'field_5392090d4e299','a:8:{s:3:\"key\";s:19:\"field_5392090d4e299\";s:5:\"label\";s:16:\"Category Sliders\";s:4:\"name\";s:0:\"\";s:4:\"type\";s:3:\"tab\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";s:1:\"0\";s:17:\"conditional_logic\";a:3:{s:6:\"status\";s:1:\"0\";s:5:\"rules\";a:1:{i:0;a:3:{s:5:\"field\";s:4:\"null\";s:8:\"operator\";s:2:\"==\";s:5:\"value\";s:0:\"\";}}s:8:\"allorany\";s:3:\"all\";}s:8:\"order_no\";i:2;}');
INSERT INTO `wp_postmeta` VALUES (900,761,'seo_page_title','');
INSERT INTO `wp_postmeta` VALUES (901,761,'_seo_page_title','field_52012d032f7a9');
INSERT INTO `wp_postmeta` VALUES (902,761,'meta_description','');
INSERT INTO `wp_postmeta` VALUES (903,761,'_meta_description','field_52012d6e2f7aa');
INSERT INTO `wp_postmeta` VALUES (904,761,'main_slider_0_image','755');
INSERT INTO `wp_postmeta` VALUES (905,761,'_main_slider_0_image','field_5391f7819a763');
INSERT INTO `wp_postmeta` VALUES (906,761,'main_slider_0_title','Andy Mineo');
INSERT INTO `wp_postmeta` VALUES (907,761,'_main_slider_0_title','field_5391f7b39a764');
INSERT INTO `wp_postmeta` VALUES (908,761,'main_slider_0_subtitle','Inside the studio');
INSERT INTO `wp_postmeta` VALUES (909,761,'_main_slider_0_subtitle','field_5391f7b99a765');
INSERT INTO `wp_postmeta` VALUES (910,761,'main_slider_0_description','“It is absolutely undeniable that hip hop is becoming the universal language,” Mineo says expressing an unbridled enthusiasm for his artistic vehicle. “It’s so influential because you are able to say so much in a short period of time. The essence of hip hop is the boldness of it so you’re able to be exactly who you are. ');
INSERT INTO `wp_postmeta` VALUES (911,761,'_main_slider_0_description','field_5391f7c19a766');
INSERT INTO `wp_postmeta` VALUES (912,761,'main_slider_0_link','#');
INSERT INTO `wp_postmeta` VALUES (913,761,'_main_slider_0_link','field_5391f7ea9a767');
INSERT INTO `wp_postmeta` VALUES (914,761,'main_slider_0_link_name','More');
INSERT INTO `wp_postmeta` VALUES (915,761,'_main_slider_0_link_name','field_5391f7ff9a768');
INSERT INTO `wp_postmeta` VALUES (916,761,'main_slider_1_image','757');
INSERT INTO `wp_postmeta` VALUES (917,761,'_main_slider_1_image','field_5391f7819a763');
INSERT INTO `wp_postmeta` VALUES (918,761,'main_slider_1_title','Some Title Here');
INSERT INTO `wp_postmeta` VALUES (919,761,'_main_slider_1_title','field_5391f7b39a764');
INSERT INTO `wp_postmeta` VALUES (920,761,'main_slider_1_subtitle','This is the subtitle');
INSERT INTO `wp_postmeta` VALUES (921,761,'_main_slider_1_subtitle','field_5391f7b99a765');
INSERT INTO `wp_postmeta` VALUES (922,761,'main_slider_1_description','Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Morbi commodo, ipsum sed pharetra gravida, orci magna rhoncus neque, id pulvinar odio lorem non turpis. Nullam sit amet enim. Suspendisse id velit vitae ligula volutpat condimentum. Aliquam erat volutpat. Sed quis velit. Nulla facilisi. ');
INSERT INTO `wp_postmeta` VALUES (923,761,'_main_slider_1_description','field_5391f7c19a766');
INSERT INTO `wp_postmeta` VALUES (924,761,'main_slider_1_link','#');
INSERT INTO `wp_postmeta` VALUES (925,761,'_main_slider_1_link','field_5391f7ea9a767');
INSERT INTO `wp_postmeta` VALUES (926,761,'main_slider_1_link_name','More');
INSERT INTO `wp_postmeta` VALUES (927,761,'_main_slider_1_link_name','field_5391f7ff9a768');
INSERT INTO `wp_postmeta` VALUES (928,761,'main_slider','2');
INSERT INTO `wp_postmeta` VALUES (929,761,'_main_slider','field_5391f76b9a762');
INSERT INTO `wp_postmeta` VALUES (930,761,'category_sliders_0_category','15');
INSERT INTO `wp_postmeta` VALUES (931,761,'_category_sliders_0_category','field_539208dc8115c');
INSERT INTO `wp_postmeta` VALUES (932,761,'category_sliders_1_category','1');
INSERT INTO `wp_postmeta` VALUES (933,761,'_category_sliders_1_category','field_539208dc8115c');
INSERT INTO `wp_postmeta` VALUES (934,761,'category_sliders','2');
INSERT INTO `wp_postmeta` VALUES (935,761,'_category_sliders','field_539208a78115b');
INSERT INTO `wp_postmeta` VALUES (936,12,'category_sliders_0_category','1');
INSERT INTO `wp_postmeta` VALUES (937,12,'_category_sliders_0_category','field_539208dc8115c');
INSERT INTO `wp_postmeta` VALUES (938,12,'category_sliders_1_category','15');
INSERT INTO `wp_postmeta` VALUES (939,12,'_category_sliders_1_category','field_539208dc8115c');
INSERT INTO `wp_postmeta` VALUES (940,12,'category_sliders','2');
INSERT INTO `wp_postmeta` VALUES (941,12,'_category_sliders','field_539208a78115b');
INSERT INTO `wp_postmeta` VALUES (942,762,'seo_page_title','');
INSERT INTO `wp_postmeta` VALUES (943,762,'_seo_page_title','field_52012d032f7a9');
INSERT INTO `wp_postmeta` VALUES (944,762,'meta_description','');
INSERT INTO `wp_postmeta` VALUES (945,762,'_meta_description','field_52012d6e2f7aa');
INSERT INTO `wp_postmeta` VALUES (946,762,'main_slider_0_image','755');
INSERT INTO `wp_postmeta` VALUES (947,762,'_main_slider_0_image','field_5391f7819a763');
INSERT INTO `wp_postmeta` VALUES (948,762,'main_slider_0_title','Andy Mineo');
INSERT INTO `wp_postmeta` VALUES (949,762,'_main_slider_0_title','field_5391f7b39a764');
INSERT INTO `wp_postmeta` VALUES (950,762,'main_slider_0_subtitle','Inside the studio');
INSERT INTO `wp_postmeta` VALUES (951,762,'_main_slider_0_subtitle','field_5391f7b99a765');
INSERT INTO `wp_postmeta` VALUES (952,762,'main_slider_0_description','“It is absolutely undeniable that hip hop is becoming the universal language,” Mineo says expressing an unbridled enthusiasm for his artistic vehicle. “It’s so influential because you are able to say so much in a short period of time. The essence of hip hop is the boldness of it so you’re able to be exactly who you are. ');
INSERT INTO `wp_postmeta` VALUES (953,762,'_main_slider_0_description','field_5391f7c19a766');
INSERT INTO `wp_postmeta` VALUES (954,762,'main_slider_0_link','#');
INSERT INTO `wp_postmeta` VALUES (955,762,'_main_slider_0_link','field_5391f7ea9a767');
INSERT INTO `wp_postmeta` VALUES (956,762,'main_slider_0_link_name','More');
INSERT INTO `wp_postmeta` VALUES (957,762,'_main_slider_0_link_name','field_5391f7ff9a768');
INSERT INTO `wp_postmeta` VALUES (958,762,'main_slider_1_image','757');
INSERT INTO `wp_postmeta` VALUES (959,762,'_main_slider_1_image','field_5391f7819a763');
INSERT INTO `wp_postmeta` VALUES (960,762,'main_slider_1_title','Some Title Here');
INSERT INTO `wp_postmeta` VALUES (961,762,'_main_slider_1_title','field_5391f7b39a764');
INSERT INTO `wp_postmeta` VALUES (962,762,'main_slider_1_subtitle','This is the subtitle');
INSERT INTO `wp_postmeta` VALUES (963,762,'_main_slider_1_subtitle','field_5391f7b99a765');
INSERT INTO `wp_postmeta` VALUES (964,762,'main_slider_1_description','Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Morbi commodo, ipsum sed pharetra gravida, orci magna rhoncus neque, id pulvinar odio lorem non turpis. Nullam sit amet enim. Suspendisse id velit vitae ligula volutpat condimentum. Aliquam erat volutpat. Sed quis velit. Nulla facilisi. ');
INSERT INTO `wp_postmeta` VALUES (965,762,'_main_slider_1_description','field_5391f7c19a766');
INSERT INTO `wp_postmeta` VALUES (966,762,'main_slider_1_link','#');
INSERT INTO `wp_postmeta` VALUES (967,762,'_main_slider_1_link','field_5391f7ea9a767');
INSERT INTO `wp_postmeta` VALUES (968,762,'main_slider_1_link_name','More');
INSERT INTO `wp_postmeta` VALUES (969,762,'_main_slider_1_link_name','field_5391f7ff9a768');
INSERT INTO `wp_postmeta` VALUES (970,762,'main_slider','2');
INSERT INTO `wp_postmeta` VALUES (971,762,'_main_slider','field_5391f76b9a762');
INSERT INTO `wp_postmeta` VALUES (972,762,'category_sliders_0_category','3');
INSERT INTO `wp_postmeta` VALUES (973,762,'_category_sliders_0_category','field_539208dc8115c');
INSERT INTO `wp_postmeta` VALUES (974,762,'category_sliders_1_category','1');
INSERT INTO `wp_postmeta` VALUES (975,762,'_category_sliders_1_category','field_539208dc8115c');
INSERT INTO `wp_postmeta` VALUES (976,762,'category_sliders','2');
INSERT INTO `wp_postmeta` VALUES (977,762,'_category_sliders','field_539208a78115b');
INSERT INTO `wp_postmeta` VALUES (978,763,'seo_page_title','');
INSERT INTO `wp_postmeta` VALUES (979,763,'_seo_page_title','field_52012d032f7a9');
INSERT INTO `wp_postmeta` VALUES (980,763,'meta_description','');
INSERT INTO `wp_postmeta` VALUES (981,763,'_meta_description','field_52012d6e2f7aa');
INSERT INTO `wp_postmeta` VALUES (982,763,'main_slider_0_image','755');
INSERT INTO `wp_postmeta` VALUES (983,763,'_main_slider_0_image','field_5391f7819a763');
INSERT INTO `wp_postmeta` VALUES (984,763,'main_slider_0_title','Andy Mineo');
INSERT INTO `wp_postmeta` VALUES (985,763,'_main_slider_0_title','field_5391f7b39a764');
INSERT INTO `wp_postmeta` VALUES (986,763,'main_slider_0_subtitle','Inside the studio');
INSERT INTO `wp_postmeta` VALUES (987,763,'_main_slider_0_subtitle','field_5391f7b99a765');
INSERT INTO `wp_postmeta` VALUES (988,763,'main_slider_0_description','“It is absolutely undeniable that hip hop is becoming the universal language,” Mineo says expressing an unbridled enthusiasm for his artistic vehicle. “It’s so influential because you are able to say so much in a short period of time. The essence of hip hop is the boldness of it so you’re able to be exactly who you are. ');
INSERT INTO `wp_postmeta` VALUES (989,763,'_main_slider_0_description','field_5391f7c19a766');
INSERT INTO `wp_postmeta` VALUES (990,763,'main_slider_0_link','#');
INSERT INTO `wp_postmeta` VALUES (991,763,'_main_slider_0_link','field_5391f7ea9a767');
INSERT INTO `wp_postmeta` VALUES (992,763,'main_slider_0_link_name','More');
INSERT INTO `wp_postmeta` VALUES (993,763,'_main_slider_0_link_name','field_5391f7ff9a768');
INSERT INTO `wp_postmeta` VALUES (994,763,'main_slider_1_image','757');
INSERT INTO `wp_postmeta` VALUES (995,763,'_main_slider_1_image','field_5391f7819a763');
INSERT INTO `wp_postmeta` VALUES (996,763,'main_slider_1_title','Some Title Here');
INSERT INTO `wp_postmeta` VALUES (997,763,'_main_slider_1_title','field_5391f7b39a764');
INSERT INTO `wp_postmeta` VALUES (998,763,'main_slider_1_subtitle','This is the subtitle');
INSERT INTO `wp_postmeta` VALUES (999,763,'_main_slider_1_subtitle','field_5391f7b99a765');
INSERT INTO `wp_postmeta` VALUES (1000,763,'main_slider_1_description','Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Morbi commodo, ipsum sed pharetra gravida, orci magna rhoncus neque, id pulvinar odio lorem non turpis. Nullam sit amet enim. Suspendisse id velit vitae ligula volutpat condimentum. Aliquam erat volutpat. Sed quis velit. Nulla facilisi. ');
INSERT INTO `wp_postmeta` VALUES (1001,763,'_main_slider_1_description','field_5391f7c19a766');
INSERT INTO `wp_postmeta` VALUES (1002,763,'main_slider_1_link','#');
INSERT INTO `wp_postmeta` VALUES (1003,763,'_main_slider_1_link','field_5391f7ea9a767');
INSERT INTO `wp_postmeta` VALUES (1004,763,'main_slider_1_link_name','More');
INSERT INTO `wp_postmeta` VALUES (1005,763,'_main_slider_1_link_name','field_5391f7ff9a768');
INSERT INTO `wp_postmeta` VALUES (1006,763,'main_slider','2');
INSERT INTO `wp_postmeta` VALUES (1007,763,'_main_slider','field_5391f76b9a762');
INSERT INTO `wp_postmeta` VALUES (1008,763,'category_sliders_0_category','1');
INSERT INTO `wp_postmeta` VALUES (1009,763,'_category_sliders_0_category','field_539208dc8115c');
INSERT INTO `wp_postmeta` VALUES (1010,763,'category_sliders_1_category','15');
INSERT INTO `wp_postmeta` VALUES (1011,763,'_category_sliders_1_category','field_539208dc8115c');
INSERT INTO `wp_postmeta` VALUES (1012,763,'category_sliders','2');
INSERT INTO `wp_postmeta` VALUES (1013,763,'_category_sliders','field_539208a78115b');
INSERT INTO `wp_postmeta` VALUES (1014,752,'rule','a:5:{s:5:\"param\";s:9:\"page_type\";s:8:\"operator\";s:2:\"==\";s:5:\"value\";s:10:\"front_page\";s:8:\"order_no\";i:0;s:8:\"group_no\";i:0;}');
INSERT INTO `wp_postmeta` VALUES (1015,764,'seo_page_title','');
INSERT INTO `wp_postmeta` VALUES (1016,764,'_seo_page_title','field_52012d032f7a9');
INSERT INTO `wp_postmeta` VALUES (1017,764,'meta_description','');
INSERT INTO `wp_postmeta` VALUES (1018,764,'_meta_description','field_52012d6e2f7aa');
INSERT INTO `wp_postmeta` VALUES (1019,764,'main_slider_0_image','755');
INSERT INTO `wp_postmeta` VALUES (1020,764,'_main_slider_0_image','field_5391f7819a763');
INSERT INTO `wp_postmeta` VALUES (1021,764,'main_slider_0_title','Andy Mineo');
INSERT INTO `wp_postmeta` VALUES (1022,764,'_main_slider_0_title','field_5391f7b39a764');
INSERT INTO `wp_postmeta` VALUES (1023,764,'main_slider_0_subtitle','Inside the studio');
INSERT INTO `wp_postmeta` VALUES (1024,764,'_main_slider_0_subtitle','field_5391f7b99a765');
INSERT INTO `wp_postmeta` VALUES (1025,764,'main_slider_0_description','“It is absolutely undeniable that hip hop is becoming the universal language,” Mineo says expressing an unbridled enthusiasm for his artistic vehicle. “It’s so influential because you are able to say so much in a short period of time. The essence of hip hop is the boldness of it so you’re able to be exactly who you are. ');
INSERT INTO `wp_postmeta` VALUES (1026,764,'_main_slider_0_description','field_5391f7c19a766');
INSERT INTO `wp_postmeta` VALUES (1027,764,'main_slider_0_link','#');
INSERT INTO `wp_postmeta` VALUES (1028,764,'_main_slider_0_link','field_5391f7ea9a767');
INSERT INTO `wp_postmeta` VALUES (1029,764,'main_slider_0_link_name','More');
INSERT INTO `wp_postmeta` VALUES (1030,764,'_main_slider_0_link_name','field_5391f7ff9a768');
INSERT INTO `wp_postmeta` VALUES (1031,764,'main_slider_1_image','757');
INSERT INTO `wp_postmeta` VALUES (1032,764,'_main_slider_1_image','field_5391f7819a763');
INSERT INTO `wp_postmeta` VALUES (1033,764,'main_slider_1_title','Some Title Here');
INSERT INTO `wp_postmeta` VALUES (1034,764,'_main_slider_1_title','field_5391f7b39a764');
INSERT INTO `wp_postmeta` VALUES (1035,764,'main_slider_1_subtitle','This is the subtitle');
INSERT INTO `wp_postmeta` VALUES (1036,764,'_main_slider_1_subtitle','field_5391f7b99a765');
INSERT INTO `wp_postmeta` VALUES (1037,764,'main_slider_1_description','Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Morbi commodo, ipsum sed pharetra gravida, orci magna rhoncus neque, id pulvinar odio lorem non turpis. Nullam sit amet enim. Suspendisse id velit vitae ligula volutpat condimentum. Aliquam erat volutpat. Sed quis velit. Nulla facilisi. ');
INSERT INTO `wp_postmeta` VALUES (1038,764,'_main_slider_1_description','field_5391f7c19a766');
INSERT INTO `wp_postmeta` VALUES (1039,764,'main_slider_1_link','#');
INSERT INTO `wp_postmeta` VALUES (1040,764,'_main_slider_1_link','field_5391f7ea9a767');
INSERT INTO `wp_postmeta` VALUES (1041,764,'main_slider_1_link_name','More');
INSERT INTO `wp_postmeta` VALUES (1042,764,'_main_slider_1_link_name','field_5391f7ff9a768');
INSERT INTO `wp_postmeta` VALUES (1043,764,'main_slider_2_image','30');
INSERT INTO `wp_postmeta` VALUES (1044,764,'_main_slider_2_image','field_5391f7819a763');
INSERT INTO `wp_postmeta` VALUES (1045,764,'main_slider_2_title','Another title');
INSERT INTO `wp_postmeta` VALUES (1046,764,'_main_slider_2_title','field_5391f7b39a764');
INSERT INTO `wp_postmeta` VALUES (1047,764,'main_slider_2_subtitle','Subtitles rock');
INSERT INTO `wp_postmeta` VALUES (1048,764,'_main_slider_2_subtitle','field_5391f7b99a765');
INSERT INTO `wp_postmeta` VALUES (1049,764,'main_slider_2_description','“It is absolutely undeniable that hip hop is becoming the universal language,” Mineo says expressing an unbridled enthusiasm for his artistic vehicle. “It’s so influential because you are able to say so much in a short period of time. The essence of hip hop is the boldness of it so you’re...');
INSERT INTO `wp_postmeta` VALUES (1050,764,'_main_slider_2_description','field_5391f7c19a766');
INSERT INTO `wp_postmeta` VALUES (1051,764,'main_slider_2_link','#');
INSERT INTO `wp_postmeta` VALUES (1052,764,'_main_slider_2_link','field_5391f7ea9a767');
INSERT INTO `wp_postmeta` VALUES (1053,764,'main_slider_2_link_name','More');
INSERT INTO `wp_postmeta` VALUES (1054,764,'_main_slider_2_link_name','field_5391f7ff9a768');
INSERT INTO `wp_postmeta` VALUES (1055,764,'main_slider','3');
INSERT INTO `wp_postmeta` VALUES (1056,764,'_main_slider','field_5391f76b9a762');
INSERT INTO `wp_postmeta` VALUES (1057,764,'category_sliders_0_category','1');
INSERT INTO `wp_postmeta` VALUES (1058,764,'_category_sliders_0_category','field_539208dc8115c');
INSERT INTO `wp_postmeta` VALUES (1059,764,'category_sliders_1_category','15');
INSERT INTO `wp_postmeta` VALUES (1060,764,'_category_sliders_1_category','field_539208dc8115c');
INSERT INTO `wp_postmeta` VALUES (1061,764,'category_sliders','2');
INSERT INTO `wp_postmeta` VALUES (1062,764,'_category_sliders','field_539208a78115b');
INSERT INTO `wp_postmeta` VALUES (1063,12,'main_slider_2_image','30');
INSERT INTO `wp_postmeta` VALUES (1064,12,'_main_slider_2_image','field_5391f7819a763');
INSERT INTO `wp_postmeta` VALUES (1065,12,'main_slider_2_title','Another title');
INSERT INTO `wp_postmeta` VALUES (1066,12,'_main_slider_2_title','field_5391f7b39a764');
INSERT INTO `wp_postmeta` VALUES (1067,12,'main_slider_2_subtitle','Subtitles rock');
INSERT INTO `wp_postmeta` VALUES (1068,12,'_main_slider_2_subtitle','field_5391f7b99a765');
INSERT INTO `wp_postmeta` VALUES (1069,12,'main_slider_2_description','“It is absolutely undeniable that hip hop is becoming the universal language,” Mineo says expressing an unbridled enthusiasm for his artistic vehicle. “It’s so influential because you are able to say so much in a short period of time. The essence of hip hop is the boldness of it so you’re...');
INSERT INTO `wp_postmeta` VALUES (1070,12,'_main_slider_2_description','field_5391f7c19a766');
INSERT INTO `wp_postmeta` VALUES (1071,12,'main_slider_2_link','#');
INSERT INTO `wp_postmeta` VALUES (1072,12,'_main_slider_2_link','field_5391f7ea9a767');
INSERT INTO `wp_postmeta` VALUES (1073,12,'main_slider_2_link_name','More');
INSERT INTO `wp_postmeta` VALUES (1074,12,'_main_slider_2_link_name','field_5391f7ff9a768');
INSERT INTO `wp_postmeta` VALUES (1075,765,'_edit_lock','1402082895:1');
INSERT INTO `wp_postmeta` VALUES (1076,765,'_edit_last','1');
INSERT INTO `wp_postmeta` VALUES (1078,765,'position','normal');
INSERT INTO `wp_postmeta` VALUES (1079,765,'layout','no_box');
INSERT INTO `wp_postmeta` VALUES (1080,765,'hide_on_screen','a:12:{i:0;s:11:\"the_content\";i:1;s:7:\"excerpt\";i:2;s:13:\"custom_fields\";i:3;s:10:\"discussion\";i:4;s:8:\"comments\";i:5;s:9:\"revisions\";i:6;s:6:\"author\";i:7;s:6:\"format\";i:8;s:14:\"featured_image\";i:9;s:10:\"categories\";i:10;s:4:\"tags\";i:11;s:15:\"send-trackbacks\";}');
INSERT INTO `wp_postmeta` VALUES (1081,765,'field_53921692e74a3','a:13:{s:3:\"key\";s:19:\"field_53921692e74a3\";s:5:\"label\";s:16:\"Category Sliders\";s:4:\"name\";s:16:\"category_sliders\";s:4:\"type\";s:8:\"repeater\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";s:1:\"0\";s:10:\"sub_fields\";a:1:{i:0;a:14:{s:3:\"key\";s:19:\"field_5392169fe74a4\";s:5:\"label\";s:8:\"Category\";s:4:\"name\";s:8:\"category\";s:4:\"type\";s:8:\"taxonomy\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";s:1:\"0\";s:12:\"column_width\";s:0:\"\";s:8:\"taxonomy\";s:8:\"category\";s:10:\"field_type\";s:6:\"select\";s:10:\"allow_null\";s:1:\"0\";s:15:\"load_save_terms\";s:1:\"0\";s:13:\"return_format\";s:6:\"object\";s:17:\"conditional_logic\";a:3:{s:6:\"status\";s:1:\"0\";s:5:\"rules\";a:1:{i:0;a:2:{s:5:\"field\";s:4:\"null\";s:8:\"operator\";s:2:\"==\";}}s:8:\"allorany\";s:3:\"all\";}s:8:\"order_no\";i:0;}}s:7:\"row_min\";s:0:\"\";s:9:\"row_limit\";s:0:\"\";s:6:\"layout\";s:5:\"table\";s:12:\"button_label\";s:10:\"Add Slider\";s:17:\"conditional_logic\";a:3:{s:6:\"status\";s:1:\"0\";s:5:\"rules\";a:1:{i:0;a:2:{s:5:\"field\";s:4:\"null\";s:8:\"operator\";s:2:\"==\";}}s:8:\"allorany\";s:3:\"all\";}s:8:\"order_no\";i:0;}');
INSERT INTO `wp_postmeta` VALUES (1082,765,'rule','a:5:{s:5:\"param\";s:13:\"page_template\";s:8:\"operator\";s:2:\"==\";s:5:\"value\";s:14:\"page-watch.php\";s:8:\"order_no\";i:0;s:8:\"group_no\";i:0;}');
INSERT INTO `wp_postmeta` VALUES (1083,766,'_edit_lock','1402083012:1');
INSERT INTO `wp_postmeta` VALUES (1084,766,'_edit_last','1');
INSERT INTO `wp_postmeta` VALUES (1087,767,'video_url','http://vimeo.com/94614809');
INSERT INTO `wp_postmeta` VALUES (1088,767,'_video_url','field_538885f2ebadf');
INSERT INTO `wp_postmeta` VALUES (1089,767,'seo_page_title','');
INSERT INTO `wp_postmeta` VALUES (1090,767,'_seo_page_title','field_52012d032f7a9');
INSERT INTO `wp_postmeta` VALUES (1091,767,'meta_description','');
INSERT INTO `wp_postmeta` VALUES (1092,767,'_meta_description','field_52012d6e2f7aa');
INSERT INTO `wp_postmeta` VALUES (1093,766,'video_url','http://vimeo.com/94614809');
INSERT INTO `wp_postmeta` VALUES (1094,766,'_video_url','field_538885f2ebadf');
INSERT INTO `wp_postmeta` VALUES (1095,766,'seo_page_title','');
INSERT INTO `wp_postmeta` VALUES (1096,766,'_seo_page_title','field_52012d032f7a9');
INSERT INTO `wp_postmeta` VALUES (1097,766,'meta_description','');
INSERT INTO `wp_postmeta` VALUES (1098,766,'_meta_description','field_52012d6e2f7aa');
INSERT INTO `wp_postmeta` VALUES (1099,768,'seo_page_title','');
INSERT INTO `wp_postmeta` VALUES (1100,768,'_seo_page_title','field_52012d032f7a9');
INSERT INTO `wp_postmeta` VALUES (1101,768,'meta_description','');
INSERT INTO `wp_postmeta` VALUES (1102,768,'_meta_description','field_52012d6e2f7aa');
INSERT INTO `wp_postmeta` VALUES (1103,768,'category_sliders_0_category','1');
INSERT INTO `wp_postmeta` VALUES (1104,768,'_category_sliders_0_category','field_5392169fe74a4');
INSERT INTO `wp_postmeta` VALUES (1105,768,'category_sliders','1');
INSERT INTO `wp_postmeta` VALUES (1106,768,'_category_sliders','field_53921692e74a3');
INSERT INTO `wp_postmeta` VALUES (1107,47,'seo_page_title','');
INSERT INTO `wp_postmeta` VALUES (1108,47,'_seo_page_title','field_52012d032f7a9');
INSERT INTO `wp_postmeta` VALUES (1109,47,'meta_description','');
INSERT INTO `wp_postmeta` VALUES (1110,47,'_meta_description','field_52012d6e2f7aa');
INSERT INTO `wp_postmeta` VALUES (1111,47,'category_sliders_0_category','1');
INSERT INTO `wp_postmeta` VALUES (1112,47,'_category_sliders_0_category','field_5392169fe74a4');
INSERT INTO `wp_postmeta` VALUES (1113,47,'category_sliders','1');
INSERT INTO `wp_postmeta` VALUES (1114,47,'_category_sliders','field_53921692e74a3');
INSERT INTO `wp_postmeta` VALUES (1115,769,'_edit_lock','1402085497:1');
INSERT INTO `wp_postmeta` VALUES (1117,90,'rule','a:5:{s:5:\"param\";s:9:\"post_type\";s:8:\"operator\";s:2:\"==\";s:5:\"value\";s:6:\"artist\";s:8:\"order_no\";i:0;s:8:\"group_no\";i:0;}');
INSERT INTO `wp_postmeta` VALUES (1118,770,'_edit_lock','1402113447:1');
INSERT INTO `wp_postmeta` VALUES (1119,770,'_edit_last','1');
INSERT INTO `wp_postmeta` VALUES (1120,770,'_wp_page_template','page-welcome.php');
INSERT INTO `wp_postmeta` VALUES (1121,771,'seo_page_title','');
INSERT INTO `wp_postmeta` VALUES (1122,771,'_seo_page_title','field_52012d032f7a9');
INSERT INTO `wp_postmeta` VALUES (1123,771,'meta_description','');
INSERT INTO `wp_postmeta` VALUES (1124,771,'_meta_description','field_52012d6e2f7aa');
INSERT INTO `wp_postmeta` VALUES (1125,770,'seo_page_title','');
INSERT INTO `wp_postmeta` VALUES (1126,770,'_seo_page_title','field_52012d032f7a9');
INSERT INTO `wp_postmeta` VALUES (1127,770,'meta_description','');
INSERT INTO `wp_postmeta` VALUES (1128,770,'_meta_description','field_52012d6e2f7aa');
INSERT INTO `wp_postmeta` VALUES (1129,772,'seo_page_title','');
INSERT INTO `wp_postmeta` VALUES (1130,772,'_seo_page_title','field_52012d032f7a9');
INSERT INTO `wp_postmeta` VALUES (1131,772,'meta_description','');
INSERT INTO `wp_postmeta` VALUES (1132,772,'_meta_description','field_52012d6e2f7aa');
INSERT INTO `wp_postmeta` VALUES (1133,770,'_thumbnail_id','76');
INSERT INTO `wp_postmeta` VALUES (1134,774,'seo_page_title','');
INSERT INTO `wp_postmeta` VALUES (1135,774,'_seo_page_title','field_52012d032f7a9');
INSERT INTO `wp_postmeta` VALUES (1136,774,'meta_description','');
INSERT INTO `wp_postmeta` VALUES (1137,774,'_meta_description','field_52012d6e2f7aa');
INSERT INTO `wp_postmeta` VALUES (1138,775,'seo_page_title','');
INSERT INTO `wp_postmeta` VALUES (1139,775,'_seo_page_title','field_52012d032f7a9');
INSERT INTO `wp_postmeta` VALUES (1140,775,'meta_description','');
INSERT INTO `wp_postmeta` VALUES (1141,775,'_meta_description','field_52012d6e2f7aa');
INSERT INTO `wp_postmeta` VALUES (1142,776,'_edit_lock','1402114926:3');
INSERT INTO `wp_postmeta` VALUES (1143,97,'artist_information','');
INSERT INTO `wp_postmeta` VALUES (1144,97,'_artist_information','field_538ccb5c967b9');
INSERT INTO `wp_postmeta` VALUES (1145,97,'profile_image','');
INSERT INTO `wp_postmeta` VALUES (1146,97,'_profile_image','field_538cba2564c21');
INSERT INTO `wp_postmeta` VALUES (1147,97,'banner_image','');
INSERT INTO `wp_postmeta` VALUES (1148,97,'_banner_image','field_5388d19fc1cc1');
INSERT INTO `wp_postmeta` VALUES (1149,97,'facebook','');
INSERT INTO `wp_postmeta` VALUES (1150,97,'_facebook','field_538cdc8a3f870');
INSERT INTO `wp_postmeta` VALUES (1151,97,'soundcloud','');
INSERT INTO `wp_postmeta` VALUES (1152,97,'_soundcloud','field_538cdc9a3f871');
INSERT INTO `wp_postmeta` VALUES (1153,97,'itunes','');
INSERT INTO `wp_postmeta` VALUES (1154,97,'_itunes','field_538cdcd83f872');
INSERT INTO `wp_postmeta` VALUES (1155,97,'twitter','');
INSERT INTO `wp_postmeta` VALUES (1156,97,'_twitter','field_538cdce83f873');
INSERT INTO `wp_postmeta` VALUES (1157,777,'_edit_lock','1402115353:1');
/*!40000 ALTER TABLE `wp_postmeta` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `wp_posts`
--

DROP TABLE IF EXISTS `wp_posts`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `wp_posts` (
  `ID` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `post_author` bigint(20) unsigned NOT NULL DEFAULT '0',
  `post_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `post_date_gmt` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `post_content` longtext NOT NULL,
  `post_title` text NOT NULL,
  `post_excerpt` text NOT NULL,
  `post_status` varchar(20) NOT NULL DEFAULT 'publish',
  `comment_status` varchar(20) NOT NULL DEFAULT 'open',
  `ping_status` varchar(20) NOT NULL DEFAULT 'open',
  `post_password` varchar(20) NOT NULL DEFAULT '',
  `post_name` varchar(200) NOT NULL DEFAULT '',
  `to_ping` text NOT NULL,
  `pinged` text NOT NULL,
  `post_modified` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `post_modified_gmt` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `post_content_filtered` longtext NOT NULL,
  `post_parent` bigint(20) unsigned NOT NULL DEFAULT '0',
  `guid` varchar(255) NOT NULL DEFAULT '',
  `menu_order` int(11) NOT NULL DEFAULT '0',
  `post_type` varchar(20) NOT NULL DEFAULT 'post',
  `post_mime_type` varchar(100) NOT NULL DEFAULT '',
  `comment_count` bigint(20) NOT NULL DEFAULT '0',
  PRIMARY KEY (`ID`),
  KEY `post_name` (`post_name`),
  KEY `type_status_date` (`post_type`,`post_status`,`post_date`,`ID`),
  KEY `post_parent` (`post_parent`),
  KEY `post_author` (`post_author`)
) ENGINE=InnoDB AUTO_INCREMENT=778 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `wp_posts`
--

LOCK TABLES `wp_posts` WRITE;
/*!40000 ALTER TABLE `wp_posts` DISABLE KEYS */;
INSERT INTO `wp_posts` VALUES (1,1,'2014-05-27 16:26:25','2014-05-27 16:26:25','Welcome to WordPress. This is your first post. Edit or delete it, then start blogging! Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Morbi commodo, ipsum sed pharetra gravida, orci magna rhoncus neque, id pulvinar odio lorem non turpis. Nullam sit amet enim. Suspendisse id velit vitae ligula volutpat condimentum. Aliquam erat volutpat. Sed quis velit. Nulla facilisi. Nulla libero. Vivamus pharetra posuere sapien. Nam consectetuer. Sed aliquam, nunc eget euismod ullamcorper, lectus nunc ullamcorper orci, fermentum bibendum enim nibh eget ipsum. Donec porttitor ligula eu dolor. Maecenas vitae nulla consequat libero cursus venenatis. Nam magna enim, accumsan eu, blandit sed, blandit a, eros.\r\n\r\nQuisque facilisis erat a dui. Nam malesuada ornare dolor. Cras gravida, diam sit amet rhoncus ornare, erat elit consectetuer erat, id egestas pede nibh eget odio. Proin tincidunt, velit vel porta elementum, magna diam molestie sapien, non aliquet massa pede eu diam. Aliquam iaculis. Fusce et ipsum et nulla tristique facilisis. Donec eget sem sit amet ligula viverra gravida. Etiam vehicula urna vel turpis. Suspendisse sagittis ante a urna. Morbi a est quis orci consequat rutrum. Nullam egestas feugiat felis. Integer adipiscing semper ligula. Nunc molestie, nisl sit amet cursus convallis, sapien lectus pretium metus, vitae pretium enim wisi id lectus. Donec vestibulum. Etiam vel nibh. Nulla facilisi. Mauris pharetra. Donec augue. Fusce ultrices, neque id dignissim ultrices, tellus mauris dictum elit, vel lacinia enim metus eu nunc.','Sample Interview','','publish','closed','closed','','hello-world','','','2014-06-06 13:57:32','2014-06-06 17:57:32','',0,'http://localhost/artsoul/?p=1',0,'post','',1);
INSERT INTO `wp_posts` VALUES (2,1,'2014-05-27 16:26:25','2014-05-27 16:26:25','This is an example page. It\'s different from a blog post because it will stay in one place and will show up in your site navigation (in most themes). Most people start with an About page that introduces them to potential site visitors. It might say something like this:\n\n<blockquote>Hi there! I\'m a bike messenger by day, aspiring actor by night, and this is my blog. I live in Los Angeles, have a great dog named Jack, and I like pi&#241;a coladas. (And gettin\' caught in the rain.)</blockquote>\n\n...or something like this:\n\n<blockquote>The XYZ Doohickey Company was founded in 1971, and has been providing quality doohickeys to the public ever since. Located in Gotham City, XYZ employs over 2,000 people and does all kinds of awesome things for the Gotham community.</blockquote>\n\nAs a new WordPress user, you should go to <a href=\"http://localhost/artsoul/wp-admin/\">your dashboard</a> to delete this page and create new pages for your content. Have fun!','Sample Page','','trash','open','open','','sample-page','','','2014-05-29 23:14:24','2014-05-30 03:14:24','',0,'http://localhost/artsoul/?page_id=2',0,'page','',0);
INSERT INTO `wp_posts` VALUES (12,1,'2014-05-27 18:45:28','2014-05-27 18:45:28','','Home','','publish','closed','closed','','home','','','2014-06-06 15:10:06','2014-06-06 19:10:06','',0,'http://artsoulonline/?page_id=12',0,'page','',0);
INSERT INTO `wp_posts` VALUES (13,1,'2014-05-27 18:45:28','2014-05-27 18:45:28','','Home','','inherit','open','open','','12-revision-v1','','','2014-05-27 18:45:28','2014-05-27 18:45:28','',12,'http://artsoulonline/uncategorized/12-revision-v1/',0,'revision','',0);
INSERT INTO `wp_posts` VALUES (14,1,'2014-05-27 18:45:45','2014-05-27 18:45:45',' ','','','publish','open','open','','14','','','2014-06-03 08:35:44','2014-06-03 12:35:44','',0,'http://artsoulonline/?p=14',1,'nav_menu_item','',0);
INSERT INTO `wp_posts` VALUES (15,1,'2014-05-29 13:19:04','2014-05-29 13:19:04','Lorem ipsum dolor sit amet, consectetuer adipiscing elit. <strong>Morbi commodo</strong>, ipsum sed pharetra gravida, orci magna rhoncus neque, id pulvinar odio lorem non turpis. Nullam sit amet enim. Suspendisse id velit vitae ligula volutpat condimentum. Aliquam erat volutpat. Sed quis velit. Nulla facilisi. Nulla libero. Vivamus pharetra posuere sapien. Nam consectetuer. Sed aliquam, nunc eget euismod ullamcorper, lectus nunc ullamcorper orci, fermentum bibendum enim nibh eget ipsum. Donec porttitor ligula eu dolor. Maecenas vitae nulla consequat libero cursus venenatis. Nam magna enim, accumsan eu, blandit sed, blandit a, eros.\r\n\r\n<img class=\"alignright wp-image-28 size-thumbnail\" src=\"http://dougsmacbookpro.local:5757/wp-content/uploads/2014/05/sponsor-soworthloving-150x150.jpg\" alt=\"sponsor-soworthloving\" width=\"150\" height=\"150\" />Quisque facilisis erat a dui. Nam malesuada ornare dolor. <em>Cras gravida, diam sit amet rhoncus ornare, erat elit consectetuer erat, id egestas pede nibh eget odio.</em> Proin tincidunt, velit vel porta elementum, magna diam molestie sapien, non aliquet massa pede eu diam. Aliquam iaculis. Fusce et ipsum et nulla tristique facilisis. Donec eget sem sit amet ligula viverra gravida. Etiam vehicula urna vel turpis. <del>Suspendisse sagittis ante a urna</del>. Morbi a est quis orci consequat rutrum. Nullam egestas feugiat felis. Integer adipiscing semper ligula. Nunc molestie, nisl sit amet cursus convallis, sapien lectus pretium metus, vitae pretium enim wisi id lectus. Donec vestibulum. Etiam vel nibh. Nulla facilisi. Mauris pharetra. Donec augue. Fusce ultrices, neque id dignissim ultrices, tellus mauris dictum elit, vel lacinia enim metus eu nunc.\r\n\r\nProin at eros non eros adipiscing mollis. Donec semper turpis sed diam. <span style=\"text-decoration: underline;\">Sed consequat ligula nec tortor.</span> Integer eget sem. Ut vitae enim eu est vehicula gravida. Morbi ipsum ipsum, porta nec, tempor id, auctor vitae, purus. Pellentesque neque. Nulla luctus erat vitae libero. Integer nec enim. Phasellus aliquam enim et tortor. Quisque aliquet, quam elementum condimentum feugiat, tellus odio consectetuer wisi, vel nonummy sem neque in elit.\r\n<blockquote>\"Curabitur eleifend wisi iaculis ipsum. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. In non velit non ligula laoreet ultrices. Praesent ultricies facilisis nisl.\"</blockquote>\r\nVivamus luctus elit sit amet mi. Phasellus pellentesque, erat eget elementum volutpat, dolor nisl porta neque, vitae sodales ipsum nibh in ligula. Maecenas mattis pulvinar diam. Curabitur sed leo.\r\n\r\nNulla facilisi. In vel sem. Morbi id urna in diam dignissim feugiat. Proin molestie tortor eu velit. Aliquam erat volutpat. Nullam ultrices, diam tempus vulputate egestas, eros pede varius leo, sed imperdiet lectus est ornare odio.\r\n<ul>\r\n	<li>Lorem ipsum dolor sit amet.</li>\r\n	<li>Consectetuer adipiscing elit.\r\n<ol>\r\n	<li>Numbered list</li>\r\n	<li>inside a</li>\r\n	<li>bulleted list</li>\r\n</ol>\r\n</li>\r\n	<li>Proin consectetuer velit in dui.</li>\r\n	<li>Phasellus wisi purus, interdum vitae.</li>\r\n</ul>\r\nRutrum accumsan, viverra in, velit. Sed enim risus, congue non, tristique in, commodo eu, metus. Aenean tortor mi, imperdiet id, gravida eu, posuere eu, felis.\r\n<ol>\r\n	<li>Mauris sollicitudin, turpis in.</li>\r\n	<li>Hendrerit sodales lectus.</li>\r\n	<li>Ipsum pellentesque ligula.\r\n<ul>\r\n	<li>Bulleted list</li>\r\n	<li>inside a</li>\r\n	<li>numbered list</li>\r\n</ul>\r\n</li>\r\n	<li>Sit amet scelerisque urna.</li>\r\n</ol>\r\nNibh ut arcu. Aliquam in lacus. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Nulla placerat aliquam wisi. Mauris viverra odio. Quisque fermentum pulvinar odio. Proin posuere est vitae ligula. Etiam euismod. Cras a eros.\r\n\r\n<hr />\r\n\r\nNunc auctor bibendum eros. Maecenas porta accumsan mauris. Etiam enim enim, elementum sed, bibendum quis, rhoncus non, metus. Fusce neque dolor, adipiscing sed, consectetuer et, lacinia sit amet, quam. Suspendisse wisi quam, consectetuer in, blandit sed, suscipit eu, eros. Etiam ligula enim, tempor ut, blandit nec, mollis eu, lectus. Nam cursus. Vivamus iaculis. Aenean risus purus, pharetra in, blandit quis, gravida a, turpis. Donec nisl. Aenean eget mi.','Study: Sample posts 10% more interesting than real posts.','','publish','open','open','','sample-post-1','','','2014-05-29 23:01:39','2014-05-30 03:01:39','',0,'http://artsoulonline/?p=15',0,'post','',0);
INSERT INTO `wp_posts` VALUES (16,1,'2014-05-29 13:19:04','2014-05-29 13:19:04','Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Morbi commodo, ipsum sed pharetra gravida, orci magna rhoncus neque, id pulvinar odio lorem non turpis. Nullam sit amet enim. Suspendisse id velit vitae ligula volutpat condimentum. Aliquam erat volutpat. Sed quis velit. Nulla facilisi. Nulla libero. Vivamus pharetra posuere sapien. Nam consectetuer. Sed aliquam, nunc eget euismod ullamcorper, lectus nunc ullamcorper orci, fermentum bibendum enim nibh eget ipsum. Donec porttitor ligula eu dolor. Maecenas vitae nulla consequat libero cursus venenatis. Nam magna enim, accumsan eu, blandit sed, blandit a, eros.\r\n\r\nQuisque facilisis erat a dui. Nam malesuada ornare dolor. Cras gravida, diam sit amet rhoncus ornare, erat elit consectetuer erat, id egestas pede nibh eget odio. Proin tincidunt, velit vel porta elementum, magna diam molestie sapien, non aliquet massa pede eu diam. Aliquam iaculis. Fusce et ipsum et nulla tristique facilisis. Donec eget sem sit amet ligula viverra gravida. Etiam vehicula urna vel turpis. Suspendisse sagittis ante a urna. Morbi a est quis orci consequat rutrum. Nullam egestas feugiat felis. Integer adipiscing semper ligula. Nunc molestie, nisl sit amet cursus convallis, sapien lectus pretium metus, vitae pretium enim wisi id lectus. Donec vestibulum. Etiam vel nibh. Nulla facilisi. Mauris pharetra. Donec augue. Fusce ultrices, neque id dignissim ultrices, tellus mauris dictum elit, vel lacinia enim metus eu nunc.\r\n\r\nProin at eros non eros adipiscing mollis. Donec semper turpis sed diam. Sed consequat ligula nec tortor. Integer eget sem. Ut vitae enim eu est vehicula gravida. Morbi ipsum ipsum, porta nec, tempor id, auctor vitae, purus. Pellentesque neque. Nulla luctus erat vitae libero. Integer nec enim. Phasellus aliquam enim et tortor. Quisque aliquet, quam elementum condimentum feugiat, tellus odio consectetuer wisi, vel nonummy sem neque in elit. Curabitur eleifend wisi iaculis ipsum. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. In non velit non ligula laoreet ultrices. Praesent ultricies facilisis nisl. Vivamus luctus elit sit amet mi. Phasellus pellentesque, erat eget elementum volutpat, dolor nisl porta neque, vitae sodales ipsum nibh in ligula. Maecenas mattis pulvinar diam. Curabitur sed leo.\r\n\r\nNulla facilisi. In vel sem. Morbi id urna in diam dignissim feugiat. Proin molestie tortor eu velit. Aliquam erat volutpat. Nullam ultrices, diam tempus vulputate egestas, eros pede varius leo, sed imperdiet lectus est ornare odio. Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Proin consectetuer velit in dui. Phasellus wisi purus, interdum vitae, rutrum accumsan, viverra in, velit. Sed enim risus, congue non, tristique in, commodo eu, metus. Aenean tortor mi, imperdiet id, gravida eu, posuere eu, felis. Mauris sollicitudin, turpis in hendrerit sodales, lectus ipsum pellentesque ligula, sit amet scelerisque urna nibh ut arcu. Aliquam in lacus. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Nulla placerat aliquam wisi. Mauris viverra odio. Quisque fermentum pulvinar odio. Proin posuere est vitae ligula. Etiam euismod. Cras a eros.\r\n\r\nNunc auctor bibendum eros. Maecenas porta accumsan mauris. Etiam enim enim, elementum sed, bibendum quis, rhoncus non, metus. Fusce neque dolor, adipiscing sed, consectetuer et, lacinia sit amet, quam. Suspendisse wisi quam, consectetuer in, blandit sed, suscipit eu, eros. Etiam ligula enim, tempor ut, blandit nec, mollis eu, lectus. Nam cursus. Vivamus iaculis. Aenean risus purus, pharetra in, blandit quis, gravida a, turpis. Donec nisl. Aenean eget mi.','Sample Post 1','','inherit','open','open','','15-revision-v1','','','2014-05-29 13:19:04','2014-05-29 13:19:04','',15,'http://artsoulonline/uncategorized/15-revision-v1/',0,'revision','',0);
INSERT INTO `wp_posts` VALUES (18,1,'2014-05-29 13:45:05','2014-05-29 13:45:05','Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Morbi commodo, ipsum sed pharetra gravida, orci magna rhoncus neque, id pulvinar odio lorem non turpis. Nullam sit amet enim. Suspendisse id velit vitae ligula volutpat condimentum. Aliquam erat volutpat. Sed quis velit. Nulla facilisi. Nulla libero. Vivamus pharetra posuere sapien. Nam consectetuer. Sed aliquam, nunc eget euismod ullamcorper, lectus nunc ullamcorper orci, fermentum bibendum enim nibh eget ipsum. Donec porttitor ligula eu dolor. Maecenas vitae nulla consequat libero cursus venenatis. Nam magna enim, accumsan eu, blandit sed, blandit a, eros.\r\n\r\nQuisque facilisis erat a dui. Nam malesuada ornare dolor. Cras gravida, diam sit amet rhoncus ornare, erat elit consectetuer erat, id egestas pede nibh eget odio. Proin tincidunt, velit vel porta elementum, magna diam molestie sapien, non aliquet massa pede eu diam. Aliquam iaculis. Fusce et ipsum et nulla tristique facilisis. Donec eget sem sit amet ligula viverra gravida. Etiam vehicula urna vel turpis. Suspendisse sagittis ante a urna. Morbi a est quis orci consequat rutrum. Nullam egestas feugiat felis. Integer adipiscing semper ligula. Nunc molestie, nisl sit amet cursus convallis, sapien lectus pretium metus, vitae pretium enim wisi id lectus. Donec vestibulum. Etiam vel nibh. Nulla facilisi. Mauris pharetra. Donec augue. Fusce ultrices, neque id dignissim ultrices, tellus mauris dictum elit, vel lacinia enim metus eu nunc.\r\n\r\nProin at eros non eros adipiscing mollis. Donec semper turpis sed diam. Sed consequat ligula nec tortor. Integer eget sem. Ut vitae enim eu est vehicula gravida. Morbi ipsum ipsum, porta nec, tempor id, auctor vitae, purus. Pellentesque neque. Nulla luctus erat vitae libero. Integer nec enim. Phasellus aliquam enim et tortor. Quisque aliquet, quam elementum condimentum feugiat, tellus odio consectetuer wisi, vel nonummy sem neque in elit. Curabitur eleifend wisi iaculis ipsum. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. In non velit non ligula laoreet ultrices. Praesent ultricies facilisis nisl. Vivamus luctus elit sit amet mi. Phasellus pellentesque, erat eget elementum volutpat, dolor nisl porta neque, vitae sodales ipsum nibh in ligula. Maecenas mattis pulvinar diam. Curabitur sed leo.\r\n\r\nNulla facilisi. In vel sem. Morbi id urna in diam dignissim feugiat. Proin molestie tortor eu velit. Aliquam erat volutpat. Nullam ultrices, diam tempus vulputate egestas, eros pede varius leo, sed imperdiet lectus est ornare odio. Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Proin consectetuer velit in dui. Phasellus wisi purus, interdum vitae, rutrum accumsan, viverra in, velit. Sed enim risus, congue non, tristique in, commodo eu, metus. Aenean tortor mi, imperdiet id, gravida eu, posuere eu, felis. Mauris sollicitudin, turpis in hendrerit sodales, lectus ipsum pellentesque ligula, sit amet scelerisque urna nibh ut arcu. Aliquam in lacus. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Nulla placerat aliquam wisi. Mauris viverra odio. Quisque fermentum pulvinar odio. Proin posuere est vitae ligula. Etiam euismod. Cras a eros.\r\n\r\nNunc auctor bibendum eros. Maecenas porta accumsan mauris. Etiam enim enim, elementum sed, bibendum quis, rhoncus non, metus. Fusce neque dolor, adipiscing sed, consectetuer et, lacinia sit amet, quam. Suspendisse wisi quam, consectetuer in, blandit sed, suscipit eu, eros. Etiam ligula enim, tempor ut, blandit nec, mollis eu, lectus. Nam cursus. Vivamus iaculis. Aenean risus purus, pharetra in, blandit quis, gravida a, turpis. Donec nisl. Aenean eget mi.','Study: Sample posts 10% more interesting than real posts.','','inherit','open','open','','15-revision-v1','','','2014-05-29 13:45:05','2014-05-29 13:45:05','',15,'http://artsoulonline/uncategorized/15-revision-v1/',0,'revision','',0);
INSERT INTO `wp_posts` VALUES (19,1,'2014-05-29 13:45:59','2014-05-29 13:45:59','Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Morbi commodo, ipsum sed pharetra gravida, orci magna rhoncus neque, id pulvinar odio lorem non turpis. Nullam sit amet enim. Suspendisse id velit vitae ligula volutpat condimentum. Aliquam erat volutpat. Sed quis velit. Nulla facilisi. Nulla libero. Vivamus pharetra posuere sapien. Nam consectetuer. Sed aliquam, nunc eget euismod ullamcorper, lectus nunc ullamcorper orci, fermentum bibendum enim nibh eget ipsum. Donec porttitor ligula eu dolor. Maecenas vitae nulla consequat libero cursus venenatis. Nam magna enim, accumsan eu, blandit sed, blandit a, eros.\r\n\r\nQuisque facilisis erat a dui. Nam malesuada ornare dolor. Cras gravida, diam sit amet rhoncus ornare, erat elit consectetuer erat, id egestas pede nibh eget odio. Proin tincidunt, velit vel porta elementum, magna diam molestie sapien, non aliquet massa pede eu diam. Aliquam iaculis. Fusce et ipsum et nulla tristique facilisis. Donec eget sem sit amet ligula viverra gravida. Etiam vehicula urna vel turpis. Suspendisse sagittis ante a urna. Morbi a est quis orci consequat rutrum. Nullam egestas feugiat felis. Integer adipiscing semper ligula. Nunc molestie, nisl sit amet cursus convallis, sapien lectus pretium metus, vitae pretium enim wisi id lectus. Donec vestibulum. Etiam vel nibh. Nulla facilisi. Mauris pharetra. Donec augue. Fusce ultrices, neque id dignissim ultrices, tellus mauris dictum elit, vel lacinia enim metus eu nunc.\r\n\r\nProin at eros non eros adipiscing mollis. Donec semper turpis sed diam. Sed consequat ligula nec tortor. Integer eget sem. Ut vitae enim eu est vehicula gravida. Morbi ipsum ipsum, porta nec, tempor id, auctor vitae, purus. Pellentesque neque. Nulla luctus erat vitae libero. Integer nec enim. Phasellus aliquam enim et tortor. Quisque aliquet, quam elementum condimentum feugiat, tellus odio consectetuer wisi, vel nonummy sem neque in elit. Curabitur eleifend wisi iaculis ipsum. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. In non velit non ligula laoreet ultrices. Praesent ultricies facilisis nisl. Vivamus luctus elit sit amet mi.','Sample author John Doe on lorem ipsum','','publish','open','open','','sample-author-john-doe-on-lorem-ipsum','','','2014-05-29 14:19:21','2014-05-29 14:19:21','',0,'http://artsoulonline/?p=19',0,'post','',0);
INSERT INTO `wp_posts` VALUES (20,1,'2014-05-29 13:45:59','2014-05-29 13:45:59','Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Morbi commodo, ipsum sed pharetra gravida, orci magna rhoncus neque, id pulvinar odio lorem non turpis. Nullam sit amet enim. Suspendisse id velit vitae ligula volutpat condimentum. Aliquam erat volutpat. Sed quis velit. Nulla facilisi. Nulla libero. Vivamus pharetra posuere sapien. Nam consectetuer. Sed aliquam, nunc eget euismod ullamcorper, lectus nunc ullamcorper orci, fermentum bibendum enim nibh eget ipsum. Donec porttitor ligula eu dolor. Maecenas vitae nulla consequat libero cursus venenatis. Nam magna enim, accumsan eu, blandit sed, blandit a, eros.\r\n\r\nQuisque facilisis erat a dui. Nam malesuada ornare dolor. Cras gravida, diam sit amet rhoncus ornare, erat elit consectetuer erat, id egestas pede nibh eget odio. Proin tincidunt, velit vel porta elementum, magna diam molestie sapien, non aliquet massa pede eu diam. Aliquam iaculis. Fusce et ipsum et nulla tristique facilisis. Donec eget sem sit amet ligula viverra gravida. Etiam vehicula urna vel turpis. Suspendisse sagittis ante a urna. Morbi a est quis orci consequat rutrum. Nullam egestas feugiat felis. Integer adipiscing semper ligula. Nunc molestie, nisl sit amet cursus convallis, sapien lectus pretium metus, vitae pretium enim wisi id lectus. Donec vestibulum. Etiam vel nibh. Nulla facilisi. Mauris pharetra. Donec augue. Fusce ultrices, neque id dignissim ultrices, tellus mauris dictum elit, vel lacinia enim metus eu nunc.\r\n\r\nProin at eros non eros adipiscing mollis. Donec semper turpis sed diam. Sed consequat ligula nec tortor. Integer eget sem. Ut vitae enim eu est vehicula gravida. Morbi ipsum ipsum, porta nec, tempor id, auctor vitae, purus. Pellentesque neque. Nulla luctus erat vitae libero. Integer nec enim. Phasellus aliquam enim et tortor. Quisque aliquet, quam elementum condimentum feugiat, tellus odio consectetuer wisi, vel nonummy sem neque in elit. Curabitur eleifend wisi iaculis ipsum. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. In non velit non ligula laoreet ultrices. Praesent ultricies facilisis nisl. Vivamus luctus elit sit amet mi.','Sample Author John Doe on Lorem Ipsum','','inherit','open','open','','19-revision-v1','','','2014-05-29 13:45:59','2014-05-29 13:45:59','',19,'http://artsoulonline/uncategorized/19-revision-v1/',0,'revision','',0);
INSERT INTO `wp_posts` VALUES (21,1,'2014-05-29 13:48:50','2014-05-29 13:48:50','Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Morbi commodo, ipsum sed pharetra gravida, orci magna rhoncus neque, id pulvinar odio lorem non turpis. Nullam sit amet enim. Suspendisse id velit vitae ligula volutpat condimentum. Aliquam erat volutpat. Sed quis velit. Nulla facilisi. Nulla libero. Vivamus pharetra posuere sapien. Nam consectetuer. Sed aliquam, nunc eget euismod ullamcorper, lectus nunc ullamcorper orci, fermentum bibendum enim nibh eget ipsum. Donec porttitor ligula eu dolor. Maecenas vitae nulla consequat libero cursus venenatis. Nam magna enim, accumsan eu, blandit sed, blandit a, eros.\r\n\r\nQuisque facilisis erat a dui. Nam malesuada ornare dolor. Cras gravida, diam sit amet rhoncus ornare, erat elit consectetuer erat, id egestas pede nibh eget odio. Proin tincidunt, velit vel porta elementum, magna diam molestie sapien, non aliquet massa pede eu diam. Aliquam iaculis. Fusce et ipsum et nulla tristique facilisis. Donec eget sem sit amet ligula viverra gravida. Etiam vehicula urna vel turpis. Suspendisse sagittis ante a urna. Morbi a est quis orci consequat rutrum. Nullam egestas feugiat felis. Integer adipiscing semper ligula. Nunc molestie, nisl sit amet cursus convallis, sapien lectus pretium metus, vitae pretium enim wisi id lectus. Donec vestibulum. Etiam vel nibh. Nulla facilisi. Mauris pharetra. Donec augue. Fusce ultrices, neque id dignissim ultrices, tellus mauris dictum elit, vel lacinia enim metus eu nunc.\r\n\r\nProin at eros non eros adipiscing mollis. Donec semper turpis sed diam. Sed consequat ligula nec tortor. Integer eget sem. Ut vitae enim eu est vehicula gravida. Morbi ipsum ipsum, porta nec, tempor id, auctor vitae, purus. Pellentesque neque. Nulla luctus erat vitae libero. Integer nec enim. Phasellus aliquam enim et tortor. Quisque aliquet, quam elementum condimentum feugiat, tellus odio consectetuer wisi, vel nonummy sem neque in elit. Curabitur eleifend wisi iaculis ipsum. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. In non velit non ligula laoreet ultrices. Praesent ultricies facilisis nisl. Vivamus luctus elit sit amet mi.','Sample author John Doe on lorem ipsum','','inherit','open','open','','19-revision-v1','','','2014-05-29 13:48:50','2014-05-29 13:48:50','',19,'http://artsoulonline/uncategorized/19-revision-v1/',0,'revision','',0);
INSERT INTO `wp_posts` VALUES (22,1,'2014-05-29 13:54:15','2014-05-29 13:54:15','','featured-image','','inherit','open','open','','featured-image','','','2014-05-29 13:54:15','2014-05-29 13:54:15','',19,'http://artsoulonline/wp-content/uploads/2014/05/featured-image.jpg',0,'attachment','image/jpeg',0);
INSERT INTO `wp_posts` VALUES (23,1,'2014-05-29 14:15:50','2014-05-29 14:15:50','','Site Options','','publish','closed','closed','','acf_site-options','','','2014-06-03 10:08:55','2014-06-03 14:08:55','',0,'http://artsoulonline/?post_type=acf&#038;p=23',0,'acf','',0);
INSERT INTO `wp_posts` VALUES (24,1,'2014-05-29 14:42:41','2014-05-29 14:42:41','','Sponsor Areas','','publish','closed','closed','','acf_sponsor-areas','','','2014-05-30 09:51:47','2014-05-30 13:51:47','',0,'http://artsoulonline/?post_type=acf&#038;p=24',0,'acf','',0);
INSERT INTO `wp_posts` VALUES (27,1,'2014-05-29 15:13:49','2014-05-29 15:13:49','','sponsor-relevant','','inherit','open','open','','sponsor-relevant','','','2014-05-29 15:13:49','2014-05-29 15:13:49','',0,'http://artsoulonline/wp-content/uploads/2014/05/sponsor-relevant.jpg',0,'attachment','image/jpeg',0);
INSERT INTO `wp_posts` VALUES (28,1,'2014-05-29 15:13:50','2014-05-29 15:13:50','','sponsor-soworthloving','','inherit','open','open','','sponsor-soworthloving','','','2014-05-29 15:13:50','2014-05-29 15:13:50','',15,'http://artsoulonline/wp-content/uploads/2014/05/sponsor-soworthloving.jpg',0,'attachment','image/jpeg',0);
INSERT INTO `wp_posts` VALUES (30,1,'2014-05-29 17:30:29','2014-05-29 17:30:29','','featured-image-kye-kye','','inherit','open','open','','featured-image-kye-kye','','','2014-05-29 17:30:29','2014-05-29 17:30:29','',15,'http://artsoulonline/wp-content/uploads/2014/05/featured-image-kye-kye.jpg',0,'attachment','image/jpeg',0);
INSERT INTO `wp_posts` VALUES (31,1,'2014-05-29 14:08:07','2014-05-29 18:08:07','','User Meta','','publish','closed','closed','','acf_user-meta','','','2014-05-29 14:08:07','2014-05-29 18:08:07','',0,'http://dougsmacbookpro.local:5757/?post_type=acf&#038;p=31',0,'acf','',0);
INSERT INTO `wp_posts` VALUES (32,1,'2014-05-29 23:00:43','2014-05-30 03:00:43','Lorem ipsum dolor sit amet, consectetuer adipiscing elit. <strong>Morbi commodo</strong>, ipsum sed pharetra gravida, orci magna rhoncus neque, id pulvinar odio lorem non turpis. Nullam sit amet enim. Suspendisse id velit vitae ligula volutpat condimentum. Aliquam erat volutpat. Sed quis velit. Nulla facilisi. Nulla libero. Vivamus pharetra posuere sapien. Nam consectetuer. Sed aliquam, nunc eget euismod ullamcorper, lectus nunc ullamcorper orci, fermentum bibendum enim nibh eget ipsum. Donec porttitor ligula eu dolor. Maecenas vitae nulla consequat libero cursus venenatis. Nam magna enim, accumsan eu, blandit sed, blandit a, eros.\n\n<img class=\"alignright wp-image-28 size-thumbnail\" src=\"http://dougsmacbookpro.local:5757/wp-content/uploads/2014/05/sponsor-soworthloving-150x150.jpg\" alt=\"sponsor-soworthloving\" width=\"150\" height=\"150\" />Quisque facilisis erat a dui. Nam malesuada ornare dolor. <em>Cras gravida, diam sit amet rhoncus ornare, erat elit consectetuer erat, id egestas pede nibh eget odio.</em> Proin tincidunt, velit vel porta elementum, magna diam molestie sapien, non aliquet massa pede eu diam. Aliquam iaculis. Fusce et ipsum et nulla tristique facilisis. Donec eget sem sit amet ligula viverra gravida. Etiam vehicula urna vel turpis. <del>Suspendisse sagittis ante a urna</del>. Morbi a est quis orci consequat rutrum. Nullam egestas feugiat felis. Integer adipiscing semper ligula. Nunc molestie, nisl sit amet cursus convallis, sapien lectus pretium metus, vitae pretium enim wisi id lectus. Donec vestibulum. Etiam vel nibh. Nulla facilisi. Mauris pharetra. Donec augue. Fusce ultrices, neque id dignissim ultrices, tellus mauris dictum elit, vel lacinia enim metus eu nunc.\n\nProin at eros non eros adipiscing mollis. Donec semper turpis sed diam. <span style=\"text-decoration: underline;\">Sed consequat ligula nec tortor.</span> Integer eget sem. Ut vitae enim eu est vehicula gravida. Morbi ipsum ipsum, porta nec, tempor id, auctor vitae, purus. Pellentesque neque. Nulla luctus erat vitae libero. Integer nec enim. Phasellus aliquam enim et tortor. Quisque aliquet, quam elementum condimentum feugiat, tellus odio consectetuer wisi, vel nonummy sem neque in elit.\n<blockquote>\"Curabitur eleifend wisi iaculis ipsum. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. In non velit non ligula laoreet ultrices. Praesent ultricies facilisis nisl.\"</blockquote>\nVivamus luctus elit sit amet mi. Phasellus pellentesque, erat eget elementum volutpat, dolor nisl porta neque, vitae sodales ipsum nibh in ligula. Maecenas mattis pulvinar diam. Curabitur sed leo.\n\nNulla facilisi. In vel sem. Morbi id urna in diam dignissim feugiat. Proin molestie tortor eu velit. Aliquam erat volutpat. Nullam ultrices, diam tempus vulputate egestas, eros pede varius leo, sed imperdiet lectus est ornare odio.\n<ul>\n	<li>Lorem ipsum dolor sit amet.</li>\n	<li>Consectetuer adipiscing elit.\n<ol>\n	<li>Numbered list</li>\n	<li>inside a</li>\n	<li>bulleted list</li>\n</ol>\n</li>\n	<li>Proin consectetuer velit in dui.</li>\n	<li>Phasellus wisi purus, interdum vitae.</li>\n</ul>\nRutrum accumsan, viverra in, velit. Sed enim risus, congue non, tristique in, commodo eu, metus. Aenean tortor mi, imperdiet id, gravida eu, posuere eu, felis.\n<ol>\n	<li>Mauris sollicitudin, turpis in hendrerit sodales, lectus ipsum pellentesque ligula, sit amet scelerisque urna nibh ut arcu. Aliquam in lacus. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Nulla placerat aliquam wisi. Mauris viverra odio. Quisque fermentum pulvinar odio. Proin posuere est vitae ligula. Etiam euismod. Cras a eros.</li>\n</ol>\n\n<hr />\n\nNunc auctor bibendum eros. Maecenas porta accumsan mauris. Etiam enim enim, elementum sed, bibendum quis, rhoncus non, metus. Fusce neque dolor, adipiscing sed, consectetuer et, lacinia sit amet, quam. Suspendisse wisi quam, consectetuer in, blandit sed, suscipit eu, eros. Etiam ligula enim, tempor ut, blandit nec, mollis eu, lectus. Nam cursus. Vivamus iaculis. Aenean risus purus, pharetra in, blandit quis, gravida a, turpis. Donec nisl. Aenean eget mi.','Study: Sample posts 10% more interesting than real posts.','','inherit','open','open','','15-autosave-v1','','','2014-05-29 23:00:43','2014-05-30 03:00:43','',15,'http://artsoulonline/uncategorized/15-autosave-v1/',0,'revision','',0);
INSERT INTO `wp_posts` VALUES (33,1,'2014-05-29 14:53:29','2014-05-29 18:53:29','Lorem ipsum dolor sit amet, consectetuer adipiscing elit. <strong>Morbi commodo</strong>, ipsum sed pharetra gravida, orci magna rhoncus neque, id pulvinar odio lorem non turpis. Nullam sit amet enim. Suspendisse id velit vitae ligula volutpat condimentum. Aliquam erat volutpat. Sed quis velit. Nulla facilisi. Nulla libero. Vivamus pharetra posuere sapien. Nam consectetuer. Sed aliquam, nunc eget euismod ullamcorper, lectus nunc ullamcorper orci, fermentum bibendum enim nibh eget ipsum. Donec porttitor ligula eu dolor. Maecenas vitae nulla consequat libero cursus venenatis. Nam magna enim, accumsan eu, blandit sed, blandit a, eros.\r\n\r\nQuisque facilisis erat a dui. Nam malesuada ornare dolor. <em>Cras gravida, diam sit amet rhoncus ornare, erat elit consectetuer erat, id egestas pede nibh eget odio.</em> Proin tincidunt, velit vel porta elementum, magna diam molestie sapien, non aliquet massa pede eu diam. Aliquam iaculis. Fusce et ipsum et nulla tristique facilisis. Donec eget sem sit amet ligula viverra gravida. Etiam vehicula urna vel turpis. <del>Suspendisse sagittis ante a urna</del>. Morbi a est quis orci consequat rutrum. Nullam egestas feugiat felis. Integer adipiscing semper ligula. Nunc molestie, nisl sit amet cursus convallis, sapien lectus pretium metus, vitae pretium enim wisi id lectus. Donec vestibulum. Etiam vel nibh. Nulla facilisi. Mauris pharetra. Donec augue. Fusce ultrices, neque id dignissim ultrices, tellus mauris dictum elit, vel lacinia enim metus eu nunc.\r\n\r\nProin at eros non eros adipiscing mollis. Donec semper turpis sed diam. <span style=\"text-decoration: underline;\">Sed consequat ligula nec tortor.</span> Integer eget sem. Ut vitae enim eu est vehicula gravida. Morbi ipsum ipsum, porta nec, tempor id, auctor vitae, purus. Pellentesque neque. Nulla luctus erat vitae libero. Integer nec enim. Phasellus aliquam enim et tortor. Quisque aliquet, quam elementum condimentum feugiat, tellus odio consectetuer wisi, vel nonummy sem neque in elit.\r\n<blockquote>Curabitur eleifend wisi iaculis ipsum. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. In non velit non ligula laoreet ultrices. Praesent ultricies facilisis nisl.</blockquote>\r\nVivamus luctus elit sit amet mi. Phasellus pellentesque, erat eget elementum volutpat, dolor nisl porta neque, vitae sodales ipsum nibh in ligula. Maecenas mattis pulvinar diam. Curabitur sed leo.\r\n\r\nNulla facilisi. In vel sem. Morbi id urna in diam dignissim feugiat. Proin molestie tortor eu velit. Aliquam erat volutpat. Nullam ultrices, diam tempus vulputate egestas, eros pede varius leo, sed imperdiet lectus est ornare odio. Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Proin consectetuer velit in dui. Phasellus wisi purus, interdum vitae, rutrum accumsan, viverra in, velit. Sed enim risus, congue non, tristique in, commodo eu, metus. Aenean tortor mi, imperdiet id, gravida eu, posuere eu, felis. Mauris sollicitudin, turpis in hendrerit sodales, lectus ipsum pellentesque ligula, sit amet scelerisque urna nibh ut arcu. Aliquam in lacus. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Nulla placerat aliquam wisi. Mauris viverra odio. Quisque fermentum pulvinar odio. Proin posuere est vitae ligula. Etiam euismod. Cras a eros.\r\n\r\n<hr />\r\n\r\nNunc auctor bibendum eros. Maecenas porta accumsan mauris. Etiam enim enim, elementum sed, bibendum quis, rhoncus non, metus. Fusce neque dolor, adipiscing sed, consectetuer et, lacinia sit amet, quam. Suspendisse wisi quam, consectetuer in, blandit sed, suscipit eu, eros. Etiam ligula enim, tempor ut, blandit nec, mollis eu, lectus. Nam cursus. Vivamus iaculis. Aenean risus purus, pharetra in, blandit quis, gravida a, turpis. Donec nisl. Aenean eget mi.','Study: Sample posts 10% more interesting than real posts.','','inherit','open','open','','15-revision-v1','','','2014-05-29 14:53:29','2014-05-29 18:53:29','',15,'http://artsoulonline/uncategorized/15-revision-v1/',0,'revision','',0);
INSERT INTO `wp_posts` VALUES (34,1,'2014-05-29 14:55:03','2014-05-29 18:55:03','Lorem ipsum dolor sit amet, consectetuer adipiscing elit. <strong>Morbi commodo</strong>, ipsum sed pharetra gravida, orci magna rhoncus neque, id pulvinar odio lorem non turpis. Nullam sit amet enim. Suspendisse id velit vitae ligula volutpat condimentum. Aliquam erat volutpat. Sed quis velit. Nulla facilisi. Nulla libero. Vivamus pharetra posuere sapien. Nam consectetuer. Sed aliquam, nunc eget euismod ullamcorper, lectus nunc ullamcorper orci, fermentum bibendum enim nibh eget ipsum. Donec porttitor ligula eu dolor. Maecenas vitae nulla consequat libero cursus venenatis. Nam magna enim, accumsan eu, blandit sed, blandit a, eros.\r\n\r\nQuisque facilisis erat a dui. Nam malesuada ornare dolor. <em>Cras gravida, diam sit amet rhoncus ornare, erat elit consectetuer erat, id egestas pede nibh eget odio.</em> Proin tincidunt, velit vel porta elementum, magna diam molestie sapien, non aliquet massa pede eu diam. Aliquam iaculis. Fusce et ipsum et nulla tristique facilisis. Donec eget sem sit amet ligula viverra gravida. Etiam vehicula urna vel turpis. <del>Suspendisse sagittis ante a urna</del>. Morbi a est quis orci consequat rutrum. Nullam egestas feugiat felis. Integer adipiscing semper ligula. Nunc molestie, nisl sit amet cursus convallis, sapien lectus pretium metus, vitae pretium enim wisi id lectus. Donec vestibulum. Etiam vel nibh. Nulla facilisi. Mauris pharetra. Donec augue. Fusce ultrices, neque id dignissim ultrices, tellus mauris dictum elit, vel lacinia enim metus eu nunc.\r\n\r\nProin at eros non eros adipiscing mollis. Donec semper turpis sed diam. <span style=\"text-decoration: underline;\">Sed consequat ligula nec tortor.</span> Integer eget sem. Ut vitae enim eu est vehicula gravida. Morbi ipsum ipsum, porta nec, tempor id, auctor vitae, purus. Pellentesque neque. Nulla luctus erat vitae libero. Integer nec enim. Phasellus aliquam enim et tortor. Quisque aliquet, quam elementum condimentum feugiat, tellus odio consectetuer wisi, vel nonummy sem neque in elit.\r\n<blockquote>\"Curabitur eleifend wisi iaculis ipsum. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. In non velit non ligula laoreet ultrices. Praesent ultricies facilisis nisl.\"</blockquote>\r\nVivamus luctus elit sit amet mi. Phasellus pellentesque, erat eget elementum volutpat, dolor nisl porta neque, vitae sodales ipsum nibh in ligula. Maecenas mattis pulvinar diam. Curabitur sed leo.\r\n\r\nNulla facilisi. In vel sem. Morbi id urna in diam dignissim feugiat. Proin molestie tortor eu velit. Aliquam erat volutpat. Nullam ultrices, diam tempus vulputate egestas, eros pede varius leo, sed imperdiet lectus est ornare odio. Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Proin consectetuer velit in dui. Phasellus wisi purus, interdum vitae, rutrum accumsan, viverra in, velit. Sed enim risus, congue non, tristique in, commodo eu, metus. Aenean tortor mi, imperdiet id, gravida eu, posuere eu, felis. Mauris sollicitudin, turpis in hendrerit sodales, lectus ipsum pellentesque ligula, sit amet scelerisque urna nibh ut arcu. Aliquam in lacus. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Nulla placerat aliquam wisi. Mauris viverra odio. Quisque fermentum pulvinar odio. Proin posuere est vitae ligula. Etiam euismod. Cras a eros.\r\n\r\n<hr />\r\n\r\nNunc auctor bibendum eros. Maecenas porta accumsan mauris. Etiam enim enim, elementum sed, bibendum quis, rhoncus non, metus. Fusce neque dolor, adipiscing sed, consectetuer et, lacinia sit amet, quam. Suspendisse wisi quam, consectetuer in, blandit sed, suscipit eu, eros. Etiam ligula enim, tempor ut, blandit nec, mollis eu, lectus. Nam cursus. Vivamus iaculis. Aenean risus purus, pharetra in, blandit quis, gravida a, turpis. Donec nisl. Aenean eget mi.','Study: Sample posts 10% more interesting than real posts.','','inherit','open','open','','15-revision-v1','','','2014-05-29 14:55:03','2014-05-29 18:55:03','',15,'http://artsoulonline/uncategorized/15-revision-v1/',0,'revision','',0);
INSERT INTO `wp_posts` VALUES (35,1,'2014-05-29 14:58:09','2014-05-29 18:58:09','Lorem ipsum dolor sit amet, consectetuer adipiscing elit. <strong>Morbi commodo</strong>, ipsum sed pharetra gravida, orci magna rhoncus neque, id pulvinar odio lorem non turpis. Nullam sit amet enim. Suspendisse id velit vitae ligula volutpat condimentum. Aliquam erat volutpat. Sed quis velit. Nulla facilisi. Nulla libero. Vivamus pharetra posuere sapien. Nam consectetuer. Sed aliquam, nunc eget euismod ullamcorper, lectus nunc ullamcorper orci, fermentum bibendum enim nibh eget ipsum. Donec porttitor ligula eu dolor. Maecenas vitae nulla consequat libero cursus venenatis. Nam magna enim, accumsan eu, blandit sed, blandit a, eros.\r\n\r\n<img class=\"alignleft size-thumbnail wp-image-28\" src=\"http://dougsmacbookpro.local:5757/wp-content/uploads/2014/05/sponsor-soworthloving-150x150.jpg\" alt=\"sponsor-soworthloving\" />Quisque facilisis erat a dui. Nam malesuada ornare dolor. <em>Cras gravida, diam sit amet rhoncus ornare, erat elit consectetuer erat, id egestas pede nibh eget odio.</em> Proin tincidunt, velit vel porta elementum, magna diam molestie sapien, non aliquet massa pede eu diam. Aliquam iaculis. Fusce et ipsum et nulla tristique facilisis. Donec eget sem sit amet ligula viverra gravida. Etiam vehicula urna vel turpis. <del>Suspendisse sagittis ante a urna</del>. Morbi a est quis orci consequat rutrum. Nullam egestas feugiat felis. Integer adipiscing semper ligula. Nunc molestie, nisl sit amet cursus convallis, sapien lectus pretium metus, vitae pretium enim wisi id lectus. Donec vestibulum. Etiam vel nibh. Nulla facilisi. Mauris pharetra. Donec augue. Fusce ultrices, neque id dignissim ultrices, tellus mauris dictum elit, vel lacinia enim metus eu nunc.\r\n\r\nProin at eros non eros adipiscing mollis. Donec semper turpis sed diam. <span style=\"text-decoration: underline;\">Sed consequat ligula nec tortor.</span> Integer eget sem. Ut vitae enim eu est vehicula gravida. Morbi ipsum ipsum, porta nec, tempor id, auctor vitae, purus. Pellentesque neque. Nulla luctus erat vitae libero. Integer nec enim. Phasellus aliquam enim et tortor. Quisque aliquet, quam elementum condimentum feugiat, tellus odio consectetuer wisi, vel nonummy sem neque in elit.\r\n<blockquote>\"Curabitur eleifend wisi iaculis ipsum. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. In non velit non ligula laoreet ultrices. Praesent ultricies facilisis nisl.\"</blockquote>\r\nVivamus luctus elit sit amet mi. Phasellus pellentesque, erat eget elementum volutpat, dolor nisl porta neque, vitae sodales ipsum nibh in ligula. Maecenas mattis pulvinar diam. Curabitur sed leo.\r\n\r\nNulla facilisi. In vel sem. Morbi id urna in diam dignissim feugiat. Proin molestie tortor eu velit. Aliquam erat volutpat. Nullam ultrices, diam tempus vulputate egestas, eros pede varius leo, sed imperdiet lectus est ornare odio. Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Proin consectetuer velit in dui. Phasellus wisi purus, interdum vitae, rutrum accumsan, viverra in, velit. Sed enim risus, congue non, tristique in, commodo eu, metus. Aenean tortor mi, imperdiet id, gravida eu, posuere eu, felis. Mauris sollicitudin, turpis in hendrerit sodales, lectus ipsum pellentesque ligula, sit amet scelerisque urna nibh ut arcu. Aliquam in lacus. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Nulla placerat aliquam wisi. Mauris viverra odio. Quisque fermentum pulvinar odio. Proin posuere est vitae ligula. Etiam euismod. Cras a eros.\r\n\r\n<hr />\r\n\r\nNunc auctor bibendum eros. Maecenas porta accumsan mauris. Etiam enim enim, elementum sed, bibendum quis, rhoncus non, metus. Fusce neque dolor, adipiscing sed, consectetuer et, lacinia sit amet, quam. Suspendisse wisi quam, consectetuer in, blandit sed, suscipit eu, eros. Etiam ligula enim, tempor ut, blandit nec, mollis eu, lectus. Nam cursus. Vivamus iaculis. Aenean risus purus, pharetra in, blandit quis, gravida a, turpis. Donec nisl. Aenean eget mi.','Study: Sample posts 10% more interesting than real posts.','','inherit','open','open','','15-revision-v1','','','2014-05-29 14:58:09','2014-05-29 18:58:09','',15,'http://artsoulonline/uncategorized/15-revision-v1/',0,'revision','',0);
INSERT INTO `wp_posts` VALUES (36,1,'2014-05-29 14:59:38','2014-05-29 18:59:38','Lorem ipsum dolor sit amet, consectetuer adipiscing elit. <strong>Morbi commodo</strong>, ipsum sed pharetra gravida, orci magna rhoncus neque, id pulvinar odio lorem non turpis. Nullam sit amet enim. Suspendisse id velit vitae ligula volutpat condimentum. Aliquam erat volutpat. Sed quis velit. Nulla facilisi. Nulla libero. Vivamus pharetra posuere sapien. Nam consectetuer. Sed aliquam, nunc eget euismod ullamcorper, lectus nunc ullamcorper orci, fermentum bibendum enim nibh eget ipsum. Donec porttitor ligula eu dolor. Maecenas vitae nulla consequat libero cursus venenatis. Nam magna enim, accumsan eu, blandit sed, blandit a, eros.\r\n\r\n<img class=\"aligncenter wp-image-28 size-thumbnail\" src=\"http://dougsmacbookpro.local:5757/wp-content/uploads/2014/05/sponsor-soworthloving-150x150.jpg\" alt=\"sponsor-soworthloving\" width=\"150\" height=\"150\" />Quisque facilisis erat a dui. Nam malesuada ornare dolor. <em>Cras gravida, diam sit amet rhoncus ornare, erat elit consectetuer erat, id egestas pede nibh eget odio.</em> Proin tincidunt, velit vel porta elementum, magna diam molestie sapien, non aliquet massa pede eu diam. Aliquam iaculis. Fusce et ipsum et nulla tristique facilisis. Donec eget sem sit amet ligula viverra gravida. Etiam vehicula urna vel turpis. <del>Suspendisse sagittis ante a urna</del>. Morbi a est quis orci consequat rutrum. Nullam egestas feugiat felis. Integer adipiscing semper ligula. Nunc molestie, nisl sit amet cursus convallis, sapien lectus pretium metus, vitae pretium enim wisi id lectus. Donec vestibulum. Etiam vel nibh. Nulla facilisi. Mauris pharetra. Donec augue. Fusce ultrices, neque id dignissim ultrices, tellus mauris dictum elit, vel lacinia enim metus eu nunc.\r\n\r\nProin at eros non eros adipiscing mollis. Donec semper turpis sed diam. <span style=\"text-decoration: underline;\">Sed consequat ligula nec tortor.</span> Integer eget sem. Ut vitae enim eu est vehicula gravida. Morbi ipsum ipsum, porta nec, tempor id, auctor vitae, purus. Pellentesque neque. Nulla luctus erat vitae libero. Integer nec enim. Phasellus aliquam enim et tortor. Quisque aliquet, quam elementum condimentum feugiat, tellus odio consectetuer wisi, vel nonummy sem neque in elit.\r\n<blockquote>\"Curabitur eleifend wisi iaculis ipsum. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. In non velit non ligula laoreet ultrices. Praesent ultricies facilisis nisl.\"</blockquote>\r\nVivamus luctus elit sit amet mi. Phasellus pellentesque, erat eget elementum volutpat, dolor nisl porta neque, vitae sodales ipsum nibh in ligula. Maecenas mattis pulvinar diam. Curabitur sed leo.\r\n\r\nNulla facilisi. In vel sem. Morbi id urna in diam dignissim feugiat. Proin molestie tortor eu velit. Aliquam erat volutpat. Nullam ultrices, diam tempus vulputate egestas, eros pede varius leo, sed imperdiet lectus est ornare odio. Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Proin consectetuer velit in dui. Phasellus wisi purus, interdum vitae, rutrum accumsan, viverra in, velit. Sed enim risus, congue non, tristique in, commodo eu, metus. Aenean tortor mi, imperdiet id, gravida eu, posuere eu, felis. Mauris sollicitudin, turpis in hendrerit sodales, lectus ipsum pellentesque ligula, sit amet scelerisque urna nibh ut arcu. Aliquam in lacus. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Nulla placerat aliquam wisi. Mauris viverra odio. Quisque fermentum pulvinar odio. Proin posuere est vitae ligula. Etiam euismod. Cras a eros.\r\n\r\n<hr />\r\n\r\nNunc auctor bibendum eros. Maecenas porta accumsan mauris. Etiam enim enim, elementum sed, bibendum quis, rhoncus non, metus. Fusce neque dolor, adipiscing sed, consectetuer et, lacinia sit amet, quam. Suspendisse wisi quam, consectetuer in, blandit sed, suscipit eu, eros. Etiam ligula enim, tempor ut, blandit nec, mollis eu, lectus. Nam cursus. Vivamus iaculis. Aenean risus purus, pharetra in, blandit quis, gravida a, turpis. Donec nisl. Aenean eget mi.','Study: Sample posts 10% more interesting than real posts.','','inherit','open','open','','15-revision-v1','','','2014-05-29 14:59:38','2014-05-29 18:59:38','',15,'http://artsoulonline/uncategorized/15-revision-v1/',0,'revision','',0);
INSERT INTO `wp_posts` VALUES (37,1,'2014-05-29 15:01:20','2014-05-29 19:01:20','Lorem ipsum dolor sit amet, consectetuer adipiscing elit. <strong>Morbi commodo</strong>, ipsum sed pharetra gravida, orci magna rhoncus neque, id pulvinar odio lorem non turpis. Nullam sit amet enim. Suspendisse id velit vitae ligula volutpat condimentum. Aliquam erat volutpat. Sed quis velit. Nulla facilisi. Nulla libero. Vivamus pharetra posuere sapien. Nam consectetuer. Sed aliquam, nunc eget euismod ullamcorper, lectus nunc ullamcorper orci, fermentum bibendum enim nibh eget ipsum. Donec porttitor ligula eu dolor. Maecenas vitae nulla consequat libero cursus venenatis. Nam magna enim, accumsan eu, blandit sed, blandit a, eros.\r\n\r\n<img class=\"alignright wp-image-28 size-thumbnail\" src=\"http://dougsmacbookpro.local:5757/wp-content/uploads/2014/05/sponsor-soworthloving-150x150.jpg\" alt=\"sponsor-soworthloving\" width=\"150\" height=\"150\" />Quisque facilisis erat a dui. Nam malesuada ornare dolor. <em>Cras gravida, diam sit amet rhoncus ornare, erat elit consectetuer erat, id egestas pede nibh eget odio.</em> Proin tincidunt, velit vel porta elementum, magna diam molestie sapien, non aliquet massa pede eu diam. Aliquam iaculis. Fusce et ipsum et nulla tristique facilisis. Donec eget sem sit amet ligula viverra gravida. Etiam vehicula urna vel turpis. <del>Suspendisse sagittis ante a urna</del>. Morbi a est quis orci consequat rutrum. Nullam egestas feugiat felis. Integer adipiscing semper ligula. Nunc molestie, nisl sit amet cursus convallis, sapien lectus pretium metus, vitae pretium enim wisi id lectus. Donec vestibulum. Etiam vel nibh. Nulla facilisi. Mauris pharetra. Donec augue. Fusce ultrices, neque id dignissim ultrices, tellus mauris dictum elit, vel lacinia enim metus eu nunc.\r\n\r\nProin at eros non eros adipiscing mollis. Donec semper turpis sed diam. <span style=\"text-decoration: underline;\">Sed consequat ligula nec tortor.</span> Integer eget sem. Ut vitae enim eu est vehicula gravida. Morbi ipsum ipsum, porta nec, tempor id, auctor vitae, purus. Pellentesque neque. Nulla luctus erat vitae libero. Integer nec enim. Phasellus aliquam enim et tortor. Quisque aliquet, quam elementum condimentum feugiat, tellus odio consectetuer wisi, vel nonummy sem neque in elit.\r\n<blockquote>\"Curabitur eleifend wisi iaculis ipsum. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. In non velit non ligula laoreet ultrices. Praesent ultricies facilisis nisl.\"</blockquote>\r\nVivamus luctus elit sit amet mi. Phasellus pellentesque, erat eget elementum volutpat, dolor nisl porta neque, vitae sodales ipsum nibh in ligula. Maecenas mattis pulvinar diam. Curabitur sed leo.\r\n\r\nNulla facilisi. In vel sem. Morbi id urna in diam dignissim feugiat. Proin molestie tortor eu velit. Aliquam erat volutpat. Nullam ultrices, diam tempus vulputate egestas, eros pede varius leo, sed imperdiet lectus est ornare odio. Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Proin consectetuer velit in dui. Phasellus wisi purus, interdum vitae, rutrum accumsan, viverra in, velit. Sed enim risus, congue non, tristique in, commodo eu, metus. Aenean tortor mi, imperdiet id, gravida eu, posuere eu, felis. Mauris sollicitudin, turpis in hendrerit sodales, lectus ipsum pellentesque ligula, sit amet scelerisque urna nibh ut arcu. Aliquam in lacus. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Nulla placerat aliquam wisi. Mauris viverra odio. Quisque fermentum pulvinar odio. Proin posuere est vitae ligula. Etiam euismod. Cras a eros.\r\n\r\n<hr />\r\n\r\nNunc auctor bibendum eros. Maecenas porta accumsan mauris. Etiam enim enim, elementum sed, bibendum quis, rhoncus non, metus. Fusce neque dolor, adipiscing sed, consectetuer et, lacinia sit amet, quam. Suspendisse wisi quam, consectetuer in, blandit sed, suscipit eu, eros. Etiam ligula enim, tempor ut, blandit nec, mollis eu, lectus. Nam cursus. Vivamus iaculis. Aenean risus purus, pharetra in, blandit quis, gravida a, turpis. Donec nisl. Aenean eget mi.','Study: Sample posts 10% more interesting than real posts.','','inherit','open','open','','15-revision-v1','','','2014-05-29 15:01:20','2014-05-29 19:01:20','',15,'http://artsoulonline/uncategorized/15-revision-v1/',0,'revision','',0);
INSERT INTO `wp_posts` VALUES (38,1,'2014-05-29 15:45:43','2014-05-29 19:45:43','Welcome to WordPress. This is your first post. Edit or delete it, then start blogging!','Hello world!','','inherit','open','open','','1-revision-v1','','','2014-05-29 15:45:43','2014-05-29 19:45:43','',1,'http://artsoulonline/uncategorized/1-revision-v1/',0,'revision','',0);
INSERT INTO `wp_posts` VALUES (39,1,'2014-05-29 15:45:57','2014-05-29 19:45:57','Welcome to WordPress. This is your first post. Edit or delete it, then start blogging! Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Morbi commodo, ipsum sed pharetra gravida, orci magna rhoncus neque, id pulvinar odio lorem non turpis. Nullam sit amet enim. Suspendisse id velit vitae ligula volutpat condimentum. Aliquam erat volutpat. Sed quis velit. Nulla facilisi. Nulla libero. Vivamus pharetra posuere sapien. Nam consectetuer. Sed aliquam, nunc eget euismod ullamcorper, lectus nunc ullamcorper orci, fermentum bibendum enim nibh eget ipsum. Donec porttitor ligula eu dolor. Maecenas vitae nulla consequat libero cursus venenatis. Nam magna enim, accumsan eu, blandit sed, blandit a, eros.\r\n\r\nQuisque facilisis erat a dui. Nam malesuada ornare dolor. Cras gravida, diam sit amet rhoncus ornare, erat elit consectetuer erat, id egestas pede nibh eget odio. Proin tincidunt, velit vel porta elementum, magna diam molestie sapien, non aliquet massa pede eu diam. Aliquam iaculis. Fusce et ipsum et nulla tristique facilisis. Donec eget sem sit amet ligula viverra gravida. Etiam vehicula urna vel turpis. Suspendisse sagittis ante a urna. Morbi a est quis orci consequat rutrum. Nullam egestas feugiat felis. Integer adipiscing semper ligula. Nunc molestie, nisl sit amet cursus convallis, sapien lectus pretium metus, vitae pretium enim wisi id lectus. Donec vestibulum. Etiam vel nibh. Nulla facilisi. Mauris pharetra. Donec augue. Fusce ultrices, neque id dignissim ultrices, tellus mauris dictum elit, vel lacinia enim metus eu nunc.\r\n','Hello world!','','inherit','open','open','','1-revision-v1','','','2014-05-29 15:45:57','2014-05-29 19:45:57','',1,'http://artsoulonline/uncategorized/1-revision-v1/',0,'revision','',0);
INSERT INTO `wp_posts` VALUES (40,1,'2014-05-29 23:01:39','2014-05-30 03:01:39','Lorem ipsum dolor sit amet, consectetuer adipiscing elit. <strong>Morbi commodo</strong>, ipsum sed pharetra gravida, orci magna rhoncus neque, id pulvinar odio lorem non turpis. Nullam sit amet enim. Suspendisse id velit vitae ligula volutpat condimentum. Aliquam erat volutpat. Sed quis velit. Nulla facilisi. Nulla libero. Vivamus pharetra posuere sapien. Nam consectetuer. Sed aliquam, nunc eget euismod ullamcorper, lectus nunc ullamcorper orci, fermentum bibendum enim nibh eget ipsum. Donec porttitor ligula eu dolor. Maecenas vitae nulla consequat libero cursus venenatis. Nam magna enim, accumsan eu, blandit sed, blandit a, eros.\r\n\r\n<img class=\"alignright wp-image-28 size-thumbnail\" src=\"http://dougsmacbookpro.local:5757/wp-content/uploads/2014/05/sponsor-soworthloving-150x150.jpg\" alt=\"sponsor-soworthloving\" width=\"150\" height=\"150\" />Quisque facilisis erat a dui. Nam malesuada ornare dolor. <em>Cras gravida, diam sit amet rhoncus ornare, erat elit consectetuer erat, id egestas pede nibh eget odio.</em> Proin tincidunt, velit vel porta elementum, magna diam molestie sapien, non aliquet massa pede eu diam. Aliquam iaculis. Fusce et ipsum et nulla tristique facilisis. Donec eget sem sit amet ligula viverra gravida. Etiam vehicula urna vel turpis. <del>Suspendisse sagittis ante a urna</del>. Morbi a est quis orci consequat rutrum. Nullam egestas feugiat felis. Integer adipiscing semper ligula. Nunc molestie, nisl sit amet cursus convallis, sapien lectus pretium metus, vitae pretium enim wisi id lectus. Donec vestibulum. Etiam vel nibh. Nulla facilisi. Mauris pharetra. Donec augue. Fusce ultrices, neque id dignissim ultrices, tellus mauris dictum elit, vel lacinia enim metus eu nunc.\r\n\r\nProin at eros non eros adipiscing mollis. Donec semper turpis sed diam. <span style=\"text-decoration: underline;\">Sed consequat ligula nec tortor.</span> Integer eget sem. Ut vitae enim eu est vehicula gravida. Morbi ipsum ipsum, porta nec, tempor id, auctor vitae, purus. Pellentesque neque. Nulla luctus erat vitae libero. Integer nec enim. Phasellus aliquam enim et tortor. Quisque aliquet, quam elementum condimentum feugiat, tellus odio consectetuer wisi, vel nonummy sem neque in elit.\r\n<blockquote>\"Curabitur eleifend wisi iaculis ipsum. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. In non velit non ligula laoreet ultrices. Praesent ultricies facilisis nisl.\"</blockquote>\r\nVivamus luctus elit sit amet mi. Phasellus pellentesque, erat eget elementum volutpat, dolor nisl porta neque, vitae sodales ipsum nibh in ligula. Maecenas mattis pulvinar diam. Curabitur sed leo.\r\n\r\nNulla facilisi. In vel sem. Morbi id urna in diam dignissim feugiat. Proin molestie tortor eu velit. Aliquam erat volutpat. Nullam ultrices, diam tempus vulputate egestas, eros pede varius leo, sed imperdiet lectus est ornare odio.\r\n<ul>\r\n	<li>Lorem ipsum dolor sit amet.</li>\r\n	<li>Consectetuer adipiscing elit.\r\n<ol>\r\n	<li>Numbered list</li>\r\n	<li>inside a</li>\r\n	<li>bulleted list</li>\r\n</ol>\r\n</li>\r\n	<li>Proin consectetuer velit in dui.</li>\r\n	<li>Phasellus wisi purus, interdum vitae.</li>\r\n</ul>\r\nRutrum accumsan, viverra in, velit. Sed enim risus, congue non, tristique in, commodo eu, metus. Aenean tortor mi, imperdiet id, gravida eu, posuere eu, felis.\r\n<ol>\r\n	<li>Mauris sollicitudin, turpis in.</li>\r\n	<li>Hendrerit sodales lectus.</li>\r\n	<li>Ipsum pellentesque ligula.\r\n<ul>\r\n	<li>Bulleted list</li>\r\n	<li>inside a</li>\r\n	<li>numbered list</li>\r\n</ul>\r\n</li>\r\n	<li>Sit amet scelerisque urna.</li>\r\n</ol>\r\nNibh ut arcu. Aliquam in lacus. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Nulla placerat aliquam wisi. Mauris viverra odio. Quisque fermentum pulvinar odio. Proin posuere est vitae ligula. Etiam euismod. Cras a eros.\r\n\r\n<hr />\r\n\r\nNunc auctor bibendum eros. Maecenas porta accumsan mauris. Etiam enim enim, elementum sed, bibendum quis, rhoncus non, metus. Fusce neque dolor, adipiscing sed, consectetuer et, lacinia sit amet, quam. Suspendisse wisi quam, consectetuer in, blandit sed, suscipit eu, eros. Etiam ligula enim, tempor ut, blandit nec, mollis eu, lectus. Nam cursus. Vivamus iaculis. Aenean risus purus, pharetra in, blandit quis, gravida a, turpis. Donec nisl. Aenean eget mi.','Study: Sample posts 10% more interesting than real posts.','','inherit','open','open','','15-revision-v1','','','2014-05-29 23:01:39','2014-05-30 03:01:39','',15,'http://artsoulonline/uncategorized/15-revision-v1/',0,'revision','',0);
INSERT INTO `wp_posts` VALUES (41,2,'2014-05-29 23:06:53','2014-05-30 03:06:53','Proin at eros non eros adipiscing mollis. Donec semper turpis sed diam. Sed consequat ligula nec tortor. Integer eget sem. Ut vitae enim eu est vehicula gravida. Morbi ipsum ipsum, porta nec, tempor id, auctor vitae, purus. Pellentesque neque. Nulla luctus erat vitae libero. Integer nec enim. Phasellus aliquam enim et tortor. Quisque aliquet, quam elementum condimentum feugiat, tellus odio consectetuer wisi, vel nonummy sem neque in elit. Curabitur eleifend wisi iaculis ipsum. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. In non velit non ligula laoreet ultrices. Praesent ultricies facilisis nisl. Vivamus luctus elit sit amet mi.\r\n\r\nLorem ipsum dolor sit amet, consectetuer adipiscing elit. Morbi commodo, ipsum sed pharetra gravida, orci magna rhoncus neque, id pulvinar odio lorem non turpis. Nullam sit amet enim. Suspendisse id velit vitae ligula volutpat condimentum. Aliquam erat volutpat. Sed quis velit. Nulla facilisi. Nulla libero. Vivamus pharetra posuere sapien. Nam consectetuer. Sed aliquam, nunc eget euismod ullamcorper, lectus nunc ullamcorper orci, fermentum bibendum enim nibh eget ipsum. Donec porttitor ligula eu dolor. Maecenas vitae nulla consequat libero cursus venenatis. Nam magna enim, accumsan eu, blandit sed, blandit a, eros.\r\n\r\nQuisque facilisis erat a dui. Nam malesuada ornare dolor. Cras gravida, diam sit amet rhoncus ornare, erat elit consectetuer erat, id egestas pede nibh eget odio. Proin tincidunt, velit vel porta elementum, magna diam molestie sapien, non aliquet massa pede eu diam. Aliquam iaculis. Fusce et ipsum et nulla tristique facilisis. Donec eget sem sit amet ligula viverra gravida. Etiam vehicula urna vel turpis. Suspendisse sagittis ante a urna. Morbi a est quis orci consequat rutrum. Nullam egestas feugiat felis. Integer adipiscing semper ligula. Nunc molestie, nisl sit amet cursus convallis, sapien lectus pretium metus, vitae pretium enim wisi id lectus. Donec vestibulum. Etiam vel nibh. Nulla facilisi. Mauris pharetra. Donec augue. Fusce ultrices, neque id dignissim ultrices, tellus mauris dictum elit, vel lacinia enim metus eu nunc.\r\n','New Album Release: Bird\'s Eye','','publish','open','open','','new-album-release-birds-eye','','','2014-05-30 11:51:23','2014-05-30 15:51:23','',0,'http://artsoulonline/?p=41',0,'post','',1);
INSERT INTO `wp_posts` VALUES (42,1,'2014-05-29 23:06:53','2014-05-30 03:06:53','Proin at eros non eros adipiscing mollis. Donec semper turpis sed diam. Sed consequat ligula nec tortor. Integer eget sem. Ut vitae enim eu est vehicula gravida. Morbi ipsum ipsum, porta nec, tempor id, auctor vitae, purus. Pellentesque neque. Nulla luctus erat vitae libero. Integer nec enim. Phasellus aliquam enim et tortor. Quisque aliquet, quam elementum condimentum feugiat, tellus odio consectetuer wisi, vel nonummy sem neque in elit. Curabitur eleifend wisi iaculis ipsum. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. In non velit non ligula laoreet ultrices. Praesent ultricies facilisis nisl. Vivamus luctus elit sit amet mi.\r\n\r\nLorem ipsum dolor sit amet, consectetuer adipiscing elit. Morbi commodo, ipsum sed pharetra gravida, orci magna rhoncus neque, id pulvinar odio lorem non turpis. Nullam sit amet enim. Suspendisse id velit vitae ligula volutpat condimentum. Aliquam erat volutpat. Sed quis velit. Nulla facilisi. Nulla libero. Vivamus pharetra posuere sapien. Nam consectetuer. Sed aliquam, nunc eget euismod ullamcorper, lectus nunc ullamcorper orci, fermentum bibendum enim nibh eget ipsum. Donec porttitor ligula eu dolor. Maecenas vitae nulla consequat libero cursus venenatis. Nam magna enim, accumsan eu, blandit sed, blandit a, eros.\r\n\r\nQuisque facilisis erat a dui. Nam malesuada ornare dolor. Cras gravida, diam sit amet rhoncus ornare, erat elit consectetuer erat, id egestas pede nibh eget odio. Proin tincidunt, velit vel porta elementum, magna diam molestie sapien, non aliquet massa pede eu diam. Aliquam iaculis. Fusce et ipsum et nulla tristique facilisis. Donec eget sem sit amet ligula viverra gravida. Etiam vehicula urna vel turpis. Suspendisse sagittis ante a urna. Morbi a est quis orci consequat rutrum. Nullam egestas feugiat felis. Integer adipiscing semper ligula. Nunc molestie, nisl sit amet cursus convallis, sapien lectus pretium metus, vitae pretium enim wisi id lectus. Donec vestibulum. Etiam vel nibh. Nulla facilisi. Mauris pharetra. Donec augue. Fusce ultrices, neque id dignissim ultrices, tellus mauris dictum elit, vel lacinia enim metus eu nunc.\r\n','New Album Release: Bird\'s Eye','','inherit','open','open','','41-revision-v1','','','2014-05-29 23:06:53','2014-05-30 03:06:53','',41,'http://artsoulonline/uncategorized/41-revision-v1/',0,'revision','',0);
INSERT INTO `wp_posts` VALUES (43,1,'2014-05-29 23:09:44','2014-05-30 03:09:44','','featured-image-dummy','','inherit','open','open','','featured-image-dummy','','','2014-05-29 23:09:44','2014-05-30 03:09:44','',41,'http://artsoulonline/wp-content/uploads/2014/05/featured-image-dummy.jpg',0,'attachment','image/jpeg',0);
INSERT INTO `wp_posts` VALUES (44,1,'2014-05-29 23:14:24','2014-05-30 03:14:24','This is an example page. It\'s different from a blog post because it will stay in one place and will show up in your site navigation (in most themes). Most people start with an About page that introduces them to potential site visitors. It might say something like this:\n\n<blockquote>Hi there! I\'m a bike messenger by day, aspiring actor by night, and this is my blog. I live in Los Angeles, have a great dog named Jack, and I like pi&#241;a coladas. (And gettin\' caught in the rain.)</blockquote>\n\n...or something like this:\n\n<blockquote>The XYZ Doohickey Company was founded in 1971, and has been providing quality doohickeys to the public ever since. Located in Gotham City, XYZ employs over 2,000 people and does all kinds of awesome things for the Gotham community.</blockquote>\n\nAs a new WordPress user, you should go to <a href=\"http://localhost/artsoul/wp-admin/\">your dashboard</a> to delete this page and create new pages for your content. Have fun!','Sample Page','','inherit','open','open','','2-revision-v1','','','2014-05-29 23:14:24','2014-05-30 03:14:24','',2,'http://artsoulonline/uncategorized/2-revision-v1/',0,'revision','',0);
INSERT INTO `wp_posts` VALUES (45,1,'2014-05-29 23:14:31','2014-05-30 03:14:31','','News','','publish','open','open','','news','','','2014-05-30 08:59:42','2014-05-30 12:59:42','',0,'http://artsoulonline/?page_id=45',0,'page','',0);
INSERT INTO `wp_posts` VALUES (46,1,'2014-05-29 23:14:31','2014-05-30 03:14:31','','News','','inherit','open','open','','45-revision-v1','','','2014-05-29 23:14:31','2014-05-30 03:14:31','',45,'http://artsoulonline/uncategorized/45-revision-v1/',0,'revision','',0);
INSERT INTO `wp_posts` VALUES (47,1,'2014-05-29 23:14:40','2014-05-30 03:14:40','','Watch','','publish','closed','closed','','watch','','','2014-06-06 15:32:54','2014-06-06 19:32:54','',0,'http://artsoulonline/?page_id=47',0,'page','',0);
INSERT INTO `wp_posts` VALUES (48,1,'2014-05-29 23:14:40','2014-05-30 03:14:40','','Watch','','inherit','open','open','','47-revision-v1','','','2014-05-29 23:14:40','2014-05-30 03:14:40','',47,'http://artsoulonline/uncategorized/47-revision-v1/',0,'revision','',0);
INSERT INTO `wp_posts` VALUES (49,1,'2014-05-29 23:15:07','2014-05-30 03:15:07',' ','','','publish','open','open','','49','','','2014-06-03 08:35:44','2014-06-03 12:35:44','',0,'http://artsoulonline/?p=49',4,'nav_menu_item','',0);
INSERT INTO `wp_posts` VALUES (50,1,'2014-05-29 23:15:06','2014-05-30 03:15:06',' ','','','publish','open','open','','50','','','2014-06-03 08:35:44','2014-06-03 12:35:44','',0,'http://artsoulonline/?p=50',2,'nav_menu_item','',0);
INSERT INTO `wp_posts` VALUES (51,1,'2014-05-30 00:04:28','2014-05-30 04:04:28',' ','','','publish','open','open','','51','','','2014-05-30 00:04:28','2014-05-30 04:04:28','',0,'http://artsoulonline/?p=51',1,'nav_menu_item','',0);
INSERT INTO `wp_posts` VALUES (52,1,'2014-05-30 00:04:29','2014-05-30 04:04:29',' ','','','publish','open','open','','52','','','2014-05-30 00:04:29','2014-05-30 04:04:29','',0,'http://artsoulonline/?p=52',2,'nav_menu_item','',0);
INSERT INTO `wp_posts` VALUES (53,1,'2014-05-30 00:04:45','2014-05-30 04:04:45',' ','','','publish','open','open','','53','','','2014-05-30 00:04:45','2014-05-30 04:04:45','',0,'http://artsoulonline/?p=53',1,'nav_menu_item','',0);
INSERT INTO `wp_posts` VALUES (54,1,'2014-05-30 00:04:45','2014-05-30 04:04:45',' ','','','publish','open','open','','54','','','2014-05-30 00:04:45','2014-05-30 04:04:45','',0,'http://artsoulonline/?p=54',2,'nav_menu_item','',0);
INSERT INTO `wp_posts` VALUES (56,1,'2014-05-30 01:39:46','2014-05-30 05:39:46','','ico-block-fb','','inherit','open','open','','ico-block-fb','','','2014-05-30 01:39:46','2014-05-30 05:39:46','',0,'http://artsoulonline/wp-content/uploads/2014/05/ico-block-fb.jpg',0,'attachment','image/jpeg',0);
INSERT INTO `wp_posts` VALUES (57,1,'2014-05-30 01:56:16','2014-05-30 05:56:16','','ico-block-g+','','inherit','open','open','','ico-block-g','','','2014-05-30 01:56:16','2014-05-30 05:56:16','',0,'http://artsoulonline/wp-content/uploads/2014/05/ico-block-g+.jpg',0,'attachment','image/jpeg',0);
INSERT INTO `wp_posts` VALUES (58,1,'2014-05-30 01:56:17','2014-05-30 05:56:17','','ico-block-vm','','inherit','open','open','','ico-block-vm','','','2014-05-30 01:56:17','2014-05-30 05:56:17','',0,'http://artsoulonline/wp-content/uploads/2014/05/ico-block-vm.jpg',0,'attachment','image/jpeg',0);
INSERT INTO `wp_posts` VALUES (59,1,'2014-05-30 01:56:18','2014-05-30 05:56:18','','ico-block-inst','','inherit','open','open','','ico-block-inst','','','2014-05-30 01:56:18','2014-05-30 05:56:18','',0,'http://artsoulonline/wp-content/uploads/2014/05/ico-block-inst.jpg',0,'attachment','image/jpeg',0);
INSERT INTO `wp_posts` VALUES (60,1,'2014-05-30 01:56:19','2014-05-30 05:56:19','','ico-block-flkr','','inherit','open','open','','ico-block-flkr','','','2014-05-30 01:56:19','2014-05-30 05:56:19','',0,'http://artsoulonline/wp-content/uploads/2014/05/ico-block-flkr.jpg',0,'attachment','image/jpeg',0);
INSERT INTO `wp_posts` VALUES (61,1,'2014-05-30 01:56:20','2014-05-30 05:56:20','','ico-block-tblr','','inherit','open','open','','ico-block-tblr','','','2014-05-30 01:56:20','2014-05-30 05:56:20','',0,'http://artsoulonline/wp-content/uploads/2014/05/ico-block-tblr.jpg',0,'attachment','image/jpeg',0);
INSERT INTO `wp_posts` VALUES (62,1,'2014-05-30 01:56:21','2014-05-30 05:56:21','','ico-block-tw','','inherit','open','open','','ico-block-tw','','','2014-05-30 01:56:21','2014-05-30 05:56:21','',0,'http://artsoulonline/wp-content/uploads/2014/05/ico-block-tw.jpg',0,'attachment','image/jpeg',0);
INSERT INTO `wp_posts` VALUES (65,1,'2014-05-30 08:43:27','2014-05-30 12:43:27','','sponsor-whatlife','','inherit','open','open','','sponsor-whatlife','','','2014-05-30 08:43:27','2014-05-30 12:43:27','',0,'http://artsoulonline/wp-content/uploads/2014/05/sponsor-whatlife.jpg',0,'attachment','image/jpeg',0);
INSERT INTO `wp_posts` VALUES (66,1,'2014-05-30 09:10:29','2014-05-30 13:10:29','Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Morbi commodo, ipsum sed pharetra gravida, orci magna rhoncus neque, id pulvinar odio lorem non turpis. Nullam sit amet enim. Suspendisse id velit vitae ligula volutpat condimentum. \r\n\r\nAliquam erat volutpat. Sed quis velit. Nulla facilisi. Nulla libero. Vivamus pharetra posuere sapien. Nam consectetuer. Sed aliquam, nunc eget euismod ullamcorper, lectus nunc ullamcorper orci, fermentum bibendum enim nibh eget ipsum. \r\n\r\nDonec porttitor ligula eu dolor. Maecenas vitae nulla consequat libero cursus venenatis. Nam magna enim, accumsan eu, blandit sed, blandit a, eros. Quisque facilisis erat a dui. Nam malesuada ornare dolor.\r\n','Rodents on turntables? Yes, please.','','publish','open','open','','video-its-totally-awesome','','','2014-05-30 09:41:47','2014-05-30 13:41:47','',0,'http://artsoulonline/?p=66',0,'post','',4);
INSERT INTO `wp_posts` VALUES (67,1,'2014-05-30 09:10:29','2014-05-30 13:10:29','Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Morbi commodo, ipsum sed pharetra gravida, orci magna rhoncus neque, id pulvinar odio lorem non turpis. Nullam sit amet enim. Suspendisse id velit vitae ligula volutpat condimentum. \r\n\r\nAliquam erat volutpat. Sed quis velit. Nulla facilisi. Nulla libero. Vivamus pharetra posuere sapien. Nam consectetuer. Sed aliquam, nunc eget euismod ullamcorper, lectus nunc ullamcorper orci, fermentum bibendum enim nibh eget ipsum. \r\n\r\nDonec porttitor ligula eu dolor. Maecenas vitae nulla consequat libero cursus venenatis. Nam magna enim, accumsan eu, blandit sed, blandit a, eros. Quisque facilisis erat a dui. Nam malesuada ornare dolor.\r\n','Video: It\'s totally awesome.','','inherit','open','open','','66-revision-v1','','','2014-05-30 09:10:29','2014-05-30 13:10:29','',66,'http://artsoulonline/uncategorized/66-revision-v1/',0,'revision','',0);
INSERT INTO `wp_posts` VALUES (68,1,'2014-05-30 09:22:12','2014-05-30 13:22:12','','Post Format: Video','','publish','closed','closed','','acf_post-format-video','','','2014-05-30 09:28:23','2014-05-30 13:28:23','',0,'http://artsoulonline/?post_type=acf&#038;p=68',0,'acf','',0);
INSERT INTO `wp_posts` VALUES (69,1,'2014-05-30 09:28:09','2014-05-30 13:28:09','Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Morbi commodo, ipsum sed pharetra gravida, orci magna rhoncus neque, id pulvinar odio lorem non turpis. Nullam sit amet enim. Suspendisse id velit vitae ligula volutpat condimentum. \r\n\r\nAliquam erat volutpat. Sed quis velit. Nulla facilisi. Nulla libero. Vivamus pharetra posuere sapien. Nam consectetuer. Sed aliquam, nunc eget euismod ullamcorper, lectus nunc ullamcorper orci, fermentum bibendum enim nibh eget ipsum. \r\n\r\nDonec porttitor ligula eu dolor. Maecenas vitae nulla consequat libero cursus venenatis. Nam magna enim, accumsan eu, blandit sed, blandit a, eros. Quisque facilisis erat a dui. Nam malesuada ornare dolor.\r\n','Video: It\'s totally awesome.','','inherit','open','open','','66-revision-v1','','','2014-05-30 09:28:09','2014-05-30 13:28:09','',66,'http://artsoulonline/uncategorized/66-revision-v1/',0,'revision','',0);
INSERT INTO `wp_posts` VALUES (70,1,'2014-05-30 09:39:44','2014-05-30 13:39:44','Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Morbi commodo, ipsum sed pharetra gravida, orci magna rhoncus neque, id pulvinar odio lorem non turpis. Nullam sit amet enim. Suspendisse id velit vitae ligula volutpat condimentum. \r\n\r\nAliquam erat volutpat. Sed quis velit. Nulla facilisi. Nulla libero. Vivamus pharetra posuere sapien. Nam consectetuer. Sed aliquam, nunc eget euismod ullamcorper, lectus nunc ullamcorper orci, fermentum bibendum enim nibh eget ipsum. \r\n\r\nDonec porttitor ligula eu dolor. Maecenas vitae nulla consequat libero cursus venenatis. Nam magna enim, accumsan eu, blandit sed, blandit a, eros. Quisque facilisis erat a dui. Nam malesuada ornare dolor.\r\n','Rodents on turntables? Yes, please.','','inherit','open','open','','66-revision-v1','','','2014-05-30 09:39:44','2014-05-30 13:39:44','',66,'http://artsoulonline/uncategorized/66-revision-v1/',0,'revision','',0);
INSERT INTO `wp_posts` VALUES (71,1,'2014-05-30 09:41:37','2014-05-30 13:41:37','','featured-image-rodent','','inherit','open','open','','featured-image-rodent','','','2014-05-30 09:41:37','2014-05-30 13:41:37','',66,'http://artsoulonline/wp-content/uploads/2014/05/featured-image-rodent.jpg',0,'attachment','image/jpeg',0);
INSERT INTO `wp_posts` VALUES (72,1,'2014-05-30 09:50:08','2014-05-30 13:50:08','','Listen','','publish','open','open','','listen','','','2014-05-30 10:09:15','2014-05-30 14:09:15','',0,'http://artsoulonline/?page_id=72',0,'page','',0);
INSERT INTO `wp_posts` VALUES (73,1,'2014-05-30 09:50:08','2014-05-30 13:50:08','','Listen','','inherit','open','open','','72-revision-v1','','','2014-05-30 09:50:08','2014-05-30 13:50:08','',72,'http://artsoulonline/uncategorized/72-revision-v1/',0,'revision','',0);
INSERT INTO `wp_posts` VALUES (74,1,'2014-05-30 09:50:47','2014-05-30 13:50:47',' ','','','publish','open','open','','74','','','2014-06-03 08:35:44','2014-06-03 12:35:44','',0,'http://artsoulonline/?p=74',5,'nav_menu_item','',0);
INSERT INTO `wp_posts` VALUES (75,1,'2014-05-30 10:01:03','2014-05-30 14:01:03','','Page: Listen','','publish','closed','closed','','acf_page-listen','','','2014-05-30 10:01:03','2014-05-30 14:01:03','',0,'http://artsoulonline/?post_type=acf&#038;p=75',0,'acf','',0);
INSERT INTO `wp_posts` VALUES (76,1,'2014-05-30 10:06:57','2014-05-30 14:06:57','','bg-radio','','inherit','open','open','','bg-radio','','','2014-05-30 10:06:57','2014-05-30 14:06:57','',72,'http://artsoulonline/wp-content/uploads/2014/05/bg-radio.jpg',0,'attachment','image/jpeg',0);
INSERT INTO `wp_posts` VALUES (77,1,'2014-05-30 10:09:10','2014-05-30 14:09:10','','header-radio','','inherit','open','open','','header-radio','','','2014-05-30 10:09:10','2014-05-30 14:09:10','',72,'http://artsoulonline/wp-content/uploads/2014/05/header-radio.png',0,'attachment','image/png',0);
INSERT INTO `wp_posts` VALUES (78,1,'2014-05-30 10:09:15','2014-05-30 14:09:15','','Listen','','inherit','open','open','','72-revision-v1','','','2014-05-30 10:09:15','2014-05-30 14:09:15','',72,'http://artsoulonline/uncategorized/72-revision-v1/',0,'revision','',0);
INSERT INTO `wp_posts` VALUES (79,1,'2014-05-30 10:18:04','2014-05-30 14:18:04','','sponsor-relevant-2','','inherit','open','open','','sponsor-relevant-2','','','2014-05-30 10:18:04','2014-05-30 14:18:04','',0,'http://artsoulonline/wp-content/uploads/2014/05/sponsor-relevant-2.jpg',0,'attachment','image/jpeg',0);
INSERT INTO `wp_posts` VALUES (80,1,'2014-05-30 10:19:05','2014-05-30 14:19:05','','sponsor-relevant-3','','inherit','open','open','','sponsor-relevant-3','','','2014-05-30 10:19:05','2014-05-30 14:19:05','',0,'http://artsoulonline/wp-content/uploads/2014/05/sponsor-relevant-3.jpg',0,'attachment','image/jpeg',0);
INSERT INTO `wp_posts` VALUES (81,1,'2014-05-30 11:36:24','2014-05-30 15:36:24','','artsoul','','trash','open','open','','artsoul','','','2014-05-30 11:51:15','2014-05-30 15:51:15','',0,'http://artsoulonline/?p=81',0,'post','',0);
INSERT INTO `wp_posts` VALUES (82,1,'2014-05-30 11:37:08','2014-05-30 15:37:08','','artsoul','','trash','open','open','','artsoul','','','2014-05-30 12:48:57','2014-05-30 16:48:57','',0,'http://artsoulonline/?page_id=82',0,'page','',0);
INSERT INTO `wp_posts` VALUES (83,1,'2014-05-30 11:37:08','2014-05-30 15:37:08','','artsoul','','inherit','open','open','','82-revision-v1','','','2014-05-30 11:37:08','2014-05-30 15:37:08','',82,'http://artsoulonline/artsoul/82-revision-v1/',0,'revision','',0);
INSERT INTO `wp_posts` VALUES (85,2,'2014-05-30 11:51:15','2014-05-30 15:51:15','','artsoul','','inherit','open','open','','81-revision-v1','','','2014-05-30 11:51:15','2014-05-30 15:51:15','',81,'http://artsoulonline/dkeeling/81-revision-v1/',0,'revision','',0);
INSERT INTO `wp_posts` VALUES (86,1,'2014-05-30 13:04:24','2014-05-30 17:04:24','','Discover','','publish','open','open','','discover','','','2014-05-30 13:04:46','2014-05-30 17:04:46','',0,'http://artsoulonline/?page_id=86',0,'page','',0);
INSERT INTO `wp_posts` VALUES (87,1,'2014-05-30 13:04:24','2014-05-30 17:04:24','','Discover','','inherit','open','open','','86-revision-v1','','','2014-05-30 13:04:24','2014-05-30 17:04:24','',86,'http://artsoulonline/artsoul/2014/05/30/86-revision-v1/',0,'revision','',0);
INSERT INTO `wp_posts` VALUES (88,1,'2014-05-30 13:05:11','2014-05-30 17:05:11',' ','','','publish','open','open','','88','','','2014-06-03 08:35:44','2014-06-03 12:35:44','',0,'http://artsoulonline/?p=88',3,'nav_menu_item','',0);
INSERT INTO `wp_posts` VALUES (89,2,'2014-05-30 14:47:36','2014-05-30 18:47:36','','All Sons &amp; Daughters','','publish','closed','closed','','all-sons-daughters','','','2014-06-04 13:50:59','2014-06-04 17:50:59','',0,'http://artsoulonline/?post_type=artist&#038;p=89',0,'artist','',0);
INSERT INTO `wp_posts` VALUES (90,1,'2014-05-30 14:42:54','2014-05-30 18:42:54','','Post Type: Artist','','publish','closed','closed','','acf_post-type-artist','','','2014-06-06 23:20:00','2014-06-07 03:20:00','',0,'http://artsoulonline/?post_type=acf&#038;p=90',0,'acf','',0);
INSERT INTO `wp_posts` VALUES (91,1,'2014-05-30 14:47:23','2014-05-30 18:47:23','','album-cover-allsonsanddaughterslive','','inherit','open','open','','album-cover-allsonsanddaughterslive','','','2014-05-30 14:47:23','2014-05-30 18:47:23','',89,'http://artsoulonline/wp-content/uploads/2014/05/album-cover-allsonsanddaughterslive.jpg',0,'attachment','image/jpeg',0);
INSERT INTO `wp_posts` VALUES (92,1,'2014-05-30 15:29:02','2014-05-30 19:29:02','','Andy Mineo','','publish','closed','closed','','andy-mineo','','','2014-06-07 00:27:58','2014-06-07 04:27:58','',0,'http://artsoulonline/?post_type=artist&#038;p=92',0,'artist','',0);
INSERT INTO `wp_posts` VALUES (95,2,'2014-05-30 15:58:40','2014-05-30 19:58:40','\"Turkey_Tune\".','Turkey_Tune','','inherit','open','open','','turkey_tune','','','2014-05-30 15:58:40','2014-05-30 19:58:40','',89,'http://artsoulonline/wp-content/uploads/2014/05/Turkey_Tune.mp3',0,'attachment','audio/mpeg',0);
INSERT INTO `wp_posts` VALUES (96,1,'2014-06-02 13:21:55','2014-06-02 17:21:55','','album-cover-andymineo','','inherit','open','open','','album-cover-andymineo','','','2014-06-02 13:21:55','2014-06-02 17:21:55','',92,'http://artsoulonline/wp-content/uploads/2014/05/album-cover-andymineo.jpg',0,'attachment','image/jpeg',0);
INSERT INTO `wp_posts` VALUES (97,1,'2014-06-02 13:24:07','2014-06-02 17:24:07','','Sounds Absurd','','publish','closed','closed','','sounds-absurd','','','2014-06-07 00:28:17','2014-06-07 04:28:17','',0,'http://artsoulonline/?post_type=artist&#038;p=97',0,'artist','',0);
INSERT INTO `wp_posts` VALUES (98,1,'2014-06-02 13:24:03','2014-06-02 17:24:03','','album-cover-soundsabsurd','','inherit','open','open','','album-cover-soundsabsurd','','','2014-06-02 13:24:03','2014-06-02 17:24:03','',97,'http://artsoulonline/wp-content/uploads/2014/06/album-cover-soundsabsurd.jpg',0,'attachment','image/jpeg',0);
INSERT INTO `wp_posts` VALUES (99,1,'2014-06-02 13:27:43','2014-06-02 17:27:43','','Kutless','','publish','closed','closed','','kutless','','','2014-06-04 13:16:35','2014-06-04 17:16:35','',0,'http://artsoulonline/?post_type=artist&#038;p=99',0,'artist','',0);
INSERT INTO `wp_posts` VALUES (100,1,'2014-06-02 13:27:30','2014-06-02 17:27:30','','album-cover-kutless','','inherit','open','open','','album-cover-kutless','','','2014-06-02 13:27:30','2014-06-02 17:27:30','',99,'http://artsoulonline/wp-content/uploads/2014/06/album-cover-kutless.jpg',0,'attachment','image/jpeg',0);
INSERT INTO `wp_posts` VALUES (101,2,'2014-06-02 13:45:47','2014-06-02 17:45:47','','Deraj','','publish','closed','closed','','deraj','','','2014-06-04 10:39:42','2014-06-04 14:39:42','',0,'http://artsoulonline/?post_type=artist&#038;p=101',0,'artist','',0);
INSERT INTO `wp_posts` VALUES (102,1,'2014-06-02 13:45:39','2014-06-02 17:45:39','','album-cover-deraj','','inherit','open','open','','album-cover-deraj','','','2014-06-02 13:45:39','2014-06-02 17:45:39','',101,'http://artsoulonline/wp-content/uploads/2014/06/album-cover-deraj.jpg',0,'attachment','image/jpeg',0);
INSERT INTO `wp_posts` VALUES (103,1,'2014-06-02 13:56:37','2014-06-02 17:56:37','','profile-deraj','','inherit','open','open','','profile-deraj','','','2014-06-02 13:56:37','2014-06-02 17:56:37','',101,'http://artsoulonline/wp-content/uploads/2014/06/profile-deraj.jpg',0,'attachment','image/jpeg',0);
INSERT INTO `wp_posts` VALUES (104,1,'2014-06-02 14:03:32','2014-06-02 18:03:32','','default-artist','','inherit','open','open','','default-artist','','','2014-06-02 14:03:32','2014-06-02 18:03:32','',0,'http://artsoulonline/wp-content/uploads/2014/06/default-artist.jpg',0,'attachment','image/jpeg',0);
INSERT INTO `wp_posts` VALUES (105,1,'2014-06-02 14:58:23','2014-06-02 18:58:23','','default-artist-profile','','inherit','open','open','','default-artist-profile','','','2014-06-02 14:58:23','2014-06-02 18:58:23','',0,'http://artsoulonline/wp-content/uploads/2014/06/default-artist-profile.jpg',0,'attachment','image/jpeg',0);
INSERT INTO `wp_posts` VALUES (106,2,'2014-06-02 15:20:22','0000-00-00 00:00:00','','Some Crazy Band','','pending','closed','closed','','','','','2014-06-02 15:20:22','2014-06-02 19:20:22','',0,'http://artsoulonline/?post_type=artist&#038;p=106',0,'artist','',0);
INSERT INTO `wp_posts` VALUES (107,1,'2014-06-02 15:32:23','2014-06-02 19:32:23','','album-cover-deraj-nofear','','inherit','open','open','','album-cover-deraj-nofear','','','2014-06-02 15:32:23','2014-06-02 19:32:23','',101,'http://artsoulonline/wp-content/uploads/2014/06/album-cover-deraj-nofear.jpg',0,'attachment','image/jpeg',0);
INSERT INTO `wp_posts` VALUES (108,1,'2014-06-02 15:48:29','2014-06-02 19:48:29','','default-album-cover','','inherit','open','open','','default-album-cover','','','2014-06-02 15:48:29','2014-06-02 19:48:29','',0,'http://artsoulonline/wp-content/uploads/2014/06/default-album-cover.jpg',0,'attachment','image/jpeg',0);
INSERT INTO `wp_posts` VALUES (109,1,'2014-06-03 08:34:24','2014-06-03 12:34:24','','Store','','publish','open','open','','store','','','2014-06-03 09:51:09','2014-06-03 13:51:09','',0,'http://artsoulonline/?page_id=109',0,'page','',0);
INSERT INTO `wp_posts` VALUES (110,1,'2014-06-03 08:34:24','2014-06-03 12:34:24','','Store','','inherit','open','open','','109-revision-v1','','','2014-06-03 08:34:24','2014-06-03 12:34:24','',109,'http://artsoulonline/artsoul/2014/06/03/109-revision-v1/',0,'revision','',0);
INSERT INTO `wp_posts` VALUES (111,1,'2014-06-03 08:35:44','2014-06-03 12:35:44',' ','','','publish','open','open','','111','','','2014-06-03 08:35:44','2014-06-03 12:35:44','',0,'http://artsoulonline/?p=111',6,'nav_menu_item','',0);
INSERT INTO `wp_posts` VALUES (112,1,'2014-06-03 08:41:09','0000-00-00 00:00:00','','Auto Draft','','auto-draft','open','open','','','','','2014-06-03 08:41:09','0000-00-00 00:00:00','',0,'http://artsoulonline/?post_type=acf&p=112',0,'acf','',0);
INSERT INTO `wp_posts` VALUES (113,1,'2014-06-03 08:41:54','0000-00-00 00:00:00','','Auto Draft','','auto-draft','open','open','','','','','2014-06-03 08:41:54','0000-00-00 00:00:00','',0,'http://artsoulonline/?post_type=acf&p=113',0,'acf','',0);
INSERT INTO `wp_posts` VALUES (114,1,'2014-06-03 08:43:29','0000-00-00 00:00:00','','Auto Draft','','auto-draft','open','open','','','','','2014-06-03 08:43:29','0000-00-00 00:00:00','',0,'http://artsoulonline/?post_type=acf&p=114',0,'acf','',0);
INSERT INTO `wp_posts` VALUES (115,1,'2014-06-03 08:44:05','2014-06-03 12:44:05','','Page: Store','','publish','closed','closed','','acf_page-store','','','2014-06-03 09:12:46','2014-06-03 13:12:46','',0,'http://artsoulonline/?post_type=acf&#038;p=115',0,'acf','',0);
INSERT INTO `wp_posts` VALUES (116,1,'2014-06-03 08:44:50','2014-06-03 12:44:50','Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Morbi commodo, ipsum sed pharetra gravida, orci magna rhoncus neque, id pulvinar odio lorem non turpis. Nullam sit amet enim. Suspendisse id velit vitae ligula volutpat condimentum. Aliquam erat volutpat. Sed quis velit. Nulla facilisi. Nulla libero. Vivamus pharetra posuere sapien. Nam consectetuer. Sed aliquam, nunc eget euismod ullamcorper, lectus nunc ullamcorper orci, fermentum bibendum enim nibh eget ipsum. Donec porttitor ligula eu dolor. Maecenas vitae nulla consequat libero cursus venenatis.\r\n\r\nLorem ipsum dolor sit amet, consectetuer adipiscing elit. Morbi commodo, ipsum sed pharetra gravida, orci magna rhoncus neque, id pulvinar odio lorem non turpis. Nullam sit amet enim. Suspendisse id velit vitae ligula volutpat condimentum. Aliquam erat volutpat. Sed quis velit. Nulla facilisi. Nulla libero. Vivamus pharetra posuere sapien. Nam consectetuer. Sed aliquam, nunc eget euismod ullamcorper, lectus nunc ullamcorper orci, fermentum bibendum enim nibh eget ipsum. Donec porttitor ligula eu dolor. Maecenas vitae nulla consequat libero cursus venenatis.','Test Page','','publish','open','open','','test-page','','','2014-06-03 08:47:19','2014-06-03 12:47:19','',0,'http://artsoulonline/?page_id=116',0,'page','',0);
INSERT INTO `wp_posts` VALUES (117,1,'2014-06-03 08:44:50','2014-06-03 12:44:50','Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Morbi commodo, ipsum sed pharetra gravida, orci magna rhoncus neque, id pulvinar odio lorem non turpis. Nullam sit amet enim. Suspendisse id velit vitae ligula volutpat condimentum. Aliquam erat volutpat. Sed quis velit. Nulla facilisi. Nulla libero. Vivamus pharetra posuere sapien. Nam consectetuer. Sed aliquam, nunc eget euismod ullamcorper, lectus nunc ullamcorper orci, fermentum bibendum enim nibh eget ipsum. Donec porttitor ligula eu dolor. Maecenas vitae nulla consequat libero cursus venenatis.\r\n\r\nLorem ipsum dolor sit amet, consectetuer adipiscing elit. Morbi commodo, ipsum sed pharetra gravida, orci magna rhoncus neque, id pulvinar odio lorem non turpis. Nullam sit amet enim. Suspendisse id velit vitae ligula volutpat condimentum. Aliquam erat volutpat. Sed quis velit. Nulla facilisi. Nulla libero. Vivamus pharetra posuere sapien. Nam consectetuer. Sed aliquam, nunc eget euismod ullamcorper, lectus nunc ullamcorper orci, fermentum bibendum enim nibh eget ipsum. Donec porttitor ligula eu dolor. Maecenas vitae nulla consequat libero cursus venenatis.','Test Page','','inherit','open','open','','116-revision-v1','','','2014-06-03 08:44:50','2014-06-03 12:44:50','',116,'http://artsoulonline/artsoul/2014/06/03/116-revision-v1/',0,'revision','',0);
INSERT INTO `wp_posts` VALUES (118,1,'2014-06-03 09:16:36','2014-06-03 13:16:36','','product-album','','inherit','open','open','','product-album','','','2014-06-03 09:16:36','2014-06-03 13:16:36','',109,'http://artsoulonline/wp-content/uploads/2014/06/product-album.jpg',0,'attachment','image/jpeg',0);
INSERT INTO `wp_posts` VALUES (119,1,'2014-06-03 09:18:23','2014-06-03 13:18:23','','Store','','inherit','open','open','','109-revision-v1','','','2014-06-03 09:18:23','2014-06-03 13:18:23','',109,'http://artsoulonline/artsoul/2014/06/03/109-revision-v1/',0,'revision','',0);
INSERT INTO `wp_posts` VALUES (120,1,'2014-06-03 09:51:09','2014-06-03 13:51:09','','Store','','inherit','open','open','','109-revision-v1','','','2014-06-03 09:51:09','2014-06-03 13:51:09','',109,'http://artsoulonline/artsoul/2014/06/03/109-revision-v1/',0,'revision','',0);
INSERT INTO `wp_posts` VALUES (225,1,'2013-08-06 17:23:03','2013-08-06 17:23:03','','SEO: Sitewide','','publish','closed','closed','','acf_seo-sitewide','','','2014-06-04 14:03:30','2014-06-04 18:03:30','',0,'http://localhost/tmd_testing/?post_type=acf&#038;p=6',2,'acf','',0);
INSERT INTO `wp_posts` VALUES (750,1,'2013-08-06 17:09:28','2013-08-06 17:09:28','','SEO: Pages','','publish','closed','closed','','acf_seo-pages','','','2013-08-06 17:09:28','2013-08-06 17:09:28','',0,'http://localhost/tmd_testing/?post_type=acf&amp;p=4',20,'acf','',0);
INSERT INTO `wp_posts` VALUES (751,1,'2014-06-04 14:49:15','0000-00-00 00:00:00','','Auto Draft','','auto-draft','open','open','','','','','2014-06-04 14:49:15','0000-00-00 00:00:00','',0,'http://artsoulonline/?p=751',0,'post','',0);
INSERT INTO `wp_posts` VALUES (752,1,'2014-06-06 13:19:12','2014-06-06 17:19:12','','Page: Front Page','','publish','closed','closed','','acf_page-front-page','','','2014-06-06 15:08:46','2014-06-06 19:08:46','',0,'http://artsoulonline/?post_type=acf&#038;p=752',0,'acf','',0);
INSERT INTO `wp_posts` VALUES (754,1,'2014-06-06 13:24:54','2014-06-06 17:24:54','','Home','','inherit','open','open','','12-revision-v1','','','2014-06-06 13:24:54','2014-06-06 17:24:54','',12,'http://artsoulonline/uncategorized/12-revision-v1/',0,'revision','',0);
INSERT INTO `wp_posts` VALUES (755,1,'2014-06-06 13:35:25','2014-06-06 17:35:25','','slide-andymineo','','inherit','open','open','','slide-andymineo','','','2014-06-06 13:35:25','2014-06-06 17:35:25','',12,'http://artsoulonline/wp-content/uploads/2014/05/slide-andymineo.jpg',0,'attachment','image/jpeg',0);
INSERT INTO `wp_posts` VALUES (756,1,'2014-06-06 13:35:31','2014-06-06 17:35:31','','Home','','inherit','open','open','','12-revision-v1','','','2014-06-06 13:35:31','2014-06-06 17:35:31','',12,'http://artsoulonline/uncategorized/12-revision-v1/',0,'revision','',0);
INSERT INTO `wp_posts` VALUES (757,1,'2014-06-06 13:42:53','2014-06-06 17:42:53','','slide-test','','inherit','open','open','','slide-test','','','2014-06-06 13:42:53','2014-06-06 17:42:53','',12,'http://artsoulonline/wp-content/uploads/2014/05/slide-test.jpg',0,'attachment','image/jpeg',0);
INSERT INTO `wp_posts` VALUES (758,1,'2014-06-06 13:43:28','2014-06-06 17:43:28','','Home','','inherit','open','open','','12-revision-v1','','','2014-06-06 13:43:28','2014-06-06 17:43:28','',12,'http://artsoulonline/uncategorized/12-revision-v1/',0,'revision','',0);
INSERT INTO `wp_posts` VALUES (759,1,'2014-06-06 13:48:45','2014-06-06 17:48:45','Welcome to WordPress. This is your first post. Edit or delete it, then start blogging! Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Morbi commodo, ipsum sed pharetra gravida, orci magna rhoncus neque, id pulvinar odio lorem non turpis. Nullam sit amet enim. Suspendisse id velit vitae ligula volutpat condimentum. Aliquam erat volutpat. Sed quis velit. Nulla facilisi. Nulla libero. Vivamus pharetra posuere sapien. Nam consectetuer. Sed aliquam, nunc eget euismod ullamcorper, lectus nunc ullamcorper orci, fermentum bibendum enim nibh eget ipsum. Donec porttitor ligula eu dolor. Maecenas vitae nulla consequat libero cursus venenatis. Nam magna enim, accumsan eu, blandit sed, blandit a, eros.\n\nQuisque facilisis erat a dui. Nam malesuada ornare dolor. Cras gravida, diam sit amet rhoncus ornare, erat elit consectetuer erat, id egestas pede nibh eget odio. Proin tincidunt, velit vel porta elementum, magna diam molestie sapien, non aliquet massa pede eu diam. Aliquam iaculis. Fusce et ipsum et nulla tristique facilisis. Donec eget sem sit amet ligula viverra gravida. Etiam vehicula urna vel turpis. Suspendisse sagittis ante a urna. Morbi a est quis orci consequat rutrum. Nullam egestas feugiat felis. Integer adipiscing semper ligula. Nunc molestie, nisl sit amet cursus convallis, sapien lectus pretium metus, vitae pretium enim wisi id lectus. Donec vestibulum. Etiam vel nibh. Nulla facilisi. Mauris pharetra. Donec augue. Fusce ultrices, neque id dignissim ultrices, tellus mauris dictum elit, vel lacinia enim metus eu nunc.','Sample Interview','','inherit','open','open','','1-autosave-v1','','','2014-06-06 13:48:45','2014-06-06 17:48:45','',1,'http://artsoulonline/uncategorized/1-autosave-v1/',0,'revision','',0);
INSERT INTO `wp_posts` VALUES (760,1,'2014-06-06 13:48:50','2014-06-06 17:48:50','Welcome to WordPress. This is your first post. Edit or delete it, then start blogging! Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Morbi commodo, ipsum sed pharetra gravida, orci magna rhoncus neque, id pulvinar odio lorem non turpis. Nullam sit amet enim. Suspendisse id velit vitae ligula volutpat condimentum. Aliquam erat volutpat. Sed quis velit. Nulla facilisi. Nulla libero. Vivamus pharetra posuere sapien. Nam consectetuer. Sed aliquam, nunc eget euismod ullamcorper, lectus nunc ullamcorper orci, fermentum bibendum enim nibh eget ipsum. Donec porttitor ligula eu dolor. Maecenas vitae nulla consequat libero cursus venenatis. Nam magna enim, accumsan eu, blandit sed, blandit a, eros.\r\n\r\nQuisque facilisis erat a dui. Nam malesuada ornare dolor. Cras gravida, diam sit amet rhoncus ornare, erat elit consectetuer erat, id egestas pede nibh eget odio. Proin tincidunt, velit vel porta elementum, magna diam molestie sapien, non aliquet massa pede eu diam. Aliquam iaculis. Fusce et ipsum et nulla tristique facilisis. Donec eget sem sit amet ligula viverra gravida. Etiam vehicula urna vel turpis. Suspendisse sagittis ante a urna. Morbi a est quis orci consequat rutrum. Nullam egestas feugiat felis. Integer adipiscing semper ligula. Nunc molestie, nisl sit amet cursus convallis, sapien lectus pretium metus, vitae pretium enim wisi id lectus. Donec vestibulum. Etiam vel nibh. Nulla facilisi. Mauris pharetra. Donec augue. Fusce ultrices, neque id dignissim ultrices, tellus mauris dictum elit, vel lacinia enim metus eu nunc.','Sample Interview','','inherit','open','open','','1-revision-v1','','','2014-06-06 13:48:50','2014-06-06 17:48:50','',1,'http://artsoulonline/uncategorized/1-revision-v1/',0,'revision','',0);
INSERT INTO `wp_posts` VALUES (761,1,'2014-06-06 14:34:10','2014-06-06 18:34:10','','Home','','inherit','open','open','','12-revision-v1','','','2014-06-06 14:34:10','2014-06-06 18:34:10','',12,'http://artsoulonline/uncategorized/12-revision-v1/',0,'revision','',0);
INSERT INTO `wp_posts` VALUES (762,1,'2014-06-06 14:41:22','2014-06-06 18:41:22','','Home','','inherit','open','open','','12-revision-v1','','','2014-06-06 14:41:22','2014-06-06 18:41:22','',12,'http://artsoulonline/uncategorized/12-revision-v1/',0,'revision','',0);
INSERT INTO `wp_posts` VALUES (763,1,'2014-06-06 15:07:33','2014-06-06 19:07:33','','Home','','inherit','open','open','','12-revision-v1','','','2014-06-06 15:07:33','2014-06-06 19:07:33','',12,'http://artsoulonline/uncategorized/12-revision-v1/',0,'revision','',0);
INSERT INTO `wp_posts` VALUES (764,1,'2014-06-06 15:10:06','2014-06-06 19:10:06','','Home','','inherit','open','open','','12-revision-v1','','','2014-06-06 15:10:06','2014-06-06 19:10:06','',12,'http://artsoulonline/uncategorized/12-revision-v1/',0,'revision','',0);
INSERT INTO `wp_posts` VALUES (765,1,'2014-06-06 15:29:01','2014-06-06 19:29:01','','Page: Watch','','publish','closed','closed','','acf_page-watch','','','2014-06-06 15:30:19','2014-06-06 19:30:19','',0,'http://artsoulonline/?post_type=acf&#038;p=765',0,'acf','',0);
INSERT INTO `wp_posts` VALUES (766,1,'2014-06-06 15:32:31','2014-06-06 19:32:31','Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Morbi commodo, ipsum sed pharetra gravida, orci magna rhoncus neque, id pulvinar odio lorem non turpis. Nullam sit amet enim. Suspendisse id velit vitae ligula volutpat condimentum. Aliquam erat volutpat. Sed quis velit. Nulla facilisi. Nulla libero. Vivamus pharetra posuere sapien. Nam consectetuer. Sed aliquam, nunc eget euismod ullamcorper, lectus nunc ullamcorper orci, fermentum bibendum enim nibh eget ipsum. Donec porttitor ligula eu dolor. Maecenas vitae nulla consequat libero cursus venenatis. Nam magna enim, accumsan eu, blandit sed, blandit a, eros.\r\n\r\nQuisque facilisis erat a dui. Nam malesuada ornare dolor. Cras gravida, diam sit amet rhoncus ornare, erat elit consectetuer erat, id egestas pede nibh eget odio. Proin tincidunt, velit vel porta elementum, magna diam molestie sapien, non aliquet massa pede eu diam. Aliquam iaculis. Fusce et ipsum et nulla tristique facilisis. Donec eget sem sit amet ligula viverra gravida. Etiam vehicula urna vel turpis. Suspendisse sagittis ante a urna.','A Tribute to Discomfort: Cory Richards','','publish','closed','closed','','a-tribute-to-discomfort-cory-richards','','','2014-06-06 15:32:31','2014-06-06 19:32:31','',0,'http://artsoulonline/?p=766',0,'post','',0);
INSERT INTO `wp_posts` VALUES (767,1,'2014-06-06 15:32:31','2014-06-06 19:32:31','Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Morbi commodo, ipsum sed pharetra gravida, orci magna rhoncus neque, id pulvinar odio lorem non turpis. Nullam sit amet enim. Suspendisse id velit vitae ligula volutpat condimentum. Aliquam erat volutpat. Sed quis velit. Nulla facilisi. Nulla libero. Vivamus pharetra posuere sapien. Nam consectetuer. Sed aliquam, nunc eget euismod ullamcorper, lectus nunc ullamcorper orci, fermentum bibendum enim nibh eget ipsum. Donec porttitor ligula eu dolor. Maecenas vitae nulla consequat libero cursus venenatis. Nam magna enim, accumsan eu, blandit sed, blandit a, eros.\r\n\r\nQuisque facilisis erat a dui. Nam malesuada ornare dolor. Cras gravida, diam sit amet rhoncus ornare, erat elit consectetuer erat, id egestas pede nibh eget odio. Proin tincidunt, velit vel porta elementum, magna diam molestie sapien, non aliquet massa pede eu diam. Aliquam iaculis. Fusce et ipsum et nulla tristique facilisis. Donec eget sem sit amet ligula viverra gravida. Etiam vehicula urna vel turpis. Suspendisse sagittis ante a urna.','A Tribute to Discomfort: Cory Richards','','inherit','open','open','','766-revision-v1','','','2014-06-06 15:32:31','2014-06-06 19:32:31','',766,'http://artsoulonline/uncategorized/766-revision-v1/',0,'revision','',0);
INSERT INTO `wp_posts` VALUES (768,1,'2014-06-06 15:32:54','2014-06-06 19:32:54','','Watch','','inherit','open','open','','47-revision-v1','','','2014-06-06 15:32:54','2014-06-06 19:32:54','',47,'http://artsoulonline/uncategorized/47-revision-v1/',0,'revision','',0);
INSERT INTO `wp_posts` VALUES (769,1,'2014-06-06 16:10:30','0000-00-00 00:00:00','','Auto Draft','','auto-draft','open','open','','','','','2014-06-06 16:10:30','0000-00-00 00:00:00','',0,'http://artsoulonline/?post_type=artist&p=769',0,'artist','',0);
INSERT INTO `wp_posts` VALUES (770,1,'2014-06-06 23:31:13','2014-06-07 03:31:13','ArtSoulKonnect allows you to submit artist information and music. Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Morbi commodo, ipsum sed pharetra gravida, orci magna rhoncus neque, id pulvinar odio lorem non turpis. Nullam sit amet enim. Suspendisse id velit vitae ligula volutpat condimentum. Aliquam erat volutpat. Sed quis velit. Nulla facilisi. Nulla libero.','Artist Profiles','','publish','closed','closed','','artist-profiles','','','2014-06-06 23:59:48','2014-06-07 03:59:48','',0,'http://artsoulonline/?page_id=770',0,'page','',0);
INSERT INTO `wp_posts` VALUES (771,1,'2014-06-06 23:31:13','2014-06-07 03:31:13','','Artist Welcome','','inherit','open','open','','770-revision-v1','','','2014-06-06 23:31:13','2014-06-07 03:31:13','',770,'http://artsoulonline/uncategorized/770-revision-v1/',0,'revision','',0);
INSERT INTO `wp_posts` VALUES (772,1,'2014-06-06 23:42:05','2014-06-07 03:42:05','','Artist Profiles','','inherit','open','open','','770-revision-v1','','','2014-06-06 23:42:05','2014-06-07 03:42:05','',770,'http://artsoulonline/uncategorized/770-revision-v1/',0,'revision','',0);
INSERT INTO `wp_posts` VALUES (773,1,'2014-06-06 23:50:17','2014-06-07 03:50:17','ArtSoulKonnect allows you to submit artist information and music.','Artist Profiles','','inherit','open','open','','770-autosave-v1','','','2014-06-06 23:50:17','2014-06-07 03:50:17','',770,'http://artsoulonline/uncategorized/770-autosave-v1/',0,'revision','',0);
INSERT INTO `wp_posts` VALUES (774,1,'2014-06-06 23:50:29','2014-06-07 03:50:29','ArtSoulKonnect allows you to submit artist information and music. Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Morbi commodo, ipsum sed pharetra gravida, orci magna rhoncus neque, id pulvinar odio lorem non turpis. Nullam sit amet enim. Suspendisse id velit vitae ligula volutpat condimentum. Aliquam erat volutpat. Sed quis velit. Nulla facilisi. Nulla libero. Vivamus pharetra posuere sapien. Nam consectetuer. Sed aliquam, nunc eget euismod ullamcorper, lectus nunc ullamcorper orci, fermentum bibendum enim nibh eget ipsum. Donec porttitor ligula eu dolor. Maecenas vitae nulla consequat libero cursus venenatis. Nam magna enim, accumsan eu, blandit sed, blandit a, eros.\r\n\r\n&nbsp;','Artist Profiles','','inherit','open','open','','770-revision-v1','','','2014-06-06 23:50:29','2014-06-07 03:50:29','',770,'http://artsoulonline/uncategorized/770-revision-v1/',0,'revision','',0);
INSERT INTO `wp_posts` VALUES (775,1,'2014-06-06 23:59:48','2014-06-07 03:59:48','ArtSoulKonnect allows you to submit artist information and music. Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Morbi commodo, ipsum sed pharetra gravida, orci magna rhoncus neque, id pulvinar odio lorem non turpis. Nullam sit amet enim. Suspendisse id velit vitae ligula volutpat condimentum. Aliquam erat volutpat. Sed quis velit. Nulla facilisi. Nulla libero.','Artist Profiles','','inherit','open','open','','770-revision-v1','','','2014-06-06 23:59:48','2014-06-07 03:59:48','',770,'http://artsoulonline/uncategorized/770-revision-v1/',0,'revision','',0);
INSERT INTO `wp_posts` VALUES (776,3,'2014-06-07 00:19:00','0000-00-00 00:00:00','','Auto Draft','','auto-draft','open','open','','','','','2014-06-07 00:19:00','0000-00-00 00:00:00','',0,'http://artsoulonline/?post_type=artist&p=776',0,'artist','',0);
INSERT INTO `wp_posts` VALUES (777,1,'2014-06-07 00:28:57','0000-00-00 00:00:00','','Auto Draft','','auto-draft','open','open','','','','','2014-06-07 00:28:57','0000-00-00 00:00:00','',0,'http://artsoulonline/?post_type=artist&p=777',0,'artist','',0);
/*!40000 ALTER TABLE `wp_posts` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `wp_rg_form`
--

DROP TABLE IF EXISTS `wp_rg_form`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `wp_rg_form` (
  `id` mediumint(8) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(150) NOT NULL,
  `date_created` datetime NOT NULL,
  `is_active` tinyint(1) NOT NULL DEFAULT '1',
  `is_trash` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `wp_rg_form`
--

LOCK TABLES `wp_rg_form` WRITE;
/*!40000 ALTER TABLE `wp_rg_form` DISABLE KEYS */;
/*!40000 ALTER TABLE `wp_rg_form` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `wp_rg_form_meta`
--

DROP TABLE IF EXISTS `wp_rg_form_meta`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `wp_rg_form_meta` (
  `form_id` mediumint(8) unsigned NOT NULL,
  `display_meta` longtext,
  `entries_grid_meta` longtext,
  `confirmations` longtext,
  `notifications` longtext,
  PRIMARY KEY (`form_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `wp_rg_form_meta`
--

LOCK TABLES `wp_rg_form_meta` WRITE;
/*!40000 ALTER TABLE `wp_rg_form_meta` DISABLE KEYS */;
/*!40000 ALTER TABLE `wp_rg_form_meta` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `wp_rg_form_view`
--

DROP TABLE IF EXISTS `wp_rg_form_view`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `wp_rg_form_view` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `form_id` mediumint(8) unsigned NOT NULL,
  `date_created` datetime NOT NULL,
  `ip` char(15) DEFAULT NULL,
  `count` mediumint(8) unsigned NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`),
  KEY `form_id` (`form_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `wp_rg_form_view`
--

LOCK TABLES `wp_rg_form_view` WRITE;
/*!40000 ALTER TABLE `wp_rg_form_view` DISABLE KEYS */;
/*!40000 ALTER TABLE `wp_rg_form_view` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `wp_rg_lead`
--

DROP TABLE IF EXISTS `wp_rg_lead`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `wp_rg_lead` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `form_id` mediumint(8) unsigned NOT NULL,
  `post_id` bigint(20) unsigned DEFAULT NULL,
  `date_created` datetime NOT NULL,
  `is_starred` tinyint(1) NOT NULL DEFAULT '0',
  `is_read` tinyint(1) NOT NULL DEFAULT '0',
  `ip` varchar(39) NOT NULL,
  `source_url` varchar(200) NOT NULL DEFAULT '',
  `user_agent` varchar(250) NOT NULL DEFAULT '',
  `currency` varchar(5) DEFAULT NULL,
  `payment_status` varchar(15) DEFAULT NULL,
  `payment_date` datetime DEFAULT NULL,
  `payment_amount` decimal(19,2) DEFAULT NULL,
  `payment_method` varchar(30) DEFAULT NULL,
  `transaction_id` varchar(50) DEFAULT NULL,
  `is_fulfilled` tinyint(1) DEFAULT NULL,
  `created_by` bigint(20) unsigned DEFAULT NULL,
  `transaction_type` tinyint(1) DEFAULT NULL,
  `status` varchar(20) NOT NULL DEFAULT 'active',
  PRIMARY KEY (`id`),
  KEY `form_id` (`form_id`),
  KEY `status` (`status`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `wp_rg_lead`
--

LOCK TABLES `wp_rg_lead` WRITE;
/*!40000 ALTER TABLE `wp_rg_lead` DISABLE KEYS */;
/*!40000 ALTER TABLE `wp_rg_lead` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `wp_rg_lead_detail`
--

DROP TABLE IF EXISTS `wp_rg_lead_detail`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `wp_rg_lead_detail` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `lead_id` int(10) unsigned NOT NULL,
  `form_id` mediumint(8) unsigned NOT NULL,
  `field_number` float NOT NULL,
  `value` varchar(200) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `form_id` (`form_id`),
  KEY `lead_id` (`lead_id`),
  KEY `lead_field_number` (`lead_id`,`field_number`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `wp_rg_lead_detail`
--

LOCK TABLES `wp_rg_lead_detail` WRITE;
/*!40000 ALTER TABLE `wp_rg_lead_detail` DISABLE KEYS */;
/*!40000 ALTER TABLE `wp_rg_lead_detail` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `wp_rg_lead_detail_long`
--

DROP TABLE IF EXISTS `wp_rg_lead_detail_long`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `wp_rg_lead_detail_long` (
  `lead_detail_id` bigint(20) unsigned NOT NULL,
  `value` longtext,
  PRIMARY KEY (`lead_detail_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `wp_rg_lead_detail_long`
--

LOCK TABLES `wp_rg_lead_detail_long` WRITE;
/*!40000 ALTER TABLE `wp_rg_lead_detail_long` DISABLE KEYS */;
/*!40000 ALTER TABLE `wp_rg_lead_detail_long` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `wp_rg_lead_meta`
--

DROP TABLE IF EXISTS `wp_rg_lead_meta`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `wp_rg_lead_meta` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `form_id` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `lead_id` bigint(20) unsigned NOT NULL,
  `meta_key` varchar(255) DEFAULT NULL,
  `meta_value` longtext,
  PRIMARY KEY (`id`),
  KEY `meta_key` (`meta_key`),
  KEY `lead_id` (`lead_id`),
  KEY `form_id_meta_key` (`form_id`,`meta_key`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `wp_rg_lead_meta`
--

LOCK TABLES `wp_rg_lead_meta` WRITE;
/*!40000 ALTER TABLE `wp_rg_lead_meta` DISABLE KEYS */;
/*!40000 ALTER TABLE `wp_rg_lead_meta` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `wp_rg_lead_notes`
--

DROP TABLE IF EXISTS `wp_rg_lead_notes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `wp_rg_lead_notes` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `lead_id` int(10) unsigned NOT NULL,
  `user_name` varchar(250) DEFAULT NULL,
  `user_id` bigint(20) DEFAULT NULL,
  `date_created` datetime NOT NULL,
  `value` longtext,
  PRIMARY KEY (`id`),
  KEY `lead_id` (`lead_id`),
  KEY `lead_user_key` (`lead_id`,`user_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `wp_rg_lead_notes`
--

LOCK TABLES `wp_rg_lead_notes` WRITE;
/*!40000 ALTER TABLE `wp_rg_lead_notes` DISABLE KEYS */;
/*!40000 ALTER TABLE `wp_rg_lead_notes` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `wp_term_relationships`
--

DROP TABLE IF EXISTS `wp_term_relationships`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `wp_term_relationships` (
  `object_id` bigint(20) unsigned NOT NULL DEFAULT '0',
  `term_taxonomy_id` bigint(20) unsigned NOT NULL DEFAULT '0',
  `term_order` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`object_id`,`term_taxonomy_id`),
  KEY `term_taxonomy_id` (`term_taxonomy_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `wp_term_relationships`
--

LOCK TABLES `wp_term_relationships` WRITE;
/*!40000 ALTER TABLE `wp_term_relationships` DISABLE KEYS */;
INSERT INTO `wp_term_relationships` VALUES (1,1,0);
INSERT INTO `wp_term_relationships` VALUES (15,1,0);
INSERT INTO `wp_term_relationships` VALUES (15,3,0);
INSERT INTO `wp_term_relationships` VALUES (52,6,0);
INSERT INTO `wp_term_relationships` VALUES (101,9,0);
INSERT INTO `wp_term_relationships` VALUES (51,6,0);
INSERT INTO `wp_term_relationships` VALUES (81,1,0);
INSERT INTO `wp_term_relationships` VALUES (101,10,0);
INSERT INTO `wp_term_relationships` VALUES (14,2,0);
INSERT INTO `wp_term_relationships` VALUES (15,4,0);
INSERT INTO `wp_term_relationships` VALUES (15,5,0);
INSERT INTO `wp_term_relationships` VALUES (19,4,0);
INSERT INTO `wp_term_relationships` VALUES (19,3,0);
INSERT INTO `wp_term_relationships` VALUES (41,1,0);
INSERT INTO `wp_term_relationships` VALUES (41,4,0);
INSERT INTO `wp_term_relationships` VALUES (50,2,0);
INSERT INTO `wp_term_relationships` VALUES (49,2,0);
INSERT INTO `wp_term_relationships` VALUES (53,7,0);
INSERT INTO `wp_term_relationships` VALUES (54,7,0);
INSERT INTO `wp_term_relationships` VALUES (66,1,0);
INSERT INTO `wp_term_relationships` VALUES (66,8,0);
INSERT INTO `wp_term_relationships` VALUES (74,2,0);
INSERT INTO `wp_term_relationships` VALUES (88,2,0);
INSERT INTO `wp_term_relationships` VALUES (111,2,0);
INSERT INTO `wp_term_relationships` VALUES (97,11,0);
INSERT INTO `wp_term_relationships` VALUES (92,10,0);
INSERT INTO `wp_term_relationships` VALUES (99,14,0);
INSERT INTO `wp_term_relationships` VALUES (97,14,0);
INSERT INTO `wp_term_relationships` VALUES (92,14,0);
INSERT INTO `wp_term_relationships` VALUES (99,15,0);
INSERT INTO `wp_term_relationships` VALUES (89,16,0);
INSERT INTO `wp_term_relationships` VALUES (1,17,0);
INSERT INTO `wp_term_relationships` VALUES (1,4,0);
INSERT INTO `wp_term_relationships` VALUES (1,5,0);
INSERT INTO `wp_term_relationships` VALUES (766,1,0);
INSERT INTO `wp_term_relationships` VALUES (766,8,0);
/*!40000 ALTER TABLE `wp_term_relationships` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `wp_term_taxonomy`
--

DROP TABLE IF EXISTS `wp_term_taxonomy`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `wp_term_taxonomy` (
  `term_taxonomy_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `term_id` bigint(20) unsigned NOT NULL DEFAULT '0',
  `taxonomy` varchar(32) NOT NULL DEFAULT '',
  `description` longtext NOT NULL,
  `parent` bigint(20) unsigned NOT NULL DEFAULT '0',
  `count` bigint(20) NOT NULL DEFAULT '0',
  PRIMARY KEY (`term_taxonomy_id`),
  UNIQUE KEY `term_id_taxonomy` (`term_id`,`taxonomy`),
  KEY `taxonomy` (`taxonomy`)
) ENGINE=MyISAM AUTO_INCREMENT=18 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `wp_term_taxonomy`
--

LOCK TABLES `wp_term_taxonomy` WRITE;
/*!40000 ALTER TABLE `wp_term_taxonomy` DISABLE KEYS */;
INSERT INTO `wp_term_taxonomy` VALUES (1,1,'category','',0,5);
INSERT INTO `wp_term_taxonomy` VALUES (2,2,'nav_menu','',0,6);
INSERT INTO `wp_term_taxonomy` VALUES (3,3,'category','',0,2);
INSERT INTO `wp_term_taxonomy` VALUES (4,4,'post_tag','',0,4);
INSERT INTO `wp_term_taxonomy` VALUES (5,5,'post_tag','',0,2);
INSERT INTO `wp_term_taxonomy` VALUES (6,6,'nav_menu','',0,2);
INSERT INTO `wp_term_taxonomy` VALUES (7,7,'nav_menu','',0,2);
INSERT INTO `wp_term_taxonomy` VALUES (8,8,'post_format','',0,2);
INSERT INTO `wp_term_taxonomy` VALUES (9,9,'post_tag','',0,1);
INSERT INTO `wp_term_taxonomy` VALUES (10,9,'genres','',14,2);
INSERT INTO `wp_term_taxonomy` VALUES (11,10,'genres','',14,1);
INSERT INTO `wp_term_taxonomy` VALUES (12,11,'genres','',14,0);
INSERT INTO `wp_term_taxonomy` VALUES (13,12,'genres','',14,0);
INSERT INTO `wp_term_taxonomy` VALUES (14,3,'genres','',14,3);
INSERT INTO `wp_term_taxonomy` VALUES (15,13,'genres','',12,1);
INSERT INTO `wp_term_taxonomy` VALUES (16,14,'genres','',0,1);
INSERT INTO `wp_term_taxonomy` VALUES (17,15,'category','',0,1);
/*!40000 ALTER TABLE `wp_term_taxonomy` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `wp_terms`
--

DROP TABLE IF EXISTS `wp_terms`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `wp_terms` (
  `term_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(200) NOT NULL DEFAULT '',
  `slug` varchar(200) NOT NULL DEFAULT '',
  `term_group` bigint(10) NOT NULL DEFAULT '0',
  PRIMARY KEY (`term_id`),
  UNIQUE KEY `slug` (`slug`),
  KEY `name` (`name`)
) ENGINE=MyISAM AUTO_INCREMENT=16 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `wp_terms`
--

LOCK TABLES `wp_terms` WRITE;
/*!40000 ALTER TABLE `wp_terms` DISABLE KEYS */;
INSERT INTO `wp_terms` VALUES (1,'Uncategorized','uncategorized',0);
INSERT INTO `wp_terms` VALUES (2,'Main','main',0);
INSERT INTO `wp_terms` VALUES (3,'Featured','featured',0);
INSERT INTO `wp_terms` VALUES (4,'Music','music',0);
INSERT INTO `wp_terms` VALUES (5,'Worship','worship',0);
INSERT INTO `wp_terms` VALUES (6,'Tags','tags',0);
INSERT INTO `wp_terms` VALUES (7,'Categories','categories',0);
INSERT INTO `wp_terms` VALUES (8,'post-format-video','post-format-video',0);
INSERT INTO `wp_terms` VALUES (9,'Hip Hop','hip-hop',0);
INSERT INTO `wp_terms` VALUES (10,'Spoken Word','spoken-word',0);
INSERT INTO `wp_terms` VALUES (11,'Pop','pop',0);
INSERT INTO `wp_terms` VALUES (12,'Easy Listening','easy-listening',0);
INSERT INTO `wp_terms` VALUES (13,'Jazz','jazz',0);
INSERT INTO `wp_terms` VALUES (14,'All Genres','all-genres',0);
INSERT INTO `wp_terms` VALUES (15,'Interviews','interviews',0);
/*!40000 ALTER TABLE `wp_terms` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `wp_usermeta`
--

DROP TABLE IF EXISTS `wp_usermeta`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `wp_usermeta` (
  `umeta_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` bigint(20) unsigned NOT NULL DEFAULT '0',
  `meta_key` varchar(255) DEFAULT NULL,
  `meta_value` longtext,
  PRIMARY KEY (`umeta_id`),
  KEY `user_id` (`user_id`),
  KEY `meta_key` (`meta_key`)
) ENGINE=InnoDB AUTO_INCREMENT=54 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `wp_usermeta`
--

LOCK TABLES `wp_usermeta` WRITE;
/*!40000 ALTER TABLE `wp_usermeta` DISABLE KEYS */;
INSERT INTO `wp_usermeta` VALUES (1,1,'first_name','Art');
INSERT INTO `wp_usermeta` VALUES (2,1,'last_name','Soul');
INSERT INTO `wp_usermeta` VALUES (3,1,'nickname','artsoul');
INSERT INTO `wp_usermeta` VALUES (4,1,'description','');
INSERT INTO `wp_usermeta` VALUES (5,1,'rich_editing','true');
INSERT INTO `wp_usermeta` VALUES (6,1,'comment_shortcuts','false');
INSERT INTO `wp_usermeta` VALUES (7,1,'admin_color','fresh');
INSERT INTO `wp_usermeta` VALUES (8,1,'use_ssl','0');
INSERT INTO `wp_usermeta` VALUES (9,1,'show_admin_bar_front','true');
INSERT INTO `wp_usermeta` VALUES (10,1,'wp_capabilities','a:1:{s:13:\"administrator\";b:1;}');
INSERT INTO `wp_usermeta` VALUES (11,1,'wp_user_level','10');
INSERT INTO `wp_usermeta` VALUES (12,1,'dismissed_wp_pointers','wp350_media,wp360_revisions,wp360_locks,wp390_widgets');
INSERT INTO `wp_usermeta` VALUES (13,1,'show_welcome_panel','0');
INSERT INTO `wp_usermeta` VALUES (14,1,'wp_dashboard_quick_press_last_post_id','751');
INSERT INTO `wp_usermeta` VALUES (15,1,'managenav-menuscolumnshidden','a:4:{i:0;s:11:\"link-target\";i:1;s:11:\"css-classes\";i:2;s:3:\"xfn\";i:3;s:11:\"description\";}');
INSERT INTO `wp_usermeta` VALUES (16,1,'metaboxhidden_nav-menus','a:0:{}');
INSERT INTO `wp_usermeta` VALUES (17,1,'nav_menu_recently_edited','2');
INSERT INTO `wp_usermeta` VALUES (18,1,'wp_user-settings','libraryContent=browse&hidetb=1&editor=tinymce&imgsize=thumbnail&align=left&urlbutton=none');
INSERT INTO `wp_usermeta` VALUES (19,1,'wp_user-settings-time','1401799485');
INSERT INTO `wp_usermeta` VALUES (20,1,'user_image','28');
INSERT INTO `wp_usermeta` VALUES (21,1,'_user_image','field_53877770a24d7');
INSERT INTO `wp_usermeta` VALUES (22,1,'closedpostboxes_nav-menus','a:0:{}');
INSERT INTO `wp_usermeta` VALUES (23,2,'first_name','Doug');
INSERT INTO `wp_usermeta` VALUES (24,2,'last_name','Keeling');
INSERT INTO `wp_usermeta` VALUES (25,2,'nickname','dkeeling');
INSERT INTO `wp_usermeta` VALUES (26,2,'description','');
INSERT INTO `wp_usermeta` VALUES (27,2,'rich_editing','true');
INSERT INTO `wp_usermeta` VALUES (28,2,'comment_shortcuts','false');
INSERT INTO `wp_usermeta` VALUES (29,2,'admin_color','fresh');
INSERT INTO `wp_usermeta` VALUES (30,2,'use_ssl','0');
INSERT INTO `wp_usermeta` VALUES (31,2,'show_admin_bar_front','true');
INSERT INTO `wp_usermeta` VALUES (32,2,'wp_capabilities','a:1:{s:11:\"contributor\";b:1;}');
INSERT INTO `wp_usermeta` VALUES (33,2,'wp_user_level','1');
INSERT INTO `wp_usermeta` VALUES (34,2,'user_image','');
INSERT INTO `wp_usermeta` VALUES (35,2,'_user_image','field_53877770a24d7');
INSERT INTO `wp_usermeta` VALUES (36,2,'dismissed_wp_pointers','wp350_media,wp360_revisions,wp360_locks,wp390_widgets');
INSERT INTO `wp_usermeta` VALUES (37,2,'wp_dashboard_quick_press_last_post_id','84');
INSERT INTO `wp_usermeta` VALUES (38,2,'wp_user-settings','libraryContent=browse');
INSERT INTO `wp_usermeta` VALUES (39,2,'wp_user-settings-time','1401475582');
INSERT INTO `wp_usermeta` VALUES (40,3,'first_name','');
INSERT INTO `wp_usermeta` VALUES (41,3,'last_name','');
INSERT INTO `wp_usermeta` VALUES (42,3,'nickname','joshshank');
INSERT INTO `wp_usermeta` VALUES (43,3,'description','');
INSERT INTO `wp_usermeta` VALUES (44,3,'rich_editing','true');
INSERT INTO `wp_usermeta` VALUES (45,3,'comment_shortcuts','false');
INSERT INTO `wp_usermeta` VALUES (46,3,'admin_color','fresh');
INSERT INTO `wp_usermeta` VALUES (47,3,'use_ssl','0');
INSERT INTO `wp_usermeta` VALUES (48,3,'show_admin_bar_front','true');
INSERT INTO `wp_usermeta` VALUES (49,3,'wp_capabilities','a:1:{s:11:\"contributor\";b:1;}');
INSERT INTO `wp_usermeta` VALUES (50,3,'wp_user_level','1');
INSERT INTO `wp_usermeta` VALUES (51,3,'default_password_nag','1');
INSERT INTO `wp_usermeta` VALUES (52,3,'user_image','');
INSERT INTO `wp_usermeta` VALUES (53,3,'_user_image','field_53877770a24d7');
/*!40000 ALTER TABLE `wp_usermeta` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `wp_users`
--

DROP TABLE IF EXISTS `wp_users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `wp_users` (
  `ID` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `user_login` varchar(60) NOT NULL DEFAULT '',
  `user_pass` varchar(64) NOT NULL DEFAULT '',
  `user_nicename` varchar(50) NOT NULL DEFAULT '',
  `user_email` varchar(100) NOT NULL DEFAULT '',
  `user_url` varchar(100) NOT NULL DEFAULT '',
  `user_registered` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `user_activation_key` varchar(60) NOT NULL DEFAULT '',
  `user_status` int(11) NOT NULL DEFAULT '0',
  `display_name` varchar(250) NOT NULL DEFAULT '',
  PRIMARY KEY (`ID`),
  KEY `user_login_key` (`user_login`),
  KEY `user_nicename` (`user_nicename`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `wp_users`
--

LOCK TABLES `wp_users` WRITE;
/*!40000 ALTER TABLE `wp_users` DISABLE KEYS */;
INSERT INTO `wp_users` VALUES (1,'artsoul','$P$BiEoivdv68BtiPp.iEG1/MPL/Xy.x00','artsoul','mail@themediadistrict.com','','2014-05-27 16:26:25','',0,'Art Soul');
INSERT INTO `wp_users` VALUES (2,'dkeeling','$P$BTJ5CcOTyDIMc2ZUix43idUHlg/4j/0','dkeeling','admin@dougkeeling.com','','2014-05-30 15:50:25','',0,'Doug Keeling');
INSERT INTO `wp_users` VALUES (3,'joshshank','$P$BmrhRTI64KmzF3KUBDZIgIGBLdYtku.','joshshank','doug@thekeelings.org','','2014-06-06 20:09:20','',0,'joshshank');
/*!40000 ALTER TABLE `wp_users` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2014-06-08  2:41:28
