( function( $ ) {
	"use strict";

	var CommonSliderProps = {
		$slider : null,
		$slides : null,
		$wrapper : null,
		$viewport : null,
		$recentlyOffset : null,

		mobileWidth : 600,
		mobileHeight : 600,
		
		loadingClass : "is-loading",
		slideClass : "slide",
		cloneClass : "bx-clone",
		viewportClass : "bx-viewport"
	};

	// ------------------------------------------------------------------
	// OVERFLOW SLIDER OBJ
	// ------------------------------------------------------------------
	var OverflowSlider = function( el, opts ){
		var self = this,
			props = $.extend( {
				transferImageClass : "slider-transfer-image"
			}, CommonSliderProps ),

			// ---------------------------------------------------------------------------------
			// INIT METHODS
			// ---------------------------------------------------------------------------------
			init = function() {
				popVars();
				doneLoading();
			},
			popVars = function() {
				props.$slider = $( el );
				props.$slides = props.$slider.find( "." + props.slideClass ).not( "." + props.cloneClass );
				props.$wrapper = props.$slider.closest( ".overflow-slider_wrapper" );
			},
			doneLoading = function() {
				// props.$slider.imagesLoaded( function() {
				var newHeight = 0;

				// images done loading; transfer them to background images...
				VB.Util.transferImages( "." + props.transferImageClass );
				// ... and init the slider
				initSlider();

				if( props.$slider.offset().top + props.$slider.height() < window.innerHeight
					&& window.innerWidth <= props.mobileWidth ) {

					newHeight = window.innerHeight;

					if( newHeight > props.mobileHeight )
						newHeight = props.mobileHeight;

					setHeight( newHeight - props.$slider.offset().top );
				}
				// } );
			},

			// ---------------------------------------------------------------------------------
			// SLIDER STATE HANDLERS 
			// Mainly for faking unbroken scrolling. BXSlider only creates one clone at the beginning
			// and end of the slides, and we need two since we're showing the overflow. This code
			// transforms the appropriate slides before/after transition to fake that effect.
			// ---------------------------------------------------------------------------------
			beforeTransHandler = function( $slide, prevIndex, newIndex ) {
				var $tgt,
					offset = 0,
					w = $slide.width(),
					total = props.$slides.length;

				if( prevIndex === 0 && newIndex === total - 1 ) {
					// we're looping backwards from first to last
					// we need to grab the 2nd-to-last slide and matrix it over to the front of the line momentarily
					$tgt = props.$slides.eq( total - 2 );

					offset = -w * total;
				} else if( prevIndex === total - 1 && newIndex === 0 ) {
					// we're looping forwards from last to first
					// we need to grab the 2nd slide and matrix it over to the end of the line momentarily
					$tgt = props.$slides.eq( 1 );

					offset = w * total;
				}

				if( $tgt ) {
					$tgt.css( {
						"transform" : "matrix( 1, 0, 0, 1, " + offset + ", 0 )"
					} );

					props.$recentlyOffset = $tgt;
				} else {
					props.$recentlyOffset = null;
				}

				toggleContent( props.$slides.eq( prevIndex ), false );
			},
			afterTransHandler = function( $slide, prevIndex, newIndex ) {
				if( props.$recentlyOffset && props.$recentlyOffset.length ) {
					props.$recentlyOffset.css( {
						"transform" : ""
					} );
				}

				toggleContent( $slide, true );
			},
			loadHandler = function() {
				 // stupid hack to add a class to our container, since it needs unique styling
				 // also define a couple more vars
				props.$viewport = props.$wrapper.find( "." + props.viewportClass );
				// show first slide's content
				toggleContent( props.$slides.eq( 0 ), true );
			},
			toggleContent = function( $tgt, show ) {
				$tgt.find( ".overflow-slider_content" )[ show ? "addClass" : "removeClass" ]( "show" );
			},

			// ---------------------------------------------------------------------------------
			// DIMENSIONAL CONTROL
			// ---------------------------------------------------------------------------------
			setHeight = function( height ) {
				if( typeof height !== "number" )
					return;

				props.$viewport.css( {
					"min-height" : height + "px"
				} );

				props.$slides.css( {
					"height" : height + "px"
				} );
			},

			// ---------------------------------------------------------------------------------
			// SLIDER INIT
			// ---------------------------------------------------------------------------------
			initSlider = function() {
				props.$slider
					.removeClass( props.loadingClass )
					.bxSlider( {
						adaptiveHeight : true,
						slideSelector : "." + props.slideClass,
						onSliderLoad : loadHandler,
						onSlideBefore : beforeTransHandler,
						onSlideAfter : afterTransHandler
					} );

				$( window ).trigger( "resize" );
			};

		init();

	};

	$.fn.overflowSlider = function( opts ) {
		var $self = this;
		if( $self.length ) {
			$self.imagesLoaded( function() {
				$self.each( function() {
					var $el = $( this ),
						overflowSlider;

					// Return early if this element already has a slider instance
					if( $el.data( "overflowSlider" ) ) {
						return;
					}

					// pass options to slider constructor
					overflowSlider = new OverflowSlider( this, opts );

					// Store slider object in this element's data
					$el.data( "overflowSlider", overflowSlider );
				} );
			} );
		}

		return $self;
	};


	// ------------------------------------------------------------------
	// SMALL CAROUSEL OBJ
	// ------------------------------------------------------------------
	var SmallCarousel = function( el, opts ) {
		// debugger;
		var props = $.extend( {
				transferImageClass : "carousel-transfer-image"
			}, CommonSliderProps ),

			// ---------------------------------------------------------------------------------
			// INIT METHODS
			// ---------------------------------------------------------------------------------
			init = function() {
				popVars();
				doneLoading();
			},
			popVars = function() {
				props.$slider = $( el );
				props.$slides = props.$slider.find( "." + props.slideClass ).not( "." + props.cloneClass );
				props.$wrapper = props.$slider.closest( ".small-carousel_wrapper" );
			},
			doneLoading = function() {
				// props.$slider.imagesLoaded( function() {
				var newHeight = 0;

				// images done loading; transfer them to background images...
				VB.Util.transferImages( "." + props.transferImageClass );
				// ... and init the slider
				initSlider();
				// } );
			},

			// ---------------------------------------------------------------------------------
			// SLIDER STATE HANDLERS 
			// Mainly for faking unbroken scrolling. BXSlider only creates one clone at the beginning
			// and end of the slides, and we need two since we're showing the overflow. This code
			// transforms the appropriate slides before/after transition to fake that effect.
			// ---------------------------------------------------------------------------------
			// beforeTransHandler = function( $slide, prevIndex, newIndex ) {
			// 	var $tgt,
			// 		offset = 0,
			// 		w = $slide.width(),
			// 		total = $slides.length;

			// 	if( prevIndex === 0 && newIndex === total - 1 ) {
			// 		// we're looping backwards from first to last
			// 		// we need to grab the 2nd-to-last slide and matrix it over to the front of the line momentarily
			// 		$tgt = $slides.eq( total - 2 );

			// 		offset = -w * total;
			// 	} else if( prevIndex === total - 1 && newIndex === 0 ) {
			// 		// we're looping forwards from last to first
			// 		// we need to grab the 2nd slide and matrix it over to the end of the line momentarily
			// 		$tgt = $slides.eq( 1 );

			// 		offset = w * total;
			// 	}

			// 	if( $tgt ) {
			// 		$tgt.css( {
			// 			"transform" : "matrix( 1, 0, 0, 1, " + offset + ", 0 )"
			// 		} );

			// 		$recentlyOffset = $tgt;
			// 	} else {
			// 		$recentlyOffset = null;
			// 	}

			// },
			// afterTransHandler = function( $slide, prevIndex, newIndex ) {
			// 	if( $recentlyOffset && $recentlyOffset.length ) {
			// 		$recentlyOffset.css( {
			// 			"transform" : ""
			// 		} );
			// 	}
			// },
			loadHandler = function() {
				 // stupid hack to add a class to our container, since it needs unique styling
				 // also define a couple more vars
				props.$viewport = props.$wrapper.find( "." + props.viewportClass );
			},
			toggleContent = function( $tgt, show) {
				$tgt.find( ".overflow-slider_content" )[ show ? "addClass" : "removeClass" ]( "show" );
			},

			// ---------------------------------------------------------------------------------
			// SLIDER INIT
			// ---------------------------------------------------------------------------------
			initSlider = function() {
				props.$slider
					.removeClass( "is-loading" )
					.bxSlider( {
						adaptiveHeight : true,
						slideSelector : "." + props.slideClass,
						minSlides: 2,
						maxSlides: 4,
						slideWidth: 240,
						slideMargin: 20,
						onSliderLoad : loadHandler
						// onSlideBefore : beforeTransHandler,
						// onSlideAfter : afterTransHandler
					} );

				$( window ).trigger( "resize" );
			};

		init();
	}

	$.fn.smallCarousel = function( opts ) {
		var $self = this;
		if( $self.length ) {
			$self.imagesLoaded( function() {
				$self.each( function() {
					var $el = $( this ),
						smallCarousel;

					// Return early if this element already has a slider instance
					if( $el.data( "smallCarousel" ) ) {
						return;
					}

					// pass options to slider constructor
					smallCarousel = new SmallCarousel( this, opts );

					// Store slider object in this element's data
					$el.data( "smallCarousel", smallCarousel );
				} );
			} );
		}

		return $self;
	};

	
} )( jQuery );



jQuery( document ).ready( function( $ ) {
	// to initialize, just call TypeOfSlider.init()
	
	$( ".overflow-slider" ).overflowSlider();
	$( ".small-carousel" ).smallCarousel();
} );